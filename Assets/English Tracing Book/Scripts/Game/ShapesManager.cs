﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

///Developed by Indie Studio
///https://www.assetstore.unity3d.com/en/#!/publisher/9268
///www.indiestd.com
///info@indiestd.com
///copyright © 2017 IGS. All rights reserved.

namespace ABC
{
    namespace Tracing
    {
        public class ShapesManager : MonoBehaviour
        {
            /// <summary>
            /// The shapes list.
            /// </summary>
			public List<Shape> shapes = new List<Shape>();

            /// <summary>
            /// The shape label (example Letter or Number).
            /// </summary>
            public string shapeLabel = "Shape";

            /// <summary>
            /// The shape prefix used for DataManager only (example Lowercase or Uppercase or Number).
            /// </summary>
            public string shapePrefix = "Shape";

            /// <summary>
            /// The name of the scene.
            /// </summary>
            public string sceneName = "";

            /// <summary>
            /// The last selected group.
            /// </summary>
            [HideInInspector]
            public int lastSelectedGroup;

            /// <summary>
            /// The name of the shape manager.
            /// </summary>
            public static string shapeManagerReference = "";

            void Awake()
            {
//                DontDestroyOnLoad(gameObject);
                lastSelectedGroup = 0;
            }

            [System.Serializable]
            public class Shape
            {
                public bool showContents = true;
                public GameObject gamePrefab;
            }
        }
    }
}