using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace ABC
{
    namespace Tracing
    {
        public class GameManager : MonoBehaviour
        {
            public static GameManager instance;

            public Pencil currentPencil;
            public GameObject navPanel;
            EnglishTracingBook.Path path;
            public Transform shapeParent;
            [HideInInspector]
            public Shape shape;
            Image pathFillImage;
            Vector3 _clickPostion;
            bool _isRunning = true;

            /// <summary>
            /// The hand reference.
            /// </summary>
            public Transform hand;

            /// <summary>
            /// The bright effect.
            /// </summary>
            public Transform brightEffect;

            /// <summary>
            /// The complete effect.
            /// </summary>
//			public ParticleSystem completeEffect;

            /// <summary>
            /// The compound shape reference.
            /// </summary>
            public static CompoundShape compoundShape;

            public Animator nextButton;

            /// <summary>
            /// The direction between click and shape.
            /// </summary>
            private Vector2 direction;

            /// <summary>
            /// The current angle , angleOffset and fill amount.
            /// </summary>
            private float angle, angleOffset, fillAmount;

            /// <summary>
            /// The clock wise sign.
            /// </summary>
            private float clockWiseSign;

            /// <summary>
            /// The default size of the cursor.
            /// </summary>
            private Vector3 cursorDefaultSize;

            /// <summary>
            /// The click size of the cursor.
            /// </summary>
            private Vector3 cursorClickSize;

            /// <summary>
            /// The target quarter of the radial fill.
            /// </summary>
            private float targetQuarter;

            /// <summary>
            /// The effects audio source.
            /// </summary>
			public AudioController AudioController;

            /// <summary>
            /// The hit2d reference.
            /// </summary>
            private RaycastHit2D hit2d;

            /// <summary>
            /// The shapes manager reference.
            /// </summary>
            private ShapesManager shapesManager;

            bool _isGameActive = true;
            bool _isInProgress = true;
            bool _isInputEnabled = true;

            void Awake()
            {
                instance = this;

                //Initiate values and setup the references
                cursorDefaultSize = hand.transform.localScale;
                cursorClickSize = cursorDefaultSize / 1.2f;

                if (!string.IsNullOrEmpty(ShapesManager.shapeManagerReference))
                {
                    shapesManager = GameObject.Find(ShapesManager.shapeManagerReference).GetComponent<ShapesManager>();
                }
                else
                {
                    Debug.LogErrorFormat("You have to start the game from the Main scene");
                }

                if (currentPencil != null)
                {
                    currentPencil.EnableSelection();
                }

                ResetTargetQuarter();
                SetShapeOrderColor();
                CreateShape();
            }

            // Update is called once per frame
            void Update()
            {
                //Game Logic is here
                if (!_isInputEnabled)
                {
                    return;
                }
                DrawHand(GetCurrentPlatformClickPosition(Camera.main));
                DrawBrightEffect(GetCurrentPlatformClickPosition(Camera.main));

                if (shape == null)
                {
                    return;
                }

                if (shape.completed)
                {
                    return;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    if (!shape.completed)
                        brightEffect.GetComponent<ParticleSystem>().Play();

                    hit2d = Physics2D.Raycast(GetCurrentPlatformClickPosition(Camera.main), Vector2.zero);
                    if (hit2d.collider != null)
                    {
                        if (hit2d.transform.tag == "Start")
                        {
                            OnStartHitCollider(hit2d);
                            shape.CancelInvoke();
                            shape.DisableTracingHand();
                            EnableHand();
                        }
                        else if (hit2d.transform.tag == "Collider")
                        {
                            shape.DisableTracingHand();
                            EnableHand();
                        }
                    }
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    brightEffect.GetComponent<ParticleSystem>().Stop();
                    DisableHand();
                    shape.Invoke("EnableTracingHand", 1);
                    ResetPath();
                }

                if (!_isRunning || path == null || pathFillImage == null)
                {
                    return;
                }

                if (path.completed)
                {
                    return;
                }

                hit2d = Physics2D.Raycast(GetCurrentPlatformClickPosition(Camera.main), Vector2.zero);
                if (hit2d.collider == null)
                {
                    AudioController.TriggerAudio("OnWrong");
                    ResetPath();
                    return;
                }

                if (path.fillMethod == EnglishTracingBook.Path.FillMethod.Radial)
                {
                    RadialFill();
                }
                else if (path.fillMethod == EnglishTracingBook.Path.FillMethod.Linear)
                {
                    LinearFill();
                }
                else if (path.fillMethod == EnglishTracingBook.Path.FillMethod.Point)
                {
                    PointFill();
                }
            }

            /// <summary>
            /// On the start hit collider event.
            /// </summary>
            /// <param name="hit2d">Hit2d.</param>
            private void OnStartHitCollider(RaycastHit2D hit2d)
            {
                path = hit2d.transform.GetComponentInParent<EnglishTracingBook.Path>();
                pathFillImage = CommonUtil.FindChildByTag(path.transform, "Fill").GetComponent<Image>();
                if (path.completed || !shape.IsCurrentPath(path))
                {
                    ReleasePath();
                }
                else
                {
                    path.StopAllCoroutines();
                    CommonUtil.FindChildByTag(path.transform, "Fill").GetComponent<Image>().color = currentPencil.value;
                }
            }

            void AutoNextAfterComplete()
            {
                if (_isGameActive && !_isInProgress)
                    NextShape(true);
            }

            /// <summary>
            /// Go to the Next shape.
            /// </summary>
			public void NextShape(bool autoNext = false)
            {
                _isInProgress = true;
                if (TableShape.selectedShape.ID < ShapesTable.shapes.Count)
                {
                    if (TableShape.selectedShape.ID + 1 > ShapesTable.FREECHARACTERS && !LocalDataManager.Instance.SaveData.IsPremiumUser())
                    {
                        _isInputEnabled = false;
                        OnEscapePress();
                    }
                    else
                    {
                        TableShape.selectedShape = ShapesTable.shapes[TableShape.selectedShape.ID];//Set the selected shape
                        if (vgm.ads.AdsCheckManager.Instance.ShowInterestialIfAllowed(vgm.ads.ActivityType.ACTIVITY))
                        {
                            Debug.Log("Not a premium user so show ad");
                            vgm.ads.InterestialService.ShowAdIfItIsReady(() => CreateShape());
                        }
                        else
                            CreateShape();//Create new shape
                    }
                }
                else
                {
                    OnEscapePress();
                }
            }

            /// <summary>
            /// Go to the previous shape.
            /// </summary>
            public void PreviousShape()
            {
                _isInProgress = true;
                if (TableShape.selectedShape.ID - 1 > 0)
                {
                    if (TableShape.selectedShape.ID - 1 > ShapesTable.FREECHARACTERS && !LocalDataManager.Instance.SaveData.IsPremiumUser())
                    {
                        _isInputEnabled = false;
                        OnEscapePress();
                    }
                    else
                    {
                        TableShape.selectedShape = ShapesTable.shapes[TableShape.selectedShape.ID - 2];
                        CreateShape();
                    }
                }
                else
                {
                    OnEscapePress();
                }
            }


            /// <summary>
            /// Create new shape.
            /// </summary>
            private void CreateShape()
            {
                LocalDataManager.Instance.SaveData.IncrementCurrentActivitySession();
                //				completeEffect.emit = false;
                //				Area.Hide ();
                PerformanceManager.Instance.IncrementAttempts();
                nextButton.SetBool("Select", false);
                CompoundShape currentCompoundShape = GameObject.FindObjectOfType<CompoundShape>();
                if (currentCompoundShape != null)
                {
                    DestroyImmediate(currentCompoundShape.gameObject);
                }
                else
                {
                    Shape shapeComponent = GameObject.FindObjectOfType<Shape>();
                    if (shapeComponent != null)
                    {
                        DestroyImmediate(shapeComponent.gameObject);
                    }
                }

                try
                {
                    shapesManager.lastSelectedGroup = TableShape.selectedShape.ID - 1;
                    GameObject shapePrefab = shapesManager.shapes[TableShape.selectedShape.ID - 1].gamePrefab;
                    GameObject shapeGameObject = Instantiate(shapePrefab, Vector3.zero, Quaternion.identity) as GameObject;
                    shapeGameObject.transform.SetParent(shapeParent);
                    shapeGameObject.transform.localPosition = shapePrefab.transform.localPosition;
                    shapeGameObject.name = shapePrefab.name;
                    shapeGameObject.transform.localScale = shapePrefab.transform.localScale;

                    compoundShape = GameObject.FindObjectOfType<CompoundShape>();
                    if (compoundShape != null)
                    {
                        shape = compoundShape.shapes[0];
                        StartAutoTracing(shape);
                    }
                    else
                    {
                        shape = GameObject.FindObjectOfType<Shape>();
                    }
                }
                catch (System.Exception ex)
                {
                    //Catch the exception or display an alert
                    Debug.Log(ex.Message);
                }

                if (shape == null)
                {
                    return;
                }

                shape.Spell();

                EnableGameManager();
            }

            /// <summary>
            /// Draw the hand.
            /// </summary>
            /// <param name="clickPosition">Click position.</param>
            private void DrawHand(Vector3 clickPosition)
            {
                if (hand == null)
                {
                    return;
                }

                hand.transform.position = clickPosition;
            }

            /// <summary>
            /// Set the size of the hand to default size.
            /// </summary>
            private void SetHandDefaultSize()
            {
                hand.transform.localScale = cursorDefaultSize;
            }

            /// <summary>
            /// Set the size of the hand to click size.
            /// </summary>
            private void SetHandClickSize()
            {
                hand.transform.localScale = cursorClickSize;
            }

            /// <summary>
            /// Get the current platform click position.
            /// </summary>
            /// <returns>The current platform click position.</returns>
            private Vector3 GetCurrentPlatformClickPosition(Camera camera)
            {
                Vector3 clickPosition = Vector3.zero;

                if (Application.isMobilePlatform)
                {//current platform is mobile
                    if (Input.touchCount != 0)
                    {
                        Touch touch = Input.GetTouch(0);
                        clickPosition = touch.position;
                    }
                }
                else
                {//others
                    clickPosition = Input.mousePosition;
                }

                clickPosition = camera.ScreenToWorldPoint(clickPosition);//get click position in the world space
                clickPosition.z = 0;
                return clickPosition;
            }

            /// <summary>
            /// Radial the fill method.
            /// </summary>
            private void RadialFill()
            {
                _clickPostion = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                direction = _clickPostion - path.transform.position;

                angleOffset = 0;
                clockWiseSign = (pathFillImage.fillClockwise ? 1 : -1);

                if (pathFillImage.fillOrigin == 0)
                {//Bottom
                    angleOffset = 0;
                }
                else if (pathFillImage.fillOrigin == 1)
                {//Right
                    angleOffset = clockWiseSign * 90;
                }
                else if (pathFillImage.fillOrigin == 2)
                {//Top
                    angleOffset = -180;
                }
                else if (pathFillImage.fillOrigin == 3)
                {//left
                    angleOffset = -clockWiseSign * 90;
                }

                angle = Mathf.Atan2(-clockWiseSign * direction.x, -direction.y) * Mathf.Rad2Deg + angleOffset;

                if (angle < 0)
                    angle += 360;

                angle = Mathf.Clamp(angle, 0, 360);
                angle -= path.radialAngleOffset;

                if (path.quarterRestriction)
                {
                    if (!(angle >= 0 && angle <= targetQuarter))
                    {
                        pathFillImage.fillAmount = 0;
                        return;
                    }

                    if (angle >= targetQuarter / 2)
                    {
                        targetQuarter += 90;
                    }
                    else if (angle < 45)
                    {
                        targetQuarter = 90;
                    }

                    targetQuarter = Mathf.Clamp(targetQuarter, 90, 360);
                }

                fillAmount = Mathf.Abs(angle / 360.0f);
                pathFillImage.fillAmount = fillAmount;
                CheckPathComplete();
            }

            /// <summary>
            /// Linear fill method.
            /// </summary>
            private void LinearFill()
            {
                _clickPostion = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                Vector3 rotation = path.transform.eulerAngles;
                rotation.z -= path.offset;

                Rect rect = CommonUtil.RectTransformToScreenSpace(path.GetComponent<RectTransform>());

                Vector3 pos1 = Vector3.zero, pos2 = Vector3.zero;

                if (path.type == EnglishTracingBook.Path.ShapeType.Horizontal)
                {
                    pos1.x = path.transform.position.x - Mathf.Sin(rotation.z * Mathf.Deg2Rad) * rect.width / 2.0f;
                    pos1.y = path.transform.position.y - Mathf.Cos(rotation.z * Mathf.Deg2Rad) * rect.width / 2.0f;

                    pos2.x = path.transform.position.x + Mathf.Sin(rotation.z * Mathf.Deg2Rad) * rect.width / 2.0f;
                    pos2.y = path.transform.position.y + Mathf.Cos(rotation.z * Mathf.Deg2Rad) * rect.width / 2.0f;
                }
                else
                {

                    pos1.x = path.transform.position.x - Mathf.Cos(rotation.z * Mathf.Deg2Rad) * rect.height / 2.0f;
                    pos1.y = path.transform.position.y - Mathf.Sin(rotation.z * Mathf.Deg2Rad) * rect.height / 2.0f;

                    pos2.x = path.transform.position.x + Mathf.Cos(rotation.z * Mathf.Deg2Rad) * rect.height / 2.0f;
                    pos2.y = path.transform.position.y + Mathf.Sin(rotation.z * Mathf.Deg2Rad) * rect.height / 2.0f;
                }

                pos1.z = path.transform.position.z;
                pos2.z = path.transform.position.z;

                if (path.flip)
                {
                    Vector3 temp = pos2;
                    pos2 = pos1;
                    pos1 = temp;
                }

                _clickPostion.x = Mathf.Clamp(_clickPostion.x, Mathf.Min(pos1.x, pos2.x), Mathf.Max(pos1.x, pos2.x));
                _clickPostion.y = Mathf.Clamp(_clickPostion.y, Mathf.Min(pos1.y, pos2.y), Mathf.Max(pos1.y, pos2.y));
                fillAmount = Vector2.Distance(_clickPostion, pos1) / Vector2.Distance(pos1, pos2);
                pathFillImage.fillAmount = fillAmount;
                CheckPathComplete();
            }

            /// <summary>
            /// Point fill.
            /// </summary>
            private void PointFill()
            {
                pathFillImage.fillAmount = 1;
                CheckPathComplete();
            }

            /// <summary>
            /// Checks wehther path completed or not.
            /// </summary>
            private void CheckPathComplete()
            {
                if (fillAmount >= path.completeOffset)
                {

                    path.completed = true;
                    path.AutoFill();
                    path.SetNumbersVisibility(false);
                    ReleasePath();
                    if (CheckShapeComplete())
                    {
                        shape.completed = true;
                        OnShapeComplete();
                    }
                    else
                    {
                        PlayCorrectSFX();
                    }

                    shape.ShowPathNumbers(shape.GetCurrentPathIndex());

                    hit2d = Physics2D.Raycast(GetCurrentPlatformClickPosition(Camera.main), Vector2.zero);
                    if (hit2d.collider != null)
                    {
                        if (hit2d.transform.tag == "Start")
                        {
                            if (shape.IsCurrentPath(hit2d.transform.GetComponentInParent<EnglishTracingBook.Path>()))
                            {
                                OnStartHitCollider(hit2d);
                            }
                        }
                    }
                }
            }

            /// <summary>
            /// Check whether the shape completed or not.
            /// </summary>
            /// <returns><c>true</c>, if shape completed, <c>false</c> otherwise.</returns>
            private bool CheckShapeComplete()
            {
                bool shapeCompleted = true;
                EnglishTracingBook.Path[] paths = shape.GetComponentsInChildren<EnglishTracingBook.Path>();
                foreach (EnglishTracingBook.Path path in paths)
                {
                    if (!path.completed)
                    {
                        shapeCompleted = false;
                        break;
                    }
                }
                return shapeCompleted;
            }

            /// <summary>
            /// On shape completed event.
            /// </summary>
            private void OnShapeComplete()
            {
                PerformanceManager.Instance.IncrementSuccess();
                bool allDone = true;
                List<Shape> shapes = new List<Shape>();

                if (compoundShape != null)
                {
                    shapes = compoundShape.shapes;
                    allDone = compoundShape.IsCompleted();

                    if (!allDone)
                    {
                        shape = compoundShape.shapes[compoundShape.GetCurrentShapeIndex()];
                        StartAutoTracing(shape);
                    }
                }
                else
                {
                    shapes.Add(shape);
                }

                if (allDone)
                {
                    //                    SaveShapeStatus(shapes);

                    DisableHand();
                    brightEffect.GetComponent<ParticleSystem>().Stop();
                    shape.Spell();
                    float t = 0;
                    foreach (Shape s in shapes)
                    {
                        Animator shapeAnimator = s.GetComponent<Animator>();
                        shapeAnimator.SetBool(s.name, false);
                        shapeAnimator.SetTrigger("Completed");
                        t = shapeAnimator.GetCurrentAnimatorStateInfo(0).length;
                    }
                    DisableGameManager();
                    Invoke("PostShapeComplete", t);

                    /*
                    Show ad here
                    */
                }
                else
                {
                    PlayCorrectSFX();
                }
            }

            void Callback()
            {
                AutoNextAfterComplete();
                //Invoke ("ShowAd", 0.5f);
                //
            }

            void PauseGame()
            {
                _isGameActive = false;
            }

            void ResumeGame()
            {
                _isGameActive = true;
                AutoNextAfterComplete();
            }

            /// <summary>
            /// Executed after OnShapeComplete when animations are complete.
            /// </summary>
            private void PostShapeComplete()
            {
                //DisableGameManager ();
                //				Area.Show ();

                _isInProgress = false;

                FinishUI.Instance.ShowFinishStars(() =>
                {
                    Callback();
                });
                AudioController.TriggerAudio("OnCompleted");
            }

            /// <summary>
            /// Draw the bright effect.
            /// </summary>
            /// <param name="clickPosition">Click position.</param>
            private void DrawBrightEffect(Vector3 clickPosition)
            {
                if (brightEffect == null)
                {
                    return;
                }

                clickPosition.z = 0;
                brightEffect.transform.position = clickPosition;
            }

            /// <summary>
            /// Reset the shape.
            /// </summary>
            public void ResetShape()
            {
                List<Shape> shapes = new List<Shape>();
                if (compoundShape != null)
                {
                    shapes = compoundShape.shapes;
                }
                else
                {
                    shapes.Add(shape);
                }

                //				completeEffect.emit = false;
                nextButton.SetBool("Select", false);
                //				Area.Hide ();
                foreach (Shape s in shapes)
                {
                    if (s == null)
                        continue;

                    s.completed = false;
                    s.GetComponent<Animator>().SetBool("Completed", false);
                    s.CancelInvoke();
                    s.DisableTracingHand();
                    EnglishTracingBook.Path[] paths = s.GetComponentsInChildren<EnglishTracingBook.Path>();
                    foreach (EnglishTracingBook.Path path in paths)
                    {
                        path.Reset();
                    }

                    if (compoundShape == null)
                    {
                        StartAutoTracing(s);
                    }
                    else if (compoundShape.GetShapeIndexByInstanceID(s.GetInstanceID()) == 0)
                    {
                        shape = compoundShape.shapes[0];
                        StartAutoTracing(shape);
                    }

                    s.Spell();
                }
            }


            /// <summary>
            /// Starts the auto tracing for the current path.
            /// </summary>
            /// <param name="s">Shape Reference.</param>
            public void StartAutoTracing(Shape s)
            {
                if (s == null)
                {
                    return;
                }

                //Hide Numbers for other shapes , if we have compound shape
                if (compoundShape != null)
                {
                    foreach (Shape ts in compoundShape.shapes)
                    {
                        if (s.GetInstanceID() != ts.GetInstanceID())
                            ts.ShowPathNumbers(-1);
                    }
                }

                s.Invoke("EnableTracingHand", 2);
                s.ShowPathNumbers(s.GetCurrentPathIndex());
            }

            /// <summary>
            /// Play the correct SFX.
            /// </summary>
            public void PlayCorrectSFX()
            {
                AudioController.TriggerAudio("OnCorrect");
            }

            /// <summary>
            /// Reset the path.
            /// </summary>
            private void ResetPath()
            {
                if (path != null)
                {
                    path.Reset();
                    AudioController.TriggerAudio("OnWrong");
                }
                ReleasePath();
                ResetTargetQuarter();
            }

            /// <summary>
            /// Reset the target quarter.
            /// </summary>
            private void ResetTargetQuarter()
            {
                targetQuarter = 90;
            }

            /// <summary>
            /// Release the path.
            /// </summary>
            private void ReleasePath()
            {
                path = null;
                pathFillImage = null;
            }

            /// <summary>
            /// Set the color of the shape order.
            /// </summary>
            public void SetShapeOrderColor()
            {
                if (currentPencil == null)
                {
                    return;
                }
                //                shapeOrder.color = currentPencil.value;
            }

            /// <summary>
            /// Enable the hand.
            /// </summary>
            public void EnableHand()
            {

                hand.GetComponent<SpriteRenderer>().enabled = true;
            }

            /// <summary>
            /// Disable the hand.
            /// </summary>
            public void DisableHand()
            {
                hand.GetComponent<SpriteRenderer>().enabled = false;
            }

            /// <summary>
            /// Disable the game manager.
            /// </summary>
            public void DisableGameManager()
            {
                navPanel.SetActive(false);
                _isRunning = false;
            }

            /// <summary>
            /// Enable the game manager.
            /// </summary>
            public void EnableGameManager()
            {
                navPanel.SetActive(true);
                //				resetButton.SetActive(true);
                _isRunning = true;
            }

            void OnDestroy()
            {
                instance = null;
                CancelInvoke();
            }

            void OnEnable()
            {
                HardwareInputManager.OnBack += OnEscapePress;
            }

            void OnDisable()
            {
                HardwareInputManager.OnBack -= OnEscapePress;
            }

            public void OnEscapePress()
            {
                if (UILoader.Instance.IsPanelActive)
                {
                    return;
                }
                HardwareInputManager.OnBack -= OnEscapePress;
                AudioController.TriggerAudio("OnClick");
                UISceneLoader.Instance.LoadScene(EScenes.TracingActivity.GetDescription());

                double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
                double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
                FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
                AnalyticsScript.instance.LogEvent("activity_session_time", "tracing_game", totalTimeSpent);
            }
        }
    }
}