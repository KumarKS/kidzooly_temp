﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class TracingActivityManager : MonoBehaviour {

	public AudioController AudioController;

	void OnEnable()
	{
		HardwareInputManager.OnBack += OnEscapePress;
	}

	void OnDisable()
	{
		HardwareInputManager.OnBack -= OnEscapePress;
	}

	public void OnEscapePress ()
	{
		if (UILoader.Instance.IsPanelActive)
		{
			return;
		}
		HardwareInputManager.OnBack -= OnEscapePress;
		AudioController.TriggerAudio ("OnClick");
		UISceneLoader.Instance.LoadScene (EScenes.MainMenu.GetDescription ());

		double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
		FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
		AnalyticsScript.instance.LogEvent("activity_session_time" , "tracing", totalTimeSpent);
	}
}
