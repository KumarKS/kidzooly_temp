﻿using UnityEngine;
using System.Collections;

///Developed by Indie Studio
///https://www.assetstore.unity3d.com/en/#!/publisher/9268
///www.indiestd.com
///info@indiestd.com
///copyright © 2017 IGS. All rights reserved.

namespace ABC
{
	namespace Tracing
	{
		public class UIEvents : MonoBehaviour
		{
			GameManager gameManager;

			void Awake ()
			{
				gameManager = GameManager.instance;
			}

			public void AlbumShapeEvent (TableShape tableShape)
			{
				if (tableShape == null) {
					return;
				}

				/*if (tableShape.isLocked)
                {
                    return;
                }*/

				TableShape.selectedShape = tableShape;
				LoadGameScene ();
			}

			public void PointerButtonEvent (Pointer pointer)
			{
				if (pointer == null) {
					return;
				}
				if (pointer.group != null) {
					ScrollSlider scrollSlider = GameObject.FindObjectOfType (typeof(ScrollSlider)) as ScrollSlider;
					if (scrollSlider != null) {
						scrollSlider.DisableCurrentPointer ();
						FindObjectOfType<ScrollSlider> ().currentGroupIndex = pointer.group.Index;
						scrollSlider.GoToCurrentGroup ();
					}
				}
			}

			public void LoadMainScene ()
			{
				gameManager.AudioController.TriggerAudio ("OnClick");
				UISceneLoader.Instance.LoadScene (EScenes.TracingActivity.GetDescription());
			}

			public void LoadGameScene ()
			{
				gameManager.AudioController.TriggerAudio ("OnClick");
				UISceneLoader.Instance.LoadScene (EScenes.TracingGame.GetDescription());			
			}

			public void NextClickEvent ()
			{
				try {
					gameManager.AudioController.TriggerAudio ("OnClick");
					gameManager.NextShape ();
				} catch (System.Exception ex) {

				}
			}

			public void PreviousClickEvent ()
			{
				try {
					gameManager.AudioController.TriggerAudio ("OnClick");
					gameManager.PreviousShape ();
				} catch (System.Exception ex) {

				}
			}

			public void SpeechClickEvent ()
			{
				gameManager.AudioController.TriggerAudio ("OnClick");
				Shape shape = GameObject.FindObjectOfType<Shape> ();
				if (shape == null) {
					return;
				}
				shape.Spell ();
			}

			public void ResetShape ()
			{
				gameManager.AudioController.TriggerAudio ("OnClick");
				gameManager.ResetShape ();
				gameManager.EnableGameManager ();
			}

			public void PencilClickEvent (Pencil pencil)
			{
				gameManager.AudioController.TriggerAudio ("OnClick");
				if (pencil == null) {
					return;
				}
//                GameManager gameManager = GameObject.FindObjectOfType<GameManager>();
				if (gameManager == null) {
					return;
				}
				if (gameManager.currentPencil != null) {
					gameManager.currentPencil.DisableSelection ();
				}
				gameManager.currentPencil = pencil;
				gameManager.SetShapeOrderColor ();
				pencil.EnableSelection ();
			}

			public void ResetConfirmDialogEvent (GameObject value)
			{
				gameManager.AudioController.TriggerAudio ("OnClick");
				if (value == null) {
					return;
				}

//                GameManager gameManager = GameObject.FindObjectOfType<GameManager>();

				if (value.name.Equals ("YesButton")) {
					Debug.Log ("Reset Confirm Dialog : Yes button clicked");
					if (gameManager != null) {
						gameManager.ResetShape ();
					}

				} else if (value.name.Equals ("NoButton")) {
					Debug.Log ("Reset Confirm Dialog : No button clicked");
				}

//                value.GetComponentInParent<Dialog>().Hide();

				if (gameManager != null) {
					gameManager.EnableGameManager ();
				}
			}
		}
	}
}