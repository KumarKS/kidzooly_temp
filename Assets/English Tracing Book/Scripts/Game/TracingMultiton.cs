﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ABC {
	namespace Tracing {
		
		public class TracingMultiton : MonoBehaviour
		{
			public static TracingMultiton instance;

			void Awake()
			{
				if (instance == null) 
				{
					instance = this;
					DontDestroyOnLoad (gameObject);
					SceneManager.sceneLoaded += SceneManager_sceneLoaded;
				}
				else 
				{
					Destroy (gameObject);
				}
			}

			void SceneManager_sceneLoaded (Scene arg0, LoadSceneMode arg1)
			{
				if (arg0.name == EScenes.MainMenu.GetDescription ())
				{
					SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
					instance = null;
					Destroy (gameObject);
				}
			}
		}
	}
}