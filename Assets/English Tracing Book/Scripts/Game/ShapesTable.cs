﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.

namespace ABC
{
	namespace Tracing
	{
		[DisallowMultipleComponent]
		public class ShapesTable : MonoBehaviour
		{
			/// <summary>
			/// The shapes list.
			/// </summary>
			public static List<TableShape> shapes;

			/// <summary>
			/// The groups parent.
			/// </summary>
			public Transform groupsParent, gU, gN;

			/// <summary>
			/// The shape prefab.
			/// </summary>
            public GameObject shapePrefab;
            public List<TableShape> lowerShapes, upperShapes, numberShapes;

			public TableType tableType;
			public float speed = 10f;
			RectTransform rect;
			Helper2D[] helpers;
			static Color[] colors;
			int currentColor = -1, activeFrame = 0;
			Vector2 targetPos = Vector2.zero;
			bool move = false;

			/// <summary>
			/// The shapes manager.
			/// </summary>
			private ShapesManager shapesManager;
			public AudioController AudioController;

			public Color lockedColor;
			public const int FREECHARACTERS = 4;

			void Awake ()
			{
//                if (!string.IsNullOrEmpty(ShapesManager.shapeManagerReference))
//                {
//                    shapesManager = GameObject.Find(ShapesManager.shapeManagerReference).GetComponent<ShapesManager>();
//                }
//                else
//                {
//                    Debug.LogError("You have to start the game from the Main scene");
//                }

				//define the shapes list
				shapes = new List<TableShape> ();
				rect = GetComponent<RectTransform> ();
				helpers = GetComponents<Helper2D> ();


				//Create new shapes
				StartCoroutine ("CreateShapes");
			}

			void Start ()
			{
//				MainButton.Instance.OnAppsStarted ();
			}

			void Update ()
			{
				if (move) {
					rect.anchoredPosition = Vector2.Lerp (rect.anchoredPosition, targetPos, speed * Time.timeScale);
					if (Vector2.Distance (rect.anchoredPosition, targetPos) < 0.01) {
						rect.anchoredPosition = targetPos;
						move = false;
					}
				}
//                if (lastShape != null)
//                {
//                    //Set the bright postion to the last shape postion
//                    if (!Mathf.Approximately(lastShape.position.magnitude, shapeBright.position.magnitude))
//                    {
//                        shapeBright.position = lastShape.position;
//                    }
//				}
			}

			public void ClickPrevious ()
			{
				activeFrame = ++activeFrame == 2 ? -1 : activeFrame;
				targetPos.x = 1280 * activeFrame;
				move = true;
				AudioController.TriggerAudio ("OnClick");
			}

			public void ClickNext ()
			{
				activeFrame = --activeFrame == -2 ? 1 : activeFrame;
				targetPos.x = 1280 * activeFrame;
				move = true;
				AudioController.TriggerAudio ("OnClick");
			}

			/// <summary>
			/// Creates the shapes in Groups.
			/// </summary>
			private IEnumerator CreateShapes ()
			{
				yield return 0;

				groupsParent.gameObject.SetActive (false);
				gU.gameObject.SetActive (false);
				gN.gameObject.SetActive (false);

				#region LowerCase Initialization
				InitTable(lowerShapes, "LShapesManager");
				#endregion

				#region UpperCase Initialization
				InitTable(upperShapes, "UShapesManager");
				#endregion

				#region Number Initialization
				InitTable(numberShapes, "NShapesManager");
				#endregion

				groupsParent.gameObject.SetActive (true);
				gU.gameObject.SetActive (true);
				gN.gameObject.SetActive (true);
			}

			public void InitTable(List<TableShape> tableShapes, string shapeManagerReference)
			{
				shapes.Clear();
				bool isPremiumUser = LocalDataManager.Instance.SaveData.IsPremiumUser();
				for (int i = 0; i < tableShapes.Count; i++) 
				{
					TableShape v = tableShapes[i];
					if (i >= FREECHARACTERS && !isPremiumUser)
					{
						v.GetComponent<Image> ().color = lockedColor;
						v.transform.GetChild(v.transform.childCount-1).gameObject.GetComponent<Image> ().enabled = true;
						v.isLocked = true;
					}
					else
					{
						v.isLocked = false;
					}
					v.GetComponent<Button>().onClick.AddListener(() => 
					{
						if(v.isLocked)
						{
							UILoader.Instance.StartLoader();
							AudioController.TriggerAudio ("OnLock", () => 
							{
								UILoader.Instance.StopLoader();
								FreeTrialsPanel.Instance.Init ();
							});
						}
						else
						{
							shapes = tableShapes;
							ShapesManager.shapeManagerReference = shapeManagerReference;
							TableShape.selectedShape = v;
							UISceneLoader.Instance.LoadScene (EScenes.TracingGame.GetDescription());	
							AudioController.TriggerAudio ("OnClick");
						}
					});
				}
			}

			Color GetRandomColor ()
			{
				if (colors == null) {
					colors = new Color[6];
					float[,] values = new float[3, 2]{ { 0.1f, 0.25f }, { 0.5f, 0.7f }, { 0.7f, 8f } };
					int i = UnityEngine.Random.Range (0, 6);
					for (int j = 0; j < 6; j++, i = (i + 1) % 6) {
						colors [j].a = 1;
						switch (i) {
						case 0:		// rgb 321 
							colors [j].r = UnityEngine.Random.Range (values [2, 0], values [2, 1]);		// 3 -> Highest value
							colors [j].g = UnityEngine.Random.Range (values [1, 0], values [1, 1]);		// 2 -> Mid value
							colors [j].b = UnityEngine.Random.Range (values [0, 0], values [0, 1]);		// 1 -> Lowest value
							break;
						case 1:		// rgb 312 
							colors [j].r = UnityEngine.Random.Range (values [2, 0], values [2, 1]);		// 3 -> Highest value
							colors [j].g = UnityEngine.Random.Range (values [0, 0], values [0, 1]);		// 1 -> Lowest value
							colors [j].b = UnityEngine.Random.Range (values [1, 0], values [1, 1]);		// 2 -> Mid value
							break;
						case 2:		// rgb 231
							colors [j].r = UnityEngine.Random.Range (values [1, 0], values [1, 1]);		// 2 -> Mid value
							colors [j].g = UnityEngine.Random.Range (values [2, 0], values [2, 1]);		// 3 -> Highest value
							colors [j].b = UnityEngine.Random.Range (values [0, 0], values [0, 1]);		// 1 -> Lowest value
							break;
						case 3:		// rgb 213
							colors [j].r = UnityEngine.Random.Range (values [1, 0], values [1, 1]);		// 2 -> Mid value
							colors [j].g = UnityEngine.Random.Range (values [0, 0], values [0, 1]);		// 1 -> Lowest value
							colors [j].b = UnityEngine.Random.Range (values [2, 0], values [2, 1]);		// 3 -> Highest value
							break;
						case 4:		// rgb 132
							colors [j].r = UnityEngine.Random.Range (values [0, 0], values [0, 1]);		// 1 -> Lowest value
							colors [j].g = UnityEngine.Random.Range (values [2, 0], values [2, 1]);		// 3 -> Highest value
							colors [j].b = UnityEngine.Random.Range (values [1, 0], values [1, 1]);		// 2 -> Mid value
							break;
						case 5:		// rgb 123 
							colors [j].r = UnityEngine.Random.Range (values [0, 0], values [0, 1]);		// 1 -> Lowest value
							colors [j].g = UnityEngine.Random.Range (values [1, 0], values [1, 1]);		// 2 -> Mid value
							colors [j].b = UnityEngine.Random.Range (values [2, 0], values [2, 1]);		// 3 -> Highest value
							break;
						}
					}
				}
				currentColor = currentColor == -1 ? UnityEngine.Random.Range (0, 6) : (currentColor + 1) % 6;
				return colors [currentColor];
			}

			string GetDisplayText (int id)
			{
				switch (tableType) {
				case TableType.LowerCase:
					return ((char)(id - 1 + 97)).ToString ();
					break;
				case TableType.UpperCase:
					return ((char)(id - 1 + 65)).ToString ();
					break;
				case TableType.Number:
					return (id == 11 ? "10" : ((char)(id - 1 + 48)).ToString ());
					break;
				}
				return "";
			}

			public enum TableType
			{
				LowerCase,
				UpperCase,
				Number
			}
		}
	}
}