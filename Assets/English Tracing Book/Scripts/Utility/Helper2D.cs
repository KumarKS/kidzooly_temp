﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Helper2D : MonoBehaviour
{
	public RectTransform helperTransform, targetTransform;

	public void ShowHelper (string id, bool shouldMove = false)
	{
		Debug.Log (id + " helper showing");
		helperTransform.gameObject.SetActive (true);
		helperTransform.localScale = Vector3.one;
		if (shouldMove) {
			helperTransform.DOLocalMove (targetTransform.localPosition, 1f).SetLoops (-1, LoopType.Restart);
		}
	}

	public void HideHelper ()
	{
		if (helperTransform != null) {
			helperTransform.DOKill ();
			helperTransform.gameObject.SetActive (false);
		}
	}
}
