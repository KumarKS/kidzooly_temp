﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundScrolling : MonoBehaviour {

	public RawImage ScrollableObj;
	public float speedT = 0.1f;
	public float start = 0;

	Vector2 offset = new Vector2 (0, 0);



	void Update()
	{	

		offset.x = start - Time.time * speedT;

		ScrollableObj.material.mainTextureOffset = offset;
	}


}
