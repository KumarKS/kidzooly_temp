﻿using UnityEngine;

namespace vgm
{
    namespace PlayerLocalDataStore
    {
        public class PlayerPrefsService : Singleton<PlayerPrefsService>
        {
            public void IncrementInt(string paramatername, int value = 1)
            {
                SetIntValue(paramatername, (GetIntValue(paramatername) + value));
            }

            public int GetIntValue(string paramatername , int defaultValue = 0)
            {
                return PlayerPrefs.GetInt(paramatername, defaultValue);
            }

            
            public void IncrementDouble(string paramatername, double value = 0.0)
            {
                SetDoubleValue(paramatername, (GetDoubleValue(paramatername) + (int)value));
            }

            public int GetDoubleValue(string paramatername , int defaultValue = 0)
            {
                return PlayerPrefs.GetInt(paramatername, defaultValue);
            }

            public void SetDoubleValue(string paramatername, int value)
            {
                PlayerPrefs.SetInt(paramatername, value);
            }

            public string GetStringValue(string paramatername)
            {
                return PlayerPrefs.GetString(paramatername);
            }

            public void SetIntValue(string paramatername, int value)
            {
                PlayerPrefs.SetInt(paramatername, value);
            }

            public void SetStringValue(string paramatername, string value)
            {
                PlayerPrefs.SetString(paramatername, value);
            }

            public bool HasKeyPresent(string paramatername)
            {
                return PlayerPrefs.HasKey(paramatername);
            }
        }
    }
}
