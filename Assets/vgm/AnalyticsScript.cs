﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Analytics;
using System.Collections.Generic;
#if !AMAZON_STORE
using Firebase.Extensions;
#endif

public class AnalyticsScript : MBSingleton<AnalyticsScript>
{
    private Dictionary<string, object> dic;
    public Action OnFirebaseInitDone;

    bool isInitDone = false;
    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
    void CheckDependencies()
    {
#if !AMAZON_STORE
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>

        {
            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                isInitDone = true;
                OnFirebaseInitDone?.Invoke();

#if !AMAZON_STORE
                Firebase.Analytics.FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
#endif
            }
            else
            {
                isInitDone = false;
                Debug.LogError(
                "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
#endif
    }

    protected override void Awake () {
        base.Awake();
        CheckDependencies();
    }


    public void LogEvent (string a,string b,string c) {
        if (isInitDone) {
            dic = new Dictionary<string,object>();
            dic.Add(b,c);
            try {
#if !AMAZON_STORE
                Firebase.Analytics.FirebaseAnalytics.LogEvent(a,b,c);

#endif
            } catch (Exception e) {
                UnityEngine.Debug.LogError(e.Message + " " + e.Data);
            }
        }

    }

    public void LogEvent (string a,string b, double c) {
        if (isInitDone) {
            dic = new Dictionary<string,object>();
            dic.Add(b,c);
            try {
#if !AMAZON_STORE
                Firebase.Analytics.FirebaseAnalytics.LogEvent(a,b,c);

#endif
            } catch (Exception e) {
                UnityEngine.Debug.LogError(e.Message + " " + e.Data);
            }
        }

    }

    public void LogEvent (string a,string b,int c) {
        if (isInitDone) {
            dic = new Dictionary<string,object>();
            dic.Add(b,c);
            try {
#if !AMAZON_STORE
                Firebase.Analytics.FirebaseAnalytics.LogEvent(a,b,c);

#endif
            } catch (Exception e) {
                UnityEngine.Debug.LogError(e.Message + " " + e.Data);
            }
        }

    }

    public void LogEvent (string a) {
        if (isInitDone) {
           
            try {
#if !AMAZON_STORE
                Firebase.Analytics.FirebaseAnalytics.LogEvent(a);

#endif
            } catch (Exception e) {
                UnityEngine.Debug.LogError(e.Message + " " + e.Data);
            }
        }

    }

   

    public static int GetCountForEvent (string eventName) {
        int currentCount = PlayerPrefs.GetInt(eventName);
        int nextCount = currentCount + 1;
        PlayerPrefs.SetInt(eventName,nextCount);
        return nextCount;
    }

    public void LogUserProperty(string a, string b)
    {
        if (isInitDone)
        {
           
                try
                {
#if !AMAZON_STORE
                    UnityEngine.Debug.LogError("xxxxxx Logging Event: " + a + " : " + b );
                    Firebase.Analytics.FirebaseAnalytics.SetUserProperty(a, b);

#endif
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogError(e.Message + " " + e.Data);
                }
        }


    }

}
