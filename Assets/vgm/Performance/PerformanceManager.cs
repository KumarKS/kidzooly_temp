﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using vgm.PlayerLocalDataStore;
using System;
using vgm.database;
using Newtonsoft.Json;
public class PerformanceManager : GenericSingleton<PerformanceManager>
{

    private string selected_activity = string.Empty;
    public string selected_sub_activity = string.Empty;
    private static int total_attempts = 0;
    private static int total_success = 0;
    private double time_spent = 0;
    private int sub_success = 0;
    const double VG_CO_FACTOR = 0.1;

    const string SPENT_TIME = "spent_time";
    const string SUCCESS_COUNT = "sucess_count";
    const string TOTAL_ATTEMPTS = "total_attempts";
    const string SELECTED_ACTIVITY = "selected_activity";
    const string DEVICE_ID = "device_id";
    const string FID_ID = "fid_id";
    public const string D_TOKEN = "d_token";

    internal static string ALPHABETS = "alphabet";
    internal static string NUMBERS = "number";
    internal static string SHAPES = "shapes";
    internal static string COLORS = "colors";
    internal static string VEGETABLES = "vegetables";
    internal static string HOUSEHOLD = "household";
    internal static string FRUITS = "fruits";
    internal static string PROFESSION = "profession";
    internal static string TRACING = "tracingmain";


    internal static string SESSION_COUNT = "session_count";

    
	private static double Memory = 0; 
	private static double Observe = 0; 
	private static double Exploring = 0;
	private static double Listen = 0; 
	private static double Analytical = 0;
	private static double Identification = 0;
	private static double Classification = 0;
	private static double Solve = 0;
    private const string TABLE_NAME = "user_activity_data";
    public const string PERFORMANCE_TABLE_NAME = "performance_json";
    public const string LOCAL_PERFORMANCE_SAVE_DATA = "performance_local_json";

    private List<string> act_names  = new List<string>()
    {
        "ShadowMatchActivity",
        "InteractiveRhymeGallery",
        "ColoringActivity",
        "ScratchingActivity",
        "JoinDotsActivity",
        "TracingMain",
        "LearningWheelActivity",
        "PuzzleActivity",
        "Video",
        "Story"
    };

    public void SetSpentTime(double t) => time_spent = t;

    public void SetSuccessCount(int t) => total_success = t;

    public void SetTotalAttempts(int t) => total_attempts = t;
    
    public void SetSelectedActivity(string t) => selected_activity = t;
    public void SetSelectedSubActivity(string t) => selected_sub_activity = t;

    Dictionary<string, int> subActivitySuccessCount = new Dictionary<string, int>()
    {
        {ALPHABETS, 4},
        {NUMBERS, 1},
        {SHAPES, 1},
        {COLORS, 1},
        {VEGETABLES, 1},
        {HOUSEHOLD, 1},
        {FRUITS, 1},
        {PROFESSION, 1}
        // {TRACING, 1}
    };
   
    public void IncrementAttempts(int val = 1)
    {
        Debug.LogError("ATTEMPTS : "+ total_attempts);
        total_attempts ++; 
        Debug.LogError("ATTEMPTS AFTER : "+ total_attempts);
    }

    public void IncrementSuccess(int val = 1)
    {
        total_success ++; 
        Debug.LogError("INcreMENT SUCCESSE : " + total_success);
    }

    public void IncrementSubSuccess()
    {
        sub_success ++;
    }
    
    public void CheckSubSuccess(string val)
    {
        Debug.LogError("CHECK SUB SUCCESS : " + val);
        if (subActivitySuccessCount.ContainsKey(val))
        {
            if(sub_success >= subActivitySuccessCount[val])
            {
                IncrementSuccess();          
            }
            sub_success = 0;
        } else {
            // IncrementSuccess();
        }
    }

    private void Calculate(string act)
    {
        int suc = GetTotalSuccess(act);
        int totAtmp = GetTotalAttempts(act);
        double timSpt = GetTotalTime(act);
        Debug.LogError("M Before : " +suc + " : " + totAtmp  + " : " + timSpt + " : " + suc.GetType().ToString());
        float sucess_rate = ((float)suc/(float)totAtmp);
        double VG_factor = (timSpt*VG_CO_FACTOR)*sucess_rate;

        var m = GameData_Deserialize.Get_Bubble(selected_activity);
        if (m != null)
        {
            Debug.LogError("M : "+ m.a + " : " + m.c + " : "+m.e + " : "+m.i + " : " + m.l + " : "+VG_factor + " : " + sucess_rate);

            Memory = Math.Round((m.m * VG_factor)/100, 2);
            Observe = Math.Round((m.o * VG_factor)/100, 2);
            Exploring = Math.Round((m.e * VG_factor)/100, 2);
            Listen = Math.Round((m.l * VG_factor)/100, 2);
            Analytical = Math.Round((m.a * VG_factor)/100, 2);
            Identification = Math.Round((m.i * VG_factor)/100, 2);
            Classification = Math.Round((m.c * VG_factor)/100, 2);
            Solve = Math.Round((m.s * VG_factor)/100, 2);

            Debug.LogError("AFTER M : "+ Analytical + " : " + Classification + " : "+Exploring + " : "+Identification + " : " + Listen);
        }
    }

    public void SaveData()
    {
        Debug.LogError("Performance Manager : "+selected_activity+SPENT_TIME+ " : " + time_spent + " : " + total_success + " : " + total_attempts);
        PlayerPrefsService.Instance.IncrementDouble(selected_activity+SPENT_TIME, time_spent);
        PlayerPrefsService.Instance.IncrementInt(selected_activity+SUCCESS_COUNT, total_success);
        PlayerPrefsService.Instance.IncrementInt(selected_activity+TOTAL_ATTEMPTS, total_attempts);
        // Calculate(selected_activity);
        // GetPerformanceData();
    }


    public int GetTotalAttempts(string val) { return PlayerPrefsService.Instance.GetDoubleValue(val+TOTAL_ATTEMPTS); }
    public int GetTotalSuccess(string val) { return PlayerPrefsService.Instance.GetDoubleValue(val+SUCCESS_COUNT); }
    public double GetTotalTime(string val) { return PlayerPrefsService.Instance.GetDoubleValue(val+SPENT_TIME); }
    public double GetMemory() { return Memory; }
    public double GetObserve() { return Observe; }
    public double GetExploring() { return Exploring; }
    public double GetListen() { return Listen; }
    public double GetAnalytical() { return Analytical; }
    public double GetIdentification() { return Identification; }
    public double GetClassification() { return Classification; }
    public double GetSolve() { return Solve; }
    public string GetDeviceId() { return PlayerPrefsService.Instance.GetStringValue(DEVICE_ID);}
    public void SetDeviceId(string val) { PlayerPrefsService.Instance.SetStringValue(DEVICE_ID, val);}
    public string GetFIDId() { return PlayerPrefsService.Instance.GetStringValue(FID_ID); }
    public void SetDeviceFID(string val) { PlayerPrefsService.Instance.SetStringValue(FID_ID, val); }
    public Dictionary<string, Dictionary<string, object>> GetData()
    {
        Dictionary<string, Dictionary<string, object>> act = new Dictionary<string, Dictionary<string, object>>();

        for (int i = 0; i < act_names.Count; i++)
        {
            Dictionary<string, object> stat = new Dictionary<string, object>();

            int at = GetTotalAttempts(act_names[i]); 
            int ss = GetTotalSuccess(act_names[i]); 
            double ts = GetTotalTime(act_names[i]); 

            stat.Add(TOTAL_ATTEMPTS, at);
            stat.Add(SUCCESS_COUNT, ss);
            stat.Add(SPENT_TIME, ts);

            act.Add(act_names[i], stat);
        }

        return act;
    }

    public IEnumerator SaveDataToServer()
    {
             
        yield return new WaitForSeconds(2f);
#if !AMAZON_STORE
        if(string.IsNullOrEmpty(PlayerPrefsService.Instance.GetStringValue(PerformanceManager.D_TOKEN)))
            PlayerPrefsService.Instance.SetStringValue(PerformanceManager.D_TOKEN, FirebaseAPI.Instance.fcmToken);
#endif
        string device_id = GetDeviceId();
        string table_name = TABLE_NAME;

        var user_data = new UserData();
        user_data.id = device_id;
        user_data.activities = PerformanceManager.Instance.GetData();
        // user_data.coins = Master.Instance.profileManager.profile.coins;
        // user_data.daysSinceInstalled = LocalStorage.GetDaysSinceInstalled();
        // user_data.installDate = DateUtils.GetDateTime(AppConstants.INSTALLTIMESTAMP).ToString();
        user_data.activeDate = DateTimeExtensions.GetCurrentUnixTime();
        user_data.sessionCount = LocalDataManager.Instance.SaveData.GetCurrentSession;
        user_data.isPremium = LocalDataManager.Instance.SaveData.IsPremiumUser ();
        user_data.dToken = PlayerPrefsService.Instance.GetStringValue(D_TOKEN);
#if !AMAZON_STORE
        FireDatabaseService.SetData(device_id, table_name, user_data, SaveSuccess, SaveFail);
#endif
    }

    // private IEnumerator SaveAfterDelay()
    // {
       
    // }

    void SaveSuccess()
    {
        Debug.LogError("SAVE SUCCESS : ");
    }

    void SaveFail(string data)
    {
        Debug.LogError("SAVE FAIL : " + data);
    }

    
    public void GetDataFromServer()
    {
        string device_id = GetDeviceId();
        string table_name = TABLE_NAME;
#if !AMAZON_STORE
        FireDatabaseService.GetData(device_id, table_name, OnGetSuccess, OnGetFail);
#endif

    }

    void OnGetSuccess(UserData data)
    {
        var fetchedData = data;
        Debug.LogError("FETCH SUCESS: " + fetchedData.id);
        
        if(string.IsNullOrEmpty(fetchedData.id))
        {

        } else {

            // Master.Instance.profileManager.profile.coins = data.coins;
            // Master.Instance.profileManager.profile.Save();

            string dateTimeString = data.installDate;
            System.DateTime dTime = System.DateTime.Parse(dateTimeString);
            // DateUtils.SaveDateTime(AppConstants.INSTALLTIMESTAMP, dTime);

            // PlayerPrefsService.Instance.IncrementInt(AppConstants.SESSIONCOUNTER, data.sessionCount);
            // LocalStorage.UpdateDaysSinceInstalled();

            foreach (var act in data.activities)
            {
                Debug.LogError("Key : "+act.Key + " Val : " +data.activities[act.Key][SPENT_TIME]);
               
                PlayerPrefsService.Instance.SetDoubleValue(act.Key+SPENT_TIME, Convert.ToInt32(data.activities[act.Key][SPENT_TIME]));
                PlayerPrefsService.Instance.SetIntValue(act.Key+SUCCESS_COUNT, Convert.ToInt32(data.activities[act.Key][SUCCESS_COUNT]));
                PlayerPrefsService.Instance.SetIntValue(act.Key+TOTAL_ATTEMPTS, Convert.ToInt32(data.activities[act.Key][TOTAL_ATTEMPTS]));
            }
        }
    }

    void OnGetFail(string msg)
    {
        Debug.LogError("Fetch FAILED : " + msg);
    }

    public void Clear()
    {
        Debug.LogError("FROM WHERE : ");
        selected_activity = string.Empty;
        selected_sub_activity = string.Empty;
        total_attempts = 0;
        total_success = 0;
        time_spent = 0;
        sub_success = 0;
    }
}
