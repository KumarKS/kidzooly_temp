﻿

using System;
using System.Collections;
using Newtonsoft.Json;
using UnityEngine;
using vgm.database;
using vgm.PlayerLocalDataStore;
// using vgm.PlayerLocalDataStore;

[System.Serializable]
public class GameRoot
{
    public Bubbles bubbles;
}

[System.Serializable]
public class Bubbles
{
    public Activity ShadowMatchActivity { get; set; }
    public Activity InteractiveRhymeGallery { get; set; }
    public Activity ColoringActivity { get; set; }
    public Activity ScratchingActivity { get; set; }
    public Activity JoinDotsActivity { get; set; }
    public Activity TracingMain { get; set; }
    public Activity LearningWheelActivity { get; set; }
    public Activity PuzzleActivity { get; set; }
    public Activity Video { get; set; }
    public Activity Story { get; set; }

}

public class Activity
{
    public int m;
    public int o;
    public int e;
    public int l;
    public int a;
    public int i;
    public int c;
    public int s;
}

enum ACTIVITY_TYPE
{
    ShadowMatchActivity,
    InteractiveRhymeGallery,
    ColoringActivity,
    ScratchingActivity,
    JoinDotsActivity,
    TracingMain,
    LearningWheelActivity,
    PuzzleActivity,
    Video,
    Story
}

public class GameData_Deserialize : MonoBehaviour
{
    private string dataAsJson = "";
    private string puzzledataAsJson = "";
    public static bool IsParsed = false;
    public static Action ParseComplete;
    public static Action ParseStart;
    public static GameRoot GameData = null;
    private static GameData_Deserialize _instance;
    public static GameData_Deserialize instance
    {
        get
        {
            if (_instance == null)
            {
                try
                {
                    GameObject singletonObjects = GameObject.Find("SingletonObjects"); if (singletonObjects == null) singletonObjects = new GameObject("SingletonObjects"); DontDestroyOnLoad(singletonObjects); GameObject gObj = new GameObject("_GameData_Deserialize");
                    _instance = gObj.AddComponent<GameData_Deserialize>();
                    gObj.transform.SetParent(singletonObjects.transform);
                }
                catch
                {
                    Debug.Log("______Unable to create Game json object Exception");
                }
            }
            return _instance;
        }
    }

    public static int TotalCategories { get; internal set; }

    void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(this);
        }
        else
            Destroy(this.gameObject);
    }

    public void StartParsing()
    {
        GameData = null;
        // StartCoroutine(_Init());
        bool isNetActive = VGInternet.Instance.Status;

        if (isNetActive)
        {
            GetDataFromFirebase();

        }
        else
        {

            string performanceData = PlayerPrefsService.Instance.GetStringValue(PerformanceManager.LOCAL_PERFORMANCE_SAVE_DATA);

            if (string.IsNullOrEmpty(performanceData))
            {
                StartCoroutine(_Init());
            }
            else
            {
                GameData = JsonConvert.DeserializeObject<GameRoot>(performanceData);
            }
        }
    }

    private IEnumerator _Init()
    {
        if (ParseStart != null && GameData != null)
        {
            ParseStart();
            IsParsed = false;
        }

        TextAsset tx = Resources.Load(GameConstants.JSON_ROOT_FOLDER + GameConstants.GAME_JSON) as TextAsset;
        yield return tx;
        dataAsJson = tx.text;

        Debug.LogError("dataAsJson" + dataAsJson);

        if (dataAsJson != null)
            GameData = JsonConvert.DeserializeObject<GameRoot>(dataAsJson);
        else
            StartCoroutine(_Init());

        // Debug.LogError("dataAsJson 1 : " + GameData.bubbles.ShadowMatchActivity);

    }


    void GetDataFromFirebase()
    {
#if !AMAZON_STORE
        FireDatabaseService.GetGeneralData("", PerformanceManager.PERFORMANCE_TABLE_NAME, OnSuccess, OnFail);
#endif
    }

    void OnSuccess(string data)
    {
        Debug.LogError("Received Data :" + data);
        GameData = JsonConvert.DeserializeObject<GameRoot>(data);
        PlayerPrefsService.Instance.SetStringValue(PerformanceManager.LOCAL_PERFORMANCE_SAVE_DATA, data);
    }

    void OnFail(string msg)
    {
        Debug.LogError("Received Data Fail :" + msg);

    }


    public static Activity Get_Bubble(string bubble = "")
    {

        if (!string.IsNullOrEmpty(bubble) && Enum.IsDefined(typeof(ACTIVITY_TYPE), bubble))
        {
            ACTIVITY_TYPE act = (ACTIVITY_TYPE)Enum.Parse(typeof(ACTIVITY_TYPE), bubble);
            Debug.LogError("act : " + act);
            switch (act)
            {
                case (ACTIVITY_TYPE.ShadowMatchActivity):
                    return GameData.bubbles.ShadowMatchActivity;
                case (ACTIVITY_TYPE.ColoringActivity):
                    return GameData.bubbles.ColoringActivity;
                case (ACTIVITY_TYPE.Video):
                    return GameData.bubbles.Video;
                case (ACTIVITY_TYPE.LearningWheelActivity):
                    return GameData.bubbles.LearningWheelActivity;
                case (ACTIVITY_TYPE.Story):
                    return GameData.bubbles.Story;
                case (ACTIVITY_TYPE.PuzzleActivity):
                    return GameData.bubbles.PuzzleActivity;
                case (ACTIVITY_TYPE.TracingMain):
                    return GameData.bubbles.TracingMain;
                case (ACTIVITY_TYPE.JoinDotsActivity):
                    return GameData.bubbles.JoinDotsActivity;
                case (ACTIVITY_TYPE.InteractiveRhymeGallery):
                    return GameData.bubbles.InteractiveRhymeGallery;
                case (ACTIVITY_TYPE.ScratchingActivity):
                    return GameData.bubbles.ScratchingActivity;
                default:
                    return GameData.bubbles.ShadowMatchActivity;
            }
        }
        else
        {
            return null;
        }
    }

}




