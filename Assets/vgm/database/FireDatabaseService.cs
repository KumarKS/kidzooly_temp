﻿using System;
using System.Collections.Generic;

namespace vgm
{
    namespace database
    {

        public static class FireDatabaseService
        {
#if !AMAZON_STORE
            static FirebaseDatabaseManager firebaseDataManagerInstance;

            static FireDatabaseService()
            {
                firebaseDataManagerInstance = new FirebaseDatabaseManager();
            }

            public static void GetData(string userid, string tablename, Action<UserData> OnSuccess, Action<string> OnError)
            {
                firebaseDataManagerInstance.Get(userid, tablename, OnSuccess, OnError);
            }

            public static void SetData(string userid, string tablename, UserData data, Action OnSuccess, Action<string> OnError)
            {
                firebaseDataManagerInstance.Save(userid, tablename, data, OnSuccess, OnError);
            }

            public static void GetGeneralData(string userid, string tablename, Action<string> OnSuccess, Action<string> OnError)
            {
                firebaseDataManagerInstance.GetGeneralData(userid, tablename, OnSuccess, OnError);
            }
#endif
        }
    }
}