﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UserData 
{
   public string id;
   public Dictionary<string, Dictionary<string, object>> activities;
   public int coins;
   public int daysSinceInstalled;
   public string installDate;
   public string activeDate;
   public int sessionCount;
   public bool isPremium;
   public string dToken;
}
