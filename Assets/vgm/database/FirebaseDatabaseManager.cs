﻿using System.Collections;
using System;
using UnityEngine;
#if !AMAZON_STORE
using Firebase.Extensions;
#endif
using Newtonsoft.Json;

namespace vgm
{
    namespace database
    {
#if !AMAZON_STORE
        public class FirebaseDatabaseManager : IDatabase
        {
            private Firebase.Database.FirebaseDatabase firebaseDatabase;
            private Firebase.Database.DatabaseReference userProgress;

            public FirebaseDatabaseManager()
            {
                firebaseDatabase = Firebase.Database.FirebaseDatabase.DefaultInstance;
            }
            
            public void Initialize(string userId, string databaseAddress)
            {
                if (string.IsNullOrEmpty (userId))
				{
					userProgress = null;
				}
				else
				{
					
				}
            }

            public void Save(string userid, string tablename, UserData data, Action OnSuccess, Action<string> OnError)
            {
				userProgress = firebaseDatabase.GetReference(tablename + "/" + userid);

                if (userProgress == null)
				{
					OnError ("[DB] Not Initialzied");
					return;
				}

				UnityEngine.Debug.LogError("[Firebase Database] Saving data.."  + ": " + JsonConvert.SerializeObject(data));


				userProgress.SetRawJsonValueAsync (JsonConvert.SerializeObject(data)).ContinueWithOnMainThread(task =>
				{
					UnityEngine.Debug.Log("[Firebase Database] Saving Start");
					if (task.IsCanceled || task.IsFaulted)
					{
						UnityEngine.Debug.LogError("[Firebase Database] Saving Error : " + task.Exception);
						if(OnError != null) OnError(task.Exception.Message);
					}
					else
					{
						OnSuccess();
					}
				});
            }

            public void Get(string userid, string tablename, Action<UserData> OnSuccess, Action<string> OnError)
            {
				userProgress = firebaseDatabase.GetReference(tablename + "/" + userid);
				
                if (userProgress == null)
				{
					OnError ("[DB] Not Initialzied");
					return;
				}

				userProgress.GetValueAsync().ContinueWithOnMainThread(task =>
				{
					if (task.IsCanceled || task.IsFaulted)
					{
						UnityEngine.Debug.LogError("[Firebase Database] Fetch Error : " + task.Exception);
						if(OnError != null) OnError(task.Exception.Message);
					}
					else
					{
						if(task.Result != null && task.Result.Exists)
						{
							UnityEngine.Debug.Log("[Firebase Database] Fetched Data Exists");
							var data = task.Result.GetRawJsonValue();
							if(string.IsNullOrEmpty(data))
							{
								UnityEngine.Debug.Log("[Firebase Database] Fetched Exists but not parsable");
                                if(OnError != null) OnError("Fetched Exists but not parsable");
							}
							else
							{
                                var parsedData = JsonConvert.DeserializeObject<UserData>(data);
                                if(parsedData != null && !string.IsNullOrEmpty(parsedData.id))
                                {
                                    OnSuccess(parsedData);
                                } 
                                else 
                                {
                                    if(OnError != null) OnError("Fetched Exists but empty");
                                }
                                
							}							
						}
						else
						{
							UnityEngine.Debug.Log("[Firebase Database] Fetched Data Not Exists");
							OnSuccess(new UserData());
						}
					}
				});
            }

				public void GetGeneralData(string userid, string tablename, Action<string> OnSuccess, Action<string> OnError)
            {
				userProgress = firebaseDatabase.GetReference(tablename + "/" + userid);
				
                if (userProgress == null)
				{
					OnError ("[DB] Not Initialzied");
					return;
				}

				userProgress.GetValueAsync().ContinueWithOnMainThread(task =>
				{
					if (task.IsCanceled || task.IsFaulted)
					{
						UnityEngine.Debug.LogError("[Firebase Database] Fetch Error : " + task.Exception);
						if(OnError != null) OnError(task.Exception.Message);
					}
					else
					{
						if(task.Result != null && task.Result.Exists)
						{
							UnityEngine.Debug.Log("[Firebase Database] Fetched Data Exists");
							var data = task.Result.GetRawJsonValue();
							if(string.IsNullOrEmpty(data))
							{
								UnityEngine.Debug.Log("[Firebase Database] Fetched Exists but not parsable");
                                if(OnError != null) OnError("Fetched Exists but not parsable");
							}
							else
							{
                                var parsedData = data;
                                if(parsedData != null)
                                {
                                    OnSuccess(parsedData);
                                } 
                                else 
                                {
                                    if(OnError != null) OnError("Fetched Exists but empty");
                                }
                                
							}							
						}
						else
						{
							UnityEngine.Debug.Log("[Firebase Database] Fetched Data Not Exists");
							OnSuccess("");
						}
					}
				});
            }

        }
#endif
    }
}
