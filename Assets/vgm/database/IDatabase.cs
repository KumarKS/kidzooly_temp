﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace vgm
{
    namespace database
    {
        public interface IDatabase
        {
           void Initialize(string userId, string databaseAddress);
           void Save(string userid, string tablename, UserData data, Action OnSuccess, Action<string> OnError);
           void Get(string userid, string tablename, Action<UserData> OnSuccess, Action<string> OnError);
        }
    }
}
