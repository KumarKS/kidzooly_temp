﻿//Attach this script to a GameObject
//This script checks the device’s ability to reach the internet and outputs it to the console window

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace Ads
{
    public class VGInternet
    {
        public static Action<bool> statusChanged;
        static bool lastStatus = false, currenStatus = false;
        public static bool Status
        {
            get
            {
                CheckInternet();
                return currenStatus;
            }
        }

        static void CheckInternet()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                currenStatus = false;
            }
            //Check if the device can reach the internet via a carrier data network
            else if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
            {
                currenStatus = true;
            }
            //Check if the device can reach the internet via a LAN
            else if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
            {
                currenStatus = true;
            }
            else
            {
                currenStatus = false;
            }
            if (lastStatus != currenStatus)
            {
                statusChanged?.Invoke(currenStatus);
                lastStatus = currenStatus;
            }
        }
    }
}