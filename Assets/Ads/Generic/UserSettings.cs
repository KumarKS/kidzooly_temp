﻿using UnityEngine;
using System.Collections;

namespace usersetting
{

    public class UserSettings : MonoBehaviour
    {
        public bool isBGMuiscOn;
        public bool isAutoPlayOn;
        public bool isSoundsOn;

        int BGMusic;
        int Sound;
        int Autoplay;


        public DifficultyType difficultyType;

        public static UserSettings Instance;

        public void Awake()
        {
            Instance = this;
            difficultyType = DifficultyType.Easy;

            if (IfValuesNull())
            {
                Init();
            }
        }

        bool IfValuesNull()
        {
            if (PlayerPrefs.HasKey("bgmusic") || PlayerPrefs.HasKey("sound") || PlayerPrefs.HasKey("autoplay"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        void Init()
        {
            BGMusic = 1;
            Sound = 1;
            Autoplay = 1;

            isBGMuiscOn = true;
            isAutoPlayOn = true;
            isSoundsOn = true;

            PlayerPrefs.SetInt("bgmusic", BGMusic);
            PlayerPrefs.SetInt("autoplay", Autoplay);
            PlayerPrefs.SetInt("sound", Sound);
        }

        public void SetBGMusicVal(bool isTrue)
        {
            isBGMuiscOn = isTrue;
            if (isTrue)
            {
                BackGroundMusic.Instance.ResumeBackGroundMusic();
                BGMusic = 1;
            }
            else
            {
                BackGroundMusic.Instance.PauseBackGroundMusic();
                BGMusic = 0;
            }
            PlayerPrefs.SetInt("bgmusic", BGMusic);
        }


        public void SetSoundsVal(bool isTrue)
        {
            isSoundsOn = isTrue;

            if (isTrue)
            {
                Sound = 1;
            }
            else
            {
                Sound = 0;
            }
            PlayerPrefs.SetInt("sound", Sound);
        }

        public void SetAutoPlayVal(bool isTrue)
        {
            isAutoPlayOn = isTrue;

            if (isTrue)
            {
                Autoplay = 1;
                Debug.LogError("Autoplay true");
            }
            else
            {
                Autoplay = 0;
                Debug.LogError("autoplay false");
            }
            PlayerPrefs.SetInt("autoplay", Autoplay);
        }

        public bool GetBGMusicVal()
        {
            BGMusic = PlayerPrefs.GetInt("bgmusic");
            if (BGMusic == 1)
            {
                isBGMuiscOn = true;
            }
            else
            {
                isBGMuiscOn = false;
            }
            return isBGMuiscOn;
        }

        public bool GetSoundsVal()
        {
            Sound = PlayerPrefs.GetInt("sound");
            if (Sound == 1)
            {
                isSoundsOn = true;
            }
            else
            {
                isSoundsOn = false;
            }
            return isSoundsOn;
        }

        public bool GetAutoPlayVal()
        {
            Autoplay = PlayerPrefs.GetInt("autoplay");
            if (Autoplay == 1)
            {
                isAutoPlayOn = true;
            }
            else
            {
                isAutoPlayOn = false;
            }
            return isAutoPlayOn;
        }
    }


    public enum DifficultyType
    {
        Easy = 0,
        Medium = 1,
        Hard = 2
    }

}