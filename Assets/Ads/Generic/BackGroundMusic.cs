﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using usersetting;
public class BackGroundMusic : MonoBehaviour 
{
	public AudioSource bgMusicSource;
	public AudioClip backgroundMusic;

	public static BackGroundMusic Instance;

	public float mainSceneVolume, appSceneVolume;

	public void Awake()
	{
		Instance = this;
	}

	public void Start () 
	{
		bgMusicSource.loop = true;
		bgMusicSource.clip = backgroundMusic;
		if (UserSettings.Instance.GetBGMusicVal())
		{
			ResumeBackGroundMusic ();
		}

		//SceneDelegates.Instance.MainScene += OnMainSceneLoad;
	}

	public void OnMainSceneLoad()
	{
		bgMusicSource.volume = mainSceneVolume;
	}

	public void PauseBackGroundMusic()
	{
		bgMusicSource.Pause ();
	}

	public void ResumeBackGroundMusic()
	{
		bgMusicSource.Play ();
	}
}
