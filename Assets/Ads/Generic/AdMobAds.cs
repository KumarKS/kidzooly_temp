﻿
using GoogleMobileAds.Api;
using System;
using System.Collections.Generic;
using UnityEngine;
using vgm.ads;
using vgm.remote;

namespace vgm
{
    namespace ads
    {
        namespace admob
        {
            public static class Manager
            {
#if TEST_AD
                public static string BANNERID = "ca-app-pub-3940256099942544/6300978111";
                public static string INTERESTIALID = "     ca-app-pub-3940256099942544/3419835294";
#else
                public static string BANNERID = RemoteService.ADMOB_BANNER_ADS_PRIORITY_KEY;
                public static string INTERESTIALID = RemoteService.ADMOB_INTERSTITIAL_ADS_PRIORITY_KEY;
#endif
                public static string REWARDID = "//not using";
                public static string ADMOB_ID = RemoteService.ADMOB_ID_KEY;
                private const string TEST_ID = "ca-app-pub-3940256099942544~3347511713";

                public static void Initialize()
                {

                }

                static Manager()
                {
#if TEST_AD
                    MobileAds.Initialize(TEST_ID);
#else
                    RequestConfiguration requestConfiguration = new RequestConfiguration.Builder()
                            .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True).SetMaxAdContentRating(MaxAdContentRating.G)
                            .build();
                    MobileAds.SetRequestConfiguration(requestConfiguration);

                    MobileAds.Initialize((initStatus) =>
        {
            Dictionary<string, AdapterStatus> map = initStatus.getAdapterStatusMap();
            foreach (KeyValuePair<string, AdapterStatus> keyValuePair in map)
            {
                string className = keyValuePair.Key;
                AdapterStatus status = keyValuePair.Value;
                switch (status.InitializationState)
                {
                    case AdapterState.NotReady:
                        // The adapter initialization did not complete.
                        MonoBehaviour.print("Adapter: " + className + " not ready.");
                        break;
                    case AdapterState.Ready:
                        // The adapter was successfully initialized.
                        MonoBehaviour.print("Adapter: " + className + " is initialized.");
                        break;
                }
            }
        });

#endif

                }
            }

            public class BannerAd : IBannerAd
            {

                public BannerAd()
                {
                    RequestAd();
                }
                #region IBannerAd implementation
                public void HideAd()
                {
                    banner.ForEach(x =>
                    {
                        x.Hide();
                    });
                    RequestAd();
                }

                public void ShowAd(BannerPos pos)
                {
                    banner.ForEach(x =>
                    {
                        if (x._isBannerReady)
                        {
                            x.Show();
                            return;
                        }
                    });
                }

                //void RequestAd()
                //{
                //    if (banner != null && banner.Count > 0)
                //    {
                //        banner.ForEach(x =>
                //        {
                //            x.UnSubscribleEvents();
                //        });
                //    }
                //    string[] adUnitIds = Manager.BANNERID.Split('|');
                //    AdSize adaptiveSize;
                //    adaptiveSize = RemoteService.ENABLE_ADAPTIVE_BANNER ? AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth) : AdSize.SmartBanner;
                //    for (int i = 0; i < adUnitIds.Length; i++)
                //    {
                //        Debug.Log("adUnitIds[i]" + adUnitIds[i]); 
                //        Debug.Log("adaptiveSize" + adaptiveSize); 
                //        Debug.Log("AdPosition.Bottom" + AdPosition.Bottom);
                //        banner.Add(new BannerObject(adUnitIds[i], adaptiveSize, AdPosition.Bottom));
                //    }


                //}

                void RequestAd()
                {
                    if (banner != null && banner.Count > 0)
                    {
                        banner.ForEach(x =>
                        {
                            x.UnSubscribleEvents();
                        });
                    }
                    string[] adUnitIds = Manager.BANNERID.Split('|');
                    banner = new List<BannerObject>(adUnitIds.Length);
                    AdSize adaptiveSize;
                    adaptiveSize = RemoteService.ENABLE_ADAPTIVE_BANNER ? AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth) : AdSize.SmartBanner;
                    for (int i = 0; i < adUnitIds.Length; i++)
                    {
                        Debug.Log("adUnitIds[i]" + adUnitIds[i]);
                        Debug.Log("adaptiveSize" + adaptiveSize);
                        Debug.Log("AdPosition.Bottom" + AdPosition.Bottom);
                        banner.Add(new BannerObject(adUnitIds[i], adaptiveSize, AdPosition.Bottom));
                    }


                }

                public bool IsReady()
                {
                    return banner.Exists(x => x._isBannerReady);
                }
                #endregion
                List<BannerObject> banner = null;
                #region BannerObject
                public class BannerObject
                {
                    public bool _isBannerReady = false;
                    private BannerView bannerView;
                    public BannerObject(string unitId, AdSize adSize, AdPosition adPos)
                    {
                        bannerView = new BannerView(unitId, adSize, adPos);
                        bannerView.LoadAd(new AdRequest.Builder().AddExtra("npa", "1").AddExtra("max_ad_content_rating", "G").Build());
                        bannerView.OnAdLoaded += Banner_OnAdLoaded;
                        bannerView.OnAdFailedToLoad += Banner_OnAdFailedToLoad;
                        bannerView.Hide();
                    }

                    public void UnSubscribleEvents()
                    {
                        bannerView.OnAdLoaded -= Banner_OnAdLoaded;
                        bannerView.OnAdFailedToLoad -= Banner_OnAdFailedToLoad;
                        bannerView.Destroy();
                    }

                    public void Show()
                    {
                        bannerView.Show();
                    }
                    public void Hide()
                    {
                        bannerView.Hide();
                    }

                    private void Banner_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
                    {
                        Debug.LogError("Ad failed with reason" + e.Message);

                        //LoadAdError loadAdError = e.LoadAdError;

                        //// Gets the domain from which the error came.
                        //string domain = loadAdError.GetDomain();

                        //// Gets the error code. See
                        //// https://developers.google.com/android/reference/com/google/android/gms/ads/AdRequest
                        //// and https://developers.google.com/admob/ios/api/reference/Enums/GADErrorCode
                        //// for a list of possible codes.
                        //int code = loadAdError.GetCode();

                        //// Gets an error message.
                        //// For example "Account not approved yet". See
                        //// https://support.google.com/admob/answer/9905175 for explanations of
                        //// common errors.
                        //string message = loadAdError.GetMessage();

                        //// Gets the cause of the error, if available.
                        //AdError underlyingError = loadAdError.GetCause();

                        //// All of this information is available via the error's toString() method.
                        //Debug.Log("Load error string: " + loadAdError.ToString());

                        //// Get response information, which may include results of mediation requests.
                        //ResponseInfo responseInfo = loadAdError.GetResponseInfo();
                        //Debug.Log("Response info: " + responseInfo.ToString());
                        //Debug.LogError("AdmobAds failed to load" + sender.ToString());
                    }

                    void Banner_OnAdLoaded(object sender, EventArgs e)
                    {
                        Debug.LogError("###Admob Banner Loaded" + sender.ToString());
                        _isBannerReady = true;
                    }
                }
                #endregion
            }

            public class InterestialAd : IInterestialAd
            {
                #region IFullScreenAd implementation

                public void RequestAd()
                {
                    if (interstitial != null && interstitial.Count > 0)
                    {
                        interstitial.ForEach(x =>
                        {
                            x.UnSubscribleEvents();
                        });
                    }

                    string[] adUnitIds = Manager.INTERESTIALID.Split('|');
                    for (int i = 0; i < adUnitIds.Length; i++)
                    {
                        Debug.Log("Admob Inter before loop" + adUnitIds[i]);
                    }
                    interstitial = new List<InterstitialObject>(adUnitIds.Length);
                    for (int i = 0; i < adUnitIds.Length; i++)
                    {
                        Debug.Log("Admob Inter in loop" + adUnitIds[i]);
                        interstitial.Add(new InterstitialObject(this.OnComplete));
                    }
                }

                public void ShowAd()
                {
                    Debug.LogError("###Admob Showing Interstitial");
                    interstitial.ForEach(x =>
                    {
                        if (x.IsAdReady())
                        {
                            Debug.Log("inter x" + x);
                            x.Show();
                            return;
                        }
                    });
                }

                public void OnAdClosed()
                {
                    Debug.LogError("Admob inter closed");
                    RequestAd();
                }

                public bool IsAdReady()
                {
                    return interstitial.Exists(x => x.IsAdReady());
                }

                #endregion

                public InterestialAd(Action onComplete)
                {
                    this.OnComplete += OnAdClosed;
                    this.OnComplete = onComplete;
                    RequestAd();
                }
                private List<InterstitialObject> interstitial;
                private readonly Action OnComplete;

                #region InterstitialObject
                public class InterstitialObject
                {
                    private Action OnComplete;
                    private InterstitialAd interstitialAd;
                    public InterstitialObject(Action OnComplete)
                    {
                        interstitialAd = new InterstitialAd(Manager.INTERESTIALID);
                        interstitialAd.OnAdClosed += Interstitial_OnAdClosed;
                        interstitialAd.OnAdLoaded += Interstitial_OnAdLoaded;
                        interstitialAd.OnAdFailedToLoad += Interstitial_OnAdFailedToLoad;
                        interstitialAd.LoadAd(new AdRequest.Builder().AddExtra("npa", "1").AddExtra("max_ad_content_rating", "G").Build());
                        this.OnComplete += OnComplete;
                    }

                    public bool IsAdReady()
                    {
                        return interstitialAd != null && interstitialAd.IsLoaded();
                    }

                    public void Show()
                    {
                        interstitialAd.Show();
                    }

                    public void UnSubscribleEvents()
                    {
                        interstitialAd.OnAdClosed -= Interstitial_OnAdClosed;
                        interstitialAd.OnAdLoaded -= Interstitial_OnAdLoaded;
                        interstitialAd.OnAdFailedToLoad -= Interstitial_OnAdFailedToLoad;
                        interstitialAd.Destroy();
                    }

                    void Interstitial_OnAdClosed(object sender, EventArgs e)
                    {
                        OnComplete?.Invoke();
                    }

                    void Interstitial_OnAdLoaded(object sender, EventArgs e)
                    {
                        Debug.LogError("###Admob inter Loaded");
                    }

                    void Interstitial_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
                    {
                        Debug.LogError("Ad failed with reason" + e.Message);

                        //LoadAdError loadAdError = e.LoadAdError;

                        //    // Gets the domain from which the error came.
                        //    string domain = loadAdError.GetDomain();

                        //    // Gets the error code. See
                        //    // https://developers.google.com/android/reference/com/google/android/gms/ads/AdRequest
                        //    // and https://developers.google.com/admob/ios/api/reference/Enums/GADErrorCode
                        //    // for a list of possible codes.
                        //    int code = loadAdError.GetCode();

                        //    // Gets an error message.
                        //    // For example "Account not approved yet". See
                        //    // https://support.google.com/admob/answer/9905175 for explanations of
                        //    // common errors.
                        //    string message = loadAdError.GetMessage();

                        //    // Gets the cause of the error, if available.
                        //    AdError underlyingError = loadAdError.GetCause();

                        //    // All of this information is available via the error's toString() method.
                        //    Debug.Log("Load error string: " + loadAdError.ToString());

                        //    // Get response information, which may include results of mediation requests.
                        //    ResponseInfo responseInfo = loadAdError.GetResponseInfo();
                        //    Debug.Log("Response info: " + responseInfo.ToString());
                    }
                }
                #endregion
            }
        }
    }
}