﻿using UnityEngine;
using System.Collections;
using System;

public static class DateUtils 
{
	public static DateTime GetDateTime(string prefix)
	{
		DateTime dateTime = new DateTime();
		int year, month, day, hour, minute, second;
		year = PlayerPrefs.GetInt (prefix + "year", 0);
		month = PlayerPrefs.GetInt (prefix + "month", 0);
		day = PlayerPrefs.GetInt (prefix + "day", 0);
		hour = PlayerPrefs.GetInt (prefix + "hour", 0);
		minute = PlayerPrefs.GetInt (prefix + "minute", 0);
		second = PlayerPrefs.GetInt (prefix + "second", 0);

		try
		{
			Debug.Log(year + ":" + month + ":" + day + ":" + hour + ":" + minute + ":" + second);
			dateTime = new DateTime(year, month, day, hour, minute, second, DateTimeKind.Utc);
		}
		catch(Exception e)
		{
			Debug.Log (e.Message + " mess");
			return DateTime.UtcNow;
		}
		return dateTime;
	}

	public static void SaveDateTime(string prefix, DateTime time)
	{
		PlayerPrefs.SetInt (prefix + "year"		, time.Year);
		PlayerPrefs.SetInt (prefix + "month"	, time.Month);
		PlayerPrefs.SetInt (prefix + "day"		, time.Day);
		PlayerPrefs.SetInt (prefix + "hour"		, time.Hour);
		PlayerPrefs.SetInt (prefix + "minute"	, time.Minute);
		PlayerPrefs.SetInt (prefix + "second"	, time.Second);
		PlayerPrefs.Save ();
	}

	public static int DifferenceInTimeFromNow_SEC(string prefix)
	{
		return (int) DateTime.UtcNow.Subtract (GetDateTime (prefix)).TotalSeconds;
	}

	public static int DifferenceInTimeFromNow_SEC(DateTime dateTime)
	{
		return (int) DateTime.UtcNow.Subtract (dateTime).TotalSeconds;
	}

}
