﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileUtils
{
    public static void DeleteFolder(string path)
    {
        DirectoryInfo info = new DirectoryInfo(path);
        if (info.Exists)
        {
            info.Delete(true);
        } else {
            UnityEngine.Debug.Log("Folder Not Found: " + path);
        }
    }

    public static T GetAsset<T>(string filePath) where T : UnityEngine.Object
    {
        return Resources.Load<T>(filePath);
    }

    public static T[] GetAllAssets<T>(string filePath) where T : UnityEngine.Object
    {
        return Resources.LoadAll<T>(filePath);
    }

}
