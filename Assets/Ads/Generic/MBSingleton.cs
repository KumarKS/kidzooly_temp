﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MBSingleton<T> : MonoBehaviour
    where T : MonoBehaviour
{
    static string s = typeof(T).ToString();
    public static T instance { get { if(_instance == null) _instance = (new GameObject(typeof(T).ToString())).AddComponent<T>(); return _instance;}} 
    static T _instance;
    
    protected virtual void Awake() {
        if(_instance == null) {
            _instance = this as T;
            DontDestroyOnLoad(gameObject);
        } else Destroy(this);
    }
}
