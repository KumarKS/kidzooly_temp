﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class AppConstants
{

	//ServerVar
	public static int SECONDSINADAY = 86400;
	public static int UPDATEAFTEREVERY_DAYS = 1;


	//Scene Names
	public static string MAINSCENE = "MainScene";
	public static string INTROSCENE = "IntroScene";

	public const int BOOL_TRUE = 1;
	public const int BOOL_FALSE = 0;

	//Player Prefs LocalID
	public static string RATEUSCOUNTER = "RateUsCounter_";
	public static string FAILEDPURCHASEITEMID = "FailedPurchaseItemID";
	public static string LASTSERVERUPDATEDTIMESTAMP = "LastServerUpdatedTimeStamp_";
	public static string INSTALLTIMESTAMP = "InstallTimeStamp_";
	public static string SESSIONCOUNTER = "SessionCounter";
	public static string DAYSSINCEAPPINSTALLED = "DaysSinceInstalled";
	public static string PURCHASEATTEMPTS = "PurchaseAttempts";
	public static string PURCHASEFAILURES = "PurchaseFailures";
	public static string UNLOCKEDSURPRISEOFFERTIMESTAMP = "UnlockedSurpriseOfferTimeStamp_";

	public static string NO_ADS = "no_ads";

	public static string HAS_RATED_APP = "hasratedapp";
	public static int GIFT_DAYS_COUNT = 3;
	public static int GIFT_NOTIFICATION_ID = 3;
	public static int OFFER_NOTIFICATION_ID = 100;
	public static int GENERIC_NOTIFICATION_ID = 200;
    public static int adsShowAfterCount = 1;
	public static int InterestialVideoCount = 2;
	public static int InterestialActivityCount = 5;

    public static string PERMISSION = "Hi,Please grant us permission to store the video file on the mobile storage.Select \"Yes\" in the next dialog.Selecting \"No\" would not allow us to play rhymes video on your device.Thanks.";

    public static float promo_version
    {
        get => PlayerPrefs.GetFloat("promo_version", 1.0f);
        set => PlayerPrefs.SetFloat("promo_version", value);
    }


    //Mail
    public static string MAILADDRESS = "support@vgminds.com";
	public static string MAILCONTENT = "Hi! %0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A Regards";
	public static string MAILPREFIX = "mailto:";


	public const string GOOGLE_MARKET_PREURL = "market://details?id=";

	public const string AMAZON_MARKET_PREURL = "http://www.amazon.com/gp/mas/dl/android?p=";

	public const string IOS_MARKET_PREURL = "https://itunes.apple.com/app/id";

    //Kidoz
    public static string KIDOZ_DEVELOPER_ID = "11818";
    public static string KIDZO_KEY = "2rfEFeLGJ9X6XrcmRiT5UNqWY1rPWydK";


#if UNITY_IOS
	public const string ONESIGNAL_ID				= "524c731c-c0d7-49ee-8ab8-a0a4f1ec9465";
	public const string APPODEAL_ID					= "257e9f85727de1696dac13adf5e15e1b60bcb761fd2b6aa8";
	public const string APPID 						= "1204186782";
	public const int BUNDLEVERSION					= 1;
	public const MarketInfo TARGET_MARKET 			= MarketInfo.IOS;


#else

    public const string ONESIGNAL_ID = "dd2e9f38-1154-4ba8-a123-5bb132aa5b22";
    public const string APPODEAL_ID = "b0b720033dcacdddeee70c5657df4aae1d5f55451ed49493";
    public const string APPID = "com.videogyan.kids_baby_ronnie";
    //public const string APPID = "com.videogyan.fingerfamily_preschool_kids_learning";
    public const MarketInfo TARGET_MARKET = MarketInfo.Google;
	#endif

//------------ Firebase Cloud Messaging
	public static string UPDATEAPPKEYWORD = "update";
	public const string PROMOKEYWORD = "promo";
	public const int BUNDLE_VERSION_CODE = 181;
	public const string VERSION = "1.81";
	public const int INTERSTITIAL_AD_DELAY = 2000;

	public const float PROMO_DELAY = 5f;

	//adincube
	public const string AdinCube_ID = "1cf7094557c140bba726";

	//SA
	public const int SA_MOB_BANNER_ID = 36612;
	public const int SA_MOB_INTERSTITAL_ID = 36611;
	public const int SA_TAB_BANNER_ID = 36613;
	public const int SA_TAB_INTERSTITAL_ID = 36610;
	public const int SA_VIDEO_ID = 36614;

}


public enum MarketInfo
{
	Google,
	Amazon,
	IOS
}