﻿using UnityEngine;

public static class LogToGUI
{
    public static void Log(object newLog)
    {
#if DEV_MODE || TEST_AD || UNITY_EDITOR || DEBUG_LOG
        Debug.Log(newLog);
#endif
    }

    public static void Log(object newLog, object obj)
    {
#if DEV_MODE || TEST_AD || UNITY_EDITOR || DEBUG_LOG
        Debug.Log(newLog + " : Object : " + obj);
#endif
    }

    public static void LogError(object newLog, object obj = null)
    {
#if DEV_MODE || TEST_AD || UNITY_EDITOR || DEBUG_LOG
        Debug.LogError(newLog + " : Object : " + obj);
#endif
    }

    public static void LogWarning(object newLog, object obj = null)
    {
#if DEV_MODE || TEST_AD || UNITY_EDITOR || DEBUG_LOG
        Debug.LogWarning(newLog + " : Object : " + obj);
#endif
    }

}