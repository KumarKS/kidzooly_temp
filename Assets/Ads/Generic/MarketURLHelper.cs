﻿using UnityEngine;
using System.Collections;

public static class MarketURLHelper 
{
	//public MarketInfo marketInfo;

	private const string GOOGLE_MARKET_PREURL = "market://details?id=";

	private const string AMAZON_MARKET_PREURL = "http://www.amazon.com/gp/mas/dl/android?p=";

	

	public static void OpenMarketUrl (string marketUrl, PromotionType type = PromotionType.None)
	{
		VGMinds.Analytics.AnalyticsManager.LogEvent("GoingToMarket" + type.ToString (), AppConstants.TARGET_MARKET.ToString (), marketUrl);
	
		if (AppConstants.TARGET_MARKET == MarketInfo.Amazon)
		{
			Application.OpenURL (AMAZON_MARKET_PREURL + marketUrl);
		}
		else if (AppConstants.TARGET_MARKET == MarketInfo.Google)
		{
			Application.OpenURL (GOOGLE_MARKET_PREURL + marketUrl);
		}
	}
}

public enum PromotionType
{
	MoreApps,
	Promo,
	None
}


