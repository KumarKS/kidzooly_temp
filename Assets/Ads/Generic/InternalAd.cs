﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class InternalAd : MonoBehaviour
{
    //public vars
    public Image whiteImage;
    public static InternalAd Instance;
    public PromoInfo[] promos;
    public Action onClose;

    //private vars
    private int currentElement;


    public void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);
        vgm.ads.AdsCheckManager.Instance.CloseBanner();
    }

    public void ShowAd()
    {
        currentElement = UnityEngine.Random.Range(0, promos.Length);
        Debug.Log("currentElement" + currentElement);
        whiteImage.sprite = promos[currentElement].images[0];
        Debug.Log("whiteImage.sprite" + whiteImage.sprite);
        Debug.Log("promos[currentElement].images[0];" + promos[currentElement].images[0]);
        //Open();
        //BackGroundMusic.Instance.PauseBackGroundMusic();
    }

    public void OnClose()
    {
        Destroy(this.gameObject);
        onClose?.Invoke();


    }

    public void OnClicked()
    {
        MarketURLHelper.OpenMarketUrl(promos[currentElement].marketId, PromotionType.Promo);
    }
}
[System.Serializable]
public class PromoInfo
{
    public string marketId;
    public Sprite[] images;
}


