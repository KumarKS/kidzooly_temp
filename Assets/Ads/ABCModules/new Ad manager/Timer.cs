﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using vgm.remote;
using UnityEditor;

public class Timer
{
    private Stopwatch stopWatch;
    private readonly float bufferPeriod = RemoteService.AD_BUFFER_PERIOD;

    public void StartTimer()
    {
        stopWatch = new Stopwatch();
        stopWatch.Start();
    }

    public bool IsTimerDone()
    {
        if (stopWatch == null || !RemoteService.ENABLE_AD_BUFFER)
        {
            return true;
        }

        UnityEngine.Debug.LogError("timer stopwatch" + stopWatch.Elapsed.Seconds);
        UnityEngine.Debug.LogError("timer bufferperiod" + bufferPeriod);
        if (stopWatch.Elapsed.TotalSeconds >= bufferPeriod)
        {
            UnityEngine.Debug.LogError("timer inside stopwatch" + stopWatch.Elapsed.Seconds);
            UnityEngine.Debug.LogError("timer inside buffer" + bufferPeriod);

            stopWatch.Stop();
            return true;
        }
        return false;
    }

}
