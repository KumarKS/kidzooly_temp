﻿
using System;
using System.Collections;
using UnityEngine;
using vgm.coroutines;

namespace vgm
{
    namespace netStatus
    {
        public enum NETSTATUS
        {
            AVAILABLE,
            NOT_AVAILABLE
        }

        public interface INetStatusService 
        {
             Action<NETSTATUS> NetStatusChange { get; set; }
        }

        public static class NetStatusService
        {
            public static event Action<NETSTATUS> NetStatusChange
            {
                add
                {
                    netStatusChange += value;
                    value(Status);
                }
                remove
                {
                    netStatusChange -= value;
                }
            }

            static NetStatusService()
            {
                netStatusChecker = NetStatusChecker();
                waitForSeconds = new WaitForSeconds(2f);
                coroutineService = (vgm.coroutines.ICoroutineService)UnityCoroutine.GetGenericInstance();
                coroutineService.StartCoroutine(netStatusChecker);
            }

            public static NETSTATUS Status { get; private set; }

            private static IEnumerator NetStatusChecker()
            {
                while (true)
                {
                    yield return waitForSeconds;
                    var oldStatus = Status;
                    Status = Application.internetReachability != NetworkReachability.NotReachable ? NETSTATUS.AVAILABLE : NETSTATUS.NOT_AVAILABLE;
                    if (oldStatus != Status)
                    {
                        netStatusChange?.Invoke(Status);
                    }
                }
            }

            private static readonly WaitForSeconds waitForSeconds;
            private static readonly IEnumerator netStatusChecker;
            private static readonly vgm.coroutines.ICoroutineService coroutineService;
            private static Action<NETSTATUS> netStatusChange;
        }
    }
}
