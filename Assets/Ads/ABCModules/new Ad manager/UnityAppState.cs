﻿
using System;
using UnityEngine;

namespace vgm
{
    namespace appstate
    {
        public class UnityAppState : MonoBehaviour
        {
            private static UnityAppState genericInsance;

            public static UnityAppState GenericInstance
            {
                get
                {
                    if (genericInsance == null)
                    {
                        genericInsance = new GameObject(GENERIC).AddComponent<UnityAppState>();
                    }
                    return genericInsance;
                }
            }

            private void Awake()
            {
                DontDestroyOnLoad(this.gameObject);
            }

            public void SetCallbacks(Action<bool> onAppPause, Action onAppQuit)
            {
                this.onAppPause += onAppPause;
                this.onAppQuit += onAppQuit;
            }

#if !UNITY_EDITOR
            private void OnApplicationQuit()
            {
                onAppQuit?.Invoke();
            }

            private void OnApplicationPause(bool pause)
            {
                onAppPause?.Invoke(pause);
            }
#else
            private void OnEnable()
            {
                UnityEditor.EditorApplication.pauseStateChanged += EditorPause;
                UnityEditor.EditorApplication.playModeStateChanged += StateChange;
            }

            private void EditorPause(UnityEditor.PauseState obj)
            {
                onAppPause?.Invoke(obj == UnityEditor.PauseState.Paused);
            }

            void StateChange(UnityEditor.PlayModeStateChange obj)
            {
                if (obj == UnityEditor.PlayModeStateChange.ExitingPlayMode)
                {
                    onAppQuit?.Invoke();
                }
            }

            private void OnDisble()
            {
                UnityEditor.EditorApplication.pauseStateChanged -= EditorPause;
                UnityEditor.EditorApplication.playModeStateChanged -= StateChange;
            }
#endif

            private Action<bool> onAppPause;
            private Action onAppQuit;
            private const string GENERIC = "GENERIC";
        }
    }
}