﻿using System.Collections;
using UnityEngine;

namespace vgm
{
    namespace coroutines
    {
        public interface ICoroutineService
        {
            Coroutine StartCoroutine(IEnumerator enumerator);
            void StopCoroutine(IEnumerator enumerator);
            void StopAllCoroutines();
        }

        public interface ICoroutineObjService : ICoroutineService
        {
            void Destroy();
        }

        public class UnityCoroutine : MonoBehaviour, ICoroutineObjService
        {
            private static ICoroutineService genericInsance;

            private static ICoroutineService GenericInsance
            {
                get
                {
                    if (genericInsance == null)
                    {
                        genericInsance = GetInstance(GENERIC);
                    }
                    return genericInsance;
                }
            }

            private const string GENERIC = "GENERIC";

            public static ICoroutineObjService GetInstance(string name = "")
            {
                GameObject obj = new GameObject(name);
                ICoroutineObjService coroutineObjService = obj.AddComponent<UnityCoroutine>();
                DontDestroyOnLoad(obj);
                return coroutineObjService;
            }

            public static ICoroutineService GetGenericInstance()
            {
                return GenericInsance;
            }

            public void Destroy()
            {
                Destroy(gameObject);
            }
        }
    }
}