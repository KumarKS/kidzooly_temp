﻿// using UnityEngine;
// using System.Collections;
// using System;
// using System.IO;

// public static class LocalStorage
// {
// 	public static void UpdateSessionCount()
// 	{
// 		IncreaseCounter(AppConstants.SESSIONCOUNTER);
// 	}

// 	public static int GetSessionCount()
// 	{
// 		return PlayerPrefs.GetInt (AppConstants.SESSIONCOUNTER);
// 	}

// 	public static void UpdateDaysSinceInstalled()
// 	{
//         if (GetSessionCount() == 1)
//         {
//             DateUtils.SaveDateTime(AppConstants.INSTALLTIMESTAMP, DateTime.UtcNow);
//             SetCounter(AppConstants.DAYSSINCEAPPINSTALLED, 0);
//         }
//         else
//         {
//             if (DateUtils.DifferenceInTimeFromNow_SEC(AppConstants.INSTALLTIMESTAMP) == 0)
//             {
//                 DateUtils.SaveDateTime(AppConstants.INSTALLTIMESTAMP, DateTime.UtcNow);
//                 SetCounter(AppConstants.DAYSSINCEAPPINSTALLED, 0);
//             }
//             else
//             {
//                 SetCounter(AppConstants.DAYSSINCEAPPINSTALLED, DateUtils.DifferenceInTimeFromNow_SEC(AppConstants.INSTALLTIMESTAMP) / AppConstants.SECONDSINADAY);
//             }
//         }		
// 	}

// 	public static int GetNextRateUsCount()
// 	{
// 		return PlayerPrefs.GetInt (AppConstants.RATEUSCOUNTER, 5);
// 	}

// 	public static void SetNextRateUsCount(int days)
// 	{
// 		SetCounter (AppConstants.RATEUSCOUNTER, GetSessionCount() + days);
// 	}

// 	public static void UpdateLastServerUpdatedTime()
// 	{
// 		DateUtils.SaveDateTime (AppConstants.LASTSERVERUPDATEDTIMESTAMP, DateTime.UtcNow);
// 	}

// 	public static bool ShouldUpdateToServer ()
// 	{
// 		int timeRemaining = DateUtils.DifferenceInTimeFromNow_SEC(AppConstants.LASTSERVERUPDATEDTIMESTAMP);
// 		//LogToGUI.Log (timeRemaining.ToString());
// 		if (timeRemaining == 0)
// 		{
// 			//LogToGUI.Log ("not set");
// 			DateUtils.SaveDateTime (AppConstants.LASTSERVERUPDATEDTIMESTAMP, DateTime.UtcNow);
// 			return true;
// 		} 
// 		else if(timeRemaining > AppConstants.UPDATEAFTEREVERY_DAYS * AppConstants.SECONDSINADAY)
// 		{
// 			//LogToGUI.Log ("new time");
// 			return true;
// 		}
// 		//LogToGUI.Log ("no other conditions");
// 		return false;
// 	}

// 	public static int GetDaysSinceInstalled()
// 	{
// 		return PlayerPrefs.GetInt (AppConstants.DAYSSINCEAPPINSTALLED);
// 	}

// 	public static void UpdatePurchaseAttempts()
// 	{
// 		IncreaseCounter(AppConstants.PURCHASEATTEMPTS);
// 	}

// 	public static void UpdatePurchaseFailures()
// 	{
// 		IncreaseCounter(AppConstants.PURCHASEFAILURES);
// 	}

// 	public static int GetPurchaseAttempts()
// 	{
// 		return PlayerPrefs.GetInt (AppConstants.PURCHASEATTEMPTS);
// 	}

// 	public static int GetPurchaseFailures()
// 	{
// 		return PlayerPrefs.GetInt (AppConstants.PURCHASEFAILURES);
// 	}

// 	public static string GetPurchasedItems()
// 	{
//         //return SoomlaHandler.Instance.GetAllPurchasedGoods ();
//         return null;
// 	}

// 	private static void IncreaseCounter(string key, int val = 1)
// 	{
// 		if (!string.IsNullOrEmpty (key))
// 		{
// 			int currentVal = PlayerPrefs.GetInt (key);
// 			currentVal += val;
// 			PlayerPrefs.SetInt (key, currentVal);
// 			//LogToGUI.Log (key + " " + PlayerPrefs.GetInt (key));
// 			PlayerPrefs.Save ();
// 		} 
// 		else
// 		{
// 			//LogToGUI.Log ("error");
// 		}
// 	}

// 	private static void SetCounter(string key, int val)
// 	{
// 		if (!string.IsNullOrEmpty (key))
// 		{
// 			PlayerPrefs.SetInt (key, val);
// 			//LogToGUI.Log (key + " " + PlayerPrefs.GetInt (key));
// 			PlayerPrefs.Save ();
// 		} 
// 		else
// 		{
// 			//LogToGUI.Log ("error");
// 		}
// 	}

// 	public static void SetFailedPurchaseItemID(string itemId)
// 	{
// 		//LogToGUI.Log (AppConstants.FAILEDPURCHASEITEMID + ": " + itemId);
// 		PlayerPrefs.SetString (AppConstants.FAILEDPURCHASEITEMID, itemId);
// 	}

// 	public static string GetFailedPurchaseItemID()
// 	{
// 		return PlayerPrefs.GetString (AppConstants.FAILEDPURCHASEITEMID, "");
// 	}

// 	//public static string GetUnlockedCategoryID()
// 	//{
// 	//	return PlayerPrefs.GetString (AppConstants.UNLOCKEDCATEGORYID, "");
// 	//}

// 	//public static void SetUnlockedCategoryID(string categoryName)
// 	//{
// 	//	PlayerPrefs.SetString (AppConstants.UNLOCKEDCATEGORYID, categoryName);
// 	//}

// 	public static float GetUnlockedSurpriseOfferTimeStamp()
// 	{
// 		return (float) DateUtils.GetDateTime(AppConstants.UNLOCKEDSURPRISEOFFERTIMESTAMP).Subtract (DateTime.UtcNow).TotalSeconds;
// 	}

// 	public static void SetUnlockedSurpriseOfferTimeStamp()
// 	{
//         DateTime targetTime = DateTime.UtcNow.AddSeconds(AppConstants.SECONDSINADAY);
//         DateUtils.SaveDateTime (AppConstants.UNLOCKEDSURPRISEOFFERTIMESTAMP, targetTime);
// 	}

//     public static bool IsAdsPurchased()
//     {
// 		return PlayerPrefs.GetInt(AppConstants.NO_ADS, AppConstants.NOTPURCHASEDVAL) == AppConstants.PURCHASEDVAL;
//     }

//     public static void SetAdsPurchased(bool isPurchased)
//     {
// 		PlayerPrefs.SetInt(AppConstants.NO_ADS, isPurchased ? AppConstants.PURCHASEDVAL : AppConstants.NOTPURCHASEDVAL);
//     }

//     public static void DeleteFolder(string path)
//     {
//         DirectoryInfo info = new DirectoryInfo(path);
//         if (info.Exists)
//         {
//             info.Delete(true);
//             LogToGUI.Log("Deleting folder: " + path);
//         }
//         else
//         {
//             LogToGUI.Log("Folder Not Found: " + path);
//         }
//     }
// }

using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;

public static class LocalStorage {
    public static void UpdateSessionCount() {
        IncreaseCounter(AppConstants.SESSIONCOUNTER);

    }

    public static int GetSessionCount() {
        return PlayerPrefs.GetInt(AppConstants.SESSIONCOUNTER);
    }

    public static void UpdateDaysSinceInstalled() {
        if (GetSessionCount() == 1) {
            DateUtils.SaveDateTime(AppConstants.INSTALLTIMESTAMP, DateTime.UtcNow);
            SetCounter(AppConstants.DAYSSINCEAPPINSTALLED, 0);
        } else {
            if (DateUtils.DifferenceInTimeFromNow_SEC(AppConstants.INSTALLTIMESTAMP) == 0) {
                DateUtils.SaveDateTime(AppConstants.INSTALLTIMESTAMP, DateTime.UtcNow);
                SetCounter(AppConstants.DAYSSINCEAPPINSTALLED, 0);
            } else {
                SetCounter(AppConstants.DAYSSINCEAPPINSTALLED, DateUtils.DifferenceInTimeFromNow_SEC(AppConstants.INSTALLTIMESTAMP) / AppConstants.SECONDSINADAY);
            }
        }
    }

    public static float GetUnlockedSurpriseOfferTimeStamp() {
        return (float)DateUtils.GetDateTime(AppConstants.UNLOCKEDSURPRISEOFFERTIMESTAMP).Subtract(DateTime.UtcNow).TotalSeconds;
    }

    public static void SetUnlockedSurpriseOfferTimeStamp() {
        DateTime targetTime = DateTime.UtcNow.AddSeconds(AppConstants.SECONDSINADAY);
        DateUtils.SaveDateTime(AppConstants.UNLOCKEDSURPRISEOFFERTIMESTAMP, targetTime);
    }

    public static bool HasEverRated() {
        return PlayerPrefs.GetInt(AppConstants.HAS_RATED_APP, AppConstants.BOOL_FALSE) == AppConstants.BOOL_TRUE;
    }

    public static bool IsAdsPurchased() {
        return PlayerPrefs.GetInt(AppConstants.NO_ADS, AppConstants.BOOL_FALSE) == AppConstants.BOOL_TRUE;
    }

    public static void SetAdsPurchased(bool isPurchased) {
        PlayerPrefs.SetInt(AppConstants.NO_ADS, isPurchased ? AppConstants.BOOL_TRUE : AppConstants.BOOL_FALSE);
    }

    public static bool IsRateUsApplicable() {
        if (PlayerPrefs.GetInt(AppConstants.HAS_RATED_APP, AppConstants.BOOL_FALSE) == AppConstants.BOOL_TRUE) {
            Debug.LogError("App already rated");
            return false;
        }
        Debug.LogError("App not rated");
        Debug.LogError("session count : " + GetSessionCount() + " next session count : " + PlayerPrefs.GetInt(AppConstants.RATEUSCOUNTER));
        if (GetSessionCount() >= PlayerPrefs.GetInt(AppConstants.RATEUSCOUNTER, 5)) {
            return true;
        } else {
            return false;
        }
    }

    public static void SetNextRateUsCount(int days) {
        SetCounter(AppConstants.RATEUSCOUNTER, GetSessionCount() + days);
    }

    public static void SetIsAppRated(bool isTrue) {
        SetCounter(AppConstants.HAS_RATED_APP, isTrue ? AppConstants.BOOL_TRUE : AppConstants.BOOL_FALSE);
    }

    public static void UpdateLastServerUpdatedTime() {
        DateUtils.SaveDateTime(AppConstants.LASTSERVERUPDATEDTIMESTAMP, DateTime.UtcNow);
    }

    public static bool ShouldUpdateToServer() {
        int timeRemaining = DateUtils.DifferenceInTimeFromNow_SEC(AppConstants.LASTSERVERUPDATEDTIMESTAMP);
        Debug.LogError(timeRemaining.ToString());
        if (timeRemaining == 0) {
            Debug.LogError("not set");
            DateUtils.SaveDateTime(AppConstants.LASTSERVERUPDATEDTIMESTAMP, DateTime.UtcNow);
            return true;
        } else if (timeRemaining > AppConstants.UPDATEAFTEREVERY_DAYS * AppConstants.SECONDSINADAY) {
            Debug.LogError("new time");
            return true;
        }
        Debug.LogError("no other conditions");
        return false;
    }

    public static int GetDaysSinceInstalled() {
        return PlayerPrefs.GetInt(AppConstants.DAYSSINCEAPPINSTALLED);
    }

    public static void UpdatePurchaseAttempts() {
        IncreaseCounter(AppConstants.PURCHASEATTEMPTS);
    }

    public static void UpdatePurchaseFailures() {
        IncreaseCounter(AppConstants.PURCHASEFAILURES);
    }

    public static int GetPurchaseAttempts() {
        return PlayerPrefs.GetInt(AppConstants.PURCHASEATTEMPTS);
    }

    public static int GetPurchaseFailures() {
        return PlayerPrefs.GetInt(AppConstants.PURCHASEFAILURES);
    }

    private static void IncreaseCounter(string key, int val = 1) {
        if (!string.IsNullOrEmpty(key)) {
            int currentVal = PlayerPrefs.GetInt(key);
            currentVal += val;
            PlayerPrefs.SetInt(key, currentVal);
            Debug.LogError("Saving Local Data : " + key + " : " + PlayerPrefs.GetInt(key));
            PlayerPrefs.Save();
        } else {
            Debug.LogError("Local Data Save error");
        }
    }

    private static void SetCounter(string key, int val) {
        if (!string.IsNullOrEmpty(key)) {
            PlayerPrefs.SetInt(key, val);
            Debug.LogError(key + " " + PlayerPrefs.GetInt(key));
            PlayerPrefs.Save();
        } else {
            Debug.LogError("error");
        }
    }

    public static void SetFailedPurchaseItemID(string itemId) {
        Debug.LogError(AppConstants.FAILEDPURCHASEITEMID + ": " + itemId);
        PlayerPrefs.SetString(AppConstants.FAILEDPURCHASEITEMID, itemId);
    }

    public static string GetFailedPurchaseItemID() {
        return PlayerPrefs.GetString(AppConstants.FAILEDPURCHASEITEMID, "");
    }

    public static bool IsItemPurchased(string key) {
        return GetBool(key);
    }

    public static void UpdateItemProperties(string key, bool isPurchased) {
        SetBool(key, isPurchased);
    }

    public static bool GetBool(string key) {
        return PlayerPrefs.GetInt(key, AppConstants.BOOL_FALSE) == AppConstants.BOOL_TRUE;
    }

    public static void SetBool(string key, bool val = false) {
        PlayerPrefs.SetInt(key, val ? AppConstants.BOOL_TRUE : AppConstants.BOOL_FALSE);
    }

    public static void DeleteFolder(string path) {
        DirectoryInfo info = new DirectoryInfo(path);
        if (info.Exists) {
            info.Delete(true);
            Debug.LogError("Deleting folder: " + path);
        } else {
            Debug.LogError("Folder Not Found: " + path);
        }
    }
    public static bool IsFileMoved() {
        //LogToGUI.Log (AppConstants.FAILEDPURCHASEITEMID + ": " + itemId);
        return PlayerPrefs.GetInt(AppConstants.AdinCube_ID, 0) == 0 ? false : true;
    }

    public static void SetFileMoved(bool isTrue) {
        //LogToGUI.Log (AppConstants.FAILEDPURCHASEITEMID + ": " + itemId);
        //PlayerPrefs.SetInt(AppConstants.MOVE_FILES_CHECK, isTrue ? 1 : 0);
    }
    public static void MoveToNewLocation() {
        string basePath = Application.persistentDataPath + Path.DirectorySeparatorChar + "PlayLocalFolder";
        string newPath = "";
        DirectoryInfo directory = new DirectoryInfo(basePath);
        if (directory.Exists) {
            //Debug.Log ("base directory exists");
            DirectoryInfo[] subDirectories = directory.GetDirectories();
            foreach (DirectoryInfo subDirectory in subDirectories) {
                //Debug.Log ("sub directory exists : " + subDirectory.Name);
                FileInfo[] files = subDirectory.GetFiles();
                foreach (FileInfo file in files) {
                    //Debug.Log ("file exists : " + file.Name);
                    if (file.Name == subDirectory.Name + ".mp4") {
                        newPath = basePath + Path.DirectorySeparatorChar + file.Name;
                        FileInfo newFile = new FileInfo(newPath);
                        if (!newFile.Exists) {
                            file.MoveTo(newPath);
                            //Debug.Log ("file found moving to new place");
                        } else {
                            //Debug.Log ("file found in new place");
                        }
                    }
                }
            }
        } else {
            //Debug.Log ("base directort not found : aborting...");
        }
    }


}