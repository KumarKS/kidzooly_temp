﻿using UnityEngine;

namespace vgm
{
    namespace remote
    {
        public static class RemoteService
        {
#if UNITY_ANDROID && !AMAZON_STORE
            //public static string APPODEAL_ADS_KEY {
            //    get => RemoteSettings.GetString(nameof(APPODEAL_ADS_KEY), "1cedec4b433d3cedce7c8794b3a8a7ce751dd9d357d46e73");

            //}
#elif AMAZON_STORE
            //public static string APPODEAL_ADS_KEY
            //{
            //    get => RemoteSettings.GetString(nameof(APPODEAL_ADS_KEY), "98852f389d53b6592e708807590333f09790d992b80b40a4");
            //    set => APPODEAL_ADS_KEY = value;
            //}
#endif
            public static string BANNER_NETWORKS_KEY
            {
                get => RemoteSettings.GetString(nameof(BANNER_NETWORKS_KEY), "superawesome|adex|readadex|admob");
            }
            public static string INTERESTIAL_NETWORKS_KEY
            {
#if UNITY_EDITOR 
                get => "superawesome|adex|readadex|admob|local";
#else
                 get => RemoteSettings.GetString(nameof(INTERESTIAL_NETWORKS_KEY), "superawesome|adex|readadex|admob|local");
#endif
                //set => INTERESTIAL_NETWORKS_KEY = value;

            }
            public static string REWARD_NETWORKS_KEY
            {
                get => RemoteSettings.GetString(nameof(REWARD_NETWORKS_KEY), "superawesome");
            }
            public static string ADMOB_BANNER_ADS_PRIORITY_KEY
            {
                get => RemoteSettings.GetString(nameof(ADMOB_BANNER_ADS_PRIORITY_KEY), "ca-app-pub-5738083548574745/2254397511|ca-app-pub-5738083548574745/8562841870|ca-app-pub-5738083548574745/9301208475");
            }
            public static string ADMOB_INTERSTITIAL_ADS_PRIORITY_KEY
            {
                get => RemoteSettings.GetString(nameof(ADMOB_INTERSTITIAL_ADS_PRIORITY_KEY), "ca-app-pub-5738083548574745/8460147918|ca-app-pub-5738083548574745/9151721589|ca-app-pub-5738083548574745/7564620825");
            }

            public static string ADMOB_ID_KEY
            {
                get => RemoteSettings.GetString(nameof(ADMOB_ID_KEY), "ca-app-pub-5738083548574745~1423172718");
            }

            public static string ADEX_BANNER_ADS_KEY
            {
                get => RemoteSettings.GetString(nameof(ADEX_BANNER_ADS_KEY), "/419163168/com.kidzooly.kids_nursery_rhymes_stories_learning.Banner");
            }

            public static string ADEX_INTERSTITIAL_ADS_KEY
            {
                get => RemoteSettings.GetString(nameof(ADEX_INTERSTITIAL_ADS_KEY), "/419163168/com.kidzooly.kids_nursery_rhymes_stories_learning.Interstitial");
            }
            public static string READADEX_BANNER_ADS_KEY
            {
                get => RemoteSettings.GetString(nameof(READADEX_BANNER_ADS_KEY), "/1009127/VGMinds_ABCSong_Banner");
            }

            public static string READADEX_INTERSTITIAL_ADS_KEY
            {
                get => RemoteSettings.GetString(nameof(READADEX_INTERSTITIAL_ADS_KEY), "/1009127/VGMinds_ABCSong_Interstitial");
            }

            public static string SA_MOB_BANNER_KEY
            {
                get => RemoteSettings.GetString(nameof(SA_MOB_BANNER_KEY), "61549");
            }

            public static string SA_TAB_BANNER_KEY
            {
                get => RemoteSettings.GetString(nameof(SA_TAB_BANNER_KEY), "61550");
            }

            public static string SA_TAB_INTER_KEY
            {
                get => RemoteSettings.GetString(nameof(SA_TAB_INTER_KEY), "61547");
            }
            public static string SA_MOB_INTER_KEY
            {
                get => RemoteSettings.GetString(nameof(SA_MOB_INTER_KEY), "61548");
            }
            public static string SA_VIDEO_ID
            {
                get => RemoteSettings.GetString(nameof(SA_VIDEO_ID), "61551");
            }

            public static bool CanEnableLocking
            {
                get => RemoteSettings.GetBool("enable_video_lock", false);
            }
            public static float AD_BUFFER_PERIOD
            {
                get => RemoteSettings.GetFloat(nameof(AD_BUFFER_PERIOD), 100f);
            }

            public static bool ENABLE_AD_BUFFER
            {
                get => RemoteSettings.GetBool(nameof(ENABLE_AD_BUFFER), true);
            }
            public static float BUFFER_PERIOD
            {
                get => RemoteSettings.GetFloat(nameof(BUFFER_PERIOD), 60f);
            }
            public static bool CHILD_AD_ALLOWED
            {
                get => RemoteSettings.GetBool(nameof(CHILD_AD_ALLOWED), true);
            }

            public static bool CAN_SHOW_ADS
            {
                get => RemoteSettings.GetBool(nameof(CAN_SHOW_ADS), true);
            }

            public static bool ENABLE_ADAPTIVE_BANNER
            {
                get => RemoteSettings.GetBool(nameof(ENABLE_ADAPTIVE_BANNER), true);
            }

        }
    }
}