﻿using System;

namespace vgm
{
    namespace appstate
    {
        public enum APPSTATETYPE
        {
            PAUSE_OR_QUIT,
            RESUME
        }

        public interface IAppStateService
        {
            event Action<APPSTATETYPE> AppStatusChange;
        }


        public static class AppStateService
        {
            public static event Action<APPSTATETYPE> AppStatusChange
            {
                add
                {
                    appStatusChange += value;
                }

                remove
                {
                    appStatusChange -= value;
                }
            }

            static AppStateService()
            {
                UnityAppState.GenericInstance.SetCallbacks(OnAppPause, OnAppQuit);
            }

            private static void OnAppPause(bool obj)
            {
                appStatusChange?.Invoke(obj ? APPSTATETYPE.PAUSE_OR_QUIT : APPSTATETYPE.RESUME);
            }

            private static void OnAppQuit()
            {
                appStatusChange?.Invoke(APPSTATETYPE.PAUSE_OR_QUIT);
            }

            private static readonly UnityAppState unityAppState;
            private static Action<APPSTATETYPE> appStatusChange;
        }
    }
}