﻿using System;
using tv.superawesome.sdk.publisher;
using UnityEngine;
using vgm.remote;

namespace vgm
{
    namespace ads
    {
        namespace SuperawesomeAds
        {
            public static class Manager
            {
                public static string SA_BAN_ID = IsTabletOrNot() ? RemoteService.SA_TAB_BANNER_KEY : RemoteService.SA_MOB_BANNER_KEY;
                public static string SA_INT_ID = IsTabletOrNot() ? RemoteService.SA_TAB_INTER_KEY : RemoteService.SA_MOB_INTER_KEY;
                public static string SA_VIDEO_ID = RemoteService.SA_VIDEO_ID;

                public static void Initialize()
                {
                    AwesomeAds.init(true);
                }

                static bool IsTabletOrNot()
                {
                    float screenWidth = Screen.width / Screen.dpi;
                    float screenHeight = Screen.height / Screen.dpi;
                    float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));
                    if (diagonalInches > 6.5f)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }

            public class BannerAd : IBannerAd
            {
                #region IBannerAd implementation

                private SABannerAd banner;
                private bool isBannerShowing, _isBannerReady;

                public BannerAd()
                {
                    RequestAd();
                }

                public void HideAd()
                {
                    if (banner != null)
                    {
                        isBannerShowing = false;
                        banner.close();
                        GameObject.Destroy(banner.gameObject);
                    }
                    RequestAd();
                }

                public void ShowAd(BannerPos pos)
                {
                    Debug.LogError("###Superawesome Banner showing");
                    if (!isBannerShowing && banner != null && _isBannerReady)
                    {
                        if (pos == BannerPos.BOTTOM)
                        {
                            banner.setPositionBottom();
                        }
                        else
                        {
                            banner.setPositionTop();
                        }
                        Debug.LogError("SA BANner is showing");
                        banner.play();
                        isBannerShowing = true;
                    }
                }

                void RequestAd(BannerPos pos = BannerPos.BOTTOM)
                {
                    if (VGInternet.Instance.Status)
                    {
                        Debug.LogError("SA banner requested");
                        banner = SABannerAd.createInstance();
                        banner.disableParentalGate();
                        banner.load(int.Parse(Manager.SA_BAN_ID));
                        banner.disableTestMode();
                        banner.setCallback((placementid, evt) =>
                        {
                            if (evt == SAEvent.adLoaded)
                            {
                                Debug.LogError("SA banner loaded");
                                _isBannerReady = true;
                            }
                            else if (evt == SAEvent.adFailedToLoad)
                            {
                                Debug.LogError("SA banner failed");
                                _isBannerReady = false;
                                //RequestAd();
                            }
                        });
                    }

                }
                public void SetNextBannerPos(BannerPos pos)
                {
                    RequestAd(pos);
                }
                #endregion


                public bool IsReady()
                {
                    return _isBannerReady;
                }

            }

            public class InterstitialAd : IInterestialAd
            {
                #region IFullScreenAd implementation

                public void RequestAd()
                {

                    if (IsAdReady() || isLoadingInProgress)
                        return;

                    SAInterstitialAd.load(int.Parse(Manager.SA_INT_ID));
                    isLoadingInProgress = true;
                }

                public void ShowAd()
                {
                    if (isReady)
                    {
                        SAInterstitialAd.play(int.Parse(Manager.SA_INT_ID));
                    }

                    VGMinds.Analytics.AnalyticsManager.LogEvent("sa_ad", "showing", this.ToString());

                }

                public bool IsAdReady()
                {
                    return isReady;
                }

                #endregion

                public InterstitialAd(Action onComplete)
                {
                    Debug.LogError("###Sa interstitial");
                    this.OnComplete = onComplete;
                    SAInterstitialAd.setCallback((interstitalPlacementId, evt) =>
                    {
                        if (evt == SAEvent.adClosed)
                        {
                            Interstitial_OnAdClosed();
                        }
                        else if (evt == SAEvent.adLoaded)
                        {
                            isReady = true;
                            Interstitial_OnAdLoaded();
                        }
                        else if (evt == SAEvent.adFailedToLoad)
                        {
                            isReady = false;
                            Interstitial_OnAdFailedToLoad();
                        }
                    });
                    SAInterstitialAd.enableBackButton();
                    SAInterstitialAd.disableParentalGate();
                    SAInterstitialAd.setOrientationLandscape();
                    RequestAd();
                }

                void Interstitial_OnAdClosed()
                {
                    OnComplete?.Invoke();
                    RequestAd();
                }

                void Interstitial_OnAdLoaded()
                {
                    Debug.Log("Superawesome Ad loaded");
                    isLoadingInProgress = false;
                }

                void Interstitial_OnAdFailedToLoad()
                {
                    Debug.Log("Superawesome failed to load");
                    isLoadingInProgress = false;
                    //RequestAd();
                }

                private bool isLoadingInProgress = false;
                private bool isReady = false;
                private readonly Action OnComplete;
            }
        }
    }
}
