﻿
using GoogleMobileAds.Api;
using System;
using UnityEngine;
using vgm.remote;

namespace vgm
{
    namespace ads
    {
        namespace readadex
        {
            public static class Manager
            {
                public static string BANNERID = RemoteService.READADEX_BANNER_ADS_KEY;
                public static string INTERESTIALID = RemoteService.READADEX_INTERSTITIAL_ADS_KEY;

                private const string TEST_ID = "ca-app-pub-3940256099942544~3347511713";

                public static void Initialize()
                {

                }

                static Manager()
                {
                    if (ENABLETESTING)
                        MobileAds.Initialize(TEST_ID);
                }

#if TEST_AD
                private const bool ENABLETESTING = true;
#else
                private const bool ENABLETESTING = false;
#endif
            }

            public class BannerAd : IBannerAd
            {

                public BannerAd()
                {
                    RequestAd();
                }
                #region IBannerAd implementation
                public void HideAd()
                {
                    if (banner != null)
                    {
                        banner.OnAdLoaded -= Banner_OnAdLoaded;
                        banner.Destroy();
                    }
                    RequestAd();
                }

                public void ShowAd(BannerPos pos)
                {
                    Debug.Log("banner  " + banner);
                    if (banner != null)
                    {
                        Debug.LogError("###ReadAdexAds Banner Showing");
                        banner.Show();
                    }
                }

                void RequestAd(BannerPos pos = BannerPos.BOTTOM)
                {
                    Debug.LogError("###ReadAdexAds Banner Requsting");
                    if (banner != null)
                    {
                        banner.OnAdLoaded -= Banner_OnAdLoaded;
                        banner.OnAdFailedToLoad -= Banner_OnAdFailed;
                        banner.Destroy();
                    }

                    AdPosition adPos = pos == BannerPos.BOTTOM ? AdPosition.Bottom : AdPosition.Top;
                   AdSize adaptiveSize = RemoteService.ENABLE_ADAPTIVE_BANNER ? AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth) : AdSize.SmartBanner;

                    banner = new BannerView(Manager.BANNERID, adaptiveSize, adPos);
                    Debug.Log("Readadex banner id" + Manager.BANNERID);
                    banner.LoadAd(new AdRequest.Builder()
        .TagForChildDirectedTreatment(RemoteService.CHILD_AD_ALLOWED).AddExtra("max_ad_content_rating", "G")
        .Build());
                    banner.OnAdLoaded += Banner_OnAdLoaded;
                    banner.OnAdFailedToLoad += Banner_OnAdFailed;
                    banner.Hide();
                }

                private void Banner_OnAdFailed(object sender, AdFailedToLoadEventArgs e)
                {
                    UnityEngine.Debug.Log("ReadAdex banner failed with reason :-" + e.Message);
                    isBannerReady = false;
                }
                #endregion
                

                void Banner_OnAdLoaded(object sender, EventArgs e)
                {
                    Debug.Log("ReadAdex banner loaded" + e.ToString());
                    isBannerReady = true;
                }

                public bool IsReady()
                {
                    return isBannerReady;
                }

                public void SetNextBannerPos(BannerPos pos)
                {
                    RequestAd(pos);
                }

                bool isBannerReady = false;
                BannerView banner = null;
            }

            public class InterestialAd : IInterestialAd
            {
                #region IFullScreenAd implementation

                public void RequestAd()
                {
                    Debug.Log("readex inside request");
                    if (IsAdReady() || isLoadingInProgress)
                    {
                        Debug.Log("readex inside request isready" + IsAdReady());
                        Debug.Log("readex inside request isLoadingInProgress" + isLoadingInProgress);
                        return;
                    }

                    if (interstitial != null)
                    {
                        Debug.Log("readex inside request interstitial is null" + interstitial);
                        interstitial.OnAdClosed -= Interstitial_OnAdClosed;
                        interstitial.OnAdLoaded -= Interstitial_OnAdLoaded;
                        interstitial.OnAdFailedToLoad -= Interstitial_OnAdFailedToLoad;
                        interstitial.Destroy();
                    }

                    interstitial = new InterstitialAd(Manager.INTERESTIALID);
                    interstitial.OnAdClosed += Interstitial_OnAdClosed;
                    interstitial.OnAdLoaded += Interstitial_OnAdLoaded;
                    interstitial.OnAdFailedToLoad += Interstitial_OnAdFailedToLoad;
                    interstitial.LoadAd(new AdRequest.Builder()
        .TagForChildDirectedTreatment(RemoteService.CHILD_AD_ALLOWED).AddExtra("max_ad_content_rating", "G")
        .Build());

                    isLoadingInProgress = true;
                }

                public void ShowAd()
                {
                    Debug.Log("readex show up");
                    interstitial.Show();
                    VGMinds.Analytics.AnalyticsManager.LogEvent("read_ad", "showing", this.ToString());
                }

                public bool IsAdReady()
                {
                    //Debug.Log("readex is ready inter readex " + "inter readex interstitial" + interstitial != null + "readex interstitial.IsLoaded()" + interstitial.IsLoaded());
                    return interstitial != null && interstitial.IsLoaded();
                }

                #endregion

                public InterestialAd(Action onComplete)
                {
                    this.OnComplete = onComplete;
                    RequestAd();
                }

                void Interstitial_OnAdClosed(object sender, EventArgs e)
                {
                    OnComplete?.Invoke();
                    RequestAd();
                }

                void Interstitial_OnAdLoaded(object sender, EventArgs e)
                {
                    Debug.Log("###readex inter Loaded");
                    isLoadingInProgress = false;
                }

                void Interstitial_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
                {
                    Debug.Log("##ReadAdex inter ad failed" + e.Message);
                    isLoadingInProgress = false;
                }

                private bool isLoadingInProgress = false;
                private InterstitialAd interstitial;
                private readonly Action OnComplete;
            }
        }
    }
}