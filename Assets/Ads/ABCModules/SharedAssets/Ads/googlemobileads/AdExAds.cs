﻿
using GoogleMobileAds.Api;
using System;
using UnityEngine;
using vgm.remote;
using vgm.ads;

namespace vgm
{
    namespace ads
    {
        namespace adex
        {
            public static class Manager
            {
                public static string BANNERID = RemoteService.ADEX_BANNER_ADS_KEY;
                public static string INTERESTIALID = RemoteService.ADEX_INTERSTITIAL_ADS_KEY;
                public static string REWARDID = "//not using";

                private const string TEST_ID = "ca-app-pub-3940256099942544~3347511713";

                public static void Initialize()
                {

                }

                static Manager()
                {
                    if (ENABLETESTING)
                        MobileAds.Initialize(TEST_ID);
                }

#if TEST_AD
                private const bool ENABLETESTING = true;
#else
                private const bool ENABLETESTING = false;
#endif
            }

            public class BannerAd : vgm.ads.IBannerAd
            {
                public BannerAd()
                {
                    RequestAd();
                }
                #region IBannerAd implementation
                public void HideAd()
                {
                    if (banner != null)
                    {
                        banner.OnAdLoaded -= Banner_OnAdLoaded;
                        banner.OnAdFailedToLoad -= Banner_OnAdFailed;
                        banner.Destroy();
                    }
                    RequestAd();
                }

                public void ShowAd(BannerPos pos)
                {
                    if (banner != null)
                    {
                        Debug.LogError("###AdexAds Banner Showing");
                        banner.Show();
                    }
                }

                void RequestAd(BannerPos pos = BannerPos.BOTTOM)
                {
                    Debug.LogError("###AdexAds Banner Requsting");
                    if (banner != null)
                    {
                        banner.OnAdLoaded -= Banner_OnAdLoaded;
                        banner.OnAdFailedToLoad -= Banner_OnAdFailed;
                        banner.Destroy();
                    }
                  AdSize adaptiveSize = RemoteService.ENABLE_ADAPTIVE_BANNER ? AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth) : AdSize.SmartBanner;

                    AdPosition adPos = pos == BannerPos.BOTTOM ? AdPosition.Bottom : AdPosition.Top;
                    banner = new BannerView(Manager.BANNERID, adaptiveSize, adPos);
                    Debug.Log("Adexads banner id" + Manager.BANNERID);
                    banner.LoadAd(new AdRequest.Builder()
        .TagForChildDirectedTreatment(RemoteService.CHILD_AD_ALLOWED).AddExtra("max_ad_content_rating", "G")
        .Build());
                    banner.OnAdLoaded += Banner_OnAdLoaded;
                    banner.OnAdFailedToLoad += Banner_OnAdFailed;
                    banner.Hide();
                }
                #endregion

                void Banner_OnAdLoaded(object sender, EventArgs e)
                {
                    Debug.LogError("adex banner is loaded");
                    isBannerReady = true;
                }

                void Banner_OnAdFailed(object sender, AdFailedToLoadEventArgs e)
                {
                    Debug.LogError("adex banner failed" + e.Message);
                    isBannerReady = false;
                }

                public bool IsReady()
                {
                    Debug.Log("adexads is ready" + isBannerReady);
                    return isBannerReady;
                }
                public void SetNextBannerPos(BannerPos pos)
                {
                    RequestAd(pos);
                }
                bool isBannerReady = false;
                BannerView banner = null;
            }

            public class InterestialAd : IInterestialAd
            {
                #region IFullScreenAd implementation

                public void RequestAd()
                {
                    Debug.Log("adex inside request");
                    if (IsAdReady() || isLoadingInProgress)
                    {
                        Debug.Log("adex inside request isready" + IsAdReady());
                        Debug.Log("adex inside request isLoadingInProgress" + isLoadingInProgress);
                        return;
                    }

                    if (interstitial != null)
                    {
                        Debug.Log("adex inside request interstitial is null" + interstitial);
                        interstitial.OnAdClosed -= Interstitial_OnAdClosed;
                        interstitial.OnAdLoaded -= Interstitial_OnAdLoaded;
                        interstitial.OnAdFailedToLoad -= Interstitial_OnAdFailedToLoad;
                        interstitial.Destroy();
                    }

                    interstitial = new InterstitialAd(Manager.INTERESTIALID);
                    interstitial.OnAdClosed += Interstitial_OnAdClosed;
                    interstitial.OnAdLoaded += Interstitial_OnAdLoaded;
                    interstitial.OnAdFailedToLoad += Interstitial_OnAdFailedToLoad;
                    interstitial.LoadAd(new AdRequest.Builder()
        .TagForChildDirectedTreatment(RemoteService.CHILD_AD_ALLOWED).AddExtra("max_ad_content_rating", "G")
        .Build());

                    isLoadingInProgress = true;
                }

                public void ShowAd()
                {
                    Debug.LogError("###AdexAds Showing Interstitial");
                    interstitial.Show();
                    VGMinds.Analytics.AnalyticsManager.LogEvent("adex_ad", "showing", this.ToString());
                }

                public bool IsAdReady()
                {
                    return interstitial != null && interstitial.IsLoaded();
                }

                #endregion

                public InterestialAd(Action onComplete)
                {
                    this.OnComplete = onComplete;
                    RequestAd();
                }

                void Interstitial_OnAdClosed(object sender, EventArgs e)
                {
                    OnComplete?.Invoke();
                    RequestAd();
                }

                void Interstitial_OnAdLoaded(object sender, EventArgs e)
                {
                    Debug.Log("###adex inter Loaded");
                    isLoadingInProgress = false;
                }

                void Interstitial_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
                {
                    Debug.Log("##Adex inter ad failed" + e.Message);
                    isLoadingInProgress = false;
                }

                private bool isLoadingInProgress = false;
                private InterstitialAd interstitial;
                private readonly Action OnComplete;
            }
        }
    }
}