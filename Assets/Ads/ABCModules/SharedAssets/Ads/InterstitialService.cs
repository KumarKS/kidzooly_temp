﻿using System;
using System.Collections;
using UnityEngine;
using vgm.remote;

namespace vgm
{
    namespace ads
    {
        public interface IFullScreenAd
        {
            void RequestAd();
            void ShowAd();
            bool IsAdReady();
        }
        public enum INTERESTIALTYPE
        {
            SHOW,
            HIDE
        }

        public interface IInterestialAd : IFullScreenAd
        {

        }

        public static class InterestialService
        {
            private static readonly IInterestialAd[] interestialAds;
            private static Action _OnComplete;
            private static readonly coroutines.ICoroutineService coroutineService;
            private static Timer timer;
            private static readonly IEnumerator checkAdStatus;
            private static Action<INTERESTIALTYPE> interestialStatusChange;
            public static event Action<INTERESTIALTYPE> InterestialStatusChange
            {
                add
                {
                    interestialStatusChange += value;
                }

                remove
                {
                    interestialStatusChange -= value;
                }
            }

            static InterestialService()
            {
                string s = RemoteService.INTERESTIAL_NETWORKS_KEY;
                for (int i = 0; i < s.Length; i++)
                {

                    Debug.Log("INTERESTIAL_NETWORKS_KEY" + s[i].ToString());
                }
                interestialAds = AdNetworkFactoryService.GetInterestialAds(s, OnComplete);
                coroutineService = coroutines.UnityCoroutine.GetGenericInstance();
                timer = new Timer();
            }

            public static void ShowAdIfItIsReady(Action onComplete = null)
            {
                bool isAdShown = false;
                _OnComplete = onComplete;

                if (!VGInternet.Instance.Status)
                {
                    foreach (IInterestialAd interestialAd in interestialAds)
                    {
                        Debug.Log("interestialAds" + interestialAds.ToString());
                        if (interestialAd is InternalAds.InterstitialAd)
                        {
                            interestialAd.ShowAd();
                            isAdShown = true;
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < interestialAds.Length; i++)
                    {
                        Debug.Log("interestialAds.Length" + interestialAds.Length);
                        Debug.Log("interestialAds[i] is ready" + interestialAds[i]);
                        IInterestialAd interestialAd = interestialAds[i];
                        if (interestialAd.IsAdReady() && timer.IsTimerDone())
                        {
                            interestialAd.ShowAd();
                            timer.StartTimer();
                            isAdShown = true;
                            return;
                        }
                    }
                    _OnComplete?.Invoke();
                }

            }


            private static void OnComplete()
            {
                coroutineService.StartCoroutine(DelayedCall());
            }

            private static void OnInComplete()
            {
                _OnComplete = null;
                CheckAdStatus();
            }

            static IEnumerator DelayedCall()
            {
                yield return new WaitForEndOfFrame();
                if (_OnComplete != null)
                {
                    _OnComplete.Invoke();
                    _OnComplete = null;
                }
                CheckAdStatus();
            }

            private static void CheckAdStatus()
            {
                //coroutineService.StopCoroutine(checkAdStatus);
                //coroutineService.StartCoroutine(checkAdStatus);
            }
        }
    }
}
