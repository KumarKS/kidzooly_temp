﻿using System.Collections;
using UnityEngine;

public interface ICoroutineService
{
    Coroutine StartCoroutine(IEnumerator routine);
    void StopCoroutine(IEnumerator routine);
}

public class CoroutineService : MonoBehaviour, ICoroutineService
{
    private static readonly ICoroutineService _instance;

    static CoroutineService()
    {
        _instance = CreateInstance();
    }

    private static ICoroutineService CreateInstance()
    {
        return new GameObject("CoroutineRunner").AddComponent<CoroutineService>();
    }

    public static ICoroutineService GetGenericInstance()
    {
        return _instance;
    }

    public static ICoroutineService GetNewInstance()
    {
        return CreateInstance();
    }
}
