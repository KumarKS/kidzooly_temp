﻿using UnityEngine;
using vgm.appstate;
using vgm.netStatus;
using vgm.remote;


namespace vgm
{
    namespace ads
    {
        public interface IBannerService
        {
            void SetBannerActive(bool val);
        }

        public enum BannerPos
        {
            TOP,
            BOTTOM
        }

        public interface IBannerAd
        {
            void HideAd();
            void ShowAd(BannerPos pos);
            bool IsReady();
           // void SetNextBannerPos(BannerPos pos);
        }

        public static class BannerService
        {

            static BannerService()
            {
                _bannerAds = AdNetworkFactoryService.GetBannerAds(RemoteService.BANNER_NETWORKS_KEY);
            }

            public static void Init()
            {
            }

            public static void ShowBannerAd(bool val)
            {
                if (val)
                {
                    if (!_adsPurchased)
                    {
                        bannerAdEnabled = true;
                        if (_netAvailable)
                        {
                            ShowNetworkBannerAd(bannerPos);
                        }
                    }
                }
                else
                {
                    bannerAdEnabled = false;
                    HideBannerAd();
                }
            }

            private static void ShowNetworkBannerAd(BannerPos pos)
            {
                for (int i = 0; i < _bannerAds.Length; i++)
                {
                    IBannerAd item = _bannerAds[i];
                    if (item.IsReady())
                    {
                        Debug.LogError("item is ready with banner:" + item);
                        bannerPos = pos;
                        item.ShowAd(bannerPos);
                        return;
                    }
                }
            }

            public static void SetNextBannerPos(BannerPos pos)
            {
            //     for (int i = 0; i < _bannerAds.Length; i++)
            //     {
            //         _bannerAds[i].SetNextBannerPos(pos);
            //     }
            }

            private static void HideBannerAd()
            {
                for (int i = 0; i < _bannerAds.Length; i++)
                {
                    IBannerAd item = _bannerAds[i];
                    item.HideAd();
                }
            }

            private static readonly bool _netAvailable = VGInternet.Instance.Status;
            private static readonly bool _adsPurchased = LocalDataManager.Instance.SaveData.IsPremiumUser();
            private static readonly IBannerAd[] _bannerAds;
            public static bool bannerAdEnabled = false;
            private static BannerPos bannerPos = default;
        }
    }
}
