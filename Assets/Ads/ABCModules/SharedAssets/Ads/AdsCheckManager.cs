﻿using UnityEngine;
using vgm.remote;

namespace vgm
{
    namespace ads
    {
        public enum ActivityType
        {
            ACTIVITY,

            VIDEO
        }
        public class AdsCheckManager : GenericSingleton<AdsCheckManager>
        {
            public void ShowBannerIfAllowed()
            {
                BannerService.ShowBannerAd(CheckConditions());
            }

            public bool ShowInterestialIfAllowed(ActivityType adType)
            {
                switch (adType)
                {
                    case ActivityType.ACTIVITY:

                        if (CheckConditions() && LocalDataManager.Instance.SaveData.ShowInterestialAd >= AppConstants.InterestialActivityCount)
                        {
                            PlayerPrefs.SetInt("AdCount", 0);
                            return true;
                        }
                        break;
                    case ActivityType.VIDEO:
                        if (CheckConditions() && VideoPlayerDataManager.Instance.currentInterstitialCount >= AppConstants.InterestialVideoCount)
                        {
                            VideoPlayerDataManager.Instance.currentInterstitialCount = 0;
                            return true;
                        }
                        break;
                }
                return false;
            }
            public bool CheckConditions()
            {
                return !LocalDataManager.Instance.SaveData.IsPremiumUser() && RemoteService.CAN_SHOW_ADS && LocalDataManager.Instance.SaveData.GetCurrentSession > 1;
            }

            public void CloseBanner()
            {
                if (BannerService.bannerAdEnabled)
                    BannerService.ShowBannerAd(false);
            }
        }
    }
}
