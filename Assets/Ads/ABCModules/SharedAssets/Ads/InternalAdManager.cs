﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InternalAdManager : MBSingleton<InternalAdManager> {
    public PromoJsonDatas promoJsonDatas;
    public PromoData[] promoData;
    public Image panelImage;
    public Action onPanelClosed;
    public GameObject panelObject;

    int currentElement = 0;
    string jsonPath = "InternalAd/PromoData";

    public void LoadData()
    {
        UnityEngine.Debug.LogError("loading internal ad");
        TextAsset tempJsonData = Resources.Load<TextAsset>(jsonPath);
        promoJsonDatas = JsonUtility.FromJson<PromoJsonDatas>(tempJsonData.text);

        UnityEngine.Debug.LogError("loading internal ad"+ tempJsonData.text);
        promoData = new PromoData[promoJsonDatas.promoJsonDatas.Length];
        int promoJsonDataCount = promoJsonDatas.promoJsonDatas.Length;
        for (int i = 0; i < promoJsonDataCount; i++)
        {
            PromoData tempData = new PromoData();
            tempData.marketUrl = promoJsonDatas.promoJsonDatas[i].marketUrlPath;
            tempData.panelSprite = FileUtils.GetAsset<Sprite>(promoJsonDatas.promoJsonDatas[i].panelSpritePath);
            promoData[i] = tempData;
        }
    }

    public void Show()
    {
        panelObject.SetActive(true);

        gameObject.SetActive(true);
        currentElement = UnityEngine.Random.Range(0, promoData.Length);
        panelImage.sprite = promoData[currentElement].panelSprite;
    }
    public void OnClick()
    {
      //  Application.OpenURL(Const.marketPrefix + promoData[currentElement].marketUrl);
        Application.OpenURL(MarketInfo.Google + promoData[currentElement].marketUrl);
    }

    public void OnClose()
    {
        panelObject.SetActive(false);
        onPanelClosed?.Invoke();
    }
}

[System.Serializable]
public class PromoJsonData
{
    public string marketUrlPath;
    public string panelSpritePath;
}

[System.Serializable]
public class PromoJsonDatas
{
    public PromoJsonData[] promoJsonDatas;
}

[System.Serializable]
public class PromoData
{
    public string marketUrl;
    public Sprite panelSprite;
}

