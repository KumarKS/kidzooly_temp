﻿using System;
using UnityEngine;

namespace vgm
{
    namespace ads
    {
        namespace InternalAds
        {
            public class InterstitialAd : IInterestialAd
            {
                #region IFullScreenAd implementation

                // public InternalAd internalAdCanvas;
                public InternalAd internalAdCanvas;

                //string internalAdPath = "InternalAd/InternalAdManager";
                string internalAdPath = "InternalAd/InternalAdManager";              

                public void RequestAd()
                {

                }

                public void ShowAd()
                {
                    UnityEngine.Debug.LogError("show internal ad" + internalAdCanvas);
                    if (internalAdCanvas)
                    {
                        internalAdCanvas.ShowAd();
                        UnityEngine.Debug.LogError("showing  ad : internal");
                        VGMinds.Analytics.AnalyticsManager.LogEvent("internal_ad", "Showing", this.ToString());
                    }
                    else
                    {
                        CreateInternalAd();
                        internalAdCanvas.ShowAd();
                    }

                }

                public bool IsAdReady()
                {
                    return true;
                }

                #endregion

                public InterstitialAd(Action onComplete)
                {
                    UnityEngine.Debug.LogError("creating internal ad");
                    this.OnComplete = onComplete;
                    Debug.LogError("onComplete" + onComplete);

                    CreateInternalAd();
                }

                void CreateInternalAd()
                {
                    //TO DO: 
                    GameObject tempInternalAdCanvas = GameObject.Instantiate(Resources.Load<GameObject>(internalAdPath));
                    Debug.LogError("tempInternalAdCanvas" + tempInternalAdCanvas);
                    internalAdCanvas = tempInternalAdCanvas.GetComponent<InternalAd>();
                    internalAdCanvas.onClose += this.OnComplete;
                    internalAdCanvas.gameObject.SetActive(true);
                }
                private readonly Action OnComplete;
            }
        }
    }
}
