﻿#define ADEX_AD_TAG
#define READADEX_AD_TAG
#define SUPERAWESOME_AD_TAG
#define LOCAL_AD_TAG
#define APPODEAL_AD_TAG
#define ADMOB_AD_TAG

using System;
using System.Collections.Generic;
using UnityEngine;

namespace vgm
{
    namespace ads
    {
        public interface IAdNetworkFactoryService
        {
            IBannerAd[] GetBannerAds(string networks, Action<IBannerAd> onBannerReady);
            IInterestialAd[] GetInterestialAds(string networks, Action OnComplete);
        }

        public static class AdNetworkFactoryService
        {
            enum ADNETWORK
            {
                ADEX,
                READADEX,
                SUPERAWESOME,
                ADMOB,
                LOCAL
            }

            public static IBannerAd[] GetBannerAds(string networks)
            {
                string[] adNetworks;
                if (networks.Contains("|"))
                {
                    Debug.LogError("network" + networks);
                    adNetworks = networks.Split('|');
                    Debug.LogError("Multiple networks ");
                }
                else
                {
                    adNetworks = new string[] { networks };
                    Debug.LogError("single networks ");
                }


                Debug.LogError("networks count" + networks.Length);
                var ads = new List<IBannerAd>();
                foreach (var item in adNetworks)
                {
                    if (networkMapping.ContainsKey(item))
                    {
                        var ad = GetBanner(networkMapping[item]);
                        if (ad != null)
                            ads.Add(ad);
                        else
                            Debug.LogError("Undefined Network Found :" + item);
                    }
                    else
                    {
                        Debug.LogError("Undefined Network Found :" + item);
                    }
                }
                Debug.LogError("networks get ad complete" + ads.ToArray().Length);
                return ads.ToArray();
            }

            private static IBannerAd GetBanner(ADNETWORK type)
            {
                switch (type)
                {
#if ADMOB_AD_TAG
                    case ADNETWORK.ADMOB:
                        return new admob.BannerAd();
#endif
#if SUPERAWESOME_AD_TAG
                    case ADNETWORK.SUPERAWESOME:
                        return new SuperawesomeAds.BannerAd();
#endif
#if ADEX_AD_TAG
                    case ADNETWORK.ADEX:
                        return new adex.BannerAd();
#endif
#if READADEX_AD_TAG
                    case ADNETWORK.READADEX:
                        return new readadex.BannerAd();
#endif
                    default:
                        return null;
                }
            }

            public static IInterestialAd[] GetInterestialAds(string networks, Action OnComplete)
            {
                Debug.Log("Networks" + networks);
                string[] adNetworks = networks.Split('|');
                for (int i = 0; i < adNetworks.Length; i++)
                {
                    UnityEngine.Debug.LogError("####### ALLOWED NETWORKS :-" + adNetworks[i]);
                }
                var ads = new List<IInterestialAd>();
                foreach (var item in adNetworks)
                {
                    if (networkMapping.ContainsKey(item))
                    {
                        Debug.Log("network mapping item " + item);
                        var ad = GetInterestial(networkMapping[item], OnComplete);
                        if (ad != null)
                            ads.Add(ad);
                        else UnityEngine.Debug.LogError("Undefined Network Found 1 :" + item);
                    }
                    else
                    {
                        UnityEngine.Debug.LogError("Undefined Network Found 2 :" + item);
                    }
                }
                return ads.ToArray();
            }

            private static IInterestialAd GetInterestial(ADNETWORK type, Action OnComplete)
            {
                switch (type)
                {

#if ADMOB_AD_TAG
                    case ADNETWORK.ADMOB:
                        return new admob.InterestialAd(OnComplete);
#endif

#if SUPERAWESOME_AD_TAG
                    case ADNETWORK.SUPERAWESOME:
                        return new SuperawesomeAds.InterstitialAd(OnComplete);
#endif
#if ADEX_AD_TAG
                    case ADNETWORK.ADEX:
                        return new adex.InterestialAd(OnComplete);
#endif
#if READADEX_AD_TAG
                    case ADNETWORK.READADEX:
                        return new readadex.InterestialAd(OnComplete);
#endif
#if LOCAL_AD_TAG
                    case ADNETWORK.LOCAL:
                        return new InternalAds.InterstitialAd(OnComplete);
#endif
                    default:
                        return null;
                }
            }

            private static readonly Dictionary<string, ADNETWORK> networkMapping = new Dictionary<string, ADNETWORK>()
            {
                {"adex",        ADNETWORK.ADEX },
                {"readadex",    ADNETWORK.READADEX },
                {"superawesome",    ADNETWORK.SUPERAWESOME },
                {"admob",    ADNETWORK.ADMOB },
                {"local",    ADNETWORK.LOCAL }
            };
        }
    }
}