﻿using System;

namespace vgm
{
    namespace ads
    {
        public static class AdsApplicableService
        {
            public static void SetAdsPurchase(bool status)
            {
                if (adsPurchased != status)
                {
                    adsStatusChange?.Invoke(status);
                }
                adsPurchased = status;
            }

            public static event Action<bool> AdsStatusChange
            {
                add
                {
                    adsStatusChange += value;
                    value(adsPurchased);
                }
                remove
                {
                    adsStatusChange -= value;
                }
            }

            private static bool adsPurchased = false;
            private static Action<bool> adsStatusChange;
        }
    }
}