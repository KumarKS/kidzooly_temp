﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class AdaptingEventSystemDragThreshold : MonoBehaviour 
{
	private int referenceDPI = 100;
	private float referencePixelDrag = 8f;

	IEnumerator Start()
	{
		yield return new WaitForFixedUpdate();
		UpdatePixelDrag(Screen.dpi);
	}

	public void UpdatePixelDrag(float screenDpi)
	{
		if (EventSystem.current == null)
		{
//			Debug.Log("Trying to set pixel drag for adapting to screen dpi, " +
//				"but there is no event system assigned to the script");
			return;
		}
		EventSystem.current.pixelDragThreshold = Mathf.RoundToInt(screenDpi/ referenceDPI*referencePixelDrag);
	}
}
