﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;

public class Tutorial : MonoBehaviour
{
	public GameObject tutorialGO;

    public Image fgImage;

    public Sprite[] bgImages;
    private int cImage, nextImage,  randomImageIndex;

	public static Action OnPageChange;


    public void OnEnable()
	{
		tutorialGO.SetActive (true);
        StartCoroutine (FadeAndChangePage ());
		SceneManager.sceneLoaded += SceneManager_sceneLoaded;

        randomImageIndex = UnityEngine.Random.Range(0, bgImages.Length-1);
        fgImage.CrossFadeAlpha(0f, 0f, false);
	}

	void SceneManager_sceneLoaded (Scene arg0, LoadSceneMode arg1)
	{
		if (arg0.name == EScenes.MainMenu.GetDescription ())
		{
			StopCoroutine (FadeAndChangePage ());
			this.enabled = false;
		}
	}

	public void OnDisable()
	{
		tutorialGO.SetActive (false);
		SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
	}

	public void OnSkipPressed ()
	{
		tutorialGO.SetActive (false);
	}

	public void OnPageChanged ()
	{
		OnPageChange.SafeInvoke ();

        randomImageIndex = (randomImageIndex + 1) % bgImages.Length;
        nextImage = randomImageIndex;
        ImageFade();
        
 
	}


    void ImageFade()
    {
        fgImage.sprite = bgImages[nextImage];
        AlphaFGImgae(1);
    }

    void AlphaFGImgae(int alpha)
    {
        fgImage.CrossFadeAlpha(alpha, 1f, false);
    }

    IEnumerator FadeAndChangePage()
	{
        while(true)
        {
            OnPageChanged();
            yield return new WaitForSeconds(2f);
        }
	}
}
