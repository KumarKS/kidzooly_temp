namespace VGMinds
{
    namespace Analytics
    {
        public static class AnalyticsManager
        {
            private static IAnalytics[] analytics;
            private static int videoPlayedCountSession;

            static AnalyticsManager()
            {
                analytics = new IAnalytics[]
                {
                    new UnityAnalytics(),
                    new FirebaseAnalytics()
                };
                videoPlayedCountSession = 0;
            }

            public static void LogActivityOpen(string ActivityName)
            {
                foreach (var item in analytics)
                {
                    item.LogActivityOpen(ActivityName);
                }
            }

            public static void LogNumberOfVideosPlayedSession()
            {
                foreach (var item in analytics)
                {
                    item.LogNumberOfVideosPlayedSession(videoPlayedCountSession);
                }
            }

            public static void LogVideoPlayed(string VideoURL)
            {
                foreach (var item in analytics)
                {
                    item.LogVideoPlayed(VideoURL);
                }
            }

            public static void LogVideoFavorited(string VideoURL)
            {
                foreach (var item in analytics)
                {
                    item.LogVideoFavorited(VideoURL);
                }
            }

            public static void LogEvent(string a, string b, string c)
            {
                foreach (var item in analytics)
                {
                    item.LogEvent(a, b, c);
                }
            }

            public static void LogVideoCategory(string VideoCategory)
            {
                foreach (var item in analytics)
                {
                    item.LogVideoCategory(VideoCategory);
                }
            }

            public static void LogSubscriptionMade(string product)
            {
                foreach (var item in analytics)
                {
                    item.LogSubscriptionMade(product);
                }
            }

            public static void LogLanguageSelected(string languageSelected)
            {
                foreach (var item in analytics)
                {
                    item.LogLanguageSelected(languageSelected);
                }
            }

            public static void LogLinkClick(string clickURL, string linkType = "")
            {
                foreach (var item in analytics)
                {
                    item.LogLinkClick(clickURL, linkType);
                }
            }

            public static void LogFAQVisit()
            {
                foreach (var item in analytics)
                {
                    item.LogFAQVisit();
                }
            }

            public static void LogStreamSelected()
            {
                foreach (var item in analytics)
                {
                    item.LogStreamSelected();
                }
            }

            public static void LogDownloadError(string url)
            {
                foreach (var item in analytics)
                {
                    item.LogDownloadError(url);
                }
            }

            public static void LogFeedback(string data)
            {
                foreach (var item in analytics)
                {
                    item.LogFeedback(data);
                }
            }

            public static void IncrementVideoPlayed()
            {
                videoPlayedCountSession++;
            }
        }
    }
}