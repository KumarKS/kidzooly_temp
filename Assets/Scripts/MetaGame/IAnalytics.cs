﻿namespace VGMinds
{
	namespace Analytics
	{
		interface IAnalytics
		{
			void LogActivityOpen (string ActivityName);

			void LogNumberOfVideosPlayedSession (int number);

			void LogVideoPlayed (string VideoURL);

			void LogVideoFavorited (string VideoURL);

			void LogVideoCategory (string VideoCategory);

			void LogSubscriptionMade (string product);

			void LogLanguageSelected (string languageSelected);

			void LogLinkClick (string clickURL, string linkType = "");

			void LogFAQVisit ();

			void LogStreamSelected ();

			void LogDownloadError (string url);

			void LogFeedback (string data);

			void LogEvent(string a, string b, string c);
		}
	}
}