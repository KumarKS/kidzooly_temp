﻿namespace VGMinds
{
	namespace CloudMessaging
	{
		public static class CloudMessagingManager
		{
			public static void Init()
			{
#if !AMAZON_STORE
				messanger = new FirebaseCloudMessaging (); 
				//FerrisWheelController.Instance.OnFirebaseInitDone += messanger.Init;
				FirebaseInit.OnFirebaseInitDone += messanger.Init;
#endif
			}

			private static ICloudMessanger messanger;
		}
	}
}
