using UnityEngine;
using System.Threading.Tasks;
using Firebase.Messaging;
using System;

namespace VGMinds
{
	namespace CloudMessaging
	{
#if !AMAZON_STORE
		class FirebaseCloudMessaging : ICloudMessanger
		{            

            string topic = "kz_all";


            private void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
            {
                Debug.Log("Received Registration Token: " + token.Token);
                FirebaseAPI.Instance.fcmToken = token.Token;
            }

            protected bool LogTaskCompletion(Task task, string operation)
            {
                bool complete = false;
                if (task.IsCanceled)
                {
                    Debug.Log(operation + " canceled.");
                }
                else if (task.IsFaulted)
                {
                    Debug.Log(operation + " encounted an error.");
                    foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
                    {
                        string errorCode = "";
                        Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                        if (firebaseEx != null)
                        {
                            errorCode = String.Format("Error.{0}: ",
                              ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
                        }
                        Debug.Log(errorCode + exception.ToString());
                    }
                }
                else if (task.IsCompleted)
                {
                    Debug.Log(operation + " completed");
                    complete = true;
                }
                return complete;
            }
            public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
            {
                Debug.Log("Received a new message from: " + e.Message.From);
            }


            public void Init()
            {
                Debug.Log("Start Checkpost 2");

                
                Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;

                Firebase.Messaging.FirebaseMessaging.SubscribeAsync(topic).ContinueWith(task =>
                {
                    LogTaskCompletion(task, "SubscribeAsync");
                });
            }
        }
#endif
	}
}
