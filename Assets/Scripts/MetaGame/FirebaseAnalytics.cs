using System.Threading.Tasks;

namespace VGMinds
{
    namespace Analytics
    {
        class FirebaseAnalytics : IAnalytics
        {
            private async Task LogEvent(string eventCategory, string eventAction, string value)
            {
                await Task.Run(() =>
                {
#if !AMAZON_STORE

                    string _eventCategory = eventCategory.RemoveSpaceAndSymbol();
                    string _eventAction = eventAction.RemoveSpaceAndSymbol();
                    string _value = value.RemoveSpaceAndSymbol();
                    Firebase.Analytics.FirebaseAnalytics.LogEvent(_eventCategory, _eventAction, _value);
#endif
                });

            }

            public void LogActivityOpen(string ActivityName)
            {
                var _ = LogEvent("Activity", "Opened", ActivityName);
            }

            public void LogNumberOfVideosPlayedSession(int number)
            {
                var _ = LogEvent("Videos", "Number of videos played in session", "" + number);
            }

            public void LogVideoPlayed(string VideoURL)
            {
                var _ = LogEvent("Video", VideoURL, "Played");
            }

            public void LogVideoFavorited(string VideoURL)
            {
                var _ = LogEvent("Video", VideoURL, "Favorited/Liked");
            }

            public void LogVideoCategory(string VideoCategory)
            {
                var _ = LogEvent("Video", VideoCategory, "Category Played");
            }

            public void LogSubscriptionMade(string product)
            {
                var _ = LogEvent("Subscription", product, "Subscribed");
            }

            public void LogLanguageSelected(string languageSelected)
            {
                var _ = LogEvent("Language Selected", languageSelected, languageSelected);
            }

            public void LogLinkClick(string clickURL, string linkType = "")
            {
                var _ = LogEvent("Link Clicked", linkType, clickURL);
            }

            public void LogFAQVisit()
            {
                var _ = LogEvent("Visited FAQ", "Visited FAQ", "1");
            }

            public void LogStreamSelected()
            {
                var _ = LogEvent("Option", "Stream", "Selected");
            }

            public void LogDownloadError(string url)
            {
                var _ = LogEvent("Error", "Download", url);
            }

            public void LogFeedback(string data)
            {
                var _ = LogEvent("Info", "Feedback", data);
            }

            void IAnalytics.LogEvent(string a, string b, string c)
            {
                var _ = LogEvent(a, b, c);
            }
        }
    }
}