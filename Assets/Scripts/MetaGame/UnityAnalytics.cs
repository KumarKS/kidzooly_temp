using System.Collections.Generic;

namespace VGMinds
{
	namespace Analytics
	{
		class UnityAnalytics : IAnalytics
		{
			private void LogEvent (string actionName)
			{
				UnityEngine.Analytics.Analytics.CustomEvent (actionName);
			}

			public void LogActivityOpen (string ActivityName)
			{
				LogEvent (ActivityName);
			}

			public void LogNumberOfVideosPlayedSession (int number)
			{
				LogEvent ("Videos Played in Session : " + number);
			}

			public void LogVideoPlayed (string VideoURL)
			{
				LogEvent (VideoURL + " Played");
			}

			public void LogVideoFavorited (string VideoURL)
			{
				LogEvent (VideoURL + " Favorited/Liked");
			}

			public void LogVideoCategory (string VideoCategory)
			{
				LogEvent (VideoCategory + " Category Played");
			}

			public void LogSubscriptionMade (string product)
			{
				LogEvent ("User subscribed, product : " + product);
			}

			public void LogLanguageSelected (string languageSelected)
			{
				LogEvent ("Language Selected " + languageSelected);
			}

			public void LogLinkClick (string clickURL, string linkType = "")
			{
				LogEvent ("Link Clicked " + clickURL);
			}

			public void LogFAQVisit ()
			{
				LogEvent ("Visited FAQ");
			}

			public void LogStreamSelected ()
			{
				LogEvent ("Selected Stream Option");
			}

			public void LogDownloadError (string url)
			{
				LogEvent ("Download error at URL : " + url);
			}

			public void LogFeedback(string data)
			{
				LogEvent ("Feedback" + data);
			}

            public void LogEvent(string a, string b, string c)
            {
				Dictionary<string, string> dic = new System.Collections.Generic.Dictionary<string, string>();
				dic.Add(b, c);
				UnityEngine.Analytics.Analytics.SendEvent(a, dic);
            }
        }
		
	}
}