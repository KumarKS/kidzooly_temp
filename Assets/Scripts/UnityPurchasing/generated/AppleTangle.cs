#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("xEoAxZyLMN42haJf9jDteYyXjjeyvbK4uq+ytLX7mq6vs7Spsq+i6qSac0MiChG9R/+wygt4YD/A8RjE/ev/3diO39DIxpqrq7e++5i+qa/m/bz7UeixLNZZFAUwePQiiLGAv6L7uqiorra+qPu6uLi+q6+6tbi+UMJSBSKQty7ccPnr2TPD5SOL0gjcN6biWFCI+wjjH2pkQZTRsCTwJ2UvqEA1Cb/UEKKU7wN55SKjJLATVKhaux3AgNL0SWkjn5Mru+NFzi7N68/d2I7f2MjWmqurt777ibS0r6+yvbK4uq+++7mi+7q1ovuruqmvcHiqSZyIjhp09JpoIyA4qxY9eJdq64M3gd/pV7NoVMYFvqgkvIW+ZxLCqS6G1Q6khEAp/thhjlSWhtYqW8/wC7KcT63SJS+wVvWbfSyclqSJvreyurW4vvu0tfuvs7Ko+7i+qau3vvuJtLSv+5ia68XM1uvt6+/pG7jorCzh3PeNMAHU+tUBYajClG71m30snJak04XrxN3Yjsb438Przdbd0vFdk10s1tra3t7b2Fna2tuH68rd2I7f0cjRmqurt777krW49epswGZImf/J8RzUxm2WR4W4E5BbzG7hdi/U1dtJ0Gr6zfWvDufWALnNq7e++5i+qa+yvbK4uq+ytLX7mq7f3cjZjojqyOvK3diO39HI0Zqrq/uYmutZ2vnr1t3S8V2TXSzW2trat777krW49er96//d2I7f0MjGmqvURuYo8JLzwRMlFW5i1QKFxw0Q5vf7uL6pr7K9sri6r777q7S3sriiWdrb3dLxXZNdLLi/3trrWinr8d3/OTAKbKsE1J46/BEqtqM2PG7MzKm6uK+yuL77qK+6r762vrWvqPXr3tvYWdrU2+tZ2tHZWdra2z9KctLtQpf2o2w2V0AHKKxAKa0JrOuUGr/u+M6QzoLGaE8sLUdFFIthGoOL6O2B67nq0OvS3diO393I2Y6I6si8VNNv+ywQd/f7tKtt5NrrV2yYFN3r1N3YjsbI2tok397r2NraJOvGtb/7uLS1v7KvsrS1qPu0vfuuqL77tL37r7O++6+zvrX7uqurt7K4utPw3dre3tzZ2s3Fs6+vq6jh9PSs04XrWdrK3diOxvvfWdrT61na3+uh61narevV3diOxtTa2iTf39jZ2t3YjsbV383fz/ALspxPrdIlL7BWgnze0qfMm43Kxa8IbFD44Jx4DrT061oY3dPw3dre3tzZ2etabcFaaJ6lxJewi02aUh+vudDLWJpc6FFaube++6ivurW/uqm/+6++qbao+7pzB6X57hH+DgLUDbAPef/4yix6d5IDrUToz756rE8S9tnY2tvaeFnarKz1uqurt771uLS29Lqrq7e+uLru6erv6+jtgczW6O7r6evi6erv68ReWF7AQuac7ClyQJtV9w9qS8kDAu2kGlyOAnxCYumZIAMOqkWleolORaHXf5xQgA/N7OgQH9SWFc+yCq+ztKmyr6LqzevP3diO39jI1pqr+7q1v/u4vqmvsr2yuLqvsrS1+6vxXZNdLNba2t7e2+u56tDr0t3YjutZ32DrWdh4e9jZ2tnZ2tnr1t3Si3FRDgE/JwvS3Oxrrq76");
        private static int[] order = new int[] { 15,11,17,10,56,40,16,51,50,55,12,39,24,26,19,32,38,38,57,31,47,55,59,23,56,38,26,56,59,44,34,53,46,53,48,47,50,55,44,43,41,47,53,59,48,52,55,48,53,54,56,56,56,54,58,57,58,58,59,59,60 };
        private static int key = 219;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
