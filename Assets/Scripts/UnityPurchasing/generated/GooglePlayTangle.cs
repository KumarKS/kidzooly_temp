#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("Z9HPnYmFBns/hZikGvH8YHpD0gAygAMgMg8ECyiESoT1DwMDAwcCAdjdf3xQEAlZFgtCJnyr5fG1jmPQlanh7rgNO+fPwHHFAimFfho2mjPdHS4SZ80TgDZAWMY4bpPP6dU6poADDQIygAMIAIADAwK6FjP4YcXofhJpHN/k61Fs7z4XyldafIrHTjn9w11Vax89NA1ma8g2VflW17lXAeR/3sRqB6UWHzHbsM2I0slkZwKtZ86B6RcBeOJg3gpcfeFnz98prdEjXshCC1WVF1Q8twdQ44Ln/XtVTLt8MDorf4yzkVnNkjBdcrJgs3DZSibVL8BtAb9Cav245xHAeEo3wbJpq6C5ICdM3DNj3qYw9LoLDrsHjPAEhyr8kyXMtQABAwID");
        private static int[] order = new int[] { 6,6,12,10,12,6,12,8,9,11,11,12,12,13,14 };
        private static int key = 2;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
