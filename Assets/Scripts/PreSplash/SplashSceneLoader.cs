﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashSceneLoader : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		//We have to set the firstEntry to 0 which we later use in RateUs process
		PlayerPrefs.SetInt ("isFirstEntry", 0);
		//
		Application.targetFrameRate = 60;
		UISceneLoader.Instance.LoadScene (EScenes.Splash.GetDescription (), false);
	}
	
	
}
