﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.IO;
using System.Collections;
using vgm.ads;
public class ScratchingActivityManager : MonoBehaviour
{
    private List<Texture2D> ScratchingImages = new List<Texture2D>();

    public RawImage Buffer1;
    public AspectRatioFitter ScratchCardAspect;
    public UIPanel LockPanel;
    public AudioController AudController;
    public Button ArrowNext;
    public Button ArrowPrev;
    public Button ResetButton;
    public List<GameObject> Arrows = new List<GameObject>();

    private GameData.ScratchingBookCollection Config;

    private static int ImageIndex = 0;
    private bool premiumStatus;

    public ScratchCard scratchCard;
    public EraseProgress eraseProgress;

    const string ScratchSuffix = "_Scratched";
    const float completedThreshold = .6f;
    private bool hasReachedThreshold = false;

    private const float delayTime = 1f;

    private void OnSceneLoad(Scene arg0, LoadSceneMode arg1)
    {

        if (string.Equals(arg0.name, EScenes.ScratchingActivity.GetDescription()))
        {
            DoUIHack();
        }

    }
    void DoUIHack()
    {
        float r = Camera.main.aspect;
        string _r = r.ToString("F2");
        string ratio = _r.Substring(0, 4);
        switch (ratio)
        {
            case "1.33": //4:3                   
            case "1.50": //3:2   
            case "0.67":
                {
                    //2:3
                    //Buffer1.GetComponent<AspectRatioFitter>().aspectRatio = 1.2f;
                    ScratchCardAspect.aspectRatio = 1.2f;
                    break;
                }
            case "0.56":
                {
                    //9:16
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void DisplayCanvas()
    {
        var bf1 = Buffer1.GetComponent<UIPanel>();
        bf1.ActivatePanel();
    }

    void FetchNewImages()
    {
        Config = LocalDataManager.Instance.GetLocalConfig<GameData.ScratchingBookCollection>(EConfigFileName.ScratchingBook);
        UILoader.Instance.StartLoader();
        scratchCard.SetInput(false);
        var urls = Config.ScratchingBookPages.Select(x => x.ScratchingBookUrl).ToList();
        FileManager.Instance.GetLocalImage(urls: urls.ToArray(), onComplete:
            (x) =>
            {
                Debug.Log("All Images Fetched");
                UILoader.Instance.StopLoader();
                scratchCard.SetInput(true);
                ScratchingImages.AddRange(x);
                LoadNewImage(0);
            }, onFail: (error) =>
                {
                    Debug.Log("Fetch Failed");
                    UILoader.Instance.StopLoader();
                    scratchCard.SetInput(true);
                    AdsCheckManager.Instance.CloseBanner();
                    UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());
                    UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDataFetchFailure") ?? "Fetch Failed, Try Again...", 5);
                });
    }


    public void LoadNewImage(int dir)
    {
        PerformanceManager.Instance.IncrementAttempts();
        LocalDataManager.Instance.SaveData.IncrementCurrentActivitySession();
        Arrows.ForEach(x => x.Deactivate());

        UILoader.Instance.StartLoader();
        ImageIndex += dir;
        ImageIndex = Mathf.Clamp(ImageIndex, 0, ScratchingImages.Count - 1);

        if (ImageIndex > 0 && ImageIndex < ScratchingImages.Count - 1)
        {
            ArrowPrev.interactable = true;
            ArrowNext.interactable = true;
            ResetButton.interactable = true;
        }
        else if (ImageIndex == 0)
        {
            ArrowPrev.interactable = false;
            ArrowNext.interactable = true;
        }
        else if (ImageIndex == ScratchingImages.Count - 1)
        {
            ArrowPrev.interactable = true;
            ArrowNext.interactable = false;
        }

        Buffer1.texture = ScratchingImages[ImageIndex] as Texture2D;
        if (vgm.ads.AdsCheckManager.Instance.ShowInterestialIfAllowed(vgm.ads.ActivityType.ACTIVITY))
        {
            Debug.Log("Not a premium user so show ad");
            vgm.ads.InterestialService.ShowAdIfItIsReady(() => DisplayCanvas());
        }
        else
            DisplayCanvas();

        Reset();

        bool showLock = ShowLock();
        bool isAlreadyScratched = PlayerPrefs.GetInt(Buffer1.texture + ScratchSuffix, 0) == 1;
        if (showLock)
        {
            scratchCard.SetImage(true);
            eraseProgress.Reset(true);
        }
        else if (isAlreadyScratched)
        {
            scratchCard.SetImage(false);
            eraseProgress.Reset(true);
        }
        else
        {
            scratchCard.SetImage(true);
            eraseProgress.Reset(false);
        }
        scratchCard.SetInput(false);
        DOTween.Sequence().AppendInterval(0.5f).AppendCallback(() =>
        {
            UILoader.Instance.StopLoader();
            Arrows.ForEach(x => x.Activate());

            if (showLock)
            {
                LockPanel.ActivatePanel();
                scratchCard.SetInput(false);
            }
            else
            {
                LockPanel.DeactivatePanel();
                scratchCard.SetInput(true);
            }
        }).Play();
    }

    private void FixedUpdate()
    {
        if (hasReachedThreshold && scratchCard.EnoughTimePassedSinceLastInput(delayTime))
        {
            OnComplete(1f);
        }
    }

    private void OnComplete(float val)
    {
        PerformanceManager.Instance.IncrementSuccess();
        hasReachedThreshold = false;
        if (Buffer1.texture != null)
            PlayerPrefs.SetInt(Buffer1.texture + ScratchSuffix, 1);
        scratchCard.SetInput(false);
        scratchCard.SetImage(false);
        AdsCheckManager.Instance.CloseBanner();
        AudController.TriggerAudio("CheerUp");
        DOTween.Sequence().AppendInterval(1f).AppendCallback(() =>
        {
            FinishUI.Instance.ShowFinishStars(LoadNextImage);
        }).Play();
    }

    public void OnThresholdReached(float val)
    {
        if (!hasReachedThreshold && val > completedThreshold)
        {
            hasReachedThreshold = true;
            ResetButton.interactable = false;
            ArrowPrev.interactable = false;
            ArrowNext.interactable = false;
        }
    }

    public void LoadNextImage()
    {
        AdsCheckManager.Instance.ShowBannerIfAllowed();
        if (ImageIndex + 1 < ScratchingImages.Count)
        {
            LoadNewImage(1);
        }
        else
        {
            ImageIndex = 0;
            LoadNewImage(0);
        }
    }

    public void Reset(bool clean = false)
    {
        hasReachedThreshold = false;
        scratchCard.Reset();
        if (clean)
        {
            if (Buffer1.texture != null)
                PlayerPrefs.SetInt(Buffer1.texture + ScratchSuffix, 0);
            scratchCard.SetImage(true);
            eraseProgress.Reset();
        }
    }

    public void GotoStore()
    {
        UILoader.Instance.StartLoader();
        AudController.TriggerAudio("OnLockClick", () =>
        {
            UILoader.Instance.StopLoader();
            AdsCheckManager.Instance.CloseBanner();
            FreeTrialsPanel.Instance.Init(null, null, () => { AdsCheckManager.Instance.ShowBannerIfAllowed(); });
        });
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoad;
        Input.multiTouchEnabled = true;
        HardwareInputManager.OnBack += MainMenu;
        eraseProgress.OnProgress += OnThresholdReached;
        eraseProgress.OnCompleted += OnComplete;
       AdsCheckManager.Instance.ShowBannerIfAllowed();
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoad;
        Input.multiTouchEnabled = false;
        FileManager.RemoveReference(this);
        HardwareInputManager.OnBack -= MainMenu;
        eraseProgress.OnProgress -= OnThresholdReached;
        eraseProgress.OnCompleted -= OnComplete;



    }
    void OnDestroy()
    {
        foreach (var item in ScratchingImages)
        {
            PlayerPrefs.DeleteKey((item + ScratchSuffix));
        }
    }

    void Start()
    {
        premiumStatus = LocalDataManager.Instance.SaveData.IsPremiumUser();
        FetchNewImages();
    }

    public void MainMenu()
    {
        AdsCheckManager.Instance.CloseBanner();

        if (UILoader.Instance.IsPanelActive)
        {
            return;
        }
        AudController.TriggerAudio("OnClick");
        Resources.UnloadUnusedAssets();
        AdsCheckManager.Instance.CloseBanner();
        UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());

        double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
        FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
        AnalyticsScript.instance.LogEvent("activity_session_time", "scratching", totalTimeSpent);
    }

    private bool ShowLock()
    {
        var match =
            Config.ScratchingBookPages.Find(x => FileManager.GetLocalFileName(x.ScratchingBookUrl) == ScratchingImages[ImageIndex].name);
        return match != null && !match.IsFree && !premiumStatus;
    }
}