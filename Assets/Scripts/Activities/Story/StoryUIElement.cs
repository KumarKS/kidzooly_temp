﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class StoryUIElement : MonoBehaviour
{
    public UIRemoteImage Thumbnail;
    public Image Loader;
    public Image LoaderBG;
    public Image DownloadOverlay;
    public Image BookCover;
    public Image TickMark;
    public Image LockImage;
    public Image DownloadButton;
    public GameData.StoryData storyData;
    private float pageProgress = 0;
    private float audioProgress = 0;
    private bool isDownloaded;

    void OnDestroy()
    {
        FileManager.RemoveReference(this);
    }

    void Start()
    {
        BookCover.sprite = StoryManager.Instance.storyUI.bookCovers.PickRandom();
    }

    void OnPictureLoaded()
    {
        Thumbnail.OnPictureLoaded -= OnPictureLoaded;
        Thumbnail.DynamicImage.enabled = true;
    }

    public void Init(GameData.StoryData data)
    {
        Thumbnail.DynamicImage.enabled = false;
        Thumbnail.OnPictureLoaded += OnPictureLoaded;
        Thumbnail.SetUrl(data.ThumbnailUrl);
        SetData(data);
    }

    public void SetData(GameData.StoryData data, bool isFailed = false)
    {
        storyData = data;
        if (!storyData.IsFree && !LocalDataManager.Instance.SaveData.IsPremiumUser())
        {
            LockStory();
        }
        else
        {
            gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            UnlockStory();
            if (FileManager.IsFileDownloaded(GetStoryImages()) && FileManager.IsFileDownloaded(storyData.VoiceOverUrl))
            {
                print("Downloaded and deactivated");
                TickMark.gameObject.SetActive(true);
                Loader.gameObject.SetActive(false);
                LoaderBG.gameObject.SetActive(false);
                DownloadButton.gameObject.SetActive(false);
                gameObject.GetComponent<Button>().onClick.AddListener(OnClick);
            }
            else
            {
                print("Downloaded and deactivated");
                TickMark.gameObject.SetActive(false);
                Loader.fillAmount = 0;
                Loader.gameObject.SetActive(false);
                LoaderBG.gameObject.SetActive(false);
                DownloadButton.gameObject.SetActive(true);


                if (!isFailed && (FileManager.IsDownloadInitiated(GetStoryImages().ToArray()) ||
                    FileManager.IsDownloadInitiated(storyData.VoiceOverUrl)))
                {
                    StartCoroutine(DownloadStory());
                }
                else
                {
                    gameObject.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        if (VGInternet.Instance.Status)
                        {

                            if (!StoryManager.Instance.blockDownloads)
                            {
                                StoryManager.Instance.blockDownloads = true;
                                StoryManager.Instance.storyUI.downloadNotificationAudio.Play();
                                StartCoroutine(DownloadStory());
                                gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                            }
                            else
                            {
                                UIToastNotification.Instance.TriggerToast("Another download is in progress, please wait for it to finish", 4f);
                            }
                        }
                        else
                        {
                            UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDownloadInterrupt") ?? "Oops! Download failed, please check your internet connection", 4f);
                            StoryManager.Instance.storyUI.audioController.TriggerAudio("OnNoInternetConnection");
                        }

                    });
                }

            }
        }
    }

    void OnClick()
    {
        LocalDataManager.Instance.SaveData.IncrementCurrentActivitySession();
        ParticleManager.Instance.EmitParticle("click", Input.mousePosition);
        StoryManager.Instance.storyUI.audioController.StopAudio("BackgroundMusic");

        if (vgm.ads.AdsCheckManager.Instance.ShowInterestialIfAllowed(vgm.ads.ActivityType.ACTIVITY))
        {
            Debug.Log("Not a premium user so show ad");
            vgm.ads.InterestialService.ShowAdIfItIsReady(() => StartCoroutine(StartStory()));
        }
        else
            StartCoroutine(StartStory());
    }
    void SetProgress()
    {
        if (Loader)
            Loader.fillAmount = Mathf.Clamp01((audioProgress + pageProgress) / 200);
    }

    IEnumerator DownloadStory()
    {
        DownloadOverlay.DOFade(0.4f, 0.2f);
        DownloadButton.gameObject.SetActive(false);
        Loader.gameObject.SetActive(true);
        LoaderBG.gameObject.SetActive(true);
        var imagesLoaded = false;
        var audioLoaded = false;
        FileManager.Instance.GetImage(urls: GetStoryImages().ToArray(), onComplete: (imgs) =>
        {
            imagesLoaded = true;
        }, owner: this, onFail: (error) =>
        {
            switch (error)
            {
                case EFailureReason.INTERNETCONNECTION:
                    UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDownloadInterrupt") ?? "Oops! Download failed, please check your internet connection", 4f);
                    StoryManager.Instance.storyUI.audioController.TriggerAudio("OnNoInternetConnection");
                    break;
                case EFailureReason.STORAGEFULL:
                    UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnStorageFull") ?? "Storage Full!", 4f);
                    break;
                case EFailureReason.TECHNICAL:
                    UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnTechnicalIssue") ?? "Technical Issue!", 4f);
                    break;
                default:
                    break;
            }
            FileManager.CancelDownload(storyData.VoiceOverUrl);
            foreach (var item in GetStoryImages())
                FileManager.RemoveFile(item);
            FileManager.RemoveFile(storyData.VoiceOverUrl);
            StoryManager.Instance.blockDownloads = false;
            SetData(storyData, true);
        }, onProgress: (y) =>
        {
            pageProgress = y;
            SetProgress();
        }, needDataOnComplete: false);
        FileManager.Instance.GetAudio(urls: storyData.VoiceOverUrl, onComplete: (aud) =>
        {
            audioLoaded = true;
        }, owner: this, onFail: (error) =>
        {
            switch (error)
            {
                case EFailureReason.INTERNETCONNECTION:
                    UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDownloadInterrupt") ?? "Oops! Download failed, please check your internet connection", 4f);
                    StoryManager.Instance.storyUI.audioController.TriggerAudio("OnNoInternetConnection");
                    break;
                case EFailureReason.STORAGEFULL:
                    UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnStorageFull") ?? "Storage Full!", 4f);
                    break;
                case EFailureReason.TECHNICAL:
                    UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnTechnicalIssue") ?? "Technical Issue!", 4f);
                    break;
                default:
                    break;
            }
            FileManager.CancelDownload(GetStoryImages().ToArray());
            foreach (var item in GetStoryImages())
                FileManager.RemoveFile(item);
            FileManager.RemoveFile(storyData.VoiceOverUrl);
            StoryManager.Instance.blockDownloads = false;
            SetData(storyData, true);
        }, onProgress: (y) =>
        {
            audioProgress = y;
            SetProgress();
        }, needDataOnComplete: false);
        yield return new WaitUntil(() => imagesLoaded);
        yield return new WaitUntil(() => audioLoaded);

        UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDownloadSuccess") ?? "Download Completed", 1f);
        SetData(storyData);
        StoryManager.Instance.blockDownloads = false;
    }

    IEnumerator StartStory()
    {
        PerformanceManager.Instance.IncrementAttempts();
        UILoader.Instance.StartLoader();
        var imagesLoaded = false;
        var audioLoaded = false;
        var images = new List<Texture2D>();
        var audio = AudioClip.Create("temp", 1, 1, 1000, false);
        FileManager.Instance.GetImage(urls: GetStoryImages().ToArray(), onComplete: (imgs) =>
        {
            images = imgs;
            imagesLoaded = true;

        }, owner: this);
        FileManager.Instance.GetAudio(urls: storyData.VoiceOverUrl, onComplete: (aud) =>
        {
            audio = aud[0];
            audioLoaded = true;
        }, owner: this);
        yield return new WaitUntil(() => imagesLoaded);
        yield return new WaitUntil(() => audioLoaded);
        StoryManager.Instance.DisplayStoryPage(storyData, images[0].GetSprite(), images[1].GetSprite(), StoryManager.Instance.storyLeftPage, images.Skip(2).Select(x => x.GetSprite()).ToList(), audio);
        StoryManager.Instance.storyUI.ActivatePanel();
        StoryManager.Instance.storyUI.nextPageButton.interactable = true;
        StoryManager.Instance.storyUI.prevPageButton.interactable = true;
        StoryManager.Instance.storyUI.storyBook.interactable = true;
        StoryManager.Instance.OnFlipPage(0, false);
        UILoader.Instance.StopLoader();
    }

    List<string> GetStoryImages()
    {
        List<string> urls = new List<string>();
        urls.Add(storyData.PageBackgroundUrl);
        urls.Add(storyData.BookCoverUrl);
        //		urls.Add (storyData.BookCoverLeftPageUrl);
        urls.AddRange(storyData.PageList.Select(x => x.ImageUrl));
        return urls;
    }

    void UnlockStory()
    {
        LockImage.gameObject.SetActive(false);
        DownloadOverlay.gameObject.SetActive(false);
        TickMark.gameObject.SetActive(true);
    }

    void LockStory()
    {
        LockImage.gameObject.SetActive(true);
        DownloadButton.gameObject.SetActive(false);
        TickMark.gameObject.SetActive(false);
        Loader.gameObject.SetActive(false);
        gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        gameObject.GetComponent<Button>().onClick.AddListener(() =>
        {
            UILoader.Instance.StartLoader();
            StoryManager.Instance.storyUI.audioController.TriggerAudio("OnLockClick", () =>
           {
               UILoader.Instance.StopLoader();
               FreeTrialsPanel.Instance.Init();
           });
        });
    }

}