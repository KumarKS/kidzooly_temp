﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using DG.Tweening;

public class StoryUI : UIPanel
{
	[Header ("Story UI")]
	public UIRemoteImage pageImage;
	public Text pageTextContainer;
	public int currPageNumber;
	public AudioSource storyAudio;
	public List<string> wordList;
	public UIPanel menuContainer;
	public AudioController audioController;
	public ScrollRect mainMenuScrollView;
	public AudioSource downloadNotificationAudio;
	public UIScrollableGrid Grid;

	[Header ("Book Refs")]
	public Book storyBook;
	//public AutoFlip autoFlip;
	public Transform rightHotspot;
	public Transform leftHotspot;
	public Image testImage;

	[HideInInspector]
	public Sprite BookCoverLeftSprite;

	public Transform fakeMoverNext;
	public Transform fakeMoverPrev;
	public AudioClip pageTurnSound;
	public Button audioOption;
	public Image muteIcon;
	public Image muteIconBG;
	public Text pageNumberText;

	[Header ("Library UI Refs")]
	public Button headlightButton;
	public Transform bus;
	public Button busHornOne;
	public Button busHornTwo;
	public AudioClip hornOne;
	public AudioClip HornTwo;

	[HideInInspector]
	public float lastTimeStamp;

	[HideInInspector]
	public Sprite originalBookBackground;

	public List<Sprite> bookCovers;

	private List<float> adjustedTimestamps;
	private List<float> actualTimestamps;
	private float resumeTime = 0f;
	private int currWordlistIndex = 0;
	private float audioResumeTime = 0f;

	[HideInInspector]
	public string blackText;

	[Header ("Story Controls")]
	public Button PreviousButton;
	public Button nextPageButton;
	public Button prevPageButton;
	public Button pauseStoryButton;
	public Button playStoryButton;

	private bool isScrollDownLeft = false;
	private bool isScrollDownRight = false;

	public bool isStoryPaused = false;

	private Sprite pauseSprite;

	private const float maxTimerForPause = 2f;
	private float currentTimerForPause = 0f;

	public CanvasGroup pausePlayCanvasGroup;

	void Start ()
	{
		wordList = new List<string> ();
		//AdsManager.Instance.ShowKidozContentPanel();
		pausePlayCanvasGroup.alpha = 0f;
		pausePlayCanvasGroup.blocksRaycasts = false;
	}

	void Update ()
	{
		if (currentTimerForPause > 0)
		{
			currentTimerForPause -= Time.deltaTime;
			if (currentTimerForPause <= 0)
			{
				HidePauseButton ();
			}
		}

		if (isScrollDownLeft) {
			MoveScrollViewRight ();
		} else if (isScrollDownRight) {
			MoveScrollViewLeft ();
		}
	}

	public void ShowControls ()
	{
		if (currPageNumber >= 0) 
		{
			currentTimerForPause = maxTimerForPause;
			pausePlayCanvasGroup.blocksRaycasts = true;
			pausePlayCanvasGroup.DOKill();
			pausePlayCanvasGroup.DOFade (1f, .5f);
		}
	}

	private void HidePauseButton()
	{
		currentTimerForPause = -1f;
		pausePlayCanvasGroup.blocksRaycasts = false;
		pausePlayCanvasGroup.DOKill();
		pausePlayCanvasGroup.DOFade (0f, .5f);
    }

	public override Tweener ShowAnimation ()
	{
		pageTextContainer.DOFade (0, 0);
		pageTextContainer.text = string.Empty;
		return base.ShowAnimation ();
	}

	public void LoadMainMenu ()
	{
        
		UISceneLoader.Instance.LoadScene (EScenes.MainMenu.GetDescription ());
        Resources.UnloadUnusedAssets();

		double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
		FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
        AnalyticsScript.instance.LogEvent("activity_session_time" , "story", totalTimeSpent);
    }

	public void BackToStories ()
	{
        
		audioController.TriggerAudio ("BackgroundMusic");
		#if UNITY_IOS
		UISceneLoader.Instance.LoadScene(EScenes.Story.GetDescription());
		#else
		DeactivatePanel ();
		menuContainer.ActivatePanel ();
		wordList.Clear ();
		ResetPageSyncData ();
		InvokePauseAudio ();
#endif
        Resources.UnloadUnusedAssets();
        storyBook.Clear();
        testImage.sprite = null;
        System.GC.Collect();
    }

    public override void ActivateControl()
    {
        //gameObject.SetActive(true);

        GetCanvasGroup.interactable = true;
        GetCanvasGroup.blocksRaycasts = true;
        IsPanelActive = true;
        GetCanvasGroup.alpha = 1;
    }

    public override void DeactivateControl()
    {
        //gameObject.SetActive(false);
        GetCanvasGroup.interactable = false;
        GetCanvasGroup.blocksRaycasts = false;
        IsPanelActive = false;
        GetCanvasGroup.alpha = 0;
    }

    override public void DeactivatePanel(bool quick = false)
    {
        CacheUIPanels();
        DeactivateControl();
    }

    public void SyncedHighlighting (AudioClip audio, float timetopause, List<float> timeStamp, bool hasText = true)
	{
		if (storyAudio.clip != null && !storyAudio.mute && hasText) {
			storyAudio.clip = audio;
			storyAudio.time = timeStamp [0];
			storyAudio.PlayDelayed (timetopause);
			adjustedTimestamps = new List<float> ();
			actualTimestamps = new List<float> ();
			actualTimestamps.AddRange (timeStamp);
			for (int i = 0; i < timeStamp.Count; i++) {
				adjustedTimestamps.Add (timeStamp [i] - timeStamp [0] + timetopause);
			}

			if (wordList.Count > adjustedTimestamps.Count) {
				wordList.RemoveRange (adjustedTimestamps.Count, wordList.Count - adjustedTimestamps.Count);
			} else if (adjustedTimestamps.Count > wordList.Count) {
				adjustedTimestamps.RemoveRange (wordList.Count, adjustedTimestamps.Count - wordList.Count);
			}

			for (int i = 0; i < adjustedTimestamps.Count; i++) {
				if (i < adjustedTimestamps.Count - 1) {
					StartCoroutine (HighlightText (wordList [i], adjustedTimestamps [i], adjustedTimestamps [i + 1], i));

				} else {
					StartCoroutine (HighlightText (wordList [i], adjustedTimestamps [i], adjustedTimestamps [i], i, true));
				}
                
			}
			lastTimeStamp = timeStamp [timeStamp.Count - 1];
		} else if (!hasText) {
			InvokePauseAudio ();
		}
	}

	public void ResetPageSyncData ()
	{
		StopAllCoroutines ();
		wordList.Clear ();
	}

	public void PlayAudioAtTime (float t, float delay = 0f)
	{
		InvokePlayAudio (delay, t);
	}

	public void InvokePauseAudio (float time = 0f)
	{
		Invoke ("PauseAudio", time);
	}

	public void InvokePlayAudio (float time = 0f, float seekTime = 0f)
	{
		Invoke ("PlayAudio", time);
		storyAudio.time = seekTime;
	}

	void PauseAudio ()
	{
		storyAudio.Pause ();
	}

	void PlayAudio ()
	{
		storyAudio.Play ();
	}

	IEnumerator HighlightText (string text, float time, float resumeTime, int wordListIndex, bool isLastWordOnPage = false)
	{
		yield return new WaitForSeconds (time);
		this.resumeTime = resumeTime;
		audioResumeTime = actualTimestamps.FirstOrDefault (x => x >= storyAudio.time);

		if (currWordlistIndex < wordList.Count - 1) {
			currWordlistIndex = wordListIndex + 1;
		} else {
			currWordlistIndex = wordListIndex;
		}
        
		var updatedtext = "<color=red>" + text + "</color>";
		wordList [wordListIndex] = updatedtext;
		var pagestory = String.Join (" ", wordList.ToArray ());
		pageTextContainer.text = pagestory;
		if (!storyAudio.isPlaying) {
			PlayAudioAtTime (audioResumeTime);
		}
		if (isLastWordOnPage) {
			InvokePauseAudio (1f);
		}
	}

	public void HeadlightSwitch ()
	{
		if (headlightButton.GetComponent<Image> ().color.a == 1) {
			headlightButton.GetComponent<Image> ().DOFade (0, 0.5f);
		} else {
			headlightButton.GetComponent<Image> ().DOFade (1, 0.5f);
		}
		var audiosource = this.GetComponent<AudioSource> ();
		audiosource.clip = hornOne;
		audiosource.Play ();
	}

	public void BusHorn ()
	{
		busHornOne.interactable = false;
		busHornTwo.interactable = false;
		bus.DOShakeRotation (1f, 15f, 3, 15f, true).OnComplete (() => {
			busHornOne.interactable = true;
			busHornTwo.interactable = true;
		});
		var audiosource = this.GetComponent<AudioSource> ();
		audiosource.clip = HornTwo;
		audiosource.Play ();
	}

	public void SimulatedNextPage ()
	{
		HidePauseButton ();
		ResetPageSyncData ();
		ActivatePauseButton ();
		InvokePauseAudio ();
		isStoryPaused = false;
		if (storyBook.currentPage < storyBook.bookPages.Length) {
			StoryControlsOff ();
			var origpos = fakeMoverNext.localPosition;
			storyBook.forceStopTween = true;
			storyBook.interactable = false;
			storyBook.DragRightPageToPoint (fakeMoverNext.localPosition);
			fakeMoverNext.DOLocalMoveX (-1200, 1f)
                .OnUpdate (() => {
				storyBook.UpdateBookRTLToPoint (fakeMoverNext.localPosition);
			})
                .OnComplete (() => {
				storyBook.forceStopTween = false;
				storyBook.interactable = true;
				storyBook.ReleasePage ();
				fakeMoverNext.localPosition = origpos;
			}).SetEase (Ease.Linear);
		}
	}

	public void SimulatedPrevPage ()
	{
		HidePauseButton ();
		ResetPageSyncData ();
		InvokePauseAudio ();
		ActivatePauseButton ();
		isStoryPaused = false;
		if (storyBook.currentPage <= 2) {
			StoryManager.Instance.ExplicitSetBackground ();
		}
		if (storyBook.currentPage > 0) {
			StoryControlsOff ();
			var origpos = fakeMoverPrev.localPosition;
			storyBook.forceStopTween = true;
			storyBook.interactable = false;
			storyBook.DragLeftPageToPoint (fakeMoverPrev.localPosition);
			fakeMoverPrev.DOLocalMoveX (1200, 1f).OnUpdate (() => {
				storyBook.UpdateBookLTRToPoint (fakeMoverPrev.localPosition);
			}).OnComplete (() => {
				storyBook.forceStopTween = false;
				storyBook.interactable = true;
				storyBook.ReleasePage ();
				fakeMoverPrev.localPosition = origpos;
			}).SetEase (Ease.Linear);
		}
	}

	public void PlayPageTurnSound ()
	{
		var audiosource = GetComponent<AudioSource> ();
		audiosource.clip = pageTurnSound;
		audiosource.Play ();
	}

	public void StoryControlsOn ()
	{
		nextPageButton.interactable = true;
		prevPageButton.interactable = true;
		PreviousButton.interactable = true;
	}

	public void StoryControlsOff ()
	{
		nextPageButton.interactable = false;
		prevPageButton.interactable = false;
		PreviousButton.interactable = false;
	}

	public void StoryControlsToggle (bool active, float delay = 0)
	{
		if (active) {
			Invoke ("StoryControlsOn", delay);
		} else {
			Invoke ("StoryControlsOff", delay);
		}
	}

	public void AudioMute ()
	{
		if (storyAudio.mute) {
			storyAudio.mute = false;
			muteIcon.DOFade (0f, 0.2f);
			muteIconBG.DOColor (Color.green, 0.2f);
			StoryManager.Instance.OnFlipPage (currPageNumber, false);
		} else {
			storyAudio.mute = true;
			muteIcon.DOFade (1f, 0.2f);
			muteIconBG.DOColor (Color.red, 0.2f);
			StopAllCoroutines ();
			pageTextContainer.text = blackText;
		}
	}

	public void ScrollMenuRight (bool value)
	{
		isScrollDownLeft = value;
	}

	public void ScrollMenuLeft (bool value)
	{
		isScrollDownRight = value;
	}

	public void MoveScrollViewRight ()
	{
		var val = mainMenuScrollView.horizontalNormalizedPosition;
		if (val < 1)
			mainMenuScrollView.DOHorizontalNormalizedPos (val + 0.01f, 0.1f).SetEase(Ease.InOutSine)
				.SetAutoKill(true);

	}

	public void MoveScrollViewLeft ()
	{
		var val = mainMenuScrollView.horizontalNormalizedPosition;
		if (val > 0)
			mainMenuScrollView.DOHorizontalNormalizedPos (val - 0.01f, 0.1f).SetEase(Ease.InOutSine)
				.SetAutoKill(true);
		
	}

	private void ActivatePlayButton()
	{
		pauseStoryButton.gameObject.SetActive (false);
		playStoryButton.gameObject.SetActive (true);
	}

	private void ActivatePauseButton()
	{
		pauseStoryButton.gameObject.SetActive (true);
		playStoryButton.gameObject.SetActive (false);
	}

	public void PauseStory ()
	{
		ShowControls ();
		if (!isStoryPaused) {
			StopAllCoroutines ();
			PauseAudio ();
			ActivatePlayButton ();
			isStoryPaused = true;
		} else {
			ActivatePauseButton ();
			var count = adjustedTimestamps.FindIndex (x => x == resumeTime);
			adjustedTimestamps.RemoveRange (0, count);
			List<float> newTimeStamps = new List<float> ();
			for (int i = 0; i < adjustedTimestamps.Count; i++) {
				newTimeStamps.Add (adjustedTimestamps [i] - resumeTime);
			}
			if (!storyAudio.mute) {
				for (int i = 0; i < newTimeStamps.Count; i++) {
					if (i < newTimeStamps.Count - 1) {
						var currWordIndexToPass = currWordlistIndex + i;
						StartCoroutine (HighlightText (wordList [currWordIndexToPass], newTimeStamps [i], adjustedTimestamps [i + 1], currWordIndexToPass));
					} else {
						var currWordIndexToPass = currWordlistIndex + i;
						StartCoroutine (HighlightText (wordList [currWordIndexToPass], newTimeStamps [i], adjustedTimestamps [i], currWordIndexToPass, true));
					}

				}
			}
			isStoryPaused = false;
		}
         
	}
}