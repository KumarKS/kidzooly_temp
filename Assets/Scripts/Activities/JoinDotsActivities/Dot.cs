﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class Dot : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerClickHandler
{
    #region VariableDeclarations

    public enum State
    {
        Idle,
        Current,
        Next,
        Joined
    };

    private State _currentState;
    private string _objectKey;
    private int _dotIndex;
    private PuzzleObject _puzzle;


    public Vector2 OrigPosition;
	public Vector2 DotPos;

    public State CurrentState
    {
        get { return _currentState; }
        set
        {
            _currentState = value;
            DOTween.Kill(Image.transform);
            switch (value)
            {
                case State.Idle:
                    {
                        Image.SetImage(Texture_Idle);

                        //random colors
                        Image.color = EnumeratorExtensions.RandomAmong(Color.green, Color.magenta, Color.cyan, new Color(0.980f, 0.180f, 0.576f), new Color(0.874f, 0.180f, 0.980f), new Color(0.980f, 0.407f, 0.180f));
                        //Key.color = Color.black;


                        Image.transform.DOScale(1f, 0.5f);
                    }
                    break;
                case State.Current:
                    {
                        Image.SetImage(Texture_Next); //client asked to make the change to make the first stars also yello
                        Image.color = Color.yellow;
                        // Key.color = Color.black;

                        Image.transform.DOScale(1f, 0.2f);
                        Image.transform.DOPunchScale(new Vector2(0.3f, 0.3f), 0.5f, 3).SetDelay(0.2f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
                    }
                    break;
                case State.Next:
                    {
                        Image.SetImage(Texture_Next);
                        Image.color = Color.yellow; //new Color(0.8f, 0, 0.45f);
                        //Key.color = Color.black;

                        Image.transform.DOScale(1.5f, 0.5f).SetEase(Ease.OutElastic);
                    }
                    break;

                case State.Joined:
                    {
                        Image.SetImage(Texture_Idle);
                        Image.color = Color.white;
                        //Key.color = Color.black;

                        Image.transform.DOScale(1f, 0.5f);
                    }
                    break;
            }
        }
    }

    public string ObjectKey
    {
        get { return _objectKey; }
        set
        {
            _objectKey = value;
            Key.text = _objectKey;
        }
    }

    public Sprite Texture_Current;
    public Sprite Texture_Next;
    public Sprite Texture_Idle;

    public Image Image;
    public Text Key;

    public bool isLinked = false;
    public bool isLinkable = false;
	public bool isFirst = false;

    #endregion

    void Start()
    {
        Image.alphaHitTestMinimumThreshold = 0.5f;

    }

    public void SetData(State state, PuzzleObject puzzle, string key = "*", int index = 0, Vector2 origPos = default(Vector2))
    {
        CurrentState = state;
        ObjectKey = key;
        _dotIndex = index;
        _puzzle = puzzle;
        OrigPosition = origPos;
    }

    public void SetState(State state, bool linkable = false, bool linked = false)
    {
        CurrentState = state;
        isLinkable = linkable;
        isLinked = linked;
    }

    void Link()
    {
        isLinked = true;
        isLinkable = false;

        CurrentState = State.Current;

        if (_puzzle)
        {
			if(MakingLines.Instance.currLines < _puzzle._dots.Count)
				MakingLines.Instance.currLines++;

			_puzzle.Linking(_dotIndex);     
			DotPos = this.gameObject.transform.position;
			// dot connected
        }

		if (_dotIndex == 0) {
			JoinDotsActivityManager.Instance.LinkPossible = false;
			_puzzle.PuzzleSolved ();

		}
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.dragging)
        {
            //			if (CurrentState == State.Current)
            //				JoinDotsActivityManager.Instance.linkPossible = true;

            if (!JoinDotsActivityManager.Instance.LinkPossible) return;

            if (isLinkable && !isLinked)
            {
                Debug.Log("Touched " + this);
				JoinDotsActivityManager.Instance.onTouch = true;
                Link();

                //todo: play index sound
                JoinDotsActivityManager.Instance.AudioController.TriggerAudio(Key.text);
            }
        }
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("pointer Down");
		if(_dotIndex == 0)
		{
			isFirst = true;
		}

		if (isFirst || isLinked)
		{
			JoinDotsActivityManager.Instance.onDots = true;
			JoinDotsActivityManager.Instance.ClickDots(eventData.pressPosition);
		}

		if (CurrentState == State.Current) {
			JoinDotsActivityManager.Instance.LinkPossible = true;
		}
    }

    public void OnPointerClick(PointerEventData eventData)
    {
		if (isLinkable && !isLinked)
		{
			MakingLines.Instance.OnclickLine ();
			Debug.Log("Clicked " + this);
			JoinDotsActivityManager.Instance.ClickDots(eventData.pressPosition);
			JoinDotsActivityManager.Instance.onTouch = true;
			Link();
		}
    }
}