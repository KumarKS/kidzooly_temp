﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class DotsPuzzleHolder
{
    public GameData.JoinCharacterPuzzle Puzzle;
    public bool Solved;

    public DotsPuzzleHolder(GameData.JoinCharacterPuzzle puzzle)
    {
        Puzzle = puzzle;
        Solved = false;
    }

    public void SetState(bool solved)
    {
        Solved = solved;
    }
}

public class JoinDotsNavigation : MonoBehaviour
{
    public Button NextButton;
    public Button PrevButton;
    public Button BackButton;

	public CanvasGroup Group;

    public ScrollRect PuzzleHolder;
    public GameObject PuzzlePrefab;
    public Image HandImage;
    public Sprite DefaultHandSprite, AltHandSprite;

    private List<DotsPuzzleHolder> _allPuzzles;

    //private List<GameData.JoinCharacterPuzzle> _allPuzzles;
    private int _currentPuzzleIndex = 0;

    public DotsPuzzleHolder CurrentPuzzle;

    #region Initialization

    // Use this for initialization
    void Start()
    {
        if (NextButton)
        {
            NextButton.onClick.RemoveAllListeners();
            NextButton.onClick.AddListener(NextPuzzle);
        }

        if (PrevButton)
        {
            PrevButton.onClick.RemoveAllListeners();
            PrevButton.onClick.AddListener(PrevPuzzle);
        }

        if (BackButton)
        {
            BackButton.onClick.RemoveAllListeners();
            BackButton.onClick.AddListener(PressBack);
        }

        if (HandImage)
        {
            HandImage.DOFade(0, 0);
        }
    }

    #endregion

    #region Data

    public void SetData(List<GameData.JoinCharacterPuzzle> puzzles)
    {
        _allPuzzles = new List<DotsPuzzleHolder>();

        //Differentiating free and premium contents
        var lockedPuzzles = puzzles.FindAll(x => !x.IsFree).Shuffle();
        var freePuzzles = puzzles.FindAll(x => x.IsFree).Shuffle();

        //free stuff always comes first
        freePuzzles.Concat(lockedPuzzles).ForEach(x => { _allPuzzles.Add(new DotsPuzzleHolder(x)); });

        StartCoroutine(PopulatePuzzles(0));
    }

    IEnumerator PopulatePuzzles(int index)
    {
        _currentPuzzleIndex = index;
		UILoader.Instance.StartLoader ();
		JoinDotsActivityManager.Instance.PuzzleNavigation.BlocksRayast (false);
		PuzzleHolder.content.gameObject.DestroyChildren();
		yield return new WaitForSeconds (.5f);
		UILoader.Instance.StopLoader (true);

		JoinDotsActivityManager.Instance.PuzzleNavigation.BlocksRayast (true);
        var puzzleObj = Instantiate(PuzzlePrefab, Vector3.zero, Quaternion.identity);
        if (puzzleObj)
        {
            puzzleObj.transform.SetParent(PuzzleHolder.content, false);
            var p = puzzleObj.GetComponent<PuzzleObject>();
            if (p)
            {
                p.SetData(ref _allPuzzles, index, HandImage, DefaultHandSprite, AltHandSprite);
            }
        }

        CurrentPuzzle = _allPuzzles[_currentPuzzleIndex];
		JoinDotsActivityManager.Instance.onTouch = false;
    }

    #endregion

    #region Action

    //todo:swift transition
    public void NextPuzzle()
    {
        var index = Mathf.Clamp((_currentPuzzleIndex + 1), 0, _allPuzzles.Count);
        if (index > _allPuzzles.Count - 1)
        {
            index = 0;
        }

        StartCoroutine(PopulatePuzzles(index));
    }

    public void PrevPuzzle()
    {
        var index = Mathf.Clamp((_currentPuzzleIndex - 1), -1, _allPuzzles.Count);
        if (index < 0)
        {
            index = _allPuzzles.Count - 1;
        }

        StartCoroutine(PopulatePuzzles(index));
    }

    void OnEnable()
    {
        HardwareInputManager.OnBack += PressBack;
    }

    void OnDisable()
    {
        HardwareInputManager.OnBack -= PressBack;
    }


    void PressBack()
    {
		if (UILoader.Instance.IsPanelActive)
		{
			return;
		}
		UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());  

        double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
        FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
        AnalyticsScript.instance.LogEvent("activity_session_time" , "joindots", totalTimeSpent);
    }

    public void RefreshPuzzle()
    {
        if (!_allPuzzles[_currentPuzzleIndex].Solved)
        {
            PuzzleHolder.content.gameObject.DestroyChildren();
            StartCoroutine(PopulatePuzzles(_currentPuzzleIndex));
        }
    }

	public void BlocksRayast (bool enable)
	{
		Group.alpha = enable ? 1f : 0f;
		Group.blocksRaycasts = enable;
	}

    #endregion
}