﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MakingLines : MonoBehaviour
{
	public static MakingLines Instance;
	public LineRenderer line;

	public GameObject LinesHolder;


	public int currLines = 0;
	public PuzzleObject Puzzle;

	Vector3 mousePos;

	void Awake()
	{
		Instance = this;
	}

	void OnDestroy()
	{
		Destroy (LinesHolder);
	}
		
	void Start () 
	{
		LinesHolder = new GameObject("LinesHolder");
	}


	void Update ()
	{

//		SparkleRect.Instance.IsPointerOverUIObject (Input.mousePosition);
		if (Input.GetMouseButtonDown (0) && JoinDotsActivityManager.Instance.LinkPossible && JoinDotsActivityManager.Instance.onDots)
		{
			MakeLine ();
		}
			
		else if (Input.GetMouseButtonUp (0) && line)
		{
			if(JoinDotsActivityManager.Instance.onTouch )
			{
				JoinDotsActivityManager.Instance.onTouch = false;
				ConnectLine ();
			}
			else
			{
				RemoveLine ();
			}
		}
			
		else if(Input.GetMouseButton(0) && line	)
		{
			if(JoinDotsActivityManager.Instance.onTouch )
			{
				JoinDotsActivityManager.Instance.onTouch = false;
				ConnectLine ();
				MakeLine ();
			}
			DragLine ();
		}
	}

	private void createLine(Vector3 pos)
	{
		line = new GameObject("Line"+currLines).AddComponent<LineRenderer>();
		line.transform.SetParent (LinesHolder.transform);
		line.material =  JoinDotsActivityManager.Instance.lineMaterial;
		line.positionCount = 2;
		line.SetWidth(0.23f,0.23f);
		line.useWorldSpace = true;
		line.numCornerVertices = 40;
		line.numCapVertices = 5;
	}
		
	public void MakeLine()
	{
		mousePos = Input.mousePosition;
		if (line == null && currLines < Puzzle.DotsToConnect.Length)
		{
			createLine (mousePos);
		}

		mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousePos.z = 0;
		if(currLines < Puzzle.DotsToConnect.Length)
			line.SetPosition (0, Puzzle.DotContainer.transform.TransformVector(Puzzle._dots[currLines].OrigPosition));
		if(line != null)
			line.SetPosition (1, mousePos);
	}

	public void ConnectLine()
	{
		mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousePos.z = 0;
		JoinDotsActivityManager.Instance.onDots = false;
		if (currLines < Puzzle.DotsToConnect.Length)
		{
			line.SetPosition (1, Puzzle.DotContainer.transform.TransformVector (Puzzle._dots [currLines].OrigPosition));
		} 
		else 
		{
			line.SetPosition (1, Puzzle.DotContainer.transform.TransformVector (Puzzle._dots [0].OrigPosition));
			this.gameObject.SetActive (false);
		}

		line = null;
	}

	public void RemoveLine()
	{
		Destroy (LinesHolder.transform.GetChild(currLines).gameObject);
	}

	public void DragLine()
	{
		mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousePos.z = 0;
		if(line != null)
			line.SetPosition(1, mousePos);
	} 

	public void OnclickLine()
	{
		MakeLine ();
		line.SetPosition (0, Puzzle.DotContainer.transform.TransformVector(Puzzle._dots[currLines].OrigPosition));
		line.SetPosition (1, Puzzle.DotContainer.transform.TransformVector(Puzzle._dots[currLines].OrigPosition));
	}
}
