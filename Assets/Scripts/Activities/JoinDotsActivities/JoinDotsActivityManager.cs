﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using vgm.ads;

public class JoinDotsActivityManager : GenericSingleton<JoinDotsActivityManager>
{
    public static Action<Vector3> OnClickOnDots;
    public static Action OnMouseUp;
    public static Action OnReleaseCleanUp;


    //Number puzzle
    private GameData.JoinPuzzleCollection Config;

    private List<GameData.JoinCharacterPuzzle> _dotsPuzzleDatas;

    [Header("UI References")]
    public JoinDotsNavigation PuzzleNavigation;

    [Header("logical")]
    public bool LinkPossible = false;
    public bool AllowClickConnection = false;
    public bool RefreshOnMouseUp = false;
    public AudioController AudioController;

    public int nextDotIndex;
    public bool onDots = false;
    public bool onTouch = false;

    public Material lineMaterial;

    // Use this for initialization
    void Start()
    {
        GetPuzzleDetails();
    }

    //getting the number puzzle
    void GetPuzzleDetails()
    {
        Config = LocalDataManager.Instance.GetLocalConfig<GameData.JoinPuzzleCollection>(EConfigFileName.JoinDots);
        _dotsPuzzleDatas = new List<GameData.JoinCharacterPuzzle>(Config.Puzzles);

        //download all required images and then populate puzzle
        UILoader.Instance.StartLoader();
        var urls = _dotsPuzzleDatas.Select(x => x.ImageUrl).ToList();
        FileManager.Instance.GetLocalImage(urls: urls.ToArray(), onComplete: (x) =>
          {
              for (int i = 0; i < x.Count; i++)
              {
                  Debug.Log("Image name" + x[i].name);
              }
              Debug.Log("All Images Fetched" + x);
              UILoader.Instance.StopLoader();
              PopulatePuzzles(); //this can be made to call later if there will be submenu to the puzzles activities
          }, onFail: (error) =>
           {
               Debug.Log("Fetch Failed");
               UILoader.Instance.StopLoader();
               UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());
               UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDataFetchFailure") ?? "Fetch Failed, Try Again...", 5);
           });

    }

    void PopulatePuzzles()
    {
        if (PuzzleNavigation)
        {
            PuzzleNavigation.SetData(_dotsPuzzleDatas);
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0) || Input.touchCount > 1)
        {
            if (RefreshOnMouseUp && LinkPossible)
                PuzzleNavigation.RefreshPuzzle();

            LinkPossible = false;

            if (OnReleaseCleanUp != null)
                OnReleaseCleanUp();

            if (OnMouseUp != null)
                OnMouseUp();
        }
    }

    public void ClickDots(Vector2 pos)
    {
        if (OnClickOnDots != null)
        {
            OnClickOnDots(pos);
        }
    }

    public DotsPuzzleHolder GetCurrentPuzzle()
    {
        return PuzzleNavigation.CurrentPuzzle;
    }
}