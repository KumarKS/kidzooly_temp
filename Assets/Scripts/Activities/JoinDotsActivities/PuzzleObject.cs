﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using vgm.ads;

[RequireComponent(typeof(CanvasGroup))]
public class PuzzleObject : MonoBehaviour
{
    public static Action<Dot> OnDotLinked;

    public GameObject DotPrefab;
    public GameObject DotContainer;
    public GameObject LineContainer;
    public UIRemoteImage RevealImage;
    public CanvasGroup DrawingCanvasGroup;
    public CanvasGroup LockGroup;
    public Vector2[] DotsToConnect;

    private CanvasGroup _canvasGroup;
    private List<DotsPuzzleHolder> _puzzles;
    private int _puzzleIndex;
    public List<Dot> _dots = new List<Dot>();
    private bool _puzzleSolved = false;
    private Sequence _selectedSequence;
    private Sequence _scaleSequence, _rotationSequence, _hybridSequence;
    private bool isLocked = false;

    //private Dot _lastDot;
    private Sequence _rewardSequence;

    private Sequence _handAnim;
    private Image _handImage;
    private Sprite _altHandImage, _defSprite;

    #region Initialization

    void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.DOFade(0, 0);
    }

    // Use this for initialization
    void Start()
    {
        _canvasGroup.DOFade(1, 1);
        RevealImage.DeactivatePanel();

        InitSequences();
    }

    void OnDisable()
    {
        if (_scaleSequence != null && _scaleSequence.IsActive()) _scaleSequence.Kill();
        if (_hybridSequence != null && _hybridSequence.IsActive()) _hybridSequence.Kill();
        if (_rotationSequence != null && _rotationSequence.IsActive()) _rotationSequence.Kill();

        //bug fix: main menu finish ui show
        if (_rewardSequence != null && _rewardSequence.IsActive()) _rewardSequence.Kill();

        HideHandAnimation();
    }

    void InitSequences()
    {
        var revielImageTransform = RevealImage.GetComponent<RectTransform>();

        _scaleSequence = DOTween.Sequence();
        _scaleSequence.id = "scale";
        _scaleSequence
            .Append(revielImageTransform.DOPunchScale(new Vector3(0.15f, 0.2f), 1f, 5).SetEase(Ease.OutCubic))
            .SetAutoKill(true);

        _rotationSequence = DOTween.Sequence();
        _rotationSequence.id = "rotation";
        _rotationSequence
            .Append(revielImageTransform.DOPunchRotation(new Vector3(0, 0, 15), 1f, 4).SetEase(Ease.OutCubic))
            .SetAutoKill(true);

        _hybridSequence = DOTween.Sequence();
        _hybridSequence.id = "hybrid";
        _hybridSequence
            .Join(revielImageTransform.DOPunchScale(new Vector3(0.15f, 0.2f), 1f, 5).SetEase(Ease.OutCubic))
            .Join(revielImageTransform.DOPunchRotation(new Vector3(0, 0, 15), 1f, 4).SetEase(Ease.OutCubic))
            .SetAutoKill(true);
    }

    #endregion

    #region Data

    public void SetData(ref List<DotsPuzzleHolder> puzzles, int index, Image handImage, Sprite defHandSprite, Sprite altHandSprite)
    {
        _puzzles = puzzles;
        _puzzleIndex = index;
        _handImage = handImage;
        _altHandImage = altHandSprite;
        _defSprite = defHandSprite;

        var currentPuzzle = _puzzles[index];

        //when the puzzle is free or the user is premium
        if (currentPuzzle.Puzzle.IsFree || LocalDataManager.Instance.SaveData.IsPremiumUser())
        {
            LockGroup.alpha = 0f;
            LockGroup.interactable = false;
            LockGroup.blocksRaycasts = false;
        }
        else
        {
            LockGroup.alpha = 1f;
            LockGroup.interactable = true;
            LockGroup.blocksRaycasts = true;
            var lockButton = LockGroup.GetComponent<Button>();
            if (lockButton)
            {
                lockButton.onClick.RemoveAllListeners();
                // lockButton.onClick.AddListener(() => ParentalLockSystem.Instance.InitializeLock(() => UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription())));
                lockButton.onClick.AddListener(OnLockedItemClicked);
            }
            isLocked = true;
        }

        _puzzleSolved = currentPuzzle.Solved;
        if (AdsCheckManager.Instance.ShowInterestialIfAllowed(ActivityType.ACTIVITY))
        {
            Debug.Log("Not a premium user so show ad");
            InterestialService.ShowAdIfItIsReady(() => StartCoroutine(CreatePuzzle(_puzzles[index].Puzzle)));
        }
        else
            StartCoroutine(CreatePuzzle(_puzzles[index].Puzzle)); //created the puzzle so that it can be called from other means later, if necessary
    }

    //##BELOW CODE SNIPPED ADDED
    void OnLockedItemClicked()
    {
        UILoader.Instance.StartLoader();
        JoinDotsActivityManager.Instance.AudioController.TriggerAudio("OnLockClick", () =>
            {
                UILoader.Instance.StopLoader();
                FreeTrialsPanel.Instance.Init();
            });
    }
    string audioName;
    IEnumerator CreatePuzzle(GameData.JoinCharacterPuzzle data)
    {
        PerformanceManager.Instance.IncrementAttempts();
        yield return new WaitForEndOfFrame();
        LocalDataManager.Instance.SaveData.IncrementCurrentActivitySession();
        RevealImage.SetUrl(data.ImageUrl, true);
        audioName = data.AudioName;
        if (MakingLines.Instance != null)
        {
            Destroy(MakingLines.Instance.gameObject);
        }
        new GameObject("MakingLines").AddComponent<MakingLines>();

        data.StarPositions.ForEach(x =>
        {
            Vector2 pointInLocalRect = x;
            /* var toScreenPoint = Camera.main.ViewportToScreenPoint(x); //converting the position data that has been stored in viewport format
             RectTransformUtility.ScreenPointToLocalPointInRectangle(_screenRectTransform, toScreenPoint, Camera.main,
                 out pointInLocalRect); //RectTransformUtility.*/

            var dot = Instantiate(DotPrefab);
            MakingLines.Instance.Puzzle = this;
            if (dot)
            {
                dot.transform.SetParent(DotContainer.transform, false);
                dot.transform.SetAsFirstSibling();
                dot.transform.localScale = Vector3.one;
                dot.transform.localRotation = Quaternion.identity;
                dot.transform.localPosition = pointInLocalRect;

                var dObj = dot.GetComponent<Dot>();
                dObj.SetData(Dot.State.Idle, this);
                _dots.Add(dObj);
            }
        });

        Activate(); //todo: later it will move to event when this puzzle become active
    }

    IEnumerator ArrangeDots()
    {
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < _dots.Count; i++)
        {
            var index = (i + _puzzles[_puzzleIndex].Puzzle.StartIndex);
            var key = (index + 1).ToString();

            if (_puzzles[_puzzleIndex].Puzzle.Type == JoinElementPuzzleTypes.AlphabetPuzzle) //if the puzzle is alphabatic
            {
                key = Constant.ALPHABETS[index].ToString();
            }

            if (i > 1)
            {
                _dots[i].SetData(Dot.State.Idle, this, key, i, _puzzles[_puzzleIndex].Puzzle.StarPositions[i]);
            }
            else
            {
                if (i == 0)
                {
                    _dots[i].SetData(Dot.State.Current, this, key, i, _puzzles[_puzzleIndex].Puzzle.StarPositions[i]);
                    JoinDotsActivityManager.Instance.ClickDots(Camera.main.WorldToScreenPoint(DotContainer.transform.TransformVector(_puzzles[_puzzleIndex].Puzzle.StarPositions[i])));
                    DotsToConnect = new Vector2[_dots.Count];
                }
                else if (i == 1)
                {
                    _dots[i].SetData(Dot.State.Next, this, key, i, _puzzles[_puzzleIndex].Puzzle.StarPositions[i]);
                    _dots[i].SetState(Dot.State.Next, true);
                }
            }
            DotsToConnect[i] = _dots[i].OrigPosition;
        }
    }

    #endregion

    #region Actions

    public void Activate()
    {
        StartCoroutine(ArrangeDots());
        if (_puzzleSolved)
        {
            RevealImage.ActivatePanel();
            DrawingCanvasGroup.DOFade(0, 0);
            HideHandAnimation();
        }
        else
        {
            DrawingCanvasGroup.DOFade(1, 0);
            StartCoroutine(ShowHandAnimation());
        }
    }

    public void Linking(int currentIndex)
    {
        if (OnDotLinked != null)
            OnDotLinked(_dots[currentIndex]);


        if (_puzzleSolved) return;

        var previndex = currentIndex - 1;
        var nextIndex = currentIndex + 1;

        if (nextIndex > _dots.Count - 1)
            nextIndex = 0;
        if (previndex < 0)
            previndex = _dots.Count - 1;


        _dots[nextIndex].SetState(Dot.State.Next, true, false); //next dot
        _dots[previndex].SetState(Dot.State.Joined); //prev dot

        HideHandAnimation();

        //client requested to sound based on index
        //JoinDotsActivityManager.Instance.AudController.TriggerAudio("OnJoin");
    }


    public void PuzzleSolved()
    {
        PerformanceManager.Instance.IncrementSuccess();
        JoinDotsActivityManager.Instance.PuzzleNavigation.BlocksRayast(false);
        _selectedSequence = EnumeratorExtensions.RandomAmong(_scaleSequence, _rotationSequence, _hybridSequence).PrependInterval(1);
        Debug.Log(_selectedSequence.id);

        _puzzleSolved = true;
        _puzzles[_puzzleIndex].Solved = true;
        RevealImage.ActivatePanel();
        DrawingCanvasGroup.DOFade(0, 1.5f);
        JoinDotsActivityManager.Instance.AudioController.TriggerAudio(audioName);
        StartCoroutine("PlayCheerUpAudio");
        _selectedSequence.Restart();

        if (MakingLines.Instance != null)
            Destroy(MakingLines.Instance.gameObject);

        _dots.ForEach(x => { x.SetState(Dot.State.Joined); });

        _rewardSequence = DOTween.Sequence();
        _rewardSequence.AppendInterval(2)
            .AppendCallback(() =>
            {
                FinishUI.Instance.ShowFinishStars(() =>
                {
                    JoinDotsActivityManager.Instance.PuzzleNavigation.NextPuzzle();
                });
            })
            .SetAutoKill(true);

        if (isActiveAndEnabled)
            _rewardSequence.Play();
    }

    public IEnumerator PlayCheerUpAudio()
    {
        yield return new WaitForSeconds(1f);
        JoinDotsActivityManager.Instance.AudioController.TriggerAudio("OnComplete");

    }

    public IEnumerator ShowHandAnimation()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(0.5f);
        if (_handImage && !isLocked)
        {
            _handImage.SetImage(_defSprite);

            var startTransform = _dots[0].transform;
            var targetTransform = _dots[1].transform;

            _handAnim = DOTween.Sequence();
            _handAnim.Append(_handImage.transform.DOMove(startTransform.position, 0))
                .Append(_handImage.DOFade(1, 0.3f))
                .AppendCallback(() => _handImage.SetImage(_altHandImage))
                .AppendInterval(0.3f)
                .Append(_handImage.transform.DOMove(targetTransform.position, 1).SetEase(Ease.OutSine))
                .AppendCallback(() => _handImage.SetImage(_defSprite))
                .Append(_handImage.DOFade(0, 0.4f).SetDelay(0.3f))
                .SetLoops(-1);


            _handAnim.Restart();
        }
    }

    public void HideHandAnimation()
    {
        if (_handImage)
        {
            _handImage.DOFade(0, 0);
        }

        if (_handAnim != null && _handAnim.IsActive())
        {
            _handAnim.Kill();
            Debug.Log("kill hand anim");
        }
    }

    #endregion
}