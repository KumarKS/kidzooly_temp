﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MatchItemController : MonoBehaviour
{
    public List<Sprite> MatchItems = new List<Sprite>();
    public List<Image> MatchImages = new List<Image>();
    public List<Image> MatchDropSpots = new List<Image>();
    public Transform ParentTransform;
    public RectTransform OnCompletePosition;
    public AudioController AudController;

    static int CurrPuzzle = 0;
    static int ItemsMatched = 0;

    static List<Vector3> MatchImageOriginalPos = new List<Vector3>();
    static List<Vector3> MatchDropSpotsOriginalPos = new List<Vector3>();

    void OnEnable()
    {
        PuzzleDropScript.OnDropSuccess += (obj) => { StartCoroutine(OnDropSuccess(obj)); };
        PuzzleDragScript.OnDropFailure += OnDropFailure;
        HardwareInputManager.OnBack += OnClickExitButton;
    }


    void OnDisable()
    {
        PuzzleDropScript.OnDropSuccess = null;
        PuzzleDragScript.OnDropFailure = null;
        HardwareInputManager.OnBack -= OnClickExitButton;
    }



    void AssertAndRecordPreviousPositions()
    {
        MatchImages = MatchImages.Take(4).ToList();
        MatchDropSpots = MatchDropSpots.Take(4).ToList();
        MatchImageOriginalPos = MatchImages.Select(x => x.GetComponent<RectTransform>().anchoredPosition3D).ToList();
        MatchDropSpotsOriginalPos = MatchDropSpots.Select(x => x.GetComponent<RectTransform>().anchoredPosition3D).ToList();
    }
    private void OnDropFailure()
    {
        AudController.TriggerAudio("MatchIncorrect");
    }

    private IEnumerator OnDropSuccess(GameObject obj)
    {
        bool check = false;
        ItemsMatched++;
        AudController.TriggerAudio("MatchCorrect");
        ParticleManager.Instance.EmitParticle("click", Input.mousePosition);
        obj.GetFirstLevelChildren()[0].gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        if (ItemsMatched >= 4)
        {
            PerformanceManager.Instance.IncrementSuccess();
            PuzzleDropScript.OnDropSuccess = null;
            MatchImages.ForEach(x => x.transform.DOKill());
            yield return new WaitForSeconds(0.2f);
            SetInvisible(0.5f);
            yield return new WaitForSeconds(0.5f);
            AudController.TriggerAudio("CheerUp");
            FinishUI.Instance.ShowFinishStars(() => check = true);
            yield return new WaitUntil(() => check);
            yield return new WaitForSeconds(0.2f);
            PuzzleDropScript.OnDropSuccess += (x) => { StartCoroutine(OnDropSuccess(x)); };
            if (vgm.ads.AdsCheckManager.Instance.ShowInterestialIfAllowed(vgm.ads.ActivityType.ACTIVITY))
            {
                Debug.Log("Not a premium user so show ad");
                vgm.ads.InterestialService.ShowAdIfItIsReady(() => StartCoroutine(GeneratePuzzle()));
            }
            else
                StartCoroutine(GeneratePuzzle());
        }
    }

    void Start()
    {
        UILoader.Instance.StartLoader();
        AssertAndRecordPreviousPositions();
        SetInvisible();
        StartCoroutine(GeneratePuzzle());
    }

    public void OnClickExitButton()
    {
        if (PuzzleDropScript.OnDropSuccess == null)
            return;
        AudController.TriggerAudio("OnClick");
        UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());

        double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
        FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
        AnalyticsScript.instance.LogEvent("activity_session_time", "match_item", totalTimeSpent);
    }

    IEnumerator GeneratePuzzle()
    {
        PerformanceManager.Instance.IncrementAttempts();
        LocalDataManager.Instance.SaveData.IncrementCurrentActivitySession();
        //ADS
        ItemsMatched = 0;

        var items = MatchItems.Skip(CurrPuzzle).Shuffle().Take(4).ToList();
        if (items.Count != 4 || CurrPuzzle >= MatchItems.Count)
        {
            CurrPuzzle = 0;
            items = MatchItems.Skip(CurrPuzzle).Shuffle().Take(4).ToList();
        }

        var matchDropItems = items.Take(4).ToList();
        MatchDropSpots.Each((x, i) =>
        {
            var item = matchDropItems.PickRandom();
            matchDropItems.RemoveAll(y => y.name == item.name);
            x.SetImage(item);
            x.SetNativeSize();
            x.name = item.name;
        });
        matchDropItems = items.Take(4).ToList();
        MatchImages.Each((x, i) =>
        {
            var item = matchDropItems.PickRandom();
            matchDropItems.RemoveAll(y => y.name == item.name);
            x.SetImage(item);
            x.SetNativeSize();
            x.name = item.name;
            x.rectTransform.rotation = Quaternion.identity;
            x.GetComponent<CanvasGroup>().blocksRaycasts = true;
        });

        CurrPuzzle += 4;

        yield return new WaitForSeconds(0.5f);
        UILoader.Instance.StopLoader();
        SetVisible();


    }

    void SetInvisible(float duration = 0)
    {

        MatchDropSpots.ForEach(x =>
        {
            x.transform.DOMove(OnCompletePosition.position, duration);
            x.DOFade(0, duration);
            x.rectTransform.DORotate(new Vector3(0, 0, EnumeratorExtensions.RandomAmong(180, -180)), duration * 2)
                .SetEase(Ease.OutCubic);

        });

        MatchImages.ForEach(x =>
        {
            x.DOFade(0, duration);

        });


    }

    void SetVisible()
    {
        MatchImages.Each((x, i) =>
        {
            x.transform.SetParent(ParentTransform);
            x.rectTransform.anchoredPosition3D = MatchImageOriginalPos[i];

        });
        MatchDropSpots.Each((x, i) =>
        {
            x.rectTransform.anchoredPosition3D = MatchDropSpotsOriginalPos[i];

        });

        MatchDropSpots.Each((x, i) =>
        {

            x.rectTransform.rotation = Quaternion.identity;
            x.DOFade(1, 1);

        });

        MatchImages.Each((x, i) =>
        {
            x.DOFade(1, 1);
        });
    }
}