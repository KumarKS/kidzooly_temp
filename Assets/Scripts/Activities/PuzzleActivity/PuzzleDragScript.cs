﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class PuzzleDragScript : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    Vector3 startPosition;
    Transform startParent;
    Canvas _Canvas;
    private Vector3 offset;
    public static Action OnDropFailure;

    public Canvas GetCanvas
    {
        get
        {
            if (_Canvas == null)
            {
                _Canvas = transform.root.GetComponent<Canvas>();
                if (_Canvas == null)
                {
                    _Canvas = transform.root.GetComponentInChildren<Canvas>();
                }
            }

            return _Canvas;
        }
        set
        {
            _Canvas = value;
        }
    }
    #region IBeginDragHandler implementation

    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = transform.position;
        startParent = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(GetCanvas.transform as RectTransform, eventData.position, GetCanvas.worldCamera, out pos);
        offset = gameObject.transform.position - GetCanvas.transform.TransformPoint(pos);
        transform.SetAsLastSibling();
        Vector3 rndPosWithin;
        rndPosWithin = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        rndPosWithin = transform.TransformPoint(rndPosWithin * .5f);
    }

    #endregion

    #region IDragHandler implementation

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(GetCanvas.transform as RectTransform, eventData.position, GetCanvas.worldCamera, out pos);
        transform.position = GetCanvas.transform.TransformPoint(pos) + offset;


    }

    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag(PointerEventData eventData)
    {
        
        if (transform.parent == startParent)
        {
            transform.DOMove(startPosition, 0.2f);
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            OnDropFailure.SafeInvoke();
        }
    }

    #endregion


}
