﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using vgm.ads;

public class PuzzleActivityManager : MonoBehaviour
{
    public List<GameObject> PuzzlePieceCollection = new List<GameObject>();
    public GameObject PuzzleActivityContainer;
    public GameObject RandomPositionContainer;
    public GameObject ScatteredObjectContainer;
    public RectTransform PuzzlePieceImageContainer;
    public UIScrollableGrid PuzzlesContainer;
    public GameObject PuzzleGalleryPrefab;
    public UIPanel PuzzleGallery;
    public UIPanel PuzzleActivity;
    public RawImage PreviewImage;
    public AudioController AudController;
    static int CurrentIndex = 0;
    private GameObject CurrentPuzzlePieces;
    private GameData.PrecutPuzzleCollection Config;
    private List<Texture2D> PuzzleImages = new List<Texture2D>();
    private int PiecesLeftToComplete = 1;
    private Sequence _wobbleSeq;
    private Sequence _rotationSeq;

    void OnEnable()
    {
        PuzzleDragScript.OnDropFailure += PlayDropFailure;
        PuzzleDropScript.OnDropSuccess += (obj) => { StartCoroutine(OnDropSuccess(obj)); };
        HardwareInputManager.OnBack += OnClickBackButton;
        AdsCheckManager.Instance.ShowBannerIfAllowed();

    }

    void OnDisable()
    {
        PuzzleDragScript.OnDropFailure = null;
        PuzzleDropScript.OnDropSuccess = null;
        HardwareInputManager.OnBack -= OnClickBackButton;
        AdsCheckManager.Instance.CloseBanner();

    }

    void InitPreviewAnim()
    {
        //if sequence already playing kill it
        if (_wobbleSeq != null) _wobbleSeq.Kill();
        if (_rotationSeq != null) _rotationSeq.Kill();

        _wobbleSeq = DOTween.Sequence();
        _rotationSeq = DOTween.Sequence();

        PreviewImage.transform.DOScale(Vector3.one, 0);
        PreviewImage.rectTransform.DORotate(Vector3.zero, 0);

        _wobbleSeq.Append(PreviewImage.rectTransform.DOPunchScale(new Vector3(0.1f, 0.1f, 0.1f), 1, 1, 0.5f).SetEase(Ease.InSine))
            .SetLoops(EnumeratorExtensions.RandomAmong(1, 2))
            .OnComplete(() => { _rotationSeq.Restart(); }).SetAutoKill(false);

        _rotationSeq.Append(PreviewImage.rectTransform.DOPunchRotation(new Vector3(0, 0, 10), 2f, 2, 0.5f).SetEase(Ease.InOutSine))
            .SetLoops(EnumeratorExtensions.RandomAmong(1, 2, 4))
            .OnComplete(() => { _wobbleSeq.Restart(); }).SetAutoKill(false);
    }

    IEnumerator OnDropSuccess(GameObject go)
    {
        PiecesLeftToComplete--;
        AudController.TriggerAudio("PuzzleCorrect");
        ParticleManager.Instance.EmitParticle("click", Input.mousePosition);
        if (PiecesLeftToComplete <= 0)
        {
            bool check = false;

            yield return new WaitForSeconds(0.5f);
            AdsCheckManager.Instance.CloseBanner();
            PuzzleActivityContainer.transform.DOScale(Vector3.one * 1.2f, 0.5f).SetLoops(2, LoopType.Yoyo).OnComplete(() => check = true).OnStart(() => AudController.TriggerAudio("OnPuzzleComplete"));
            yield return new WaitUntil(() => check);
            FinishUI.Instance.ShowFinishStars(() =>
            {
                //##BELOW CODE CHANGED
                // var nextPuzz = PuzzleData[testing(CurrentIndex++)];
                // if (!nextPuzz.IsFree && !LocalDataManager.Instance.SaveData.IsPremiumUser())
                // {
                //     AudController.TriggerAudio("OnLockClick", () =>
                //     {
                //         ParentalLockSystem.Instance.InitializeLock(
                //             () => UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription()), () => DisplayPuzzle(CurrentIndex));
                //     });
                // }
                // else
                // {
                //     DisplayPuzzle(testing(CurrentIndex++));
                // }
                PerformanceManager.Instance.IncrementSuccess();
                if (LocalDataManager.Instance.SaveData.IsPremiumUser())
                    DisplayPuzzle(++CurrentIndex);
                else
                    DisplayPuzzle(GetTheFreeIndex(++CurrentIndex));

            });
        }
    }

    //##BELOW NEW CODE SNIPPET ADDED
    int GetTheFreeIndex(int currentNumber)
    {
        for (int i = currentNumber; i < PuzzleImages.Count; i++)
        {
            var match = Config.PrecutPuzzleImages.Find(x => FileManager.GetLocalFileName(x.PuzzleImageUrl) == PuzzleImages[i].name);
            if (match.IsFree)
            {
                return i;
            }
        }

        for (int i = 0; i < currentNumber; i++)
        {
            var match = Config.PrecutPuzzleImages.Find(x => FileManager.GetLocalFileName(x.PuzzleImageUrl) == PuzzleImages[i].name);
            if (match.IsFree)
            {
                return i;
            }
        }
        return 0;
    }

    void PlayDropFailure()
    {
        AudController.TriggerAudio("PuzzleIncorrect");
    }

    void MakePuzzleHolder()
    {
        var puzzle = PuzzleActivityContainer.AddChild(CurrentPuzzlePieces);
        foreach (Transform child in puzzle.transform)
        {
            var img = child.gameObject.GetComponent<Image>();
            img.raycastTarget = true;
            img.alphaHitTestMinimumThreshold = 0.5f;
            child.gameObject.AddComponent<PuzzleDropScript>();
        }
    }

    void MakePuzzlePieces()
    {
        var puzzle = PuzzleActivityContainer.AddChild(CurrentPuzzlePieces);
        var colors = new List<string>() { "#0d0221", "#0F084B", "#26408B", "#A6CFD5", "#C2E7D9" };
        var positions = RandomPositionContainer.GetAllChildren().Select(x => x.transform.position).ToList();
        var color = new Color();
        ColorUtility.TryParseHtmlString(colors.PickRandom(), out color);
        foreach (Transform child in puzzle.transform)
        {
            child.gameObject.AddComponent<Mask>().showMaskGraphic = true;
            var cv = child.gameObject.AddComponent<CanvasGroup>();
            var img = child.gameObject.GetComponent<Image>();

            cv.alpha = 1;
            cv.interactable = true;
            cv.blocksRaycasts = true;
            img.alphaHitTestMinimumThreshold = 0.5f;
            child.gameObject.AddComponent<PuzzleDragScript>();


            img.color = color;
            img.raycastTarget = true;
        }
        foreach (Transform child in puzzle.transform)
        {
            var go = PuzzleActivityContainer.AddChild(PuzzlePieceImageContainer.gameObject);
            go.transform.SetParent(child);
        }
        var children = puzzle.GetFirstLevelChildren();
        foreach (GameObject child in children)
        {
            child.transform.SetParent(ScatteredObjectContainer.transform);
            var index = Random.Range(0, positions.Count);
            child.transform.position = positions[index];
            ComponentExtensions.PutChildInsideParent(child.GetComponent<RectTransform>(), ScatteredObjectContainer.GetComponent<RectTransform>());
            positions.RemoveAt(index);
        }

        Destroy(puzzle);
    }

    void DisplayPuzzle(int index)
    {
        PerformanceManager.Instance.IncrementAttempts();
        AdsCheckManager.Instance.ShowBannerIfAllowed();
        LocalDataManager.Instance.SaveData.IncrementCurrentActivitySession();
        //  UILoader.Instance.StartLoader();
        PuzzleGallery.DeactivatePanel();
        PuzzleActivityContainer.DestroyChildren();
        ScatteredObjectContainer.DestroyChildren();
        CurrentPuzzlePieces = PuzzlePieceCollection.PickRandom();
        PiecesLeftToComplete = CurrentPuzzlePieces.transform.childCount;
        if (index < 0)
            index = PuzzleImages.Count - 1;
        if (index > PuzzleImages.Count - 1)
            index = 0;
        CurrentIndex = index;
        PuzzlePieceImageContainer.GetComponent<RawImage>().texture = PuzzleImages[CurrentIndex];
        //GetPuzzleImageDominantColor();
        if (vgm.ads.AdsCheckManager.Instance.ShowInterestialIfAllowed(vgm.ads.ActivityType.ACTIVITY))
        {
            Debug.Log("Not a premium user so show ad");
            vgm.ads.InterestialService.ShowAdIfItIsReady(() => MakePuzzleHolder());
        }
        else
            MakePuzzleHolder();

        MakePuzzlePieces();

        //preview image animation
        InitPreviewAnim();

        PreviewImage.DOFade(0, 1).From();
        EnumeratorExtensions.RandomAmong(_wobbleSeq, _rotationSeq).Restart();

        PreviewImage.texture = PuzzleImages[CurrentIndex];
        PuzzleActivity.ActivatePanel();
    }

    void GeneratePuzzleGallery()
    {
        ScatteredObjectContainer.DestroyChildren();
        var premiumStatus = LocalDataManager.Instance.SaveData.IsPremiumUser();
        PuzzlesContainer.SetPrefab(PuzzleGalleryPrefab).SetData(PuzzleImages).SetFunction(
            (data, index, go) =>
            {
                go.transform.GetChild(0).GetComponent<RawImage>().texture = PuzzleImages[index];
                var goBut = go.GetComponent<Button>();
                var match = Config.PrecutPuzzleImages.Find(x => FileManager.GetLocalFileName(x.PuzzleImageUrl) == PuzzleImages[index].name);
                if (match != null && !match.IsFree && !premiumStatus)
                {
                    go.transform.GetChild(go.transform.childCount - 1).gameObject.Activate();
                    goBut.onClick.RemoveAllListeners();
                    goBut.onClick.AddListener(() =>
                    {
                        UILoader.Instance.StartLoader();
                        AudController.TriggerAudio("OnLockClick", () =>
                        {
                            UILoader.Instance.StopLoader();
                            AdsCheckManager.Instance.CloseBanner();
                            FreeTrialsPanel.Instance.Init(null, null, () => { AdsCheckManager.Instance.ShowBannerIfAllowed(); });
                        });
                    });
                }
                else
                {
                    go.transform.GetChild(go.transform.childCount - 1).gameObject.Deactivate();
                    goBut.onClick.RemoveAllListeners();
                    goBut.onClick.AddListener(() =>
                    {
                        DisplayPuzzle(index);
                        AudController.TriggerAudio("OnClick");
                    });
                }
            }).Initialize();
        PuzzleGallery.ActivatePanel();
    }

    IEnumerator FetchNewImages()
    {
        Config = LocalDataManager.Instance.GetLocalConfig<GameData.PrecutPuzzleCollection>(EConfigFileName.PrecutPuzzle);
        var downloading = true;

        var urls = Config.PrecutPuzzleImages.Select(x => x.PuzzleImageUrl).ToList();
        FileManager.Instance.GetLocalImage(urls: urls.ToArray(), onComplete:
            (x) =>
            {
                PuzzleImages.AddRange(x);
                downloading = false;
            }, onFail: (error) =>
               {
                   Debug.Log("Available images fetch Failed");
                   AdsCheckManager.Instance.CloseBanner();
                   UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());
                   UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDataFetchFailure") ?? "Fetch Failed, Try Again...", 5);
               });
        yield return new WaitUntil(() => !downloading);
        yield return new WaitForEndOfFrame();
    }


    IEnumerator Initialize()
    {
        UILoader.Instance.StartLoader();
        yield return StartCoroutine(FetchNewImages());
        GeneratePuzzleGallery();
        yield return new WaitForEndOfFrame();

        UILoader.Instance.StopLoader();
    }

    void Start()
    {
        StartCoroutine(Initialize());
    }

    public void OnClickBackButton()
    {
        if (UILoader.Instance.IsPanelActive)
        {
            return;
        }

        if (PiecesLeftToComplete <= 0)
            return;
        AudController.TriggerAudio("OnClick");
        if (PuzzleActivity.IsPanelActive)
        {
            PuzzleActivity.DeactivatePanel();
            PuzzleGallery.ActivatePanel();
        }
        else
        {
            Resources.UnloadUnusedAssets();
            AdsCheckManager.Instance.CloseBanner();
            UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());
        }

        double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
        FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
        AnalyticsScript.instance.LogEvent("activity_session_time", "puzzle_activity", totalTimeSpent);

    }
}