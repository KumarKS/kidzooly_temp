﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class PuzzleDropScript : MonoBehaviour, IDropHandler
{
    public static Action<GameObject> OnDropSuccess;

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null && string.Equals(gameObject.name, eventData.pointerDrag.name))
        {
            eventData.pointerDrag.transform.SetParent(transform);
            eventData.pointerDrag.transform.DOMove(transform.position, 0.2f);
            eventData.pointerDrag.GetComponent<CanvasGroup>().blocksRaycasts = false;
            OnDropSuccess.SafeInvoke(gameObject);
        }
    }
}