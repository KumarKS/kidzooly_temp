﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RollerCoasterController : MonoBehaviour
{
    public static int CurrentActivity = 0;
    public static List<LearningWheelActivity> AllActivities = new List<LearningWheelActivity>();
    public int NumberOfCabins;
    public AudioController AudioController;
    public GameObject CabinPrefab;
    public static List<RollerCoasterCabin> Cabins = new List<RollerCoasterCabin>();
    public RawImage CurrentActivityBorderImage;
    public List<Texture> ActivityBorderImages;
    static int ActivityBGIndex = 0;
    public List<Sprite> ActivityBackGrounds;
    public Image ActivityBG;
    private Vector3 direction;
    private float baseAngle;
    public UIPanel ActivityInput;

    private Vector3 followVector = new Vector3();

    private Vector3 topObjPos;
    private Vector3 swipeDir;
    private float quickSwipeTimer;
    private float quickSwipeTime = .2f;
    private int maxSwipe = 100;
    private int LastSelectedCabinIndex;

    void GetAllActivities()
    {
        AllActivities = FindObjectsOfType<LearningWheelActivity>().ToList();
    }

    void OnEnable()
    {
        HardwareInputManager.OnBack += ExitActivity;
    }

    void OnDisable()
    {
        HardwareInputManager.OnBack -= ExitActivity;
    }

    void DoUIHack()
    {
        float r = Camera.main.aspect;
        string _r = r.ToString("F2");
        string ratio = _r.Substring(0, 4);

        switch (ratio)
        {
            case "1.33"://4:3                   
            case "1.50"://3:2   
            case "0.67":
                {
                    //2:3
                    transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
                    break;
                }
            case "0.56":
                {
                    //9:16
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void Start()
    {
        GetAllActivities();

        AllActivities.ForEach(x => x.DeactivatePanel());
        CurrentActivity = AllActivities.FindIndex(x => x.ActivityName == ELearningWheelActivities.Piano);
        AllActivities[CurrentActivity].ActivatePanel();
        topObjPos = CabinPrefab.transform.position;
        SpawnCabins(NumberOfCabins);
        transform.DORotate(new Vector3(0, 0, 360), 2, RotateMode.LocalAxisAdd)
            .SetEase(Ease.InOutSine)
            .OnStart(() => AudioController.TriggerAudio("RotationSound"))
            .OnComplete(() =>
            {
                AudioController.StopAudio("RotationSound");
                ActivityInput.ActivatePanel();
                var cv = GetComponent<CanvasGroup>();
                cv.interactable = true;
                cv.blocksRaycasts = true;
                int index = Cabins.FindIndex(x => x.Thumbnail.sprite == AllActivities[CurrentActivity].Thumbnail);
                RollerCoasterCabin.SelectedCabin = Cabins[index];
                LastSelectedCabinIndex = index;
            });
    }

    public void ExitActivity()
    {
        UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());

        double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
        FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
        AnalyticsScript.instance.LogEvent("activity_session_time", "learning_wheel", totalTimeSpent);
    }

    void SpawnCabins(int numberOfCabins)
    {
        var colors = new List<string>() { "#78d349", "#f84266" };
        int x = 0;
        var rt = GetComponent<RectTransform>();

        Cabins.Clear();
        for (int i = 0; i < numberOfCabins; i++)
        {
            Vector3 pos = RandomCircle(i * (360 / numberOfCabins), transform.position, ((rt.rect.size.x / 2) * 1.25f) / transform.root.GetComponent<CanvasScaler>().referencePixelsPerUnit);
            Quaternion rot = Quaternion.FromToRotation(Vector3.down, transform.position - pos);
            var child = gameObject.AddChild(CabinPrefab);

            child.transform.position = pos;
            child.transform.rotation = Quaternion.Euler(new Vector3(0, 0, rot.eulerAngles.z));
            var color = new Color();

            if (x > colors.Count - 1)
                x = 0;

            ColorUtility.TryParseHtmlString(colors[x++], out color);
            child.GetComponent<Image>().color = color;
            Cabins.Add(child.GetComponent<RollerCoasterCabin>());
        }

        for (int i = 0,
        index = CurrentActivity;
            i < Cabins.Count / 2;
            i++)
        {
            if (index > AllActivities.Count - 1)
                index = 0;

            Cabins[i].gameObject.name = (index).ToString();
            Cabins[i].Thumbnail.SetImage(AllActivities[index].Thumbnail);

            index++;
        }
        for (int i = Cabins.Count - 1, index = CurrentActivity - 1; i >= Cabins.Count / 2; i--)
        {
            if (index < 0)
                index = AllActivities.Count - 1;

            Cabins[i].gameObject.name = (index).ToString();
            Cabins[i].Thumbnail.SetImage(AllActivities[index].Thumbnail);

            index--;
        }

        DoUIHack();
    }

    Vector3 RandomCircle(float angle, Vector3 center, float radius)
    {
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(angle * Mathf.Deg2Rad);
        pos.y = center.y + radius * Mathf.Cos(angle * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }


    public void OnBeginDrag(BaseEventData dt)
    {
        AllActivities[CurrentActivity].DeactivatePanel();
        RollerCoasterCabin.SelectedCabin = null;
        var dir = Camera.main.WorldToScreenPoint(transform.position);
        dir = Input.mousePosition - dir;

        baseAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        baseAngle -= Mathf.Atan2(transform.right.y, transform.right.x) * Mathf.Rad2Deg;
        followVector = transform.rotation.eulerAngles;
        quickSwipeTimer = Time.time;
        swipeDir = Input.mousePosition;
    }

    public void OnDrag(BaseEventData dt)
    {
        var dir = Camera.main.WorldToScreenPoint(transform.position);
        dir = Input.mousePosition - dir;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - baseAngle;
        transform.DORotate(Quaternion.AngleAxis(angle, Vector3.forward).eulerAngles, 0.5f).SetId("Rotating").OnComplete(() => AudioController.StopAudio("Swipe"));
    }

    public void OnEndDrag(BaseEventData dt)
    {
        AudioController.TriggerAudio("Swipe");
        transform.DOKill();
        int nextIndex;
        swipeDir -= Input.mousePosition;
        if (Time.time - quickSwipeTimer <= quickSwipeTime && Mathf.Abs(swipeDir.x) >= maxSwipe)
        {
            nextIndex = GetNearest(swipeDir.x);
            transform.DORotate(transform.eulerAngles - Cabins[nextIndex].transform.eulerAngles, 0.5f).SetId("Rotating")
                .OnComplete(() =>
               {
                   AfterRotationComplete(nextIndex);
               });
        }
        else
        {
            nextIndex = GetNeareast();
            transform.DORotate(transform.eulerAngles - Cabins[nextIndex].transform.eulerAngles, 0.5f).SetDelay(.25f).SetId("Rotating")
                .OnComplete(() =>
               {
                   AfterRotationComplete(nextIndex);
               });
        }
    }

    private void AfterRotationComplete(int i)
    {
        AudioController.StopAudio("Swipe");
        LastSelectedCabinIndex = i;
        CurrentActivityBorderImage.texture =
            ActivityBorderImages.Where(x => x.name != CurrentActivityBorderImage.texture.name).ElementAt(0);

        AllActivities[CurrentActivity].DeactivatePanel();
        CurrentActivity = AllActivities.FindIndex(x => x.Thumbnail == Cabins[i].Thumbnail.sprite);
        RollerCoasterCabin.SelectedCabin = Cabins[i];
        if (vgm.ads.AdsCheckManager.Instance.ShowInterestialIfAllowed(vgm.ads.ActivityType.ACTIVITY))
        {
            Debug.Log("Not a premium user so show ad");
            vgm.ads.InterestialService.ShowAdIfItIsReady(() => AllActivities[CurrentActivity].ActivatePanel());
        }
        else
            AllActivities[CurrentActivity].ActivatePanel();

        ActivityBG.SetImage(ActivityBackGrounds[ActivityBGIndex > ActivityBackGrounds.Count - 1 ? ActivityBGIndex = 0 : ActivityBGIndex++]);
    }
    
    void Update()
    {
        followVector = Vector3.LerpUnclamped(followVector, transform.rotation.eulerAngles, 0.8f);
    }


    public void OnClick(RollerCoasterCabin rc)
    {
    }


    private int GetNeareast()
    {
        float minDistance = Vector3.Distance(Cabins[0].transform.position, topObjPos);
        int nearestIndex = 0;
        for (int i = 1; i < Cabins.Count; i++)
        {
            float distance = Vector3.Distance(Cabins[i].transform.position, topObjPos);
            if (distance < minDistance)
            {
                minDistance = distance;
                nearestIndex = i;
            }
        }
        return nearestIndex;
    }

    private int GetNearest(float i)
    {
        int index = LastSelectedCabinIndex;
        if (i > 0)
        {
            index++;
            if (index > Cabins.Count - 1)
            {
                index = 0;
            }
        }
        else
        {
            index--;
            if (index < 0)
            {
                index = Cabins.Count - 1;
            }
        }
        return index;
    }
}