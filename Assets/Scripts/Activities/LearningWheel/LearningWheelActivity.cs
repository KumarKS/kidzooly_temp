﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public enum ELearningWheelActivities : int
{
    Alphabets = 0,
    DomesticAnimals = 1,
    Vegetables = 2,
    WildAnimals = 3,
    Numbers = 4,
    Color = 5,
    SeaAnimals = 6,
    Fruits = 7,
    Shapes = 8,
    Food = 9,
    Vehicle = 10,
    Piano = 11
}

public class LearningWheelActivity : UIPanel
{
    public ELearningWheelActivities ActivityName;
    public Sprite Thumbnail;
    public GameObject ActivityContainer = null;
    public List<AudioClip> ActivityAudioClips;
    static RollerCoasterController _controller;

    protected static RollerCoasterController Controller
    {
        get
        {
            if (_controller == null)
                _controller = FindObjectOfType<RollerCoasterController>();

            return _controller;
        }
    }

    protected virtual void Start()
    {
        InitializeActivity();
    }

    protected virtual void InitializeActivity()
    {
        var activityItems = (ActivityContainer == null ? gameObject : ActivityContainer).GetFirstLevelChildren();
        foreach (var item in activityItems)
        {
            var button = item.AddComponent<Button>();
            var source = item.AddComponent<AudioSource>();
            button.transition = Selectable.Transition.None;
            button.onClick.AddListener(() =>
            {
                PerformanceManager.Instance.IncrementAttempts();
                PerformanceManager.Instance.IncrementSuccess();
                if (DOTween.IsTweening("IsWobbling" + button.gameObject.GetInstanceID()))
                    return;
                ParticleManager.Instance.EmitParticle("click", Input.mousePosition);
                var outline = button.gameObject.transform.GetComponent<Outline>();
                if (outline != null)
                {
                    outline.DOFade(1, 0);
                    outline.DOFade(0, 1);
                }
                button.gameObject.transform.DOShakeScale(1f, new Vector3(0.1f, 0.1f, 0.1f)).SetId("IsWobbling" + button.gameObject.GetInstanceID());

                if (ActivityAudioClips.Any())
                {
                    source.clip = ActivityAudioClips.Find(x => string.Equals(x.name, item.name));
                    source.playOnAwake = false;
                    source.Play();
                }
                else
                {
                    Controller.AudioController.TriggerAudio(item.name);
                }
            });

        }
    }
}