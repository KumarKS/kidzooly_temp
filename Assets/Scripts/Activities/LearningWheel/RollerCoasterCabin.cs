﻿using System.Diagnostics;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine;

public class RollerCoasterCabin : UIPanel
{
    public Image Thumbnail;
	public Image ThumbnailBG;
	public Image ThumbnailBGOutline;

    public bool IsActive = false;

	private static RollerCoasterCabin _SelectedCabin;

	public static RollerCoasterCabin SelectedCabin
	{
		get
		{
			return _SelectedCabin;
		}
		set
		{
			if (_SelectedCabin != null)
			{
				_SelectedCabin.SetSelected (false);
			}

			_SelectedCabin = value;

			if (_SelectedCabin != null)
			{
				_SelectedCabin.SetSelected (true);
			}
		}
	}

	public Color SelectedColor;
	public Color SelectedColorOutline;
	public Color UnSelectedColor;
	public Color UnSelectedColorOutline;

	private void SetSelected(bool value)
	{
		if (value)
		{
			ThumbnailBG.DOColor (SelectedColor, .25f);
			ThumbnailBGOutline.DOColor (SelectedColorOutline, .15f);
		}
		else
		{
			ThumbnailBG.DOColor (UnSelectedColor, .25f);
			ThumbnailBGOutline.DOColor (UnSelectedColorOutline, .15f);
		}
	}

	protected override void Awake ()
	{
		base.Awake ();
		SetSelected (false);
	}
        
    public bool IsOnRight()
    {
        return gameObject.transform.position.x > transform.root.position.x / 2;
    }
    public override void OnVisible()
    {
        if (!DOTween.IsTweening("Rotating"))
            return;

        var IsSwipeRight = IsOnRight();

        var indexOfObjectJustVisible = RollerCoasterController.Cabins.FindIndex(x => x.GetInstanceID() == GetInstanceID());
        int indexOfObjectJustBeforeVisibleObject = -1;

        if (IsSwipeRight)
            indexOfObjectJustBeforeVisibleObject = indexOfObjectJustVisible - 1 < 0 ? RollerCoasterController.Cabins.Count - 1 : indexOfObjectJustVisible - 1;
        else
            indexOfObjectJustBeforeVisibleObject = indexOfObjectJustVisible + 1 > RollerCoasterController.Cabins.Count - 1 ? 0 : indexOfObjectJustVisible + 1;




        var incomingObjIndex = RollerCoasterController.Cabins[indexOfObjectJustBeforeVisibleObject].name.ToInt();
        int newIndex = -1;
        if (!IsSwipeRight)
            newIndex = incomingObjIndex - 1 < 0 ? RollerCoasterController.AllActivities.Count - 1 : incomingObjIndex - 1;
        else
            newIndex = incomingObjIndex + 1 > RollerCoasterController.AllActivities.Count - 1 ? 0 : incomingObjIndex + 1;


        Thumbnail.SetImage(RollerCoasterController.AllActivities[newIndex].Thumbnail);
		LocalDataManager.Instance.SaveData.IncrementCurrentActivitySession();
		gameObject.name = newIndex.ToString();
        IsActive = true;
    }

}