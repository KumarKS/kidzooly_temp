﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class PianoActivity : LearningWheelActivity
{
    public GameData.EMusicKey Key = GameData.EMusicKey.C;
    public GameData.EMusicScale Scale = GameData.EMusicScale.Major;
    public GameObject PianoKeyPrefab;
    public Button SwitchKeyButton;
    public Button SwitchScaleButton;
    public Dictionary<int, EventTrigger> PianoKeys = new Dictionary<int, EventTrigger>();
    static GameData.PianoMusicCollection AutoPlayMusic;
    public GameObject StarPrefab;

    public List<Button> AutoPlayMusicButtons = new List<Button>();

    void GeneratePiano()
    {
        PerformanceManager.Instance.IncrementAttempts();
        PerformanceManager.Instance.IncrementSuccess();
        var pitchesList = Scale.GetDescription().Split('|').Select(x => x).ToList();
        var pitches = pitchesList.Select(x => Convert.ToInt32(x)).ToList();
        ActivityContainer.DestroyChildren();
        PianoKeys.Clear();
        int i = 1;

        foreach (int note in pitches)
        {
            var pitch = Mathf.Pow(2.0f, note / 12.0f);
            var go = ActivityContainer.AddChild(PianoKeyPrefab);
            go.name = i++.ToString();
            AudioSource audSource = go.GetComponent<AudioSource>();
            audSource.pitch = pitch * Mathf.Pow(2.0f, Convert.ToInt32(Key.GetDescription()) / 12.0f);
            audSource.clip = ActivityAudioClips[0];
            PianoKeys.Add(Convert.ToInt32(go.name), go.GetComponent<EventTrigger>());
            //var color = new Color();


            // go.FindGameObjectsInChildren("Star")[0].GetComponent<Image>().color = color;
        }
    }

    void SwitchKey()
    {
        var key = ((int)Key);
        key += 1;
        if (key < Enum.GetValues(typeof(GameData.EMusicKey)).Length)
        {
            Key = (GameData.EMusicKey)key;
        }
        else
        {
            Key = GameData.EMusicKey.A;
        }


        SwitchKeyButton.GetComponentInChildren<Text>().text = Key.ToString();
        GeneratePiano();
    }

    void SwitchScale()
    {
        var scale = ((int)Scale);
        scale += 1;
        if (scale < Enum.GetValues(typeof(GameData.EMusicScale)).Length)
        {
            Scale = (GameData.EMusicScale)scale;
        }
        else
        {
            Scale = GameData.EMusicScale.Major;
        }

        SwitchScaleButton.GetComponentInChildren<Text>().text = Scale.ToString();
        GeneratePiano();
    }

    protected override void Start()
    {
        SwitchKeyButton.onClick.AddListener(SwitchKey);
        SwitchScaleButton.onClick.AddListener(SwitchScale);
        SwitchScaleButton.GetComponentInChildren<Text>().text = Scale.ToString();
        SwitchKeyButton.GetComponentInChildren<Text>().text = Key.ToString();
        GeneratePiano();
        AutoPlayMusic = LocalDataManager.Instance.GetConfig<GameData.PianoMusicCollection>(EConfigFileName.PianoMusicData);
        AutoPlayMusic.Music = AutoPlayMusic.Music.Shuffle().ToList();
        AutoPlayMusicButtons.Each((x, index) =>
        {
            if (index < AutoPlayMusic.Music.Count && AutoPlayMusic.Music[index].Scale != GameData.EMusicScale.None)
            {
                x.onClick.AddListener(() =>
                {

                    AutoplayMusic(AutoPlayMusic.Music[index]);
                });
            }


            x.transform.DOBlendableLocalMoveBy(new Vector3(Random.Range(-6.0f, 6.0f), Random.Range(-12.0f, 12.0f)), Random.Range(1.0f, 2.0f)).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
        });
    }
    void FadeOutAndDestroy()
    {
        foreach (var key in PianoKeys)
        {
            for (int i = 2; i < key.Value.transform.childCount; i++)
            {
                try
                {
                    Destroy(key.Value.transform.GetChild(i).gameObject);
                }
                catch (Exception e)
                {
                }
            }
        }
    }
    void AutoplayMusic(GameData.PianoMusicData autoPlayMusicData)
    {
        Key = autoPlayMusicData.Key;
        Scale = autoPlayMusicData.Scale;
        DOTween.Kill("AutoPlay");
        GeneratePiano();
        var eventSystem = FindObjectOfType<EventSystem>();
        autoPlayMusicData.Tab = autoPlayMusicData.Tab.OrderBy(x => x.Key).ToDictionary(x => x.Key, v => v.Value);
        Sequence SEQ = DOTween.Sequence();
        float startTime = 0;
        var colors = new List<string>() { "#FA5286FF", "#FDD12AFF", "#F74D146F", "#FF7132FF", "#3262FFFF", "#8432FFFF", "#FF32FDFF" };
        int colorIndex = 0;
        FadeOutAndDestroy();
        //foreach (var key in PianoKeys)
        //{
        //    for (int i = 2; i < key.Value.transform.childCount; i++)
        //    {
        //        try
        //        {
        //            Destroy(key.Value.transform.GetChild(i).gameObject);
        //        }
        //        catch (Exception e)
        //        {
        //        }
        //    }
        //}
        foreach (var tab in autoPlayMusicData.Tab)
        {
            var color = new Color();
            if (colorIndex > colors.Count - 1)
                colorIndex = 0;
            var co = colors[colorIndex++];
            ColorUtility.TryParseHtmlString(co, out color);
            color.a = 0;
            var piano = PianoKeys[autoPlayMusicData.Tab[tab.Key]];
            var variableTime = tab.Key - startTime;

            SEQ.InsertCallback(tab.Key, () =>
            {
                var star = piano.gameObject.AddChild(StarPrefab);
                star.GetComponent<Image>().color = color;
                star.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 382);
            });
            SEQ.InsertCallback(tab.Key + 0.015625f, () =>
            {
                var star =
                    piano.transform.GetChild(
                        piano.transform.childCount - 1);
                star.GetComponent<Image>().DOFade(1, variableTime);
            });
            SEQ.InsertCallback(tab.Key + 0.03125f, () =>
            {
                var star =
                    piano.transform.GetChild(
                        piano.transform.childCount - 1);
                star.GetComponent<RectTransform>().DOAnchorPosY(0, variableTime).OnComplete(() =>
                {
                    var pointerEnterTrigger =
                        PianoKeys[autoPlayMusicData.Tab[tab.Key]].triggers.Find(x => x.eventID == EventTriggerType.PointerEnter);
                    pointerEnterTrigger.callback.Invoke(new BaseEventData(eventSystem));

                    star.GetComponent<RectTransform>().DOAnchorPosY(0, 0.025f).OnComplete(() =>
                    {
                        var pointerExitTrigger =
                            piano.triggers.Find(x => x.eventID == EventTriggerType.PointerExit);
                        pointerExitTrigger.callback.Invoke(new BaseEventData(eventSystem));
                    });
                });
            });
            startTime = tab.Key;
        }
        SEQ.SetId("AutoPlay");
        SEQ.Play();
    }
}