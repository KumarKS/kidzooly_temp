﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LearningActivityController : MonoBehaviour
{

    static ELearningWheelActivities CurrentActivity = ELearningWheelActivities.Piano;
    static List<LearningWheelActivity> AllActivities = new List<LearningWheelActivity>();
    public Button SwitchButton;


    void GetAllActivities()
    {
        AllActivities = FindObjectsOfType<LearningWheelActivity>().ToList();
    }

    void Start()
    {
        SwitchButton.onClick.AddListener(OnClickActivitySwitch);
        SwitchButton.GetComponentInChildren<Text>().text = CurrentActivity.ToString();

        GetAllActivities();

        AllActivities.ForEach(x => x.DeactivatePanel());
        AllActivities.Find(x => x.ActivityName == CurrentActivity).ActivatePanel();

    }

    public void OnClickActivitySwitch()
    {
        AllActivities.Find(x => x.ActivityName == CurrentActivity).DeactivatePanel();
        var currActivity = ((int)CurrentActivity);
        currActivity += 1;
        if (currActivity < Enum.GetValues(typeof(ELearningWheelActivities)).Length)
        {
            CurrentActivity = (ELearningWheelActivities)currActivity;
        }
        else
        {
            CurrentActivity = ELearningWheelActivities.Alphabets;
        }


        SwitchButton.GetComponentInChildren<Text>().text = CurrentActivity.ToString();
        AllActivities.Find(x => x.ActivityName == CurrentActivity).ActivatePanel();

    }
    public void ExitActivity()
    {
        UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());
    }
}
