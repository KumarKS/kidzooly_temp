﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphabetActivity : LearningWheelActivity
{
    protected override void Start()
    {

        var colors = new List<string>() { "#f91f81", "#ff7132", "#ffe032", "#32ffa1", "#3262ff", "#8432ff", "#ff32fd" };
        var alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        int x = 0;
        var activityItems = (ActivityContainer == null ? gameObject : ActivityContainer).GetFirstLevelChildren();
        for (int i = 0; i < activityItems.Count; i++)
        {
            var color = new Color();

            if (x > colors.Count - 1)
                x = 0;

            ColorUtility.TryParseHtmlString(colors[x++], out color);
            activityItems[i].GetComponent<Image>().color = color;
            activityItems[i].GetComponentInChildren<Text>().SetText(alphabets[i]);
            activityItems[i].name = alphabets[i].ToString();
        }

        base.Start();
    }

}
