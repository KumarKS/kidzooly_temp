﻿using System.Collections.Generic;
using UnityEngine;

public class GenericDrawLine : MonoBehaviour
{
    List<Vector3> linePoints = new List<Vector3>();
    public LineRenderer lineRenderer;
    public float startWidth = 1.0f;
    public float endWidth = 1.0f;
    public float threshold = 0.001f;
    Camera thisCamera;
    int lineCount = 0;

    Vector3 lastPos = Vector3.one * float.MaxValue;

    public bool isNewLineRequired = true;

    void Awake()
    {
        thisCamera = Camera.main;
    }

    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = thisCamera.nearClipPlane;
        Vector2 mouseWorld = thisCamera.ScreenToWorldPoint(mousePos);

        RaycastHit2D hit = Physics2D.Raycast(mouseWorld, Vector2.zero);

        if (Input.GetMouseButtonDown(0))
        {
            if (hit)
            {
                SetNewLine();
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (hit)
            {
                float dist = Vector3.Distance(lastPos, mouseWorld);
                if (dist <= threshold)
                    return;

                lastPos = mouseWorld;
                if (linePoints == null)
                    linePoints = new List<Vector3>();
                linePoints.Add(mouseWorld);

                UpdateLine();
                isNewLineRequired = true;
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Debug.Log(DrawingActivityManager.Instance.CheckIfCanProceed());
        }
    }

    void UpdateLine()
    {
        lineRenderer.startWidth = startWidth;
        lineRenderer.endWidth = endWidth;
        lineRenderer.positionCount = linePoints.Count;

        for (int i = lineCount; i < linePoints.Count; i++)
        {
            lineRenderer.SetPosition(i, linePoints[i]);
        }
        lineCount = linePoints.Count;
    }

    public void SetNewLine()
    {
        if (isNewLineRequired)
        {
            DrawingActivityManager.Instance.CreateNewLineRenderer();
            linePoints.Clear();
            UpdateLine();
        }
    }
}