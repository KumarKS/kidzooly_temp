﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DrawingActivityManager : GenericSingleton<DrawingActivityManager> {

    List<Color32> ColorPalette = new List<Color32>()
    {
        new Color32(255, 165, 0,255),//orange 
        new Color32(128, 0, 0,255),//maroon 
        new Color32(255, 0, 0,255),//red 
        new Color32(128, 128, 0,255),//olive 
        new Color32(255, 255, 0,255),//yellow
        new Color32(0, 128, 0,255),//green 
        new Color32(0, 255, 0,255),//lime 
        new Color32(0, 128, 128,255),//teal 
        new Color32(0, 255, 255,255),//aqua 
        new Color32(0, 0, 255,255),//blue 
        new Color32(128, 0, 128,255),//purple 
        new Color32(0, 0, 128,255),//navy 
    };

    [Header("Object Refs")]
    public GenericDrawLine drawLine;
    public GameObject lineRendererObject;
    public GameObject currFrame;
    public Transform colorPalette;

    [Header("UI Refs")]
    public Button eraser;
    public Image bg;
    public Button proceed;

    [Header("Values")]
    public Color selectedColor;
    public float zIndex = 0f;

    void Start()
    {
        for (int i = 0; i < colorPalette.childCount; i++)
        {
            colorPalette.GetChild(i).GetComponent<Image>().color = ColorPalette[i];
            int local_i = i;
            colorPalette.GetChild(i).GetComponent<Button>().onClick.AddListener(() => SetColor(ColorPalette[local_i]));
        }
        eraser.onClick.AddListener(() => SetColor(bg.sprite.texture.GetPixel(0, 0)));
    }

    void Update()
    {

    }

    public void CreateNewLineRenderer()
    {
        var go = GameObject.Instantiate(lineRendererObject, currFrame.transform);
        drawLine.lineRenderer = go.GetComponent<LineRenderer>();
        go.GetComponent<Renderer>().material.color = selectedColor;
        go.transform.DOLocalMoveZ(zIndex, 0f);
        zIndex = zIndex - 0.1f;
    }

    public void SetColor(Color color)
    {
        drawLine.SetNewLine();
        drawLine.isNewLineRequired = false;
        selectedColor = color;
        drawLine.lineRenderer.GetComponent<Renderer>().material.color = selectedColor;
    }

    public void PlayAnimation()
    {
        currFrame.transform.DOShakePosition(1f, 3, 3, 90); //Random animation for testing purposes
    }

    public void CleanUpLines()
    {

    }

    public bool CheckIfCanProceed()
    {
        if (currFrame.transform.childCount >= 3 || IsFrameFilled())
        {
            proceed.interactable = true;
            return true;
        }
            
        else
            return false;
    }

    public bool IsFrameFilled()
    {
        var totalApproxLength = (currFrame.GetComponent<Collider2D>().bounds.size.x + currFrame.GetComponent<Collider2D>().bounds.size.y) * 2;
        float totalLineLength = 0f;
        if(currFrame.transform.childCount > 0)
        {
            for (int i = 0; i < currFrame.transform.childCount; i++)
            {
                var x = currFrame.transform.GetChild(i).GetComponent<LineRenderer>().bounds.size.x;
                var y = currFrame.transform.GetChild(i).GetComponent<LineRenderer>().bounds.size.y;
                var currLineLength = (x + y) * 2;
                totalLineLength = totalLineLength + currLineLength;
            }
        }
        Debug.Log("Fill Compare, Length condition :: " + (totalApproxLength - currFrame.GetComponent<Collider2D>().bounds.size.x) + ", Line length :: " + totalLineLength);
        if (totalLineLength >= totalApproxLength - currFrame.GetComponent<Collider2D>().bounds.size.x)
            return true;
        else
            return false;
    }
}
