﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ColoringActivityManager : MonoBehaviour
{
    private List<Texture2D> ColouringImages = new List<Texture2D>();
    public Button ScreenshotButton;
    public RawImage Buffer1;
    public RawImage Buffer2;
    public UIPanel LockPanel;
    public GameObject ColorPalettePrefab;
    public GameObject PaletteContainer;
    public UIPanel FlashPanel;
    public Ease FlashEaseType;
    public Texture2D WaterMarkTexture;
    public AudioController AudController;
    Vector2 ImageDimension = new Vector2(1024, 768);
    const int totalPixelSize = 1024 * 768;
    public GameObject SnapshotParentObject;
    public Button ArrowNext;
    public Button ArrowPrev;
    public Transform GalleryTransform;
    public List<GameObject> Arrows = new List<GameObject>();


    List<Color32> ColorPalette = new List<Color32>()
    {
        new Color32(255, 255, 255, 255), //white 
        new Color32(128, 0, 0, 255), //maroon 
        new Color32(255, 0, 0, 255), //red 
        new Color32(128, 128, 0, 255), //olive 
        new Color32(255, 255, 0, 255), //yellow
        new Color32(0, 128, 0, 255), //green 
        new Color32(0, 255, 0, 255), //lime 
        new Color32(0, 128, 128, 255), //teal 
        new Color32(0, 255, 255, 255), //aqua 
        new Color32(0, 0, 128, 255), //navy 
        new Color32(0, 0, 255, 255), //blue 
        new Color32(128, 0, 128, 255), //purple 
        //new Color32(255, 0, 255,255),//fuchsia 
        //new Color32(255,165,0,255),//orange
    };

    private Color32[] Colors;
    private bool[] OutLine = new bool[totalPixelSize];
    private Texture2D Duplicate = null;
    private GameData.ColoringBookCollection Config;

    private static int ImageIndex = 0;
    private RawImage activeBuffer;
    private bool premiumStatus;

    private void OnSceneLoad(Scene arg0, LoadSceneMode arg1)
    {
        if (string.Equals(arg0.name, EScenes.ColoringActivity.GetDescription()))
        {
            DoUIHack();
        }

    }
    void DoUIHack()
    {
        float r = Camera.main.aspect;
        string _r = r.ToString("F2");
        string ratio = _r.Substring(0, 4);
        switch (ratio)
        {
            case "1.33": //4:3                   
            case "1.50": //3:2   
            case "0.67":
                {
                    //2:3
                    Buffer1.GetComponent<AspectRatioFitter>().aspectRatio = 1.2f;
                    Buffer2.GetComponent<AspectRatioFitter>().aspectRatio = 1.2f;
                    break;
                }
            case "0.56":
                {
                    //9:16
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void PopulateColorPalette()
    {
        foreach (Color32 color in ColorPalette)
        {
            var go = PaletteContainer.AddChild(ColorPalettePrefab);
            go.GetComponent<ColorPrefab>().SetColor(color);
            go.name = color.ToString();
        }
        PaletteContainer.transform.GetChild(4).GetComponent<Toggle>().isOn = true;
    }

    void MakeDuplicate()
    {

        Duplicate = new Texture2D((int)ImageDimension.x, (int)ImageDimension.y, TextureFormat.ARGB32, false);
        Duplicate.filterMode = FilterMode.Bilinear;
        Duplicate.wrapMode = TextureWrapMode.Clamp;
        Duplicate.anisoLevel = Int32.MaxValue;
        var texture2D = ColouringImages[ImageIndex] as Texture2D;
        Colors = texture2D.GetPixels32();
        Duplicate.SetPixels32(Colors);
        Duplicate.Apply();

    }

    void FillWhite()
    {

        var c = new Color32[totalPixelSize];

        for (int i = 0; i < totalPixelSize; i++)
        {
            if (Colors[i].a > 0)
            {
                c[i] = Colors[i];
            }
            else
            {
                c[i] = Color.white;
            }
        }
        Colors = c;
        Duplicate.SetPixels32(Colors);
        Duplicate.Apply();
    }
    void GenerateOutline()
    {
        for (int i = 0; i < totalPixelSize; i++)
            OutLine[i] = Colors[i].a > 0;

    }



    void ProcessImageForDrawing()
    {

        MakeDuplicate();
        GenerateOutline();
        GC.Collect();

    }

    void DisplayCanvas(bool dontSwitchBuffer = false)
    {
        var bf1 = Buffer1.GetComponent<UIPanel>();
        var bf2 = Buffer2.GetComponent<UIPanel>();

        if (dontSwitchBuffer)
        {
            Buffer1.texture = Duplicate;
            Buffer2.texture = Duplicate;
        }
        else
        {
            if (bf1.IsPanelActive)
            {
                Buffer2.texture = Duplicate;
                bf1.DeactivatePanel();
                bf2.ActivatePanel();
            }
            else
            {
                Buffer1.texture = Duplicate;
                bf2.DeactivatePanel();
                bf1.ActivatePanel();
            }
        }

        activeBuffer = bf1.IsPanelActive ? Buffer1 : Buffer2;
    }

    void FetchNewImages()
    {
        Config = LocalDataManager.Instance.GetLocalConfig<GameData.ColoringBookCollection>(EConfigFileName.ColoringBook);
        UILoader.Instance.StartLoader();
        var urls = Config.ColoringBookPages.Select(x => x.ColoringPageUrl).ToList();

        FileManager.Instance.GetLocalImage(urls: urls.ToArray(), onComplete:
            (x) =>
            {
                Debug.Log("All Images Fetched");
                UILoader.Instance.StopLoader();
                ColouringImages.AddRange(x);
                LoadNewImage(0);
            }, onFail: (error) =>
                {
                    Debug.Log("Fetch Failed");
                    UILoader.Instance.StopLoader();
                    UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());
                    UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDataFetchFailure") ?? "Fetch Failed, Try Again...", 5);
                });
    }


    public void LoadNewImage(int dir)
    {
        PerformanceManager.Instance.IncrementAttempts();
        LocalDataManager.Instance.SaveData.IncrementCurrentActivitySession();
        Arrows.ForEach(x => x.Deactivate());

        UILoader.Instance.StartLoader();
        var bf1Rt = Buffer1.GetComponent<RectTransform>();
        var bf2Rt = Buffer2.GetComponent<RectTransform>();
        bf1Rt.offsetMax = bf1Rt.offsetMin = bf2Rt.offsetMax = bf2Rt.offsetMin = Vector2.zero;
        ImageIndex += dir;
        ImageIndex = Mathf.Clamp(ImageIndex, 0, ColouringImages.Count - 1);

        if (ImageIndex > 0 && ImageIndex < ColouringImages.Count - 1)
        {
            ArrowPrev.interactable = true;
            ArrowNext.interactable = true;
        }
        else if (ImageIndex == 0)
        {
            ArrowPrev.interactable = false;
            ArrowNext.interactable = true;
        }
        else if (ImageIndex == ColouringImages.Count - 1)
        {
            ArrowPrev.interactable = true;
            ArrowNext.interactable = false;
        }

        ProcessImageForDrawing();
        if (vgm.ads.AdsCheckManager.Instance.ShowInterestialIfAllowed(vgm.ads.ActivityType.ACTIVITY))
        {
            Debug.Log("Not a premium user so show ad");
            vgm.ads.InterestialService.ShowAdIfItIsReady(() => DisplayCanvas());
        }
        else
            DisplayCanvas();

        ShowLock();

        DOTween.Sequence().AppendInterval(0.5f).AppendCallback(() =>
        {
            UILoader.Instance.StopLoader();
            Arrows.ForEach(x => x.Activate());
        }).Play();

    }

    private void ShowLock()
    {
        var match =
            Config.ColoringBookPages.Find(x => FileManager.GetLocalFileName(x.ColoringPageUrl) == ColouringImages[ImageIndex].name);
        if (match != null && !match.IsFree && !premiumStatus)
        {
            ScreenshotButton.interactable = false;
            LockPanel.ActivatePanel();
        }
        else
        {
            ScreenshotButton.interactable = true;
            LockPanel.DeactivatePanel();
        }
    }

    public void OnClickCanvas(BaseEventData data)
    {
        if (PinchZoom.isPinchZooming || UIDraggable.IsDragging)
            return;

        var pointerClickData = (data as PointerEventData);

        Texture2D tex = Duplicate;
        Rect r = activeBuffer.rectTransform.rect;
        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(activeBuffer.rectTransform, pointerClickData.position, pointerClickData.pressEventCamera, out localPoint);

        int px = Mathf.Clamp(0, (int)(((localPoint.x - r.x) * tex.width) / r.width), tex.width);
        int py = Mathf.Clamp(0, (int)(((localPoint.y - r.y) * tex.height) / r.height), tex.height);

        localPoint = new Vector2(px, py);
        Texture2dUtilities.DoFloodFill(ref Duplicate, ref Colors, localPoint, ref OutLine,
            ColorPrefab.CurrentColor);
        AudController.TriggerAudio("ColorSound");
        ParticleManager.Instance.EmitParticle("click", Input.mousePosition);
        DisplayCanvas(true);




    }

    public void OnClickScreenShot()
    {
        if (LockPanel.IsPanelActive)
            return;
        if (NativeGallery.CheckPermission() == NativeGallery.Permission.Granted)
        {
            FlashPanel.GetCanvasGroup.DOFade(1, 0.15f)
           .SetLoops(2, LoopType.Yoyo)
           .SetEase(FlashEaseType)
           .OnStart(() => FlashPanel.ActivateControl())
           .OnComplete(() =>
           {
               PicToGalleryAnim();
               FlashPanel.DeactivateControl();
           });
            FillWhite();
            var finalTexture = Duplicate.AlphaBlend(WaterMarkTexture);
            var result = NativeGallery.SaveImageToGallery(finalTexture.EncodeToJPG(), Application.productName, Application.productName + "_" + System.DateTime.UtcNow.Ticks.ToString() + ".jpg");
        }
        else
        {
            UIPopupBox.Instance.SetDataOk("Please grant storage permission to save screenshot.\n Open Settings->Kidzooly App->Permissions->Storage to enable permission", null);
        }
    }


    public void GotoStore()
    {
        UILoader.Instance.StartLoader();
        AudController.TriggerAudio("OnLockClick", () =>
        {
            UILoader.Instance.StopLoader();
            FreeTrialsPanel.Instance.Init();
        });
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoad;
        Input.multiTouchEnabled = true;
        HardwareInputManager.OnBack += MainMenu;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoad;
        Input.multiTouchEnabled = false;
        FileManager.RemoveReference(this);
        HardwareInputManager.OnBack -= MainMenu;
    }


    void Start()
    {
        premiumStatus = LocalDataManager.Instance.SaveData.IsPremiumUser();
        FetchNewImages();
        PopulateColorPalette();
    }

    public void MainMenu()
    {
        if (UILoader.Instance.IsPanelActive)
        {
            return;
        }
        AudController.TriggerAudio("OnClick");
        UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());
        PerformanceManager.Instance.IncrementSuccess();
        double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
        FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
        AnalyticsScript.instance.LogEvent("activity_session_time", "coloring", totalTimeSpent);
    }

    public void PicToGalleryAnim()
    {
        var bf1 = Buffer1.GetComponent<UIPanel>();
        var bf2 = Buffer2.GetComponent<UIPanel>();
        GameObject currImage;
        if (bf1.IsPanelActive)
        {
            currImage = bf1.gameObject;
        }
        else
        {
            currImage = bf2.gameObject;
        }
        var go = Instantiate(currImage, SnapshotParentObject.transform, true);
        var panel = go.GetComponent<UIPanel>();
        panel.CallBackOnPanelDeactivate += () => { Destroy(go); };
        panel.ActivatePanel();
        GalleryTransform.GetComponent<Image>().DOFade(1, 0.5f);
        go.transform.localScale = new Vector3(1, 1, 1);
        Sequence snapshotSequence = DOTween.Sequence();
        snapshotSequence.Append(go.transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), 1f));
        snapshotSequence.Join(go.transform.DOLocalMove(GalleryTransform.localPosition, 1f));
        snapshotSequence.Join(go.GetComponent<CanvasGroup>().DOFade(0f, 1f));
        snapshotSequence.OnComplete(() =>
        {
            panel.DeactivatePanel();
            GalleryTransform.GetComponent<Image>().DOFade(0, 0.5f);
            UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnSaveImage") ?? "Your masterpiece is saved!", 1f);
        });
        snapshotSequence.Play();
    }
}