﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ColorPrefab : MonoBehaviour
{
    public Image PaletteColor;
    public static Color32 CurrentColor;
    public AudioController Aud;
    public GameObject toggleHighlight;
    public Shadow BucketShadow;
    private Sequence _bounceTweener = null;

    public void SetColor(Color32 color)
    {
        PaletteColor.color = color;

        _bounceTweener = DOTween.Sequence();

        _bounceTweener.Append(transform.DOPunchScale(new Vector3(0.12f, 0.12f), 0.7f, 1, 0.5f))
            .SetLoops(-1);
        DOTween.ToAlpha(() => BucketShadow.effectColor, x => BucketShadow.effectColor = x, 0,0);
    }

    public void OnValueChanged(bool value)
    {
        if (value)
        {
            CurrentColor = PaletteColor.color;
            toggleHighlight.Activate();
            Aud.TriggerAudio("OnSelection");
            DOTween.ToAlpha(() => BucketShadow.effectColor, x => BucketShadow.effectColor = x, 0.7f, 0.2f);
            if (_bounceTweener != null)
            {
                _bounceTweener.Restart();
            }
        }
        else
        {
            DOTween.ToAlpha(() => BucketShadow.effectColor, x => BucketShadow.effectColor = x, 0, 0.2f);
            toggleHighlight.Deactivate();
            if (_bounceTweener != null)
            {
                _bounceTweener.Pause();
                transform.DOScale(Vector3.one, 0);
            }
        }
    }
}