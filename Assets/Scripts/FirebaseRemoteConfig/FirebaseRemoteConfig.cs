﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class FirebaseRemoteConfig : GenericSingleton<FirebaseRemoteConfig>
{

    private Dictionary<string, string> RemoteConfigData = new Dictionary<string, string>();
    private Dictionary<string, object> defaults = new Dictionary<string, object>();

    private Action<Dictionary<string,string>> localUpdatedValues;

    public void Initiate(Action<Dictionary<string,string>> updatedValues)
    {
        #if !AMAZON_STORE
        this.localUpdatedValues = updatedValues;
        #elif AMAZON_STORE
            updatedValues(new Dictionary<string, string>());
        #endif
#if !AMAZON_STORE
        //FerrisWheelController.Instance.OnFirebaseInitDone += InitializeFirebase;
        FirebaseInit.OnFirebaseInitDone += InitializeFirebase;
#endif

    }

    private void InitializeFirebase()
    {
        // These are the values that are used if we haven't fetched data from the
        // service yet, or if we ask for values that the service doesn't have:

        defaults.Add("Item_1", "kidzooly.subscription.3month");
        defaults.Add("Item_2", "kidzooly.subscription.6month");
        defaults.Add("Item_3", "kidzooly.subscription.1year");
#if !AMAZON_STORE
        Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);

        FetchDataAsync();
#endif
    }

    /// <Remote_config_Start>

    // Display the currently loaded data.  If fetch has been called, this will be
    // the data fetched from the server.  Otherwise, it will be the defaults.
    // Note:  Firebase will cache this between sessions, so even if you haven't
    // called fetch yet, if it was called on a previous run of the program, you
    //  will still have data from the last time it was run.

    private void DisplayData()
    {
        Debug.Log("Current Data:");
        Debug.Log("config_test_string: " +
                 Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("config_test_string").StringValue);
        Debug.Log("config_test_int: " +
                 Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("config_test_int").LongValue);
        Debug.Log("config_test_float: " +
                 Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("config_test_float").DoubleValue);
        Debug.Log("config_test_bool: " +
                 Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("config_test_bool").BooleanValue);
    }

    private void DisplayAllKeys()
    {
        Debug.Log("Current Keys:");
        System.Collections.Generic.IEnumerable<string> keys =
            Firebase.RemoteConfig.FirebaseRemoteConfig.Keys;

        foreach (string key in keys)
        {
            Debug.Log("    " + key + "   " + defaults.ContainsKey(key) + "   " + Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(key).StringValue);

            if (defaults.ContainsKey(key)) { 
                RemoteConfigData.Add(defaults[key].ToString(), Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(key).StringValue);
            }
            
        }

        localUpdatedValues(RemoteConfigData);
    }

    // Start a fetch request.
    private Task FetchDataAsync()
    {
        Debug.Log("Fetching data...");
        // FetchAsync only fetches new data if the current data is older than the provided
        // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
        // By default the timespan is 12 hours, and for production apps, this is a good
        // number.  For this example though, it's set to a timespan of zero, so that
        // changes in the console will always show up immediately.
        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(
            TimeSpan.Zero);
        return fetchTask.ContinueWith(FetchComplete);
    }

    void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            Debug.Log("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            Debug.Log("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            Debug.Log("Fetch completed successfully!");
        }

        var info = Firebase.RemoteConfig.FirebaseRemoteConfig.Info;
        switch (info.LastFetchStatus)
        {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();
                Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).",
                                       info.FetchTime));
                DisplayAllKeys();
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                switch (info.LastFetchFailureReason)
                {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.Log("Fetch failed for unknown reason");
                        break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                        break;
                }
                localUpdatedValues(RemoteConfigData);
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.Log("Latest Fetch call still pending.");
                localUpdatedValues(RemoteConfigData);
                break;
        }
    }

    /// </Remote_Config_End>

}
