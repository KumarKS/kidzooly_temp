﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class FCMclass : MonoBehaviour
{
#if !AMAZON_STORE
    // Start is called before the first frame update
    void Start()
    {
        Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
        string topic = "kz_all";
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(tassk =>
      {
          if (tassk.IsFaulted || tassk.IsCanceled)
          {
              Debug.Log("is faulted");
          }
          dependencyStatus = tassk.Result;
          if (dependencyStatus == Firebase.DependencyStatus.Available)
          {
              Debug.Log("Firebase cloud messaging Dependencies resolved");
              Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
              Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
              Firebase.Messaging.FirebaseMessaging.SubscribeAsync(topic).ContinueWith(task =>
              {
                  LogTaskCompletion(task, "SubscribeAsync");
              });
          }
          else
          {
              Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
              // 						Firebase Unity SDK is not safe to use here.
          }
      });
    }

    private void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {

    }

    private void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        Debug.Log("Received Registration Token: " + token.Token);
        FirebaseAPI.Instance.fcmToken = token.Token;
    }
    protected bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string errorCode = "";
                Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                if (firebaseEx != null)
                {
                    errorCode = String.Format("Error.{0}: ",
                      ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
                }
                Debug.Log(errorCode + exception.ToString());
            }
        }
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed");
            complete = true;
        }
        return complete;
    }
#endif
}
