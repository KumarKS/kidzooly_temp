﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppIcon : MonoBehaviour
{
    public Text AppNameText;
    public Button ClickButton;
    public UIRemoteImage ThumbnailImage;
    public GameData.OtherAppData Data;
	public bool askParentalControls = false;


	public void SetData(string appName, string thumbnailURL, string gLink, string iLink, string amazonLink, bool askParentalControls = false)
    {
		Data = new GameData.OtherAppData(appName, thumbnailURL, gLink, iLink, amazonLink);

        AppNameText.SetText(Data.AppName);
		ThumbnailImage.SetUrl(Data.ThumbnailLink);

        if (ClickButton)
        {
            ClickButton.onClick.RemoveAllListeners();
            ClickButton.onClick.AddListener(OnClick);
        }
		this.askParentalControls = askParentalControls;
    }

    void OnClick()
    {
		if (askParentalControls)
		{
			ParentalLockSystem.Instance.InitializeLock (() =>
			{
				OpenLink ();
			});
		}
		else
		{
			OpenLink ();
		}
    }

	private void OpenLink()
	{
		#if AmazonStoreBuild
		string activeLink = Data.AmazonAppsLink;
		#elif AppleStoreBuild
		string activeLink = Data.AppStoreLink;
		#else
		string activeLink = Data.GooglePlayLink;
		#endif

		if (!string.IsNullOrEmpty(activeLink))
		{
			Application.OpenURL(activeLink);
			VGMinds.Analytics.AnalyticsManager.LogLinkClick(Data.AppName, "Promo App");
		}
	}
}