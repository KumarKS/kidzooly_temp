﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherAppUI : UIPanel
{
    [Header("Other APP UI")]
    public GameObject AppIconPrefab;

    public Transform AppContainer;
    private GameData.OtherAppCollection _otherAppCollection;
    private List<GameData.OtherAppData> _otherAppDatas;
	private bool askParentalControls = false;
	public static OtherAppUI Instance;

    protected override void Awake()
    {
        base.Awake();
		Instance = this;
        CallBackOnPanelActivate += () => { StartCoroutine(PopulateOtherApps()); };
        CallBackOnPanelDeactivate += () => { AppContainer.gameObject.DestroyChildren(); };
    }

    
    void Start()
    {
        GetOtherAppsData();
    }

    void GetOtherAppsData()
    {
        _otherAppCollection = LocalDataManager.Instance.GetConfig<GameData.OtherAppCollection>(EConfigFileName.OtherApps);
        _otherAppDatas = new List<GameData.OtherAppData>(_otherAppCollection.OtherApps);
        //StartCoroutine(PopulateOtherApps());
    }


    IEnumerator PopulateOtherApps()
    {
        UILoader.Instance.StartLoader();
        yield return new WaitForSeconds(0.5f);
        foreach (var otherAppData in _otherAppDatas)
        {
            var icon = GameObject.Instantiate(AppIconPrefab);
            icon.transform.SetParent(AppContainer, false);

            var iconData = icon.GetComponent<AppIcon>();
            if (iconData)
            {
				iconData.SetData(otherAppData.AppName, otherAppData.ThumbnailLink, otherAppData.GooglePlayLink, otherAppData.AppStoreLink, otherAppData.AmazonAppsLink, askParentalControls);
            }
            yield return true;
        }

        UILoader.Instance.StopLoader();
    }

	public void OpenPanel (bool askParentalControls)
	{
		this.askParentalControls = askParentalControls;
		ActivatePanel ();
	}
}