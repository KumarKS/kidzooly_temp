﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FerrisCabin : UIPanel
{
    public Image Thumbnail;
	public Text Name;
    public List<GameObject> Characters;
    public Sprite sprite;

    public void SetImage(Sprite sprite)
    {
        this.Thumbnail.sprite = sprite;
        this.sprite = sprite;
    }

    public void ReloadImage()
    {
        this.Thumbnail.sprite = null;

        this.Thumbnail.sprite = sprite;
    }

    void OnDisable()
    {
        FileManager.RemoveReference(this);
    }

    protected override void Update()
    {
        transform.parent.rotation = Quaternion.identity;
    }
}