﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class BlinkingStars : MonoBehaviour
{
    private Image _starImage;


    void Start()
    {
        _starImage = GetComponent<Image>();

        _starImage.DOFade(0f, 0);
        _starImage.rectTransform.DORotate(new Vector3(0, 0, Random.Range(-90, 90)), 0);
        _starImage.rectTransform.DOScale(Random.Range(0.5f, 1), 0);

        _starImage.DOFade(Random.Range(0.1f, 0.3f), 0.3f).SetDelay(1).OnComplete(() => { _starImage.DOFade(0.6f, Random.Range(0.8f, 1.5f)).SetEase(Ease.InOutBack).SetLoops(-1, LoopType.Yoyo); });
    }

    void OnDisable()
    {
        DOTween.Kill(_starImage);
    }
}