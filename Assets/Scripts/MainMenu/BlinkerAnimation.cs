﻿using DG.Tweening;
using UnityEngine;

public class BlinkerAnimation : MonoBehaviour
{


    void Start()
    {
        var blinkers = gameObject.GetFirstLevelChildren();
        var seq = DOTween.Sequence();

        seq.AppendInterval(0.15f).AppendCallback(() =>
        {
            blinkers[0].Activate();
            blinkers[3].Activate();


            blinkers[2].Deactivate();
            blinkers[1].Deactivate();

        }).AppendInterval(0.15f).AppendCallback(() =>
        {
            blinkers[0].Deactivate();
            blinkers[3].Deactivate();


            blinkers[2].Activate();
            blinkers[1].Activate();
        }).AppendInterval(0.15f);
        seq.Play().SetLoops(-1, LoopType.Yoyo);

    }


}
