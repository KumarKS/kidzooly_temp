﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class SkyPainter : MonoBehaviour
{
    public Material Gradient;
    public GameObject StarContainer;
    static readonly List<string> MorningColors = new List<string>() {"#c7effb", "#d3f2fb"};
    static readonly List<string> NightColors = new List<string>() {"#052339", "#0e2133"};

    void Start()
    {
        var color1 = Color.white;
        var color2 = Color.white;
        var colorPalette = new List<string>();

        if (DateTime.Now.Hour >= 6 && DateTime.Now.Hour <= 18)
        {
            colorPalette = MorningColors;
            StarContainer.Deactivate();
            StarContainer.GetComponent<CanvasGroup>().DOFade(0, 0);
        }
        else
        {
            colorPalette = NightColors;
            StarContainer.Activate();
            StarContainer.GetComponent<CanvasGroup>().DOFade(0, 0.3f).From();
        }


        ColorUtility.TryParseHtmlString(colorPalette.Take(2).First(), out color1);
        ColorUtility.TryParseHtmlString(colorPalette.Take(2).Last(), out color2);
        Gradient.SetColor("_Color", color1);
        Gradient.SetColor("_Color2", color2);
    }
}