﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DG.Tweening;
using UnityEngine;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
using UnityEngine.Purchasing.Security;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsUI : UIPanel
{
    [Header("Settings UI")]
    public Button MusicButton;

    public Button DownloadToggler;
    public Sprite streamingIcon, downloadIcon;

    public Button SoundButton;
    public Button ClearCacheButton;
    public Button LangaugeButton;
    public Button StreamButton;
    public Button AutoplayButton;
    public Button CloseButton;
    public Button OptionsSliderButton;
    public Button SubscriberButton;
    public Button RestoreButton;
    public GameObject SubscriptionGO, RestoreGO, SettingsPanelDetector;
    public LocalizationUI Localization;
    public UIPanel SettingsContainer;
    public OtherAppUI OtherAppContaner;
    public UIPanel RateUsContainer;
    public UIPanel SubscriberStatus;
    public UIPanel LoveusPanel;
    public Text SubscriptionText;
    public RectTransform OptionsPanel;
    public Transform[] RateUsStars;
    public Text Header;
    public Text AppVersion;
    private bool isOptionsPanelVisible = true;
    public string FacebookLink;
    public string YoutubeLink;
    public string WebsiteLink;
    public string FaqLink;
    public string iOSAppId = "1247374119";

    void AutoSlideOutSettingsSlider()
    {
        KillAutoSlide();
        DOTween.Sequence().AppendInterval(5).AppendCallback(() => SlideOptionsPanel(true)).SetId("AutoSlideOutSettingsSlider").Play().SetAutoKill(true).SetId("AutoSlideOutSettingsSlider");
    }

    public void OnPrivacyButtonClicked()
    {
        Application.OpenURL("http://www.vgminds.com/privacy.html");
    }
    void KillAutoSlide()
    {
        DOTween.Kill("AutoSlideOutSettingsSlider");
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoad;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoad;
    }

    private void OnSceneLoad(Scene arg0, LoadSceneMode arg1)
    {
        if (string.Equals(arg0.name, EScenes.MainMenu.GetDescription()))
        {
            DownloadToggler.GetComponent<Image>().sprite = LocalDataManager.Instance.SaveData.AllowDownload ? downloadIcon : streamingIcon;
        }
        AppVersion.text = Application.version;
    }

    void Start()
    {
        ReflectSettings();
        MusicButton.onClick.RemoveAllListeners();
        MusicButton.onClick.AddListener(OnClickMusic);
        MusicButton.onClick.AddListener(ReflectSettings);

        SoundButton.onClick.RemoveAllListeners();
        SoundButton.onClick.AddListener(OnClickSound);
        SoundButton.onClick.AddListener(ReflectSettings);

        ClearCacheButton.onClick.RemoveAllListeners();
        ClearCacheButton.onClick.AddListener(OnClickClearCache);

        StreamButton.onClick.RemoveAllListeners();
        StreamButton.onClick.AddListener(OnClickStream);
        StreamButton.onClick.AddListener(ReflectSettings);

        DownloadToggler.onClick.RemoveAllListeners();
        DownloadToggler.onClick.AddListener(OnClickStream);
        DownloadToggler.onClick.AddListener(ReflectSettings);

        AutoplayButton.onClick.RemoveAllListeners();
        AutoplayButton.onClick.AddListener(OnClickAutoplay);
        AutoplayButton.onClick.AddListener(ReflectSettings);

        LangaugeButton.onClick.RemoveAllListeners();
        LangaugeButton.onClick.AddListener(OnClickLanguage);

        CloseButton.onClick.RemoveAllListeners();
        CloseButton.onClick.AddListener(() =>
        {
            if (Localization.IsPanelActive)
            {
                Header.SetText("Settings");
                Localization.DeactivatePanel();
                SettingsContainer.ActivatePanel();
            }
            else if (SubscriberStatus.IsPanelActive)
            {
                Header.SetText("Settings");
                SubscriberStatus.DeactivatePanel();
                SettingsContainer.ActivatePanel();
            }
            else if (SettingsContainer.IsPanelActive)
            {
                DeactivatePanel();
                AutoSlideOutSettingsSlider();
            }
        });

        OptionsSliderButton.onClick.RemoveAllListeners();
        OptionsSliderButton.onClick.AddListener(() => SlideOptionsPanel());
        DOTween.Sequence().AppendInterval(0.5f).AppendCallback(() => SlideOptionsPanel()).SetId("OptionSlideOut").Play().SetAutoKill(true);

#if UNITY_ANDROID || !UNITY_IOS
        RestoreGO.Deactivate();
#else
        SubscriptionGO.Deactivate();
        RestoreButton.gameObject.Activate();
        RestoreButton.onClick.RemoveAllListeners();
        RestoreButton.onClick.AddListener(() =>
        {
            UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnRestorePurchase") ?? "Restoring your purchases...", 2);
            PurchaseManager.Instance.RestorePurchases(() => UIToastNotification.Instance.TriggerToast("Restore Successfull!", 5), () => UIToastNotification.Instance.TriggerToast("Restore Failed...", 5));
        });
#endif
    }

    void ReflectSettings()
    {
        MusicButton.GetComponent<SettingsItem>().isOn =
            LocalDataManager.Instance.SaveData.AudioLayerToggle[AudioController.EAudioLayer.Music];
        MusicButton.GetComponent<SettingsItem>().OnValueChange();

        SoundButton.GetComponent<SettingsItem>().isOn =
            LocalDataManager.Instance.SaveData.AudioLayerToggle[AudioController.EAudioLayer.Sound];
        SoundButton.GetComponent<SettingsItem>().OnValueChange();

        StreamButton.GetComponent<SettingsItem>().isOn = LocalDataManager.Instance.SaveData.AllowDownload;
        StreamButton.GetComponent<SettingsItem>().OnValueChange();

        AutoplayButton.GetComponent<SettingsItem>().isOn = LocalDataManager.Instance.SaveData.Autoplay;
        AutoplayButton.GetComponent<SettingsItem>().OnValueChange();

        LocalDataManager.Instance.Save();
    }

    public void OnClickFeedback()
    {
#if UNITY_ANDROID
        //         UIPopupBox.Instance.SetDataYesNo(LocalizedText.GetLocalizedText("OnDoULike") ?? @"Do you like Our APP - KIdzooly?.

        // Help us and Please take a few Seconds
        // to rate us By writing your Opinion.", OnClickRateUsDialog,
        //             () => { FeedbackSystem.Instance.Initialize(); }, EDialogBoxSizes.Small_Landscape);
        UIPopupBox.Instance.SetDataYesNo("Enjoying Kidzooly app ?", OnClickRateUsDialog,
            () =>
            {
                FeedbackSystem.Instance.Initialize();
            }, EDialogBoxSizes.Small_Landscape);
#elif UNITY_IOS
        Device.RequestStoreReview();
#endif
    }

    public void OnClickSubscriberStatus()
    {
        Header.SetText(LocalizedText.GetLocalizedText("SubscriptionStatusHeader") ?? "Subscription Status");
        SettingsContainer.DeactivatePanel();
        if (!LocalDataManager.Instance.SaveData.IsPremiumUser())
        {
            SubscriptionText.SetText(LocalizedText.GetLocalizedText("NotASubscriber") ?? "You are currently not a subscriber");
            SubscriberButton.onClick.RemoveAllListeners();
            SubscriberButton.onClick.AddListener(() => UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription()));
            SubscriberButton.GetComponentInChildren<Text>().SetText(LocalizedText.GetLocalizedText("Subscribe") ?? "Subscribe");
        }
        else
        {
            var dispText =
                (LocalizedText.GetLocalizedText("ActiveSubscriber") ?? "You are subscribed for TITLE from DATE").Replace("TITLE",
                    LocalDataManager.Instance.SaveData.SubscriptionDetails["Title"] ?? "N/A")
                .Replace("DATE", LocalDataManager.Instance.SaveData.SubscriptionDetails["Date"] ?? "N/A");


            SubscriptionText.SetText(dispText);
            SubscriberButton.onClick.RemoveAllListeners();
            SubscriberButton.onClick.AddListener(() => UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription()));
            SubscriberButton.GetComponentInChildren<Text>().SetText(LocalizedText.GetLocalizedText("ManageSubscription") ?? "Manage Subscription");
        }
        SubscriberStatus.ActivatePanel();
    }

    public void OnClickRateUsDialog()
    {
        if (RateUsContainer.IsPanelActive) return; //if already activated, then early exit

        AnalyticsScript.instance.LogEvent("rate_us", "clicked", "");

        RateUsContainer.ActivatePanel();
        Sequence starSequence = DOTween.Sequence();
        for (int i = 0; i < RateUsStars.Length; i++)
        {
            starSequence.Append(
                RateUsStars[i].DOLocalRotate(new Vector3(0, 0, -15f), 0.25f)
                    .SetLoops(-1, LoopType.Yoyo)
            );
            starSequence.Append(
                RateUsStars[i].DOLocalRotate(new Vector3(0, 0, 15f), 0.25f)
                    .SetLoops(-1, LoopType.Yoyo)
            );
        }
        starSequence.SetLoops(-1, LoopType.Yoyo);
        starSequence.Play();
    }

    public void OnClickSettingsPanel()
    {
        KillAutoSlide();
        DOTween.Kill("OptionSlideOut");
        ActivatePanel();
    }

    public void OnClickRateUs()
    {
        //##Added 
        //After sending them to market we store it as a string for future use.
        PlayerPrefs.SetInt("isRated", 1);
        //##

        DOTween.Kill("OptionSlideOut");
#if AmazonStoreBuild
		string StoreLink = string.Format("https://www.amazon.com/gp/mas/dl/android?p={0}", Application.identifier);
#elif AppleStoreBuild
        string StoreLink = string.Format("https://itunes.apple.com/us/app/apple-store/id{0}?mt=8", iOSAppId);
#else
        string StoreLink = string.Format("https://play.google.com/store/apps/details?id={0}", Application.identifier);
#endif
        Application.OpenURL(StoreLink);
    }

    public void OnClickLikeUs()
    {
        DOTween.Kill("OptionSlideOut");
        Application.OpenURL(FacebookLink);
        VGMinds.Analytics.AnalyticsManager.LogLinkClick("Facebook");
    }

    public void OnClickWatchUs()
    {
        DOTween.Kill("OptionSlideOut");
        Application.OpenURL(YoutubeLink);
        VGMinds.Analytics.AnalyticsManager.LogLinkClick("YouTube");
    }

    public void OnClickVisitUs()
    {
        DOTween.Kill("OptionSlideOut");
        Application.OpenURL(WebsiteLink);
        VGMinds.Analytics.AnalyticsManager.LogLinkClick("Website");
    }

    public void OnClickFAQ()
    {
        DOTween.Kill("OptionSlideOut");
        KillAutoSlide();
        UIPopupBox.Instance.SetDataYesNo(LocalizedText.GetLocalizedText("OnNotWorking") ?? "Is Kidzooly not working the way it should?\nor \nDo you have any Questions?", () =>
        {
            AutoSlideOutSettingsSlider();
            Application.OpenURL(FaqLink);

        },
            AutoSlideOutSettingsSlider);
        VGMinds.Analytics.AnalyticsManager.LogFAQVisit();
    }

    public void OnClickCart()
    {
        DOTween.Kill("OptionSlideOut");
        FreeTrialsPanel.Instance.Init(false);
    }

    public void OnClickMoreGames()
    {
        AutoSlideOutSettingsSlider();
        if (OtherAppContaner)
        {
            DOTween.Kill("OptionSlideOut");
            OtherAppContaner.OpenPanel(false);
        }
    }

    private void OnClickLanguage()
    {
        SettingsContainer.DeactivatePanel();
        Localization.ActivatePanel();
        Header.SetText("Languages");
    }

    void OnClickMusic()
    {
        LocalDataManager.Instance.SaveData.AudioLayerToggle[AudioController.EAudioLayer.Music] =
            !LocalDataManager.Instance.SaveData.AudioLayerToggle[AudioController.EAudioLayer.Music];

        UILoader.Instance.StartLoader();
        ConfigManager.Instance.FetchLocalizedAudio(() =>
        {
            UILoader.Instance.StopLoader();
        }, () =>
        {
            UILoader.Instance.StopLoader();
        }, (x) => Debug.Log(x));
    }

    void OnClickStream()
    {
        LocalDataManager.Instance.SaveData.AllowDownload =
            !LocalDataManager.Instance.SaveData.AllowDownload;
        DownloadToggler.GetComponent<Image>().sprite = null;
        DownloadToggler.GetComponent<Image>().sprite = LocalDataManager.Instance.SaveData.AllowDownload ? downloadIcon : streamingIcon;
        if (LocalDataManager.Instance.SaveData.AllowDownload)
            UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("DownloadEnabled") ?? "Download Enabled", 3);
        else
            UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("DownloadDisabled") ?? "Streaming Enabled", 3);
    }

    void OnClickAutoplay()
    {
        LocalDataManager.Instance.SaveData.Autoplay =
            !LocalDataManager.Instance.SaveData.Autoplay;
    }

    void OnClickSound()
    {
        LocalDataManager.Instance.SaveData.AudioLayerToggle[AudioController.EAudioLayer.Sound] =
            !LocalDataManager.Instance.SaveData.AudioLayerToggle[AudioController.EAudioLayer.Sound];
        UILoader.Instance.StartLoader();

        ConfigManager.Instance.FetchLocalizedAudio(() =>
        {
            UILoader.Instance.StopLoader();
        }, () =>
        {
            UILoader.Instance.StopLoader();
        }, (x) => Debug.Log(x));
    }

    void OnClickClearCache()
    {
        UIPopupBox.Instance.SetDataYesNo(LocalizedText.GetLocalizedText("OnEraseContent") ?? "Would you like to erase all downloaded Videos and Stories ?", () =>
        {
            FileManager.RemoveFolder(EFileType.Video);

            var urls = new List<string>() { };

            GameData.StoryCollection collection = LocalDataManager.Instance.GetConfig<GameData.StoryCollection>(EConfigFileName.Stories);

            var storyPageBGUrls = collection.Stories.Select(x => x.PageBackgroundUrl).ToList();
            var storyBookCoverUrls = collection.Stories.Select(x => x.BookCoverUrl).ToList();
            var storyBookCoverLeftPageUrls = collection.Stories.Select(x => x.BookCoverLeftPageUrl).ToList();
            var storyVoiceUrls = collection.Stories.Select(x => x.VoiceOverUrl).ToList();

            urls.AddRange(storyPageBGUrls);
            urls.AddRange(storyBookCoverUrls);
            urls.AddRange(storyBookCoverLeftPageUrls);
            urls.AddRange(storyVoiceUrls);

            foreach (var item in collection.Stories)
            {
                urls.AddRange(item.PageList.Select(x => x.ImageUrl));
            }

            foreach (var item in urls)
            {
                if (FileManager.IsFileDownloaded(item))
                    FileManager.RemoveFile(item);
            }

        }, null);
    }

    public void OnClickLoveUsPanel()
    {
        if (LoveusPanel.IsVisible)
        {
            AutoSlideOutSettingsSlider();
            LoveusPanel.DeactivatePanel();
        }
        else
        {
            KillAutoSlide();
            LoveusPanel.ActivatePanel();
        }
    }

    public void SlideOptionsPanel(bool forceClose = false)
    {
        if (forceClose)
        {
            OptionsPanel.DOAnchorPosX(0, 0.5f);//.SetDelay(0.1f).OnComplete(() => OptionsSliderButton.transform.DOScale(1.2f, 0.2f).SetEase(Ease.InOutBack));
            isOptionsPanelVisible = false;
            return; //early exit if force close
        }

        if (!isOptionsPanelVisible)
        {
            var seq = DOTween.Sequence();

            seq.AppendCallback(() =>
            {
                seq.Pause();
                ParentalLockSystem.Instance.InitializeLock(() =>
                {
                    seq.Play();
                    AutoSlideOutSettingsSlider();
                    SettingsPanelDetector.SetActive(true);
                    isOptionsPanelVisible = true;
                }, () =>
                {
                    OptionsPanel.DOAnchorPosX(0, 0.5f);
                        //OptionsSliderButton.transform.DOScale(1.2f, 0.2f).SetEase(Ease.OutBack);
                        SettingsPanelDetector.SetActive(false);
                    isOptionsPanelVisible = false;
                });
            })
            .Append(OptionsPanel.DOAnchorPosX(-(OptionsPanel.rect.width + (OptionsPanel.rect.width / 7)), 0.5f).SetDelay(0.1f));

            seq.Play();
        }
        else
        {
            OptionsPanel.DOAnchorPosX(0, 0.5f);//.SetDelay(0.1f).OnComplete(() => OptionsSliderButton.transform.DOScale(1.2f, 0.2f).SetEase(Ease.InOutBack));
            SettingsPanelDetector.SetActive(false);
            isOptionsPanelVisible = false;
        }
    }
}