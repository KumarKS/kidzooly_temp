﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LocalizationUI : UIPanel
{
    public GameObject LanguagePrefab;
    public GameObject Container;
    private GameData.LocalizationData ConfigData;

    void Start()
    {
        InitLanguages();
    }


    void InitLanguages()
    {
        if (ConfigData == null)
        {
            ConfigData = LocalDataManager.Instance.GetConfig<GameData.LocalizationData>(EConfigFileName.LocalizationConfig);
            Debug.Log(ConfigData);
        }
            

        Container.DestroyChildren();

        //Adding Default Language
        var obj = Container.AddChild(LanguagePrefab);
        obj.GetComponentInChildren<Text>().text = GameConstants.DEFAULT_LANGUAGE;
        obj.name = GameConstants.DEFAULT_LANGUAGE;
        if (string.Equals(LocalDataManager.Instance.SaveData.CurrentLanguage, GameConstants.DEFAULT_LANGUAGE))
            obj.GetComponent<Image>().color = Color.green;
        else
            obj.GetComponent<Image>().color = Color.white;

        obj.GetComponent<Button>().onClick.AddListener(() => OnChangeLanguage(obj));


        foreach (var language in ConfigData.Languages)
        {
            var go = Container.AddChild(LanguagePrefab);
            go.GetComponentInChildren<Text>().text = language;
            go.name = language;
            if (string.Equals(LocalDataManager.Instance.SaveData.CurrentLanguage, language))
                go.GetComponent<Image>().color = Color.green;
            else
                go.GetComponent<Image>().color = Color.white;

            go.GetComponent<Button>().onClick.AddListener(() => OnChangeLanguage(go));
        }




    }

    private void OnChangeLanguage(GameObject go)
    {
        var backup = LocalDataManager.Instance.SaveData.CurrentLanguage;
        if (!string.Equals(LocalDataManager.Instance.SaveData.CurrentLanguage, go.name))
        {
            LocalDataManager.Instance.SaveData.CurrentLanguage = go.name;
            InitLanguages();
            ConfigManager.OnLocalizationTextLoaded.SafeInvoke();
            if (ConfigData.LocalizationAudioBindings.Any())
            {
                UILoader.Instance.StartLoader();
                ConfigManager.Instance.FetchLocalizedAudio(() =>
                {
                    UILoader.Instance.StopLoader();
                    InitLanguages();

                }, () =>
                {
                    UILoader.Instance.StopLoader();
                    LocalDataManager.Instance.SaveData.CurrentLanguage = backup;
                    InitLanguages();
                    ConfigManager.OnLocalizationTextLoaded.SafeInvoke();
                });
            }
        }

        VGMinds.Analytics.AnalyticsManager.LogLanguageSelected(LocalDataManager.Instance.SaveData.CurrentLanguage);
        LocalDataManager.Instance.Save();
    }
}
