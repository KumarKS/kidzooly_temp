using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FerrisWheelController : GenericSingleton<FerrisWheelController>
{

    public static string ACTIVITY_NAME = "";
    public static double ACTIVITY_START_TIME = 0.0;
    public static double ACTIVITY_SPENT_TIME = 0.0;

    //FerrisWheelController instance;
    private int NumberOfActivities
    {
        get
        {
            return Menuitems.Count;
        }
    }

    private int OtherAppsSpoke = 1;
    public Sprite otherAppsImage;

    private int TotalSpokes
    {
        get
        {
            return NumberOfActivities + OtherAppsSpoke;
        }
    }

    public GameObject ObjectPoolContainer;
    public GameObject FerrisArmPrefab;
    public GameObject StarPrefab;
    public GameObject InnerContainer;
    public RectTransform WheelCenter;
    public List<Sprite> ThumbnailImages = new List<Sprite>();
    public static List<FerrisCabin> Cabins = new List<FerrisCabin>();
    public Rigidbody2D RigidBody;
    public AudioController AudController;
    //ferris rotate
    private Vector3 followVector = new Vector3();
    public Action OnFirebaseInitDone;

    protected override bool IsPersistant
    {
        get { return true; }
    }

    public Vector3 startPos { get; private set; }

    public static readonly List<KeyValuePair<string, EScenes>> Menuitems = new List<KeyValuePair<string, EScenes>>()
    {
        new KeyValuePair<string, EScenes>("Colouring", EScenes.ColoringActivity),
        new KeyValuePair<string, EScenes>("Scratch It", EScenes.ScratchingActivity),
        new KeyValuePair<string, EScenes>("Join Dots", EScenes.JoinDotsActivity),
        new KeyValuePair<string, EScenes>("Tracing", EScenes.TracingActivity),
        new KeyValuePair<string, EScenes>("Learning", EScenes.LearningWheel),
        new KeyValuePair<string, EScenes>("Puzzle", EScenes.PuzzleActivity),
        new KeyValuePair<string, EScenes>("Rhymes", EScenes.Video),
        new KeyValuePair<string, EScenes>("Shows", EScenes.Video),
        new KeyValuePair<string, EScenes>("Story", EScenes.Story),
        new KeyValuePair<string, EScenes>("Shadow", EScenes.ShadowMatch),
        new KeyValuePair<string, EScenes>("InteractiveRhyme", EScenes.InteractiveRhyme),
    };


    private bool _canclick;
    private static bool isLoadAnimationOver = false;
    private static Vector3 DefaultPos = Vector3.zero;
    private float baseAngle;
    private bool isSoundPlaying = false;
    //Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
    void DoUIHack()
    {
        float r = Camera.main.aspect;
        string _r = r.ToString("F2");
        string ratio = _r.Substring(0, 4);

        switch (ratio)
        {
            case "1.33": //4:3                   
            case "1.50": //3:2   
            case "0.67":
                {
                    //2:3
                    WheelCenter.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                    WheelCenter.anchoredPosition = new Vector2(WheelCenter.anchoredPosition.x, -750);
                    break;
                }
            case "0.56":
                {
                    //9:16
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoad;
        AudController.OnControllerInit += () =>
        {
            if (isLoadAnimationOver) AudController.TriggerAudio("OnGiantWheelRotate");
        };
        HardwareInputManager.OnBack += OnBack;
    }


    void OnDisable()
    {
        HardwareInputManager.OnBack -= OnBack;
        AudController.OnControllerInit = null;
    }

    private void OnBack()
    {
        Application.Quit();
    }

    private void OnSceneLoad(Scene arg0, LoadSceneMode arg1)
    {
        if (string.Equals(arg0.name, EScenes.MainMenu.GetDescription()))
        {
            if (!string.IsNullOrEmpty(ACTIVITY_NAME))
                UpdatePerformance();

            this.Activate();
            GetComponent<Canvas>().worldCamera = Camera.main;
            SplashLoader.Instance.StopLoader();
            RigidBody.bodyType = RigidbodyType2D.Dynamic;
            GetComponent<UIPanel>().ActivatePanel();
            if (isLoadAnimationOver && AudController.IsInitialized)
                AudController.TriggerAudio("OnGiantWheelRotate");
        }
        else
        {
            if (RigidBody != null)
            {
                RigidBody.bodyType = RigidbodyType2D.Static;
                this.Deactivate();
            }
        }
    }

    void UpdatePerformance()
    {
        //Performance manager
        PerformanceManager.Instance.SetSpentTime(ACTIVITY_SPENT_TIME);
        string act_name = ACTIVITY_NAME;
        // main_act_name = SceneManager.GetActiveScene().name.ToLower();
        PerformanceManager.Instance.CheckSubSuccess(act_name);
        PerformanceManager.Instance.SetSelectedActivity(ACTIVITY_NAME);
        PerformanceManager.Instance.SaveData();
        PerformanceManager.Instance.Clear();


        // DelayPopUp();
        ACTIVITY_NAME = string.Empty;
        ACTIVITY_SPENT_TIME = 0.0;
    }

    void DelayPopUp()
    {
        Debug.LogError("DElay POPUP : " + PerformanceManager.Instance.GetClassification());
        string nam = ACTIVITY_NAME;

        string txt = "Activity Name : " + ACTIVITY_NAME.ToUpper() + "\n"
        + "Time Spent : " + PerformanceManager.Instance.GetTotalTime(nam).ToString() + "\n"
        + "Total Attempts : " + PerformanceManager.Instance.GetTotalAttempts(nam).ToString() + "\n"
        + "Total Success :  " + PerformanceManager.Instance.GetTotalSuccess(nam).ToString() + "\n"

        + "Memory :  " + PerformanceManager.Instance.GetMemory().ToString() + "\n"
        + "Observe :  " + PerformanceManager.Instance.GetObserve().ToString() + "\n"
        + "Exploring :  " + PerformanceManager.Instance.GetExploring().ToString() + "\n"
        + "Listen :  " + PerformanceManager.Instance.GetListen().ToString() + "\n"
        + "Analytical :  " + PerformanceManager.Instance.GetAnalytical().ToString() + "\n"
        + "Identification :  " + PerformanceManager.Instance.GetIdentification().ToString() + "\n"
        + "Classification :  " + PerformanceManager.Instance.GetClassification().ToString() + "\n"
        + "Solve :  " + PerformanceManager.Instance.GetSolve().ToString();

        // DialogBoxCreator.Instance.CreateNewInfoBox (txt, "Performance");

        UIPopupBox.Instance.SetDataOk(txt, null);
    }


    void DoOnLoadAnimation()
    {
        if (DefaultPos == Vector3.zero)
            DefaultPos = WheelCenter.GetComponent<RectTransform>().anchoredPosition3D;

        var uiPanel = GetComponent<UIPanel>();
        WheelCenter.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, -3000), 0);
        uiPanel.GetCanvasGroup.DOFade(0, 0);
        WheelCenter.GetComponent<RectTransform>().DOAnchorPos(DefaultPos, 2).SetEase(Ease.OutQuart).SetDelay(1).OnComplete(
            () =>
            {
                isLoadAnimationOver = true;
                AudController.TriggerAudio("OnGiantWheelRotate");

                var cabins = this.GetComponentsInChildren<FerrisCabin>();
                foreach (var item in cabins)
                {
                    item.ReloadImage();
                }

            });
        uiPanel.GetCanvasGroup.DOFade(1, 2).SetEase(Ease.OutQuart).SetDelay(1);
    }


    void SpawnSpokes()
    {
        Vector3 center = WheelCenter.GetChild(1).GetComponent<RectTransform>().position;
        var colors = new List<string>() { "#ff82c6", "#fdb712", "#3d68c6", "#c1e93b" };
        int x = 0;
        Cabins.ForEach(Destroy);
        Cabins.Clear();
        var radius = WheelCenter.GetChild(1).GetComponent<RectTransform>().rect.width / 2;
        radius *= GetComponent<RectTransform>().localScale.x;
        for (int i = 0; i < TotalSpokes; i++)
        {
            Vector3 pos = RandomCircle(i * (360 / TotalSpokes), center, radius);
            Quaternion rot = Quaternion.FromToRotation(Vector3.down, center - pos);
            GameObject child = null;
            if (ObjectPoolContainer.transform.childCount > 0)
            {
                child = ObjectPoolContainer.transform.GetChild(0).gameObject;
                child.Activate();

                child.transform.SetParent(WheelCenter.transform);
            }
            else
                child = WheelCenter.gameObject.AddChild(FerrisArmPrefab);

            child.transform.SetAsFirstSibling();
            child.transform.rotation = Quaternion.Euler(new Vector3(0, 0, rot.eulerAngles.z));
            child.transform.position = pos;
            var color = new Color();

            if (x > colors.Count - 1)
                x = 0;

            ColorUtility.TryParseHtmlString(colors[x++], out color);
            child.transform.GetChild(1).GetComponent<Image>().color = color;
            var cabin = child.FindComponentInAllChildren<FerrisCabin>();

            Cabins.Add(cabin);

            var star = InnerContainer.AddChild(StarPrefab);
            pos = RandomCircle(i * (360 / TotalSpokes), center, radius * 0.65f);
            rot = Quaternion.FromToRotation(Vector3.down, center - pos);
            star.transform.position = pos;
            star.transform.rotation = rot;
            cabin.Name.fontSize = 75;
            if (i < NumberOfActivities)
            {
                var imageIndex = ThumbnailImages.FindIndex(img => img.name == Menuitems[i].Key);
                cabin.SetImage(ThumbnailImages[imageIndex]);
                cabin.Name.SetText(ThumbnailImages[imageIndex].name.SpacesFromCamel());
            }
            else
            {
                cabin.SetImage(otherAppsImage);
                cabin.Name.SetText("Promo");
            }
        }
        for (int i = 0; i < Cabins.Count; i++)
        {
            //thumbnail tween anim
            var chance = EnumeratorExtensions.RandomAmong("scale", "rotate");
            switch (chance)
            {
                case "scale":
                    {
                        Cabins[i].Thumbnail.transform.DOShakeScale(1.5f, new Vector3(0.04f, 0.06f, 0.03f), 5).SetLoops(-1, LoopType.Restart);
                    }
                    break;
                case "rotate":
                    {
                        Cabins[i].Thumbnail.transform.DOPunchRotation(new Vector3(0.03f, 0.04f, 10f), 1.5f, 5).SetLoops(-1, LoopType.Restart).SetEase(Ease.InOutBack);
                    }
                    break;
            }

        }
        int chIndex = 0;
        Cabins.PickRandom(Cabins[0].Characters.Count).ForEach(ch => ch.Characters[chIndex++].Activate());
        DoUIHack();
    }


    void Start()
    {
        SpawnSpokes();
        DoOnLoadAnimation();
#if UNITY_EDITOR
        if (GameData_Deserialize.instance)
        {
            Debug.Log("GameData_Deserialize Json Parsing... ");
            //GameData_Deserialize.instance.StartParsing();
        }
#endif

#if !AMAZON_STORE
        Debug.LogError("In 0" + PerformanceManager.Instance.GetDeviceId() + " : " + PerformanceManager.Instance.GetFIDId());
        //Performance Manager for new user
        if (string.IsNullOrEmpty(PerformanceManager.Instance.GetDeviceId()) && string.IsNullOrEmpty(PerformanceManager.Instance.GetFIDId()))
        {
            Debug.LogError("In New user00");
            /*InitializeFirebase*/
            FirebaseInit.InitializeFirebase((id) =>
{
    PerformanceManager.Instance.GetDataFromServer();
});

        }
        else if (!string.IsNullOrEmpty(PerformanceManager.Instance.GetDeviceId())) // old user
        {
            Debug.LogError("In Old user00 " + LocalStorage.GetSessionCount());
            /* InitializeFirebase*/
            FirebaseInit.InitializeFirebase((id) =>
            {
                //Performance Manager

                Debug.LogError("In Old user00 Inside " + LocalStorage.GetSessionCount());
                StartCoroutine(PerformanceManager.Instance.SaveDataToServer());

            });
        }
#endif
    }

    //void InitializeFirebase(Action<string> callback)
    //{
    //    Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
    //    {
    //        dependencyStatus = task.Result;
    //        if (dependencyStatus == Firebase.DependencyStatus.Available)
    //        {
    //            Firebase.Installations.FirebaseInstallations.DefaultInstance.GetIdAsync().ContinueWith(idTask =>
    //            {
    //                string device_id = idTask.Result;
    //                PerformanceManager.Instance.SetDeviceId(device_id);
    //                PerformanceManager.Instance.SetDeviceFID(device_id);
    //                Debug.LogError("MY UNIQUE DEVICE ID : " + device_id);

    //                callback(device_id);
    //            });

    //            OnFirebaseInitDone?.Invoke();

    //            if (GameData_Deserialize.instance)
    //            {
    //                Debug.Log("GameData_Deserialize Json Parsing... ");
    //                GameData_Deserialize.instance.StartParsing();
    //            }
    //        }
    //        else
    //        {
    //            Debug.LogError(
    //            "Could not resolve all Firebase dependencies: " + dependencyStatus);
    //        }
    //    });

    //}
    Vector3 RandomCircle(float angle, Vector3 center, float radius)
    {
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(angle * Mathf.Deg2Rad);
        pos.y = center.y + radius * Mathf.Cos(angle * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }

    void Update()
    {
        if (WheelCenter != null && AudController != null)
        {
            followVector = Vector3.LerpUnclamped(followVector, WheelCenter.rotation.eulerAngles, 0.8f);
            var diff = WheelCenter.rotation.eulerAngles - followVector;

            if (diff.sqrMagnitude <= float.Epsilon)
            {
                AudController.StopAudio("OnGiantWheelMove");
                isSoundPlaying = false;
            }
        }
    }


    public void OnBeginDrag(BaseEventData dt)
    {
        var dir = Camera.main.WorldToScreenPoint(WheelCenter.transform.position);
        dir = Input.mousePosition - dir;

        baseAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        baseAngle -= Mathf.Atan2(WheelCenter.transform.right.y, WheelCenter.transform.right.x) * Mathf.Rad2Deg;
        _canclick = false;
        AudController.StopAudio("OnGiantWheelRotate");
        DOTween.Kill("Ferris");
        RigidBody.bodyType = RigidbodyType2D.Static;
        followVector = WheelCenter.rotation.eulerAngles;
    }

    public void OnDrag(BaseEventData dt)
    {
        var dir = Camera.main.WorldToScreenPoint(WheelCenter.transform.position);
        dir = Input.mousePosition - dir;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - baseAngle;
        WheelCenter.transform.DORotate(Quaternion.AngleAxis(angle, Vector3.forward).eulerAngles, 0.5f).SetId("OnDrag");

        _canclick = false;

        if (!isSoundPlaying)
        {
            AudController.TriggerAudio("OnGiantWheelMove");
            isSoundPlaying = true;
        }
    }

    public void OnDragEnd(BaseEventData dt)
    {
        AudController.TriggerAudio("OnGiantWheelRotate");
        AudController.StopAudio("OnGiantWheelMove");
        isSoundPlaying = false;
        RigidBody.bodyType = RigidbodyType2D.Dynamic;
    }

    public void OnMouseDown()
    {
        _canclick = true;

        ParticleManager.Instance.EmitParticle("click", Input.mousePosition);
    }

    public void OnClick(Image thumbnail)
    {
        if (!_canclick) return;

        AudController.TriggerAudio("OnClick");

        var index = Menuitems.FindIndex(item => string.Equals(item.Key, thumbnail.sprite.name));
        if (index < 0)
        {
            if (OtherAppUI.Instance != null)
            {
                OtherAppUI.Instance.OpenPanel(false);
            }
        }
        else
        {
            GetComponent<UIPanel>().DeactivatePanel();
            //additional data for video player
            VideoPlayerDataManager.ContentType = Menuitems[index].Key.ToEnum(VideoContentTypes.Rhymes);
            ACTIVITY_NAME = Menuitems[index].Value.GetDescription();
            ACTIVITY_START_TIME = DateTime.Now.ToUnixTimeDouble();
            UISceneLoader.Instance.LoadScene(Menuitems[index].Value.GetDescription());

            //bug: causing small but noticing lag for first cabin click
            //bug fix: initialize the firebase on the splash screen
            VGMinds.Analytics.AnalyticsManager.LogActivityOpen(Menuitems[index].Value.GetDescription());
        }
    }
}