﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SettingsItem : MonoBehaviour
{
    public bool isOn = true;

    public Image OnImage;
    public Image OffImage;

    void Start()
    {
        OnValueChange();
    }

    public void OnValueChange()
    {
        if (isOn)
        {
            OffImage.DOFade(0, 0.1f);
            OnImage.DOFade(1, 0.1f);
        }
        else
        {
            OffImage.DOFade(1, 0.1f);
            OnImage.DOFade(0, 0.1f);
        }
    }
}
