﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkStatusUI : MonoBehaviour
{
    private VGInternet _irVerifier;

    public Image On, Off;
    public Text NetworkStatus;

    void Awake()
    {
        //deactivating
        NetworkStatus.Deactivate();
        On.Deactivate();
        Off.Deactivate();
    }

    void Start()
    {
		_irVerifier = VGInternet.Instance;
		_irVerifier.statusChanged += OnStatusChanged;
		OnStatusChanged (_irVerifier.Status);
    }

    void OnStatusChanged(bool status)
    {
		if (status)
		{
			SetStatus(true);
		}
		else
		{
			SetStatus(false);
			if (status)
			{
				UIToastNotification.Instance.TriggerToast("Seems you are Offline!!\nPlease check your Internet Connection.", 3.5f);
			}
		}
    }

    void SetStatus(bool active)
    {
        if (Off && On && NetworkStatus) //if all the component are available
        {
            if (active)
            {
                On.Activate();
                Off.Deactivate();
                NetworkStatus.SetText("Online");
            }
            else
            {
                On.Deactivate();
                Off.Activate();
                NetworkStatus.SetText("Offline");
            }
            NetworkStatus.Activate();
        }
    }
}
