﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using System.Linq;
using System.Collections;
using vgm.ads;

public class VideoSystemPanel : UIPanel
{
    [Header("Video System")]
    public VideoPanel VideoPanel;

    public GalleryPanel GalleryPanel;
    public List<GameData.VideoGroup> VideoGroups;

    private GameData.VideoCollection _videoCollection;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.1f);
        UILoader.Instance.StartLoader();


    }

    void OnEnable()
    {
        CallBackOnPanelActivate += GetGalleryDetails;
        AdsCheckManager.Instance.CloseBanner();

       AdsCheckManager.Instance.ShowBannerIfAllowed();
    }

    void OnDisable()
    {
        CallBackOnPanelActivate = null;
        AdsCheckManager.Instance.CloseBanner();
    }

    void GetGalleryDetails()
    {

        UILoader.Instance.StopLoader(true);
        UILoader.Instance.StartLoader();

        _videoCollection = LocalDataManager.Instance.GetConfig<GameData.VideoCollection>(EConfigFileName.Video);

        var groupThumbs = new List<string>() { };

        switch (VideoPlayerDataManager.ContentType)
        {
            case VideoContentTypes.Rhymes:
                {
                    groupThumbs = LocalDataManager.Instance.GetConfig<GameData.VideoCollection>(EConfigFileName.Video).Rhymes.Select(x => x.GroupThumbnail).ToList();
                    VideoGroups = new List<GameData.VideoGroup>(_videoCollection.Rhymes);
                }
                break;

            case VideoContentTypes.Shows:
                {
                    groupThumbs = LocalDataManager.Instance.GetConfig<GameData.VideoCollection>(EConfigFileName.Video).Shows.Select(x => x.GroupThumbnail).ToList();
                    VideoGroups = new List<GameData.VideoGroup>(_videoCollection.Shows);
                }
                break;
        }

        FileManager.Instance.GetImage(urls: groupThumbs.ToArray(),
            onComplete: (x) =>
        {
            Debug.Log("All Images Downloaded");
            UILoader.Instance.StopLoader(true);
            AddVideosToGallery(VideoGroups);
        }, owner: this, onFail: (error) =>
        {
            Debug.Log("Batch Download Failed");
            UILoader.Instance.StopLoader(true);
            AddVideosToGallery(VideoGroups);
        });
    }

    void AddVideosToGallery(List<GameData.VideoGroup> inVideoGroups)
    {
        if (GalleryPanel)
        {
            VideoPlayerDataManager.Instance.ViewController.Activate(GalleryPanel, UIType.BaseView);
            StartCoroutine(GalleryPanel.PopulateGallery(inVideoGroups));
        }
    }

    public void ReadyVideoPlayer(string videoUrl, List<GameData.VideoData> relatedVideos, GameData.VideoData currenVideoData)
    {
        //refresh downloaded video cache
        VideoPanel.RefreshDownloadCache();

        if (!VideoPanel)
            return;
        if (VideoPlayerDataManager.Instance.mediaPlayer.Control.IsPlaying()) return;


        StartCoroutine(VideoPanel.FillFilmstrip(videoUrl, relatedVideos, currenVideoData));

        //activate the video panel
        var seq = DOTween.Sequence();
        seq.SetDelay(0.5f)
            .AppendCallback(() => VideoPlayerDataManager.Instance.ViewController.Activate(VideoPanel));

        seq.Play();

        //uncomment, if video player to be started in fullscreen
        //if (!VideoPlayerDataManager.Instance.Controls.IsFullScreen())
        //    VideoPlayerDataManager.Instance.Controls.FullScreen();

        //stopping bg sound
        VideoPlayerDataManager.Instance.AudioController.StopAudio("BackgroundMusic");
    }
}