﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using RenderHeads.Media.AVProVideo;
using vgm.ads;

public class VideoPlayerManager : MonoBehaviour, IPlayer
{
    public static VideoPlayerManager Instance;

    [SerializeField]
    public MediaPlayer videoPlayer;
    public Action onEndReached, onPlayerStarted, onPrepared;
    public Slider slider;
    public VideoControls videoControls;
    public float currentTimeForControlsOnScreen, maxTimeForControlsOnScreen;
    private bool isSliderAllowed = true;
    public VideoPanel videoPanel;
    public Text currentVideoTime, totalVideoTime;
    public Action<float> normalizedFrameTime;
    public GameObject pauseButton, playButton;
    public int bufferTime = 1;
    private float time;
    private int videoTime;
    public bool offlineVideo, shouldPlay;
    public GameObject bufferPanel;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        videoPlayer = this.GetComponent<MediaPlayer>();
        SetMediaEndReached(OnEndReached);
        SetPrepared(OnPrepared);
    }
    void Start()
    {
        videoPlayer.Events.AddListener(OnMediaPlayerEvent);
    }

    private void OnMediaPlayerEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.FirstFrameReady:
                onPrepared?.Invoke();
                break;
            case MediaPlayerEvent.EventType.FinishedPlaying:
                onEndReached?.Invoke();
                break;
            case MediaPlayerEvent.EventType.Started:
                onPlayerStarted?.Invoke();
                break;
        }
    }

    private void OnPrepared()
    {
        slider.value = 0f;
        ToggleBuffer(false);
        GetTotalSeconds();
        InvokeRepeating("BufferingCheck", 0f, 1f);
    }
    void OnEndReached()
    {
        PerformanceManager.Instance.IncrementSuccess();
        VGMinds.Analytics.AnalyticsManager.LogEvent("video_end", "Video Ended Filename", "");
        if (LocalDataManager.Instance.SaveData.Autoplay)
        {
            if (AdsCheckManager.Instance.ShowInterestialIfAllowed(ActivityType.VIDEO))
            {
                Debug.Log("CanShowInterestial true");
                InterestialService.ShowAdIfItIsReady(() => videoPanel.JumpToVideoBy(1));
                Play();
            }
            else
                videoPanel.JumpToVideoBy(1);
        }
        else
        {
            BackVideoList();
            if (BannerService.bannerAdEnabled)
                BannerService.ShowBannerAd(false);
        }
    }


    public void BackVideoList()
    {
        Resources.UnloadUnusedAssets();
        if (videoPanel.IsPanelActive)
            videoPanel.PressBack();
    }

    public void SetPrepared(Action onPrepared)
    {
        this.onPrepared += onPrepared;
    }

    public void SetPlayerStartedAction(Action onPlayerStarted)
    {
        this.onPlayerStarted += onPlayerStarted;
    }

    public IEnumerator onCoroutine()
    {
        while (true)
        {
            SetVideoTime();
            yield return new WaitForSeconds(1f);
        }
    }

    private void SetVideoTime()
    {
        if (normalizedFrameTime != null)
            normalizedFrameTime(time);
    }

    public void TogglePause(bool shouldShow)
    {
        pauseButton.SetActive(shouldShow);
    }
    public void Pause()
    {
        StopCoroutine(onCoroutine());
        videoPlayer.Pause();
        TogglePause(false);
        shouldPlay = false;
    }

    public void Stop()
    {
        videoPlayer.Stop();
        Debug.LogError("Videoplayermanager stop function!!!!!!!!");
        StopCoroutine(onCoroutine());
        videoPlayer.Control.SeekToFrame(0);
        TogglePause(false);
        shouldPlay = false;
    }

    public void Play()
    {
        shouldPlay = true;
        StartCoroutine(onCoroutine());
        videoPlayer.Play();
        TogglePause(true);
    }

    public void OnPointerDown()
    {
        currentTimeForControlsOnScreen = maxTimeForControlsOnScreen;
        Pause();
        isSliderAllowed = false;
    }

    public void OnPointerUp(Slider slider)
    {
        Seek(slider.value);
        Play();
    }

    public void MoveSlider(float obj)
    {
        slider.value = obj;
    }

    public void SetVideoPath(string caller)
    {
        PerformanceManager.Instance.IncrementAttempts();
        Debug.LogError("SetVideoPath");
        videoPlayer.OpenMedia(MediaPathType.AbsolutePathOrURL, caller, autoPlay: true);
        onPlayerStarted?.Invoke();
        Debug.Log("caller" + caller);

        VGMinds.Analytics.AnalyticsManager.LogEvent("video_start", "VideoPath", caller);
        AdsCheckManager.Instance.CloseBanner();
        Play();
        //BannerService.isToShowTop = true;
        //if (!LocalDataManager.Instance.SaveData.IsPremiumUser())
        //{
        //    Debug.Log("sunil if false so not full scrn banr pos top ");
        //    videoControls.NotFullScreen();
        //}
        //else
        //{
        //    Debug.Log("sunil is premium ");
        //    videoControls.FullScreen();
        //}
    }

    public void EmptyPath()
    {
        videoPlayer.CloseMedia();
    }

    public bool IsVideoPlaying()
    {
        return videoPlayer.Control.IsPlaying();
    }

    public void SetMediaEndReached(Action reached)
    {
        Debug.LogError("on end reached " + reached);
        onEndReached += reached;
    }

    public void ResetTimeVariables()
    {
        bufferTime = 1;
    }

    public void BufferingCheck()
    {
        if (bufferTime > videoTime && !offlineVideo)
        {
            ToggleBuffer(true);
        }
        else if (bufferTime < videoTime)
        {
            ToggleBuffer(false);
            bufferTime += 1;
        }
        else return;
    }

    public void ToggleBuffer(bool show)
    {
        bufferPanel.SetActive(show);
    }

    public void Seek(float sliderPos)
    {
        double temp = videoPlayer.Info.GetDuration() * sliderPos;
        videoPlayer.Control.Seek(temp);
    }

    public string CurrentTime()
    {
        return GetTimeFromSeconds(videoPlayer.Control.GetCurrentTime()).ToString();
    }

    public string GetTotalSeconds()
    {
        return GetTimeFromSeconds(videoPlayer.Info.GetDuration()).ToString();
    }

    string GetTimeFromSeconds(double seconds)
    {
        TimeSpan t = TimeSpan.FromSeconds(seconds);

        string answer = string.Format("{0:D2}:{1:D2}",
                        t.Minutes,
                        t.Seconds);
        return answer;
    }

    public double GetSliderValue()
    {
        return videoPlayer.Control.GetCurrentTime() / videoPlayer.Info.GetDuration();
    }
}
