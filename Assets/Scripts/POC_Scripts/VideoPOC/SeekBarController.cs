﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SeekBarController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public VideoPlayerManager videoPlayerDataManager;
    public IPlayer player;
    public GameObject seekbarListenerObject;
    public GameObject playerListenerObject;
    private bool isSliderAllowed = true;

    private void Start()
    {
        player = playerListenerObject.GetComponent<IPlayer>();
        player.SetPrepared(ResetBool);
        player.SetPlayerStartedAction(ResetBool);
    }

    private void ResetBool()
    {
        isSliderAllowed = true;
    }

    void Update()
    {
        if (player.IsVideoPlaying())
            MoveSliderWithFrame(float.Parse(player.GetSliderValue().ToString()));
    }

    private void MoveSliderWithFrame(float obj)
    {
        if (player.IsVideoPlaying() && isSliderAllowed)
        {
            videoPlayerDataManager.MoveSlider(obj);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isSliderAllowed = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        player.Seek(videoPlayerDataManager.slider.value);
        SeekCompleted();
    }

    void SeekCompleted()
    {
        StartCoroutine(SetBool());
    }

    IEnumerator SetBool()
    {
        yield return new WaitForSeconds(2f);
        isSliderAllowed = true;
    }

    void OnEnable()
    {
        isSliderAllowed = true;
    }


}
