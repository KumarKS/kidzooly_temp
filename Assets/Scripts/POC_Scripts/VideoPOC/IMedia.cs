﻿using System.Collections.Generic;
using UnityEngine;

public interface IMedia
{
    string GetMediaName();
    MediaStatus GetMediaStatus();
    void SetMediaStatus(MediaStatus status);
    string GetVideoDownloadableUrl();
    void OnVideoDownloadComplete();
    void OnVideoDownloadFailed();
    void DownloadProgress(float progressCompleted);
    void ToggleAnimationState(bool shouldStartAnim);
    string GetMediaDownlaodedName();
    string GetMediaExtension();
    Sprite GetSprite();
    void StartUpAnimation(ParticleSystem particleSystem, bool shoulAnimate = false);
    bool GetLockStatus();
}

public interface IPlaylist
{
    IMedia GetNextMedia();
    IMedia GetPreviousMedia();
    string GetVideoDownloadableUrl(IMedia media);
    void OnMediaClicked(IMedia media);
    void OnMediaPlayerClosed();
    void ToggleMediaAnimations(bool shouldAnimate);
    bool GetSliderStatus();
}

public enum MediaStatus
{
    PLAY,
    DOWNLOAD,
    DOWNLOADING,
    PLAYING
}
