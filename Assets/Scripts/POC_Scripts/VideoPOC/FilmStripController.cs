﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;


public class FilmStripController : MonoBehaviour
{
    private Sequence controlsSeq;
    private CanvasGroup _canvasGroup;
    private float _activeDuration = 2.5f;
    public int hidePos, showPos;
    private RectTransform _rectPanel;
    public GameObject closeButton, filmStripContainer, fimStripContainerContent, BG_Black_Image;
    public Image viewPort, filmStripScroll;

    void OnEnable()
    {
        if (fimStripContainerContent.GetComponent<ContentSizeFitter>() != null)
            fimStripContainerContent.AddComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
    }
    void Start()
    {
        closeButton.SetActive(false);
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.alpha = 0;
    }
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    void Update()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                //save ended touch 2d point
                secondPressPos = new Vector2(t.position.x, t.position.y);

                //create vector from the two points
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                //normalize the 2d vector
                currentSwipe.Normalize();

                //swipe upwards
                if (currentSwipe.x > 0.5 && currentSwipe.x > -1f)
                {
                    return;
                }
                else if (currentSwipe.y > 0.5 && currentSwipe.x > -1f)
                {
                    OnShowFilmStripDrag();
                }
                //swipe down
                if (currentSwipe.x < 0.5 && currentSwipe.x < -0.5f)
                {
                    return;
                }
                else if (currentSwipe.y < -0.5 && currentSwipe.x > -1f)
                {
                    OnHideFilmStrip();
                }

            }

        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
            currentSwipe.Normalize();
            Debug.LogError("current swipe" + currentSwipe);

            if (currentSwipe.x > 0.5 && currentSwipe.x > -1f)
            {
                return;
            }
            else if (currentSwipe.y > 0.5 && currentSwipe.x > -1f)
            {
                OnShowFilmStripDrag();
            }
            if (currentSwipe.x < 0.5 && currentSwipe.x < -0.5f)
            {
                return;
            }
            else if (currentSwipe.y < -0.5f && currentSwipe.x > -1f)
            {
                OnHideFilmStrip();
            }
        }
#endif
    }

    public void SetControlBar(bool active)
    {
        if (controlsSeq != null)
            controlsSeq.Kill();

        if (active)
        {
            controlsSeq = DOTween.Sequence()
                .Append(_canvasGroup.DOFade(1f, .5f))
                .AppendCallback(() => _canvasGroup.blocksRaycasts = true)
                .AppendInterval(_activeDuration)
                .Append(_canvasGroup.DOFade(0f, .5f))
                .AppendCallback(() => _canvasGroup.blocksRaycasts = false);
        }
        controlsSeq.Play();
    }

    public void SetControlBarUp(bool active)
    {
        if (controlsSeq != null)
            controlsSeq.Kill();
        if (active)
        {
            _rectPanel = filmStripContainer.GetComponentInChildren<RectTransform>();
            controlsSeq = DOTween.Sequence()
               .Append(_rectPanel.DOAnchorPosY(showPos, .5f))
                //.Append(_canvasGroup.DOFade(1f, _activeDuration))
                .AppendCallback(() => _canvasGroup.blocksRaycasts = true)
                .AppendInterval(_activeDuration)
                //.Append(_canvasGroup.DOFade(0f, .5f))
                .AppendCallback(() => _canvasGroup.blocksRaycasts = false);
        }
        controlsSeq.Play();

    }

    public void OnShowFilmStripDrag()
    {
        _activeDuration = 15f;
        SetControlBarUp(true);
        Invoke("OnHideFilmStrip", _activeDuration);
        closeButton.SetActive(true);
        viewPort.enabled = false;
        filmStripScroll.enabled = true;
        filmStripScroll.raycastTarget = false;
        BG_Black_Image.GetComponent<Image>().raycastTarget = false;
    }
    public void OnHideFilmStrip()
    {
        hidePos = 36;
        BG_Black_Image.GetComponent<Image>().raycastTarget = true;
        filmStripScroll.raycastTarget = true;
        _activeDuration = 2.5f;
        filmStripContainer.GetComponent<Image>().enabled = false;
        viewPort.enabled = true;
        filmStripScroll.enabled = false;
        controlsSeq.Append(_rectPanel.DOAnchorPosY(hidePos, .5f));
        controlsSeq.Play().SetLoops(0);
        if (closeButton.activeInHierarchy)
            closeButton.SetActive(false);
    }
}
