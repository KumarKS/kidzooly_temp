﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using RenderHeads.Media.AVProVideo;


[Serializable]
public class FavoriteVideoData
{
    public List<string> FavVideoList = new List<string>();

    public void AddFav(string videoID)
    {
        FavVideoList.NonDuplicatedAdd(videoID);
    }

    public void RemoveFav(string videoID)
    {
        FavVideoList.FindAndRemove(videoID);
    }
}

public class VideoPlayerDataManager : GenericSingleton<VideoPlayerDataManager>
{
    public static VideoContentTypes ContentType = VideoContentTypes.Rhymes;

    public VideoPanel videoPanel;
    public MediaPlayer mediaPlayer;
    public VideoSystemPanel VideoSystem;
    public VideoControls Controls;
    public UIViewController ViewController;
    public AudioController AudioController;
    public FilmStripController filmStripController;
    public GameObject playerListenerObject;
    public GameObject GalleryThumbnailPrefab;
    public GameObject VideoThumbnailPrefab;
    public GameObject FilmstripThumbnailPrefab;
    public GameObject pauseButton, playButton;
    public List<GameData.VideoData> FavoriteVideos = new List<GameData.VideoData>();
    public float currentTimeForControlsOnScreen, maxTimeForControlsOnScreen;
    public int currentInterstitialCount;
    protected override void Awake()
    {
        base.Awake();

        if (!mediaPlayer)
        {
            mediaPlayer = GetComponentInChildren<MediaPlayer>();
        }

        if (!ViewController)
        {
            ViewController = UIViewController.Instance;
        }
    }
    private void Start()
    {
    }

    #region FavoriteVideos

    public void AddFavoriteTag(GameData.VideoData vData)
    {
        if (!FavoriteVideos.Exists(x => x.VideoUrl == vData.VideoUrl))
        {
            FavoriteVideos.Add(vData);
            LocalDataManager.Instance.SaveData.FavoriteVideos.AddFav(vData.VideoUrl);
            LocalDataManager.Instance.Save();
        }
    }

    public void RemoveFavoriteTag(GameData.VideoData vData)
    {
        //debug.log ("Removing");
        var itemToRemove = FavoriteVideos.Find(x => x.VideoUrl == vData.VideoUrl);
        if (itemToRemove != null)
        {
            FavoriteVideos.Remove(itemToRemove);
            LocalDataManager.Instance.SaveData.FavoriteVideos.RemoveFav(vData.VideoUrl);
            LocalDataManager.Instance.Save();
        }
    }

    #endregion

    #region VideoItemActions

    public void VideoItemInitialize(GameData.VideoData videoData, ThumbnailHelper thumbnail)
    {
        var isFavorite = FavoriteVideos.Any(x => x.VideoUrl == videoData.VideoUrl); //if this video is one of the favorite videos
        thumbnail.SetVideoData(videoData);

        thumbnail.IsDownloading = false;

        if (FileManager.IsDownloadInitiated(videoData.VideoUrl))
        {
            thumbnail.IsDownloading = true;
            FileManager.Instance.GetVideo(urls: videoData.VideoUrl, onComplete: (localurl) =>
            {
                thumbnail.SetVideoDetails(true);
            }, owner: this, onFail: (error) =>
            {
                //debug.log ("Video Retrival error!!");
                switch (error)
                {
                    case EFailureReason.INTERNETCONNECTION:
                        UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDownloadInterrupt") ?? "Oops! Download failed, please check your internet connection", 4f);
                        AudioController.TriggerAudio("OnNoInternetConnection");
                        break;
                    case EFailureReason.STORAGEFULL:
                        UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnStorageFull") ?? "Storage Full!", 4f);
                        break;
                    case EFailureReason.TECHNICAL:
                        UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnTechnicalIssue") ?? "Technical Issue!", 4f);
                        break;
                    default:
                        break;
                }
                FileManager.RemoveFile(videoData.VideoUrl);
                thumbnail.SetVideoDetails(false);
            }, onProgress: thumbnail.ShowProgress);
        }


        thumbnail.SetVideoDetails(FileManager.IsFileDownloaded(videoData.VideoUrl), true, isFavorite, isOn =>
        {
            if (isOn)
            {
                AddFavoriteTag(videoData);
            }
            else
            {
                RemoveFavoriteTag(videoData);
            }
        });


        if (!videoData.isFree && !LocalDataManager.Instance.SaveData.IsPremiumUser())
        {
            thumbnail.SetFreeContent(false);
        }
        else
        {
            thumbnail.SetFreeContent(true);
        }
    }

    //this function is messed up right now
    public void VideoItemOnClick(GameData.VideoData videoData, ThumbnailHelper thumbnail, List<GameData.VideoData> containingVideoDatas, Action extraActionOnDownload = null, Action extrActionOnPlay = null, bool autoplayOnDownload = true, bool sameWindow = false)
    {
        thumbnail.IsDownloading = false;
        if (LocalDataManager.Instance.SaveData.AllowDownload)
        {
            if (!FileManager.IsFileDownloaded(videoData.VideoUrl))
            {
                if (!sameWindow && VGInternet.Instance.Status)
                    AudioController.TriggerAudio("OnVideoDownload");

                thumbnail.IsDownloading = true;
                FileManager.Instance.GetVideo(urls: videoData.VideoUrl, onComplete: (vFile) =>
                {
                    thumbnail.SetVideoDetails(true);
                    extraActionOnDownload.SafeInvoke();
                    VideoSystem.VideoPanel.RefreshDownloadCache();

                }, owner: this, onFail: (error) =>
                {

                    //debug.log ("Video Retrival error!!");
                    switch (error)
                    {
                        case EFailureReason.INTERNETCONNECTION:
                            UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnDownloadInterrupt") ?? "Oops! Download failed, please check your internet connection", 4f);
                            AudioController.StopAudio("OnVideoDownload");
                            AudioController.TriggerAudio("OnNoInternetConnection");
                            break;
                        case EFailureReason.STORAGEFULL:
                            UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnStorageFull") ?? "Storage Full!", 4f);
                            break;
                        case EFailureReason.TECHNICAL:
                            UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnTechnicalIssue") ?? "Technical Issue!", 4f);
                            break;
                        default:
                            break;
                    }
                    FileManager.RemoveFile(videoData.VideoUrl);
                    thumbnail.SetVideoDetails(false);
                }, onProgress: thumbnail.ShowProgress);
            }
            else
            {
                VideoPlayerManager.Instance.offlineVideo = true;
                PlayOfflineVideo(videoData, thumbnail, containingVideoDatas, sameWindow);
                extrActionOnPlay.SafeInvoke();
            }
        }
        else
        {
            if (FileManager.IsFileDownloaded(videoData.VideoUrl))
            {
                VideoPlayerManager.Instance.offlineVideo = true;
                PlayOfflineVideo(videoData, thumbnail, containingVideoDatas, sameWindow);
            }
            else
            {
                if (VGInternet.Instance.Status)
                {
                    if (sameWindow)
                    {
                        VideoPlayerManager.Instance.offlineVideo = false;
                        PlayVideo(videoData.VideoUrl);
                    }
                    else
                    {

                        VideoPlayerManager.Instance.offlineVideo = false;
                        VideoSystem.ReadyVideoPlayer(videoData.VideoUrl, containingVideoDatas, videoData);
                    }

                }
                else
                {
                    AudioController.TriggerAudio("OnNoInternetConnection");
                    UIToastNotification.Instance.TriggerToast("Seems you are Offline!!\nPlease check your Internet Connection to continue.", 4);
                }
            }

            extrActionOnPlay.SafeInvoke();
        }
    }

    public void PlayOfflineVideo(GameData.VideoData videoData, ThumbnailHelper thumbnail, List<GameData.VideoData> containingVideoDatas, bool sameWindow = false)
    {
        FileManager.Instance.GetVideo(urls: videoData.VideoUrl, onComplete: (vFile) =>
        {
            if (thumbnail != null)
                thumbnail.SetVideoDetails(true);

            mediaPlayer.Stop();

            if (sameWindow)
            {
                PlayVideo(vFile[0]);
            }
            else
            {
                VideoPlayerManager.Instance.onCoroutine();
                VideoSystem.ReadyVideoPlayer(vFile[0], containingVideoDatas, videoData);
            }
            vFile = null;
        }, owner: this);
    }



    public void PlayVideo(string videoUrl)
    {
        VideoPlayerManager.Instance.onCoroutine();
        VideoPlayerManager.Instance.ToggleBuffer(false);
        mediaPlayer.Stop();
        VideoPlayerManager.Instance.SetVideoPath(videoUrl);
        currentInterstitialCount ++;
        //Analytics
        AnalyticsScript.instance.LogEvent("Video Started");
        VGMinds.Analytics.AnalyticsManager.IncrementVideoPlayed();
        VGMinds.Analytics.AnalyticsManager.LogVideoCategory(ContentType.ToString());
    }

    #endregion
}