﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using vgm.ads;
using Random = UnityEngine.Random;

[Serializable]
public class ThumbnailFrameDetail
{
    public Sprite FrameSprite;
    public VideoContentTypes RelativeContentType;
}

[Serializable]
public class FilmstripThumbnailState
{
    public Sprite ActiveSprite;
    public Sprite DeactiveSprite;
}

[Serializable]
public class TEvent : UnityEvent<float>
{

}

[Serializable]
public class BEvent : UnityEvent<bool>
{

}

[RequireComponent(typeof(Button))]
public class ThumbnailHelper : MonoBehaviour
{
    private VideoContentTypes _contentType;

    [SerializeField]
    public VideoContentTypes ContentType
    {
        get { return _contentType; }
        set
        {
            _contentType = value;

            if (ThumbnailType == ThumbnailTypes.Gallery)
                switch (value)
                {
                    case VideoContentTypes.Shows:
                        {
                            TitleText.color = Color.yellow;
                        }
                        break;
                    case VideoContentTypes.Rhymes:
                        {
                            TitleText.color = Color.red;
                        }
                        break;
                }
            else
            {
                TitleText.color = Color.white;
            }
        }
    }

    public ThumbnailTypes ThumbnailType;
    public Text TitleText;
    //public RawImage ThumbnailImage;
    public float ClickResetTime = 1;

    [Header("Video")]
    public CanvasGroup StatusGroup;
    public Image LoaderImage;
    public GameObject PlayPanel;
    public GameObject DownloadIcon;
    public GameObject PremiumPanel;
    public Toggle FavToggle;

    public GameData.VideoData VideoData;

    public bool IsDownloading;

    public UnityEvent OnDownloadStarts;
    public TEvent Downloading;
    public BEvent IfDownloading;
    public UnityEvent OnDownloadCompleted;

    [Header("Frames")]
    public Image FrameR;

    public Image FrameL;

    [SerializeField]
    public List<ThumbnailFrameDetail> Frames;

    [Header("FilmStrip")]
    public Image FilmstripState;

    [SerializeField]
    public FilmstripThumbnailState FilmstripSprites;

    [SerializeField]
    public FilmstripThumbnailState PlaySprites;

    public Button clickButton
    {
        get
        {
            return GetComponent<Button>();
        }
    }

    private bool _isDownloaded = false;

    private bool _alreadyClicked = false;

    void Awake()
    {
        if (FavToggle)
        {
            FavToggle.Deactivate();
        }

        if (StatusGroup)
        {
            StatusGroup.alpha = 0;
            PlayPanel.Deactivate();
            DownloadIcon.Deactivate();
            LoaderImage.Deactivate();
        }

        VGInternet.Instance.statusChanged += NetStatusChange;
    }

    void OnDestroy()
    {
        VGInternet.Instance.statusChanged -= NetStatusChange;
    }

    private void NetStatusChange(bool val)
    {
        if (!LocalDataManager.Instance.SaveData.AllowDownload)
        {
            if (val)
            {
                PlayPanel.SetImage(PlaySprites.ActiveSprite);
            }
            else
            {
                if (_isDownloaded) PlayPanel.SetImage(PlaySprites.ActiveSprite);
                else PlayPanel.SetImage(PlaySprites.DeactiveSprite);
            }
        }
    }

    public void SetContentType(ThumbnailTypes thumbType, VideoContentTypes contentType)
    {
        if (!FrameR || !FrameL)
            return;

        ThumbnailType = thumbType;
        ContentType = contentType;

        foreach (var frame in Frames)
        {
            if (frame.RelativeContentType == contentType)
            {
                FrameL.sprite = FrameR.sprite = frame.FrameSprite;
            }
        }
    }

    public void SetThumbnailDetails(string thumbnailName, string thumbnailUrl, Action onInitAction = null, Action onClickAction = null)
    {
        onInitAction.SafeInvoke();
        TitleText.text = thumbnailName;
        GetComponentInChildren<UIRemoteImage>().SetUrl(thumbnailUrl);


        clickButton.onClick.RemoveAllListeners(); //if any, removing previous listeners [safe mode]
        clickButton.onClick.AddListener(() =>
        {
           
            if (!_alreadyClicked)
            {
                if (PremiumPanel && PremiumPanel.activeSelf)
                {
                    VideoPlayerManager.Instance.ResetTimeVariables();
                    OpenStore();
                }
                else
                {
                    VideoPlayerManager.Instance.ResetTimeVariables();
                    VideoPlayerDataManager.Instance.AudioController.TriggerAudio("OnClick");
                    onClickAction.SafeInvoke();
                }

                //bug fix: double click - double item replication issue
                _alreadyClicked = true;
                StartCoroutine(ResetClick(ClickResetTime));
            }
        });
    }


    public void SetVideoDetails(bool isDownloaded, bool resetFavoriteButton = false, bool isFavorite = false, Action<bool> onFavoriteToggle = null)
    {
        _isDownloaded = isDownloaded;
        if (StatusGroup)
        {
            StatusGroup.DOFade(1, 0.5f).SetDelay(0.3f);
            LoaderImage.fillAmount = 0;
            LoaderImage.Deactivate();
            PlayPanel.SetImage(PlaySprites.ActiveSprite);
            if (LocalDataManager.Instance.SaveData.AllowDownload)
            {
                if (isDownloaded)
                {
                    PlayPanel.Activate();
                    DownloadIcon.Deactivate();

                    if (OnDownloadCompleted != null)
                        OnDownloadCompleted.Invoke();

                    if (IfDownloading != null)
                        IfDownloading.Invoke(false);
                }
                else
                {
                    PlayPanel.Deactivate();
                    DownloadIcon.Activate();
                }
            }
            else
            {
                NetStatusChange(VGInternet.Instance.Status);
                PlayPanel.Activate();
            }

            if (FavToggle && resetFavoriteButton)
            {
                FavToggle.onValueChanged.RemoveAllListeners();
                FavToggle.isOn = isFavorite;
                FavToggle.onValueChanged.AddListener((x) =>
                {
                    onFavoriteToggle.SafeInvoke(x);
                    VideoPlayerDataManager.Instance.AudioController.TriggerAudio("OnArrow");
                });

                FavToggle.Activate();
            }

            //Analytics
            if (isFavorite)
            {
                VGMinds.Analytics.AnalyticsManager.LogVideoFavorited(VideoData.VideoName);
            }

            _alreadyClicked = false;
        }
    }

    public void SetVideoData(GameData.VideoData vData)
    {
        VideoData = vData;
    }

    public void SetFilmstripState(bool isActive)
    {
        if (!FilmstripState) return;
        FilmstripState.SetImage(isActive ? FilmstripSprites.ActiveSprite : FilmstripSprites.DeactiveSprite);
    }

    public void SetFreeContent(bool isFree)
    {
        if (isFree)
        {
            PremiumPanel.Deactivate();

            if (LocalDataManager.Instance.SaveData.AllowDownload)
            {
                if (_isDownloaded)
                {
                    LoaderImage.Deactivate();
                    PlayPanel.Activate();
                }
                else
                {
                    PlayPanel.Deactivate();
                    LoaderImage.Deactivate();
                    DownloadIcon.Activate();
                }
            }
            else
            {
                LoaderImage.Deactivate();
                PlayPanel.Activate();
            }
        }
        else
        {
            PlayPanel.Deactivate();
            LoaderImage.Deactivate();
            DownloadIcon.Deactivate();
            PremiumPanel.Activate();
        }
    }

    public void ShowProgress(float progress)
    {
        if (!IsDownloading)
        {
            if (IfDownloading != null)
                IfDownloading.Invoke(false);

            return;
        }
        var rProgress = progress / 100; //relative progress

        if (LoaderImage)
        {
            if (!LoaderImage.IsActive())
            {
                LoaderImage.Activate();
                DownloadIcon.Deactivate();

                if (OnDownloadStarts != null)
                {
                    OnDownloadStarts.Invoke();
                }

                if (IfDownloading != null)
                {
                    IfDownloading.Invoke(true);
                }
            }

            LoaderImage.fillAmount = rProgress;
            if (Downloading != null)
                Downloading.Invoke(rProgress);
        }

        _alreadyClicked = true;
    }

    void OpenStore()
    {
        UILoader.Instance.StartLoader();
        if (FilmstripState) //only on film strip
        {
            VideoPlayerDataManager.Instance.mediaPlayer.Pause();
        }
        VideoPlayerDataManager.Instance.AudioController.StopAudio("OnVideoDownload");
        VideoPlayerDataManager.Instance.AudioController.TriggerAudio("OnLockClick", () =>
        {
            UILoader.Instance.StopLoader();
            AdsCheckManager.Instance.CloseBanner();
            FreeTrialsPanel.Instance.Init(null, () =>
            {
                if (FilmstripState)
                { //only on film strip, play after the parental dialog is closed
                    VideoPlayerDataManager.Instance.mediaPlayer.Play();
                }
            });
        });
    }


    public bool IsReady()
    {
        if (VGInternet.Instance.Status || _isDownloaded)
        {
            return true;
        }
        return false;
    }

    //bug fix
    IEnumerator ResetClick(float time)
    {
        yield return new WaitForSeconds(time);

        if (LoaderImage)
        {
            if (!LoaderImage.IsActive())
                _alreadyClicked = false;
        }
        else
        {
            _alreadyClicked = false;
        }
    }
}