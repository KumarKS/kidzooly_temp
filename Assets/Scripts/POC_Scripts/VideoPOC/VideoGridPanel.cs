﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoGridPanel : UIPanel
{

    void OnEnable()
    {
        HardwareInputManager.OnBack += PressBack;
    }

    void OnDisable()
    {
        HardwareInputManager.OnBack -= PressBack;
    }

    public override void PressBack()
    {
        vgm.ads.AdsCheckManager.Instance.CloseBanner();
        UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());
    }
}
