﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[Serializable]
public class DownloadedVideos
{
	public GameData.VideoData DownloadedVideo = new GameData.VideoData ();
	public int GlobalIndex;

	public DownloadedVideos (GameData.VideoData videoData, List<GameData.VideoData> relatedVideoDatas)
	{
		DownloadedVideo = videoData;
		GlobalIndex = relatedVideoDatas.FindIndex (x => x.VideoUrl.Equals (videoData.VideoUrl));
	}
}

public class VideoPanel : UIPanel
{
	[Header ("FilmStrip")]
	public UIScrollable FilmStripScrollable;

	private VideoControls _controls;
	private FilmStripController filmStripController;

	public List<GameData.VideoData> _relatedVideos = new List<GameData.VideoData> ();
	private GameData.VideoData _currentVideoData = new GameData.VideoData ();
	public int _currentVideoIndex = 0;
	public List<DownloadedVideos> _downloadedVideos = new List<DownloadedVideos> ();
    public int _downloadVideoIndex = 0;
	private bool _autoplay = false;
	private bool _interstitialPlaying = false;
	private bool _unityFocus = true;
	private bool _shouldPlayVideoOnFocus = false;
    
	#region initialization

	void OnEnable ()
	{
		CallBackOnPanelDeactivate += delegate {
			VideoPlayerDataManager.Instance.mediaPlayer.Stop ();
			VideoPlayerDataManager.Instance.AudioController.TriggerAudio ("BackgroundMusic");
		};

		//VideoPlayerDataManager.Instance.videoPlayer.loopPointReached += OnPlayerReachEnd;

		_autoplay = LocalDataManager.Instance.SaveData.Autoplay;
		_controls = VideoPlayerDataManager.Instance.Controls;
		filmStripController = VideoPlayerDataManager.Instance.filmStripController;
	}

	void OnDisable ()
	{
		HardwareInputManager.OnBack -= PressBack;

		CallBackOnPanelActivate = null;
		CallBackOnPanelDeactivate = null;
        
	}

	public override Tweener ShowAnimation ()
	{
		transform.DOMoveY (0, 0).SetEase (Ease.OutCubic);
		//hardware back button
		HardwareInputManager.OnBack += PressBack;

		_downloadVideoIndex = 0;
		if (_controls) {
			_controls.SetControlBar (true);
			filmStripController.SetControlBar(true);
		}

		return base.ShowAnimation ();
	}

	public override Tweener HideAnimation ()
	{
		base.HideAnimation ();

		//hardware back button
		HardwareInputManager.OnBack -= PressBack;

		return transform.DOMoveY (-Screen.height * 2, 0.3f).SetEase (Ease.InCubic).SetDelay (0.5f);
	}

	//explicitly set the video player to pause when app goes off focus
	/* private void OnApplicationFocus(bool focus)
     {
         Debug.Log("App Focus: " + focus);
         if (!focus)
         {
             _unityFocus = false;
             if (VideoPlayerManager.Instance.UMediaPlayer.IsPlaying)
                 _shouldPlayVideoOnFocus = true;

             VideoPlayerManager.Instance.UMediaPlayer.Pause();
         }
         else
         {
             _unityFocus = true;
             if (_shouldPlayVideoOnFocus)
             {
                 VideoPlayerManager.Instance.UMediaPlayer.Play();
                 _shouldPlayVideoOnFocus = false;
             }
         }
     }*/

	#endregion

	#region Data

	public IEnumerator FillFilmstrip (string videoUrl, List<GameData.VideoData> relatedVideos, GameData.VideoData currentVideoData)
	{
        

        UILoader.Instance.StartLoader ();
        FilmStripScrollable.ClearContent(); //force clear
        yield return new WaitForEndOfFrame ();

		_currentVideoData = currentVideoData;
		_relatedVideos = relatedVideos;
		_currentVideoIndex = relatedVideos.FindIndex (x => string.Equals (x.VideoUrl, _currentVideoData.VideoUrl));

		yield return StartCoroutine (PopulateFilmstrip (relatedVideos));

		yield return new WaitForEndOfFrame ();
		UILoader.Instance.StopLoader ();


		//play the video after the loading completes
		//VideoPlayerManager.Instance.UMediaPlayer.Play();

		VideoPlayerDataManager.Instance.PlayVideo (videoUrl);

        yield return new WaitForSeconds (0.5f);
		FilmStripScrollable.ScrollToIndex (_currentVideoIndex);
	}

	IEnumerator PopulateFilmstrip (List<GameData.VideoData> relatedVideos)
	{
		_downloadedVideos.Clear (); //clearing the downloaded video list

		foreach (var vData in _relatedVideos) {
			//init downloaded video list
			if (FileManager.IsFileDownloaded (vData.VideoUrl) && (vData.isFree || LocalDataManager.Instance.SaveData.IsPremiumUser ())) {
				_downloadedVideos.Add (new DownloadedVideos (vData, _relatedVideos));
			}
		}

        var initialized = FilmStripScrollable.SetPrefab(VideoPlayerDataManager.Instance.FilmstripThumbnailPrefab)
            .SetData(_relatedVideos).SetFunction((data, index, obj) =>
            {
                var videoDatas = (List<GameData.VideoData>)data;
                var vHelper = obj.GetComponent<ThumbnailHelper>();
                var isFavorite = VideoPlayerDataManager.Instance.FavoriteVideos.Any(x => x.VideoUrl == videoDatas[index].VideoUrl); //if this video is one of the favorite videos

                //highligting at the initialization
                if (_currentVideoData == null || videoDatas[index].VideoUrl == _currentVideoData.VideoUrl)
                { //if we show the current playing video
                    obj.transform.DOScale(1, 0.3f);
                    vHelper.SetFilmstripState(true);
                }
                else
                {
                    obj.transform.DOScale(0.9f, 0.3f);
                    vHelper.SetFilmstripState(false);
                }

                vHelper.SetVideoData(videoDatas[index]);
                vHelper.SetVideoDetails(FileManager.IsFileDownloaded(videoDatas[index].VideoUrl), true, isFavorite, isOn =>
                {
                    if (isOn)
                    {
                        VideoPlayerDataManager.Instance.AddFavoriteTag(videoDatas[index]);
                    }
                    else
                    {
                        VideoPlayerDataManager.Instance.RemoveFavoriteTag(videoDatas[index]);
                    }
                });
                vHelper.SetThumbnailDetails(videoDatas[index].VideoName, videoDatas[index].ThumbnailUrl,
                    () =>
                    {
                        VideoPlayerDataManager.Instance.VideoItemInitialize(videoDatas[index], vHelper);
                    },
                    () =>
                    {
                        VideoPlayerDataManager.Instance.VideoItemOnClick(videoDatas[index], vHelper, _relatedVideos, () =>
                        {
                            _downloadedVideos.Add(new DownloadedVideos(videoDatas[index], _relatedVideos));
                           OrganiseDownloadVideoCache(_currentVideoData);
                        }, () =>
                        {
                            Highlight(relatedVideos, videoDatas[index]);
                            OrganiseDownloadVideoCache(_currentVideoData);
                        }, false, true);
                    });
            }).Initialize();

        yield return new WaitUntil (() => initialized);
		OrganiseDownloadVideoCache (_currentVideoData);
	}

    

    #endregion

    #region Actions

    public void NextVideo ()
	{
        
        VideoPlayerManager.Instance.ResetTimeVariables();
        
        JumpToVideoBy(1);
        
	}

	public void PreviousVideo ()
	{
          
        VideoPlayerManager.Instance.ResetTimeVariables();
        
        JumpToVideoBy (-1);
       

    }

	public void RefreshDownloadCache ()
	{
		_downloadedVideos.Clear (); //clearing the downloaded video list

		foreach (var vData in _relatedVideos) {
			//init downloaded video list
			if (FileManager.IsFileDownloaded (vData.VideoUrl)) {
				_downloadedVideos.Add (new DownloadedVideos (vData, _relatedVideos));
			}
		}

		OrganiseDownloadVideoCache (_currentVideoData);
	}

	void OrganiseDownloadVideoCache (GameData.VideoData currentVideoData)
	{
		_downloadedVideos.Sort ((a, b) => a.GlobalIndex.CompareTo (b.GlobalIndex));
		_downloadVideoIndex = currentVideoData != null ? _downloadedVideos.FindIndex (x => x.DownloadedVideo.VideoUrl.Equals (currentVideoData.VideoUrl)) : -1;
	}

	 public void JumpToVideoBy (int value)
	{
		GameData.VideoData queuedVideo;

		if (LocalDataManager.Instance.SaveData.AllowDownload) {
            Debug.Log("Going in If Condition");

            queuedVideo = _downloadedVideos.JumpBy (_downloadVideoIndex, value).DownloadedVideo; //each call will loop through the downloaded videos
			if (queuedVideo.isFree || LocalDataManager.Instance.SaveData.IsPremiumUser ()) {
                _currentVideoData = queuedVideo;
                _downloadVideoIndex = _downloadedVideos.FindIndex(x => x.DownloadedVideo.VideoName.Equals(queuedVideo.VideoName));
                
                Highlight(_relatedVideos, queuedVideo);
                VideoPlayerDataManager.Instance.PlayOfflineVideo (queuedVideo, null, _relatedVideos, true);
                
				 

				
			} else {
				UIToastNotification.Instance.TriggerToast (LocalizedText.GetLocalizedText ("VideoPremium") ?? "Please subscribe to continue watching videos.", 2f);
			}
		} else {
			queuedVideo = _relatedVideos.JumpBy (_currentVideoIndex, value);
            Debug.Log("Going in else Condition");
            if (queuedVideo.isFree || LocalDataManager.Instance.SaveData.IsPremiumUser ()) {
                Debug.Log("Going in Else (if) Condition");

                if (FileManager.IsFileDownloaded (queuedVideo.VideoUrl)) {
                    Debug.Log("Going in Else (if (if)) Condition");
                    FileManager.Instance.GetVideo (urls: queuedVideo.VideoUrl, onComplete: (vFile) => {
                        Highlight(_relatedVideos, queuedVideo);
                        VideoPlayerDataManager.Instance.PlayVideo (vFile [0]);
                    }, owner: this);
				} else {
                    Debug.Log("Going in Else (if (else)) Condition");
                    Highlight(_relatedVideos, queuedVideo);
                    VideoPlayerDataManager.Instance.PlayVideo(queuedVideo.VideoUrl);
                }

				
			} else {
				UIToastNotification.Instance.TriggerToast (LocalizedText.GetLocalizedText ("VideoPremium") ?? "Please subscribe to continue watching videos.", 2f);
			}
		}
	}

    

	public void OnPlayerReachEnd (VideoPlayer source)
	{
		if (!_autoplay) {
			// UIViewController.Instance.Back();
			//if (_controls.IsFullScreen ()) {
			//	_controls.FullScreen ();
			//}
		} else {
			JumpToVideoBy (1);
			Invoke ("DelayedVideoPaying", 0.1f);
		}
	}

	//TEST CODE
	void DelayedVideoPaying ()
	{
		VideoPlayerDataManager.Instance.mediaPlayer.Play ();
	}

	public override void PressBack ()
	{
		base.PressBack ();
        Resources.UnloadUnusedAssets();
		//vgm.ads.AdsCheckManager.Instance.CloseBanner();
		//vgm.ads.AdsCheckManager.Instance.ShowBannerIfAllowed();
  
	}

	void Highlight (List<GameData.VideoData> relatedVideoData, GameData.VideoData currentVideoData)
	{
		if (currentVideoData == null)
			return;

		FilmStripScrollable.GetContents ().ForEach (x => {
			var vHelper = x.GetComponent<ThumbnailHelper> ();
			if (vHelper.VideoData.VideoUrl.Equals (currentVideoData.VideoUrl) && vHelper.IsReady()) {
				x.DOScale (1f, 0.3f);
				vHelper.SetFilmstripState (true);
			} else {
				x.DOScale (0.9f, 0.3f);
				vHelper.SetFilmstripState (false);
			}
		});

		_currentVideoData = currentVideoData;
        _currentVideoIndex = relatedVideoData.FindIndex(x => string.Equals(x.VideoUrl, _currentVideoData.VideoUrl));
        //for (int i = 0; i < relatedVideoData.Count; i++)
        //{

        //    if (relatedVideoData[i].VideoName.Equals(currentVideoData.VideoName))
        //    {
        //        //Debug.Logerror(i + queuedVideo.VideoUrl);
        //        _currentVideoIndex = i;
        //    }
        //}
        var maxDur = (relatedVideoData.Count / 100) * 5; //max duration is calculated per 100 elements with a factor of 4 secs
		maxDur = Mathf.Clamp (maxDur, 3, maxDur);
		FilmStripScrollable.ScrollToIndex (_currentVideoIndex, maxDur);
	}

	#endregion
}