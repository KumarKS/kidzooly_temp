﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GalleryPanel : UIPanel
{
    [Header("Data")]
    private List<GameData.VideoGroup> _currentVideoGroups;

    private List<GameData.VideoData> _allFreeVideoDatas = new List<GameData.VideoData>();
    private List<GameData.VideoData> _allPremiumVideoDatas = new List<GameData.VideoData>();
    private List<GameData.VideoData> _allVideoData = new List<GameData.VideoData>();
    private List<GameObject> _currentVideoObj = new List<GameObject>();


    [Header("Gallery Panel")]
    public UIScrollable GalleryScrollable;

    [Header("Video Grid")]
    public UIPanel VideoGridPanel;

    public Text VideoGridTitle;
    public UIScrollable VideoListScrollable;
    private bool _initializingFavoriteData;
    [Header("Extra Navigation")]
    public ToggleGroup NavGroup;

    public Toggle NewButton;
    public Toggle FavButton;
    public Toggle FreeButton;

    protected override void Awake()
    {
        base.Awake();
        ActivatePanel(true);
    }

    #region Initialization

    void OnEnable()
    {

        CallBackOnPanelActivate = null;

        VideoGridPanel.CallBackOnPanelActivate = null;
        VideoGridPanel.CallBackOnPanelActivate += () =>
        {
            StartCoroutine(RefreshVideoList());
            NavGroup.SetAllTogglesOff();
        };

        VideoGridPanel.CallBackOnPanelDeactivate = null;
        VideoGridPanel.CallBackOnPanelDeactivate += () =>
        {
            VideoPlayerDataManager.Instance.AudioController.StopAudio("OnVideoDownload");
            LocalDataManager.Instance.Save(); //force save for better preservability of the fav video list
            FileManager.RemoveReference(this);
        };

        //hardware back button functionality
        HardwareInputManager.OnBack += BackGallery;
        HardwareInputManager.OnBack += BackVideoList;
        //if (vgm.ads.BannerService.bannerAdEnabled)
        //    vgm.ads.BannerService.ShowBannerAd(false);

        //vgm.ads.AdsCheckManager.Instance.ShowBannerIfAllowed();
    }

    void OnDisable()
    {
        HardwareInputManager.OnBack -= BackGallery;
        HardwareInputManager.OnBack -= BackVideoList;

        CallBackOnPanelActivate = null;
        VideoGridPanel.CallBackOnPanelActivate = null;
        VideoGridPanel.CallBackOnPanelDeactivate = null;
        //if (vgm.ads.BannerService.bannerAdEnabled)
        //    vgm.ads.BannerService.ShowBannerAd(false);
    }

    #region BackFunctionality

    void BackGallery()
    {
        Resources.UnloadUnusedAssets();
        if (IsPanelActive) PressBack();
    }

    void BackVideoList()
    {
        Resources.UnloadUnusedAssets();
        if (VideoGridPanel.IsPanelActive)
            VideoGridPanel.PressBack();
    }

    #endregion


    void Start()
    {

        RefreshVideoGridView();

        if (NewButton)
        {
            var newVideos = GetNewVideosList();
            if (newVideos != null && newVideos.Count > 0)
            {
                NewButton.onValueChanged.RemoveAllListeners();
                NewButton.onValueChanged.AddListener((isOn) =>
               {
                   if (isOn)
                       StartCoroutine(OnPressNew());
               });
            }
            else
            {
                NewButton.Deactivate();
            }
        }

        if (FreeButton)
        {
            FreeButton.onValueChanged.RemoveAllListeners();
            FreeButton.onValueChanged.AddListener((isOn) =>
            {
                if (isOn)
                    StartCoroutine(OnPressFree());
            });
        }

        if (FavButton)
        {
            FavButton.onValueChanged.RemoveAllListeners();
            FavButton.onValueChanged.AddListener((isOn) =>
            {
                if (isOn)
                    StartCoroutine(OnPressFav());
            });
        }
    }

    #endregion

    #region Data

    void RefreshVideoGridView()
    {
        Canvas.ForceUpdateCanvases();
        Debug.Log("Refresh");
        StartCoroutine(OnPressFav(true)); //initialize the fav whenever coming back to gallery view
    }

    IEnumerator RefreshVideoList()
    {
        yield return new WaitForEndOfFrame();

        _currentVideoObj.ForEach(videoItem =>
        {
            var vHelper = videoItem.GetComponent<ThumbnailHelper>();
            var isFavorite = VideoPlayerDataManager.Instance.FavoriteVideos.Any(x => x.VideoUrl == vHelper.VideoData.VideoUrl);

            vHelper.SetVideoDetails(FileManager.IsFileDownloaded(vHelper.VideoData.VideoUrl), true, isFavorite,
                isOn =>
                {
                    if (isOn)
                    {
                        VideoPlayerDataManager.Instance.AddFavoriteTag(vHelper.VideoData);
                    }
                    else
                    {
                        VideoPlayerDataManager.Instance.RemoveFavoriteTag(vHelper.VideoData);
                    }
                });
        });

        VideoListScrollable.Refresh();
    }


    public IEnumerator PopulateGallery(List<GameData.VideoGroup> inVideoGroups)
    {

        UILoader.Instance.StartLoader();
        yield return new WaitForSeconds(0.3f);

        Debug.Log("populate");
        if (inVideoGroups.Any())
        {
            _currentVideoGroups = inVideoGroups.Clone();

            _currentVideoGroups.ForEach(x =>
            {
                _allFreeVideoDatas.AddRange(from element in x.VideosList.Where(item => item.isFree.Equals(true))
                                            orderby element.UploadDate.FromUnixTime() descending
                                            select element);

                _allPremiumVideoDatas.AddRange(from element in x.VideosList.Where(item => item.isFree.Equals(false))
                                               orderby element.UploadDate.FromUnixTime() descending
                                               select element);
            });

            _allVideoData = _allFreeVideoDatas.Concat(_allPremiumVideoDatas).ToList();


            StartCoroutine(OnPressFav(true)); //initialize the fav on gallery populate

            //---new code[UIScrollable]
            GalleryScrollable.SetPrefab(VideoPlayerDataManager.Instance.GalleryThumbnailPrefab)
                .SetData(inVideoGroups).SetFunction((data, index, obj) =>
                {
                    var gData = (List<GameData.VideoGroup>)data;
                    var gHelper = obj.GetComponent<ThumbnailHelper>();
                    gHelper.SetContentType(ThumbnailTypes.Gallery, VideoPlayerDataManager.ContentType);
                    gHelper.SetThumbnailDetails(gData[index].GroupName, gData[index].GroupThumbnail, null,
                        () => StartCoroutine(InstantiateVideoObjects(gData[index], VideoListScrollable)));
                }).Initialize();
        }

        yield return new WaitForEndOfFrame();
        UILoader.Instance.StopLoader(true);
    }

    #endregion

    #region Button Actions

    IEnumerator OnPressNew()
    {
        UILoader.Instance.StartLoader();

        List<GameData.VideoData> videos = GetNewVideosList();

        GameData.VideoGroup vg = new GameData.VideoGroup("New Videos", videos);
        yield return StartCoroutine(InstantiateVideoObjects(vg, VideoListScrollable));

        yield return new WaitForEndOfFrame();

        UILoader.Instance.StopLoader(true);
    }

    List<GameData.VideoData> GetNewVideosList()
    {
        List<GameData.VideoData> videos = new List<GameData.VideoData>();
        var today = DateTimeExtensions.GetCurrentUnixTime();

        if (_allVideoData.Any())
        {
            foreach (var video in _allVideoData)
            {
                //new videos are now considered only if they are atmost 7 days old...more than that, it will ignore
                var diff = DateTimeExtensions.DifferenceBetweenTwoUnixTime(today, video.UploadDate);
                if (diff <
                    new TimeSpan(7, 0, 0, 0).TotalSeconds)
                {
                    videos.Add(video);
                }
                //  yield return true;
            }
        }
        return videos;
    }

    IEnumerator OnPressFree()
    {
        UILoader.Instance.StartLoader();

        List<GameData.VideoData> videos = new List<GameData.VideoData>();

        if (_allFreeVideoDatas.Any())
        {
            _allFreeVideoDatas.ForEach(video => { videos.Add(video); });
            yield return new WaitForEndOfFrame();
        }

        GameData.VideoGroup vg = new GameData.VideoGroup("Free Videos", videos);
        yield return StartCoroutine(InstantiateVideoObjects(vg, VideoListScrollable));

        yield return new WaitForEndOfFrame();

        UILoader.Instance.StopLoader(true);
    }

    IEnumerator OnPressFav(bool initcall = false)
    {

        if (initcall)
        {
            List<GameData.VideoData> videos = new List<GameData.VideoData>();
            var storedFavList = LocalDataManager.Instance.SaveData.FavoriteVideos.FavVideoList.Clone();


            _initializingFavoriteData = true;
            //finding the fav videos

            foreach (var video in _allVideoData)
            {
                for (int i = 0; i < storedFavList.Count; i++)
                {
                    if (video.VideoUrl == storedFavList[i])
                    {
                        videos.Add(video);

                        storedFavList.Remove(storedFavList[i]); //optimizing-> removing the fav that is already found
                    }
                }


                if (!storedFavList.Any()) //break the loop after finding all the favorites
                {
                    break;
                }
            }


            VideoPlayerDataManager.Instance.FavoriteVideos = videos;



            _initializingFavoriteData = false;

        }
        else
        {
            UILoader.Instance.StartLoader();

            //yield return new WaitWhile(() => true);


            GameData.VideoGroup vg = new GameData.VideoGroup("Favorite Videos", VideoPlayerDataManager.Instance.FavoriteVideos);

            yield return StartCoroutine(InstantiateVideoObjects(vg, VideoListScrollable));
        }

        yield return new WaitForEndOfFrame();

        UILoader.Instance.StopLoader(true);
    }

    public override void PressBack()
    {
        vgm.ads.AdsCheckManager.Instance.CloseBanner();
        UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());

        double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
        FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
        AnalyticsScript.instance.LogEvent("activity_session_time", "gallery", totalTimeSpent);
    }

    #endregion

    #region Action

    IEnumerator InstantiateVideoObjects(GameData.VideoGroup videoGroup, UIScrollable videoScrollable)
    {


        UILoader.Instance.StartLoader();
        _currentVideoObj.Clear();
        yield return new WaitForEndOfFrame();

        ;
        videoGroup.VideosList.Sort((a, b) => b.UploadDate.FromUnixTime().CompareTo(a.UploadDate.FromUnixTime())); //descending

        //Differentiating free and premium contents 
        var locked = videoGroup.VideosList.FindAll(x => !x.isFree);
        var free = videoGroup.VideosList.FindAll(x => x.isFree);

        var groupVideos = free.Concat(locked).ToList();


        if (groupVideos.Any())
        {

            //---new code[UIScrollable]
            videoScrollable.SetPrefab(VideoPlayerDataManager.Instance.VideoThumbnailPrefab)
                .SetData(groupVideos).SetFunction((data, index, obj) =>
                {


                    _currentVideoObj.Add(obj);
                    var videoDatas = (List<GameData.VideoData>)data;
                    var vHelper = obj.GetComponent<ThumbnailHelper>();

                    vHelper.SetContentType(ThumbnailTypes.Video, VideoPlayerDataManager.ContentType);

                    vHelper.SetThumbnailDetails(videoDatas[index].VideoName, videoDatas[index].ThumbnailUrl,
                        () => { VideoPlayerDataManager.Instance.VideoItemInitialize(videoDatas[index], vHelper); }, //moved the defination to Video Player Manager class
                        () => { VideoPlayerDataManager.Instance.VideoItemOnClick(videoDatas[index], vHelper, groupVideos); });
                }).Initialize();
        }
        else
        {
            Debug.LogError("Clear Content getting called In Else    ");
            videoScrollable.ClearContent();
        }

        VideoGridTitle.text = videoGroup.GroupName;
        VideoPlayerDataManager.Instance.ViewController.Activate(VideoGridPanel, UIType.NormalView);
        yield return new WaitForEndOfFrame();
        UILoader.Instance.StopLoader();
    }

    #endregion
}