﻿

public interface IPlayer
{
    void SetVideoPath(string caller);
    void Play();
    void Pause();
    void Stop();
    bool IsVideoPlaying();
    void SetMediaEndReached(System.Action onEndReached);
    void SetPlayerStartedAction(System.Action onPrepared);
    void SetPrepared(System.Action onPrepared);
    void Seek(float sliderPos);
    void EmptyPath();

    string CurrentTime();
    string GetTotalSeconds();
    double GetSliderValue();
}

