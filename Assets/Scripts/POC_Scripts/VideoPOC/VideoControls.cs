﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using vgm.ads;

[RequireComponent(typeof(CanvasGroup))]
public class VideoControls : MonoBehaviour
{
    public EventTrigger Trigger;
    public RectTransform VideoPlayer;
    public RectTransform RootTransform;
    public Button FullscreenButton, MinimizeButton;
    public AspectRatioFitter aspectRatioFitter;

    public bool _fullscreen = false;
    private Vector2 _defaultPlayerSize;
    private Vector2 _defaultPlayerPos;
    private Vector2 _defAnchorMax, _defAnchorMin;


    private Coroutine _hideControlCoroutine;
    private CanvasGroup _canvasGroup;
    private float _activeDuration = 4f;
    private Sequence controlsSeq;



    public static Action<bool> OnVideoFullScreen;
    public Button backBtn;

    public float minimizedYSize, maximizedYSize;

    [Header("Video Grid")]
    public UIPanel videoPanel;

    void Awake()
    {
        _defaultPlayerSize = VideoPlayer.sizeDelta;
        _defaultPlayerPos = VideoPlayer.anchoredPosition;
        _defAnchorMax = VideoPlayer.anchorMax;
        _defAnchorMin = VideoPlayer.anchorMin;
    }

    void Start()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.alpha = 0;
        MinimizeButton.Deactivate();
    }

    public void DeleteAd()
    {
        AdsCheckManager.Instance.CloseBanner();
        StartCoroutine(ShowBannerIfAllowed());
    }

    IEnumerator ShowBannerIfAllowed()
    {
        yield return new WaitForSeconds(0.3f);
        AdsCheckManager.Instance.ShowBannerIfAllowed();
    }

    public void SetControlBar(bool active)
    {
        if (controlsSeq != null)
            controlsSeq.Kill();

        if (active)
        {
            StartCoroutine(Delay());
            IEnumerator Delay()
            {
                _canvasGroup.alpha = 1;
                yield return new WaitForSeconds(_activeDuration);
                _canvasGroup.alpha = 0;
            }
        }
    }

    public void FullScreen()
    {
        aspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
        aspectRatioFitter.aspectRatio = 2.28f;
        var rect = backBtn.GetComponent<RectTransform>();
        rect.offsetMax = new Vector2(rect.offsetMax.x, 1070f);
    }

    public void NotFullScreen()
    {
        aspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
        aspectRatioFitter.aspectRatio = 1.70f;
        var rect = backBtn.GetComponent<RectTransform>();
        rect.offsetMax = new Vector2(rect.offsetMax.x, 900f);
    }
}