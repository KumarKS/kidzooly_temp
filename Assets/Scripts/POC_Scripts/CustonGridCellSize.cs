﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustonGridCellSize : MonoBehaviour
{
    public float width;
    public int adjustedCellSize = 70;

    // Use this for initialization
    void Start()
    {
        width = this.gameObject.GetComponent<RectTransform>().rect.width;
        Vector2 newSize = new Vector2((width / 2) - adjustedCellSize, this.gameObject.GetComponent<GridLayoutGroup>().cellSize.y);
        this.gameObject.GetComponent<GridLayoutGroup>().cellSize = newSize;
    }
}
