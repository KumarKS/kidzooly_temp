﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StoreItem : MonoBehaviour 
{
	public Text descriptionText, priceText, offPercText, freeTrialDaysText,billingText;

	public void Init(string description, string price, int offPerc = 0, int freeTrialDays = 0)
	{
        string num = new String(description.Where(Char.IsDigit).ToArray());
        this.billingText.text = string.Format("Billed every {0} {1}", num, GetPeriodString(description));

        this.descriptionText.text = description;
		this.priceText.text = price;

		if (offPerc > 0)
		{
			offPercText.transform.parent.Activate ();
            
			offPercText.text = offPerc.ToString () + " %\nOff";
		}
		else
		{
			offPercText.transform.parent.Deactivate ();
		}

		if (freeTrialDays > 0)
		{
			freeTrialDaysText.Activate ();
			freeTrialDaysText.text = "(Free " + freeTrialDays.ToString () + " Days Trial)";
		}
		else
		{
			freeTrialDaysText.Deactivate ();
		}
	}

    string GetPeriodString(string maintext)
    {
        if (maintext.Contains("Week")) return "Week";
        else if (maintext.Contains("Month")) return "Month";
        else return "Year";
    }
}
