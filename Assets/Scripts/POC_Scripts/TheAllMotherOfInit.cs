﻿using System;
using Assets.SimpleAndroidNotifications;
using UnityEngine;
using System.Collections;

public class TheAllMotherOfInit : MonoBehaviour
{
    private const int MaxScreensNeedsToBeShown = 3;
    private int currentScreensShown;

    void Awake()
    {
#if UNITY_EDITOR || LOGS
        UnityEngine.Debug.unityLogger.logEnabled = true;
#else
        UnityEngine.Debug.unityLogger.logEnabled = false;
#endif
    }

    IEnumerator WaitForTutorialAndLoad()
    {
        while (currentScreensShown < MaxScreensNeedsToBeShown)
            yield return new WaitForSeconds(1f);
        UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription(), false);
    }

    void OnEnable()
    {
        LocalDataManager.Instance.SaveData.IncrementCurrentSession();

        Input.multiTouchEnabled = false;
        Tutorial.OnPageChange += () => { currentScreensShown++; };
        ConfigManager.OnConfigLoaded += () =>
        {
            ConfigManager.Instance.FetchLocalizedAudio(
                () => StartCoroutine(WaitForTutorialAndLoad()),
                () =>
                    UIPopupBox.Instance.SetDataOk("Oops!, There seems to be an issue....",
                    ConfigManager.Instance.FetchVersion));

            SplashLoader.Instance.SetVersion(LocalDataManager.Instance.SaveData.ConfigVersion.ToString());
        };

        //Queue Local Notification On App Start
#if UNITY_ANDROID && !UNITY_EDITOR
        NotificationManager.SendWithAppIcon(TimeSpan.FromDays(1), "KidZooly", GameConstants.NOTIFICATIONTEXTS[UnityEngine.Random.Range(0, GameConstants.NOTIFICATIONTEXTS.Length)], new Color(0, 0.6f, 1));
        NotificationManager.SendWithAppIcon(TimeSpan.FromDays(2), "KidZooly", GameConstants.NOTIFICATIONTEXTS[UnityEngine.Random.Range(0, GameConstants.NOTIFICATIONTEXTS.Length)], new Color(0, 0.6f, 1));

#elif UNITY_IOS && !UNITY_EDITOR
        
//        UnityEngine.iOS.NotificationServices.RegisterForNotifications(
//             NotificationType.Alert |
//             NotificationType.Badge |
//             NotificationType.Sound);
//
//        UnityEngine.iOS.LocalNotification iosNotificationTomorrow = new UnityEngine.iOS.LocalNotification();
//        UnityEngine.iOS.LocalNotification iosNotificationDayAfter = new UnityEngine.iOS.LocalNotification();
//
//        iosNotificationTomorrow.alertAction = "KidZooly";
//        iosNotificationTomorrow.alertBody =
//            GameConstants.NOTIFICATIONTEXTS[UnityEngine.Random.Range(0, GameConstants.NOTIFICATIONTEXTS.Length)];
//
//        iosNotificationDayAfter.alertAction = "KidZooly";
//        iosNotificationDayAfter.alertBody =
//            GameConstants.NOTIFICATIONTEXTS[UnityEngine.Random.Range(0, GameConstants.NOTIFICATIONTEXTS.Length)];
//
//        var now = DateTime.Now;
//        var tomorrow = now.AddDays(1);
//        var dayaftertomorrow = now.AddDays(2);
//
//        iosNotificationTomorrow.fireDate = tomorrow;
//        iosNotificationDayAfter.fireDate = dayaftertomorrow;
//
//        UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(iosNotificationTomorrow);
//        UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(iosNotificationDayAfter);

#endif
    }

    void OnDisable()
    {
        ConfigManager.ProgressCounter = null;
        ConfigManager.OnConfigLoaded = null;
        Tutorial.OnPageChange = null;
    }

    void Start()
    {
        VGMinds.CloudMessaging.CloudMessagingManager.Init();
    }

    public void Initialize()
    {
        Debug.Log("MotherOfAllInitCalled");

        ConfigManager.Instance.FetchVersion();

        SplashLoader.Instance.SetText("Loading");
    }
}