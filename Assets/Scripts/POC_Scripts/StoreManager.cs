﻿using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Linq;

public class StoreManager : MonoBehaviour
{
	public StoreItem StoreItemPrefab;
    public GameObject ProductContainer;
    public AudioController AudController;
    public Text PremiumUserStatus;

    public GameObject SubscriptionDetails;

    void OnEnable()
    {
        PurchaseManager.OnInitSuccess += OnStoreInitSuccess;
        PurchaseManager.OnInitFailure += OnStoreInitFailure;

        PurchaseManager.OnPurchaseSuccess += OnPurchaseSuccess;
        PurchaseManager.OnPurchaseFailure += OnPurchaseFailure;
        HardwareInputManager.OnBack += Back;

#if UNITY_ANDROID
		SubscriptionDetails.Deactivate();
#endif

    }



    void OnDisable()
    {
        PurchaseManager.OnInitSuccess -= OnStoreInitSuccess;
        PurchaseManager.OnInitFailure -= OnStoreInitFailure;

        PurchaseManager.OnPurchaseSuccess -= OnPurchaseSuccess;
        PurchaseManager.OnPurchaseFailure -= OnPurchaseFailure;
        HardwareInputManager.OnBack -= Back;
    }


    void OnPurchaseSuccess()
    {
		Debug.Log ("OnPurchaseSuccess");
        UILoader.Instance.StopLoader(true);
        UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnPurchaseSuccess") ?? "Purchase Successfull", 5);
        AudController.TriggerAudio("ThanksForPurchase");
        PopulateStoreItems();
    }

    void OnPurchaseFailure()
    {
		Debug.Log ("OnPurchaseFailure");
        UILoader.Instance.StopLoader(true);
        UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnPurchaseFailure") ?? "Purchase Failed", 5);
        PopulateStoreItems();
    }


    void Start()
    {
        PremiumUserStatus.gameObject.Deactivate();

		UILoader.Instance.StartLoader();
		if (PurchaseManager.Instance.IsInitialized ())
		{
			PurchaseManager.Instance.InitializeStore (new System.Collections.Generic.Dictionary<string, string>());
		}
		else
		{
			OnStoreInitFailure ();
		}
        PopulateStoreItems();
    }

    private void OnStoreInitSuccess()
    {
        UILoader.Instance.StopLoader(true);
        PopulateStoreItems();
    }

    //testcode
    /**void temp()
    {
        string description = "1 Week Subscription";
        string num = new String(description.Where(Char.IsDigit).ToArray());
        Debug.LogError(string.Format("Billed every {0} {1}", num, GetPeriodString(description)));
    }

    string GetPeriodString(string maintext)
    {
        if (maintext.Contains("Week")) return "Week";
        else if (maintext.Contains("Month")) return "Month";
        else return "Year";
    }**/
    //

    void PopulateStoreItems()
    {
        ProductContainer.GetComponentsInChildren<StoreItem>(false).ToList().ForEach(x => Destroy(x.gameObject));
        Debug.LogError("PopulateStoreItems");
        if (LocalDataManager.Instance.SaveData.IsPremiumUser())
        {
			Debug.Log("PremiumUser");
            PremiumUserStatus.gameObject.Activate();
            var dispText =
                (LocalizedText.GetLocalizedText("ActiveSubscriber") ?? "You are subscribed for TITLE and NO ADs from DATE").Replace("TITLE",
                    LocalDataManager.Instance.SaveData.SubscriptionDetails["Title"] ?? "N/A")
                .Replace("DATE", LocalDataManager.Instance.SaveData.SubscriptionDetails["Date"] ?? "N/A");


            PremiumUserStatus.SetText(dispText);
        }
        else
        {
			Debug.LogError("NotAPremiumUser");
            PurchaseManager.Instance.GetProductsForStore().ForEach(product =>
            {
                Debug.LogError("Store ITem" + product.metadata + "Reciept" + product.receipt    );
                var storeItem = Instantiate(StoreItemPrefab, ProductContainer.transform, false);
                Debug.LogError("NotApremUser 1");
				storeItem.gameObject.SetActive(true);

                Debug.LogError("NotApremUser 2");
                int trialPeriod = 0;
				int percOff = 0;
                Debug.LogError("NotApremUser 3");
                PurchaseManager.Instance.GetPayouts(product.definition.id).ForEach(item => 
				{
					if(item.data == "TrialPeriod")
					{
						trialPeriod = (int) item.quantity;
					}
					if(item.data == "OfferPercentage")
					{
						percOff = (int) item.quantity;
					}
				});
                Debug.LogError("NotApremUser 4");
                string iapTitle = product.metadata.localizedDescription.Before("Subscription");

                Debug.LogError("NotApremUser 5");
                if (string.IsNullOrEmpty(iapTitle))
				{
					iapTitle = PurchaseManager.Instance.GetDefaultProductDescription(product.definition.id).Before("Subscription");
				}

                Debug.LogError("NotApremUser 6");
                iapTitle += "";

                Debug.LogError("NotApremUser 7");
                storeItem.Init(iapTitle, product.metadata.localizedPriceString, percOff , trialPeriod);

                Debug.LogError("NotApremUser 8");
                storeItem.GetComponent<Button>().onClick.AddListener(() =>
                {
					if(VGInternet.Instance.Status)
					{
						UILoader.Instance.StartLoader();
						UIToastNotification.Instance.TriggerToast("Processing Purchase...", 5);
						Debug.Log("Purchasing " + product.definition.id);
						PurchaseManager.Instance.BuyProduct(product.definition.id);
					}
					else
					{
						UIToastNotification.Instance.TriggerToast("No Internet Connection", 5);
					}
                });
                Debug.LogError("NotApremUser 9");

            });
        }

    }

    public void OnClickFAQ()
    {
        Application.OpenURL("http://www.vgminds.com/faq.html");
    }
    public void OnClickTOS()
    {
        Application.OpenURL("http://www.vgminds.com/terms.html");
    }

    public void OnClickPrivacyPolicy()
    {
        Application.OpenURL("http://www.vgminds.com/privacy.html");
    }

    public void OnClickSubscriptionDetails()
    {
        UIPopupBox.Instance.SetDataOk("* Subscribe to get access to full content including future updates.\r\n* Five options : $0.99/Week, $1.99/Month, $4.99/3 months, $9.49/6 \r\n Months and $15.49/1 Year.\r\n* Payment will be charged to iTunes Account at confirmation of purchase.\r\n* Cancel subscription renewal anytime under Manage App Subscriptions in your iTunes Settings.\r\n* Subscription automatically renews unless auto-renewal is turned off at least 24-hours before the end of the current period.\r\n* Account will be charged for renewal within 24-hours prior to the end of the current period.\r\n* Use the subscription in any iPad/iPhone/iPod registered with your Apple account.\r\n* No cancellation of the current subscription is allowed during active subscription period.", null);
    }



    private void OnStoreInitFailure()
    {
        UIToastNotification.Instance.TriggerToast(LocalizedText.GetLocalizedText("OnStoreLoadFail") ?? "Store Failed To Load...Come Back Again", 5);
        UILoader.Instance.StopLoader(true);
    }

    public void Back()
    {
        UISceneLoader.Instance.LoadPreviousScene();
    }
}