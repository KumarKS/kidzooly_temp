﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

[Serializable]
public class ParticleElements
{
    public ParticleSystem Particle;
    public string ParticleTag;
}

public class ParticleManager : GenericManager<ParticleManager>
{
    #region Singleton

    public static ParticleManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(ParticleManager)) as ParticleManager;
                if (_instance == null)
                {
                    var go = Instantiate(Resources.Load("ParticleManager")) as GameObject;
                    _instance = go.GetComponent<ParticleManager>();
                    //  _instance.GetCanvas.worldCamera = Camera.main;
                    _instance.gameObject.name = _instance.GetType().Name;
                    isSpawnedAlready = true;
                }
            }
            return _instance;
        }
        set { _instance = value; }
    }

    #endregion

    public List<ParticleElements> Particles;

    public void EmitParticle(string particleTag, Vector3 emitLocation)
    {
        var origObj = Particles.Find(x => x.ParticleTag.Equals(particleTag));
        if (origObj != null)
        {
            var gObj = Instantiate(origObj.Particle, emitLocation, Quaternion.identity);
            gObj.Activate();

#if UNITY_ANDROID || UNITY_IOS
            if (!Application.isEditor)
                gObj.transform.DOScale(2, 0);
#endif
            gObj.transform.SetParent(transform, true);
			gObj.transform.position = emitLocation;
        }
    }

    public void EmitMultipleParticles(List<string>particleTags, Vector3 emitLocation)
    {
        foreach (var pTags in particleTags)
        {
            EmitParticle(pTags, emitLocation);
        }
    }

    //test---
    public void OnClick(BaseEventData bData)
    {
        var pData = (PointerEventData) bData;

        EmitParticle("click", pData.position);
    }
}