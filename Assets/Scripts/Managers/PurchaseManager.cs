﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using System.Text;
using System.Collections;
using FirebaseAuthAPI;

public class PurchaseManager : GenericManager<PurchaseManager>, IStoreListener
{
    private static IStoreController StoreController = null;
    private static IExtensionProvider StoreExtensionProvider = null;

    public static Action OnPurchaseSuccess;
    public static Action OnInitSuccess;
    public static Action OnInitFailure;
    public static Action OnPurchaseFailure;
    private bool _isInitializing = false;

    private IAppleConfiguration appleConfig;

    //
    private bool _isRestoreAllowed = false;
    private ProductCatalog catalog;

    private IReceiptValidator receiptValidator;



    public void InitializeStore(Dictionary<string, string> updatedValues)
    {
        StartCoroutine(_InitializeStore(updatedValues));

    }

    public IEnumerator _InitializeStore(Dictionary<string, string> updatedValues)
    {
        yield return new WaitForSeconds(1f);
        Debug.Log("Initializing");
        if (IsInitialized())
        {
            Debug.LogError("Purchase init done");
            OnInitSuccess.SafeInvoke();
            yield break;
        }
        Debug.Log("Not initialized");
        if (_isInitializing)
        {
            yield break;
        }
        Debug.Log("Not initializing");
        _isInitializing = true;
        var module = StandardPurchasingModule.Instance();
        var builder = ConfigurationBuilder.Instance(module);
        var catalogJson = LocalDataManager.Instance.GetConfigJson(EConfigFileName.IAPCatalog);
        Debug.LogError("CATALOG : " + catalogJson.ToString());
        catalog = ProductCatalog.Deserialize(catalogJson);


#if UNITY_IOS
        appleConfig = builder.Configure<IAppleConfiguration>();
#endif
        foreach (var product in catalog.allProducts)
        {
            if (product.allStoreIDs.Count > 0)
            {
                Debug.LogError("Before adding it to builder :-" + product.id);
                var ids = new IDs();
                foreach (var storeId in product.allStoreIDs)
                {
                    Debug.LogError("Purchasemanager 1" + storeId.id);
                    if (updatedValues.ContainsKey(storeId.id))
                    {
                        ids.Add(updatedValues[storeId.id], storeId.store);
                    }
                    else
                    {
                        Debug.LogError("Purchasemanager 2");
                        ids.Add(storeId.id, storeId.store);
                    }


                }
                builder.AddProduct(product.id, product.type, ids);
            }
            else
            {

                Debug.LogError("Purchasemanager 3");
                if (updatedValues.ContainsKey(product.id))
                {
                    builder.AddProduct(updatedValues[product.id], product.type);
                }
                else
                {
                    builder.AddProduct(product.id, product.type);
                }

            }
        }
        UnityPurchasing.Initialize(this, builder);
    }

    void RevalidateSubscription(Action<string> onSuccess = null, Action onFail = null)
    {
        Debug.Log("Revalidating");
        receiptValidator.Validate(StoreController.products.all,
            (result) =>
        {
            Debug.Log("Successfully validated : " + result.id + " " + result.purchaseDate);
            _isRestoreAllowed = true;
            LocalDataManager.Instance.SaveData.SubscriptionDetails.SafeAdd("Title", result.id);
            LocalDataManager.Instance.SaveData.SubscriptionDetails.SafeAdd("Date", result.purchaseDate);
            LocalDataManager.Instance.SaveData.SubscriptionDetails.SafeAdd("TransactionID", result.transactionID);
            OnPurchaseSuccess.SafeInvoke();
            onSuccess.SafeInvoke(result.id);
            LocalDataManager.Instance.CheckClientAndServerStatus();
        },
            () =>
        {
            Debug.Log("Validation failed");
            _isRestoreAllowed = false;
            LocalDataManager.Instance.SaveData.SubscriptionDetails.Clear();
            OnPurchaseFailure.SafeInvoke();
            onFail.SafeInvoke();
            LocalDataManager.Instance.CheckClientAndServerStatus();
        }
        );
    }

    public bool IsInitialized()
    {
        return StoreController != null && StoreExtensionProvider != null;
    }

    public void BuyProduct(string productid)
    {
        UILoader.Instance.StartLoader();
        BuyProductId(productid);
    }


    public void BuyProductId(string productId)
    {
        AnalyticsScript.instance.LogEvent("purchase", "clicked", productId);

        if (IsInitialized())
        {
            Product product = StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                StoreController.InitiatePurchase(product);
                VGMinds.Analytics.AnalyticsManager.LogSubscriptionMade("new_subscription_success" + product.definition.id);
            }
            else
            {
                UILoader.Instance.StopLoader(true);
                OnPurchaseFailure.SafeInvoke();
                Debug.Log("BuyProductId: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            UILoader.Instance.StopLoader(true);
            OnPurchaseFailure.SafeInvoke();
            Debug.Log("BuyProductId FAIL. Not initialized.");
        }
    }

    public void RestorePurchases(Action onSuccess, Action onFailure)
    {
        UILoader.Instance.StartLoader();
        if (!IsInitialized())
        {
            //Debug.Log ("RestorePurchases FAIL. Not initialized.");
            onFailure.SafeInvoke();
            UILoader.Instance.StopLoader(true);
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            //Debug.Log ("RestorePurchases started ...");

            var apple = StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                    //				//Debug.Log ("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");

                    if (result)
                {
                    if (_isRestoreAllowed)
                    {
                        _isRestoreAllowed = false;
                        onSuccess.SafeInvoke();
                        UIPopupBox.Instance.SetDataOk("Restoration Successful!", null);
                    }
                    else
                    {
                        onFailure.SafeInvoke();
                        UIPopupBox.Instance.SetDataOk("No valid purchase available in this account to restore!!", null);
                    }
                    UILoader.Instance.StopLoader(true);
                }
                else
                {
                    onFailure.SafeInvoke();
                    UILoader.Instance.StopLoader(true);
                }
            });
        }
        else
        {
            //Debug.Log ("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            onFailure.SafeInvoke();
            UILoader.Instance.StopLoader(true);
        }
    }


    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized");
        _isInitializing = false;
        StoreController = controller;
        StoreExtensionProvider = extensions;

#if AmazonStoreBuild
		    receiptValidator = new AmazonStoreReceiptValidation (this, extensions.GetExtension<IAmazonExtensions> ().amazonUserId);
#elif GoogleStoreBuild
            receiptValidator = new GooglePlayReceiptValidation();
#elif AppleStoreBuild
        receiptValidator = new AppleStoreReceiptValidation(appleConfig);
#else
		    receiptValidator = new DefaultReceiptValidation();
#endif
        RevalidateSubscription();
        OnInitSuccess.SafeInvoke();
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        OnInitFailure.SafeInvoke();
        _isInitializing = false;
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log("ProcessPurchase");
        Product purchasedProduct = args.purchasedProduct;

        if (purchasedProduct != null)
        {

#if UNITY_ANDROID && !AmazonStoreBuild
            AnalyticsScript.instance.LogEvent("purchase", "success", args.purchasedProduct.definition.id);

            var wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(args.purchasedProduct.receipt);
            var payload = (string)wrapper["Payload"];
            var gpDetails = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
            var gpJson = (string)gpDetails["json"];
            var gpDetails1 = (Dictionary<string, object>)MiniJson.JsonDecode(gpJson);
            string pToken = gpDetails1["purchaseToken"].ToString();
                FirebaseAPI.Instance.CheckUserExits(
                        (success) =>
                        {
                            var receivedUid = (Dictionary<string, object>)MiniJson.JsonDecode(success);
                            string olduid = (string)receivedUid["uid"];

                            if (olduid == null)
                            {
                                FirebaseAPI.Instance.VerifyTheGoogleReceipt(
                                    (s) => { Debug.Log("OnMarketPurchase receipt s : " + s);
                                        LocalDataManager.Instance.CheckSubscriptionStatus();
                                        LocalDataManager.Instance.CheckClientAndServerStatus();
                                    },
                               
                                    (f) => { Debug.Log("OnMarketPurchase receipt f: " + f);
                                    },

                                    args.purchasedProduct.definition.id, pToken);
                            }
                            else
                            {
                                LoginHandler.Instance.DebugLog("USER already exists : " + olduid);
                                PlayerPrefs.SetString("userID", olduid);
                                PlayerPrefs.Save();
                                FirebaseAPI.Instance.GetTheUserCustomToken((s) => {
                                    Debug.Log("GetTheUserCustomToken: " + s);
                                    LoginHandler.Instance.SignInWithCustomToken(s);
                                }, olduid);

                            }
                        },
                        pToken
                );
#endif


            // LogToGUI.Log("cvvvvvvvvv");
            //            Answers.LogPurchase(purchasedProduct.metadata.localizedPrice, purchasedProduct.metadata.isoCurrencyCode, true, purchasedProduct.metadata.localizedTitle, purchasedProduct.definition.type.ToString(), purchasedProduct.definition.id);
        } 

        else {
            AnalyticsScript.instance.LogEvent("purchase", "fail", args.purchasedProduct.definition.id);
        }

        RevalidateSubscription((result) =>
        {
            UILoader.Instance.StopLoader(true);
            VGMinds.Analytics.AnalyticsManager.LogSubscriptionMade(result);
        });
        //
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        OnPurchaseFailure.SafeInvoke();
        UILoader.Instance.StopLoader(true);
    }

    public string GetDefaultProductDescription(string id)
    {
        if (IsInitialized() && StoreController != null)
        {
            var ProductCatalogItem = catalog.allProducts.ToList().Find(x => x.id == id);
            if (ProductCatalogItem != null)
            {
                return ProductCatalogItem.defaultDescription.Title;
            }
        }
        return string.Empty;
    }

    public List<Product> GetProductsForStore()
    {
        var products = new List<Product>();
        if (IsInitialized() && StoreController != null)
        {
#if AmazonStoreBuild
				    return StoreController.products.all.ToList ().FindAll (x => x.definition.id != AmazonStoreReceiptValidation.parentId);
#else
            return StoreController.products.all.ToList();
#endif
        }
        return products;
    }

    public Product GetProduct(string id)
    {
        if (IsInitialized())
        {
            return StoreController.products.all.ToList().Find(x => x.definition.id == id || x.definition.storeSpecificId == id);
        }
        return null;
    }

    public List<ProductCatalogPayout> GetPayouts(string id)
    {
        List<ProductCatalogPayout> payouts = new List<ProductCatalogPayout>();
        ProductCatalogItem item = catalog.allProducts.ToList().Find(x => x.id == id);
        if (item != null)
        {
            return item.Payouts.ToList();
        }
        return payouts;
    }
}