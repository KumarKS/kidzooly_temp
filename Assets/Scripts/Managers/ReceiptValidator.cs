﻿using System;
using System.Linq;

namespace UnityEngine.Purchasing.Security
{
	class ReceiptInfo
	{
		public string id;
		public string purchaseDate;
        public string transactionID;
		public ReceiptInfo(string id, string purchaseDate , string transactionId)
		{
			this.id = id;
			this.purchaseDate = purchaseDate;
            this.transactionID = transactionId;
		}
	}

	interface IReceiptValidator
	{
		void Validate (Product[] products, Action<ReceiptInfo> OnReceiptFound, Action OnReceiptNotFound = null);
	}

	class DefaultReceiptValidation : IReceiptValidator
	{
		public void Validate (Product[] products, Action<ReceiptInfo> OnReceiptFound, Action OnReceiptNotFound = null)
		{
			foreach (var product in products)
			{
				if (product.hasReceipt)
				{
					OnReceiptFound(new ReceiptInfo (product.metadata.localizedTitle, null, null));
					return;
				}
			}
			OnReceiptNotFound ();
		}
	}

	class GooglePlayReceiptValidation : IReceiptValidator
	{
		public void Validate (Product[] products, Action<ReceiptInfo> OnReceiptFound, Action OnReceiptNotFound = null)
		{
			try
			{
				var validator = new CrossPlatformValidator(GooglePlayTangle.Data(), null, Application.identifier);
				foreach (var product in products)
				{
					if (product.hasReceipt)
					{
						var result = validator.Validate(product.receipt);
						if (result != null && result.Length > 0)
						{
							OnReceiptFound(new ReceiptInfo (product.metadata.localizedTitle, result [0].purchaseDate.ToShortDateString (), result[0].transactionID));
							return;
						}
					}
				}
				OnReceiptNotFound ();
			}
			catch (IAPSecurityException e)
			{
				Debug.Log("Error on Revalidating " + e.Message);
				OnReceiptNotFound ();
			}
		}
	}

	class AppleStoreReceiptValidation : IReceiptValidator
	{
		IAppleConfiguration appleConfig;

		public AppleStoreReceiptValidation(IAppleConfiguration appleConfig)
		{
			this.appleConfig = appleConfig;
		}

		public void Validate (Product[] products, Action<ReceiptInfo> OnReceiptFound, Action OnReceiptNotFound = null)
		{
			try
			{
				var validator = new CrossPlatformValidator(null, AppleTangle.Data(), Application.identifier);

				if(appleConfig != null && !string.IsNullOrEmpty(appleConfig.appReceipt))
				{
					string appReceipt = appleConfig.appReceipt;
					string unityIAPReceipt = "{ \"Store\": \"AppleAppStore\", \"Payload\": \"" + appReceipt + "\" }";
					var result = validator.Validate(unityIAPReceipt);

					foreach (IPurchaseReceipt productReceipt in result)
					{
						var prodReceipt = productReceipt as AppleInAppPurchaseReceipt;
						var difference =
							DateTimeExtensions.DifferenceBetweenTwoUnixTime(
								prodReceipt.subscriptionExpirationDate.ToUnixTime(),
								DateTimeExtensions.GetCurrentUnixTime());
						if (difference > 0)
						{
                            OnReceiptFound(new ReceiptInfo(products.ToList().Find(x => x.definition.id == prodReceipt.productID || x.definition.storeSpecificId == prodReceipt.productID).metadata.localizedTitle, productReceipt.purchaseDate.ToShortDateString(), productReceipt.transactionID));
                            Debug.LogError("TransationID bottom :" + result[0].transactionID);
                            return;
						}
					}
				}
				OnReceiptNotFound ();
			}
			catch (IAPSecurityException e)
			{
				Debug.Log("Error on Revalidating " + e.Message);
				OnReceiptNotFound ();
			}
		}
	}

	class AmazonStoreReceiptValidation : IReceiptValidator
	{
		public const string parentId = "kidzooly.subscription.parent";

		const string amazonSharedSecret = "2:rLU2WIKjMvB5ZINcKOhrhWoF02PImIq_ArJkVm9GBnnd-DZRDJKYvlsiYTZPaemJ:bkmiJuzlGMqJPbrE0v09Cg==";
		string amazonUser;
		MonoBehaviour caller;

		const string amazonOfflineReceiptID = "amazonOfflineReceiptID";

		public AmazonStoreReceiptValidation(MonoBehaviour caller, string userID)
		{
			this.caller = caller;
			this.amazonUser = userID;
		}

		public void Validate (Product[] products, Action<ReceiptInfo> OnReceiptFound, Action OnReceiptNotFound = null)
		{
			Product receiptProduct = products.ToList ().Find (x => x.hasReceipt && !string.IsNullOrEmpty(x.transactionID));
			if (receiptProduct != null)
			{
				Debug.Log (receiptProduct.definition.id + " : " + receiptProduct.receipt);
				string url = "https://appstore-sdk.amazon.com/version/1.0/verifyReceiptId/developer/" + amazonSharedSecret + "/user/" + amazonUser + "/receiptId/" + receiptProduct.transactionID;
				caller.StartCoroutine 
				(
					DownloadData(url, (data)=> 
					{
						string downloadedData = data;

						if(string.IsNullOrEmpty(downloadedData))
						{
							Debug.Log("No data, checking offline receipts");
							downloadedData = PlayerPrefs.GetString(amazonOfflineReceiptID, string.Empty);
							if(string.IsNullOrEmpty(downloadedData))
							{
								Debug.Log("No offline receipts.. exiting");
								OnReceiptNotFound();
							}
							else
							{
								Debug.Log("offline receipts found");
								VerifyData(downloadedData, 
									(result)=> OnReceiptFound (new ReceiptInfo(result.term, DateTimeExtensions.FromUnixTime(result.purchaseDate).ToShortDateString(), null)),
									()=> OnReceiptNotFound());
							}
						}
						else
						{
							Debug.Log("online data found, saving offline receipt");
							PlayerPrefs.SetString(amazonOfflineReceiptID, downloadedData);
							VerifyData(downloadedData,
								(result)=> OnReceiptFound (new ReceiptInfo(result.term, DateTimeExtensions.FromUnixTime(result.purchaseDate).ToShortDateString(), null)),
								()=> OnReceiptNotFound());
						}
					})
				);
			} 
			else
			{
				Debug.Log ("No receipts product found");
				OnReceiptNotFound ();
			}
		}

		System.Collections.IEnumerator DownloadData(string url, Action<string> downloadedData)
		{
			Debug.Log (url);
			WWW www = new WWW (url);
			while (!www.isDone)
			{
				yield return null;
			}
			if (www.error == null)
			{
				downloadedData (www.text);
			}
			else
			{
				downloadedData (string.Empty);
			}
		}

		void VerifyData(string message, Action<AmazonReturnData> OnPurchaseFound, Action OnNoPurchaseFound)
		{
			Debug.Log ("verifying data");
			AmazonReturnData returnData = JsonUtility.FromJson<AmazonReturnData> (message);
			if (returnData != null)
			{
				Debug.Log ("Subscription found, checking validity");
				if (returnData.cancelDate == null)
				{
					Debug.Log ("Valid Subscription found");
					OnPurchaseFound (returnData);
				}
				else
				{
					Debug.Log ("Subscription Expired");
					OnNoPurchaseFound ();
				}
			} 
			else
			{
				Debug.Log ("Invalid data returned");
				OnNoPurchaseFound ();
			}
		}

		class AmazonReturnData
		{
			public bool betaProduct;
			public object cancelDate;
			public object parentProductId;
			public string productId;
			public string productType;
			public long purchaseDate;
			public object quantity;
			public string receiptId;
			public long renewalDate;
			public string term;
			public string termSku;
			public bool testTransaction;
		}
	}
}