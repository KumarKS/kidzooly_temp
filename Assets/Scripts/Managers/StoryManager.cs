﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;

public class StoryManager : GenericSingleton<StoryManager>
{
	public StoryUI storyUI;
	public GameObject StoryPrefab;
	public UIScrollableGrid gridParent;
	private GameData.StoryData currStory;
	private List<GameData.StoryData> StoryCollection;
	public Sprite storyLeftPage;

	[HideInInspector]
	public bool blockDownloads = false;

	void OnEnable ()
	{
		storyUI.storyBook.OnFlipWithNextPage += OnFlipPage;
		HardwareInputManager.OnBack += MainMenu;
		storyUI.audioController.TriggerAudio ("BackgroundMusic");
	}

	void OnDisable ()
	{
		storyUI.storyBook.OnFlipWithNextPage -= OnFlipPage;
		FileManager.RemoveReference (this);
		HardwareInputManager.OnBack -= MainMenu;
	}

	public void MainMenu ()
	{
		if (UILoader.Instance.IsPanelActive)
		{
			return;
		}
		UISceneLoader.Instance.LoadScene (EScenes.MainMenu.GetDescription ());
	}

	void Start ()
	{
		var r = ParticleManager.Instance;

		var orderedList = LocalDataManager.Instance.GetConfig<GameData.StoryCollection> (EConfigFileName.Stories).Stories.OrderBy (o => o.UploadDate.FromUnixTime ()).ToList ().ToList ();
		StoryCollection = orderedList.OrderBy (o => !o.IsFree).ToList ();
		PopulateMenu ();
	}

	public void PopulateMenu ()
	{
		gridParent.SetPrefab (StoryPrefab).SetData (StoryCollection).SetFunction ((o, ind,
		                                                                              g) => {
			var data = ((List<GameData.StoryData>)o) [ind];
			g.GetComponent<StoryUIElement> ().Init (data);
		}).Initialize ();


	}

	public void DisplayStoryPage (GameData.StoryData data, Sprite bg, Sprite cover, Sprite coverLeft, List<Sprite> pages, AudioClip storyClip)
	{
		currStory = data;
		storyUI.lastTimeStamp = 0f;
		storyUI.storyBook.bookPages = new Sprite[(currStory.PageList.Count * 2)];
		storyUI.BookCoverLeftSprite = coverLeft;

		//Set cover
		storyUI.storyBook.RightNext.sprite = cover; //Book Cover
		storyUI.storyBook.bookPages [0] = cover; //Book Cover
        
		storyUI.testImage.sprite = bg; //Book Background
        
		storyUI.storyBook.currentPage = 0;
		storyUI.originalBookBackground = storyUI.storyBook.background = storyUI.storyBook.LeftNext.sprite = storyUI.BookCoverLeftSprite; // Cover left page

        // Reducing performance overhead from extra initialization(s).
        int page_count = storyUI.storyBook.bookPages.Length;
        //Sprite[] book = storyUI.storyBook.bookPages;
        //book[1] = pages[0];
        //for (int i = 3; i < page_count; i+=2)
        //{
        //    book[i-1] = bg;
        //    book[i] = pages[i / 2];
        //}

        List<int> pagenumbers = new List<int>();
        for (int i = 1; i < storyUI.storyBook.bookPages.Length; i = i + 2)
        {
            pagenumbers.Add(i);
        }

        for (int i = 0; i < pagenumbers.Count; i++)
        {
            storyUI.storyBook.bookPages[pagenumbers[i]] = pages[i];
        }

        for (int i = 2; i < storyUI.storyBook.bookPages.Length; i = i + 2)
        {
            storyUI.storyBook.bookPages[i] = bg;
        }

        var pageText = data.PageList [0].PageStory;
		storyUI.storyAudio.clip = storyClip;
		if (!string.IsNullOrEmpty (pageText))
        {
            //storyUI.wordList.AddRange(pageText.Split(' '));
            var wordList = GetIndividualWords(pageText);
            int l = wordList.Count();
            for (int i = 0; i < l; i++)
            {
                storyUI.wordList.Add(wordList[i]);
            }
        }
	}

	public void ExplicitSetBackground ()
	{
		storyUI.storyBook.background = storyUI.BookCoverLeftSprite;
		storyUI.storyBook.LeftNext.sprite = storyUI.BookCoverLeftSprite;
	}

	public void OnFlipPage (int nextpage, bool pageSound = true, bool allowHighlight = true)
	{
		float delayOnPageSound = 0f;
		storyUI.InvokePauseAudio ();
		storyUI.ResetPageSyncData ();
		if (pageSound)
		{
			storyUI.PlayPageTurnSound ();
			delayOnPageSound = storyUI.pageTurnSound.length;
		}

		storyUI.currPageNumber = (storyUI.storyBook.currentPage - 2) / 2;
		if (storyUI.currPageNumber >= 0) {
			storyUI.pageNumberText.text = (storyUI.currPageNumber + 1) + "/" + currStory.PageList.Count;
		} else {
			storyUI.pageNumberText.text = String.Empty;
		}
        

		if (nextpage >= storyUI.storyBook.TotalPageCount) {
			storyUI.storyBook.background = storyUI.testImage.sprite;
		}

		if (storyUI.currPageNumber == currStory.PageList.Count - 1) {
			PerformanceManager.Instance.IncrementSuccess();
			storyUI.nextPageButton.interactable = false;
			storyUI.prevPageButton.interactable = true;
			storyUI.PreviousButton.interactable = true;
		} else if (storyUI.currPageNumber == -1) {
			storyUI.nextPageButton.interactable = true;
			storyUI.prevPageButton.interactable = false;
			storyUI.PreviousButton.interactable = true;
		} else {
			storyUI.StoryControlsToggle (true, 0.3f);
		}

		if (storyUI.currPageNumber <= currStory.PageList.Count) {
			storyUI.wordList.Clear ();
			storyUI.pageTextContainer.text = "";
			storyUI.pageTextContainer.DOColor (Color.black, 0);

			//page turn effect with text
			storyUI.pageTextContainer.transform.parent.SetParent (storyUI.storyBook.transform, true);
			storyUI.pageTextContainer.transform.parent.SetAsLastSibling ();

			if (storyUI.currPageNumber >= 0) {
				var pageText = currStory.PageList [storyUI.currPageNumber].PageStory;
				if (!string.IsNullOrEmpty (pageText)) {
					storyUI.pageTextContainer.text = pageText;
					storyUI.blackText = pageText;
					storyUI.pageTextContainer.DOFade (1, 0.4f);
					var wordList = GetIndividualWords (pageText);
					foreach (var word in wordList) {
						storyUI.wordList.Add (word);
					}
				}
				var timestamps = currStory.PageList [storyUI.currPageNumber].TimeStamp;
				float nexttimestamp = 0f;
				if (storyUI.currPageNumber < currStory.PageList.Count - 1) {
					nexttimestamp = currStory.PageList [storyUI.currPageNumber + 1].TimeStamp [0];
				}

				if (storyUI.storyAudio.clip != null && !String.IsNullOrEmpty (pageText) && allowHighlight) {
					storyUI.SyncedHighlighting (storyUI.storyAudio.clip, delayOnPageSound, timestamps);
				} else {
					storyUI.SyncedHighlighting (storyUI.storyAudio.clip, delayOnPageSound, timestamps, false);
				}
			}
		}
	}

	public List<string> GetIndividualWords (string sentence)
	{
		var wordArray = sentence.Split (' ');
		List<string> wordList = new List<string> ();
		wordList.AddRange (wordArray);
		return wordList;
	}
}