﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Security;
using UnityEngine;

#region Enums

public enum JoinElementPuzzleTypes
{
    NumberPuzzle,
    AlphabetPuzzle
}

public enum ThumbnailTypes
{
    Gallery,
    Video
}

public enum VideoContentTypes
{
    Shows,
    Rhymes
}

#endregion

#region constants

public class Constant
{

    public static readonly char[] ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

	//Mail
	public static string MAILADDRESS = "support@vgminds.com";
	public static string MAILCONTENT = "Hi! %0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A%0D%0A Regards";
	public static string MAILPREFIX = "mailto:";
}

#endregion

#region DataStructure
[Serializable]
public class FeedbackStatusData
{
    public bool RatingDone;
    public int LocalAppSession;

    public void ResetRatingFlag()
    {
        SetRatingStatus(false);
    }

    public void SetRatingStatus(bool newStatus)
    {
        RatingDone = newStatus;
    }

    public void IncrementAppSession()
    {
        LocalAppSession++;
    }

    //Below method added
    public int GetCurrentAppSession()
    {
        return LocalAppSession;
    }
}

#endregion