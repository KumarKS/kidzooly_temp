﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using UnityEngine;

public class GameData
{
    #region Game Configuration

    [Serializable]
    public class GameConfigData
    {
        public string LocalizationUrl;
        public string VideoDataUrl;
        public string StoryDataurl;
        public string PianoDataUrl;
        public string OtherAppUrl;
        public string IAPCatalogUrl;

        public GameConfigData()
        {
            LocalizationUrl = "LocalizationUrl";
            VideoDataUrl = "LocalizationUrl";
            StoryDataurl = "StoryDataurl";
            PianoDataUrl = "PianoMusicData";
            OtherAppUrl = "OtherAppsUrl";
            IAPCatalogUrl = "IAPCatalogUrl";
        }
    }

    #endregion

    #region Video Data

    [Serializable]
    public class VideoData
    {
        public string VideoName;
        public bool isFree;
        public string UploadDate;
        public string ThumbnailUrl;
        public string VideoUrl;

        public VideoData()
        {
            isFree = true;
            UploadDate = DateTimeExtensions.GetCurrentUnixTime();
            ThumbnailUrl = String.Empty;
            VideoUrl = String.Empty;
            VideoName = "$Name$";
        }
    }

    [Serializable]
    public class VideoCollection
    {
        public List<VideoGroup> Rhymes;
        public List<VideoGroup> Shows;
    }

    [Serializable]
    public class VideoGroup
    {
        public string GroupName;
        public string GroupThumbnail;
        public List<GameData.VideoData> VideosList;

        public VideoGroup()
        {
            GroupName = "$Group$";
            VideosList = new List<GameData.VideoData>();
            GroupThumbnail = String.Empty;
        }

        public VideoGroup(string groupName, List<GameData.VideoData> videoList, string groupThumbnail = "")
        {
            this.GroupName = groupName;
            this.GroupThumbnail = groupThumbnail;
            this.VideosList = videoList.Clone().OrderByDescending(x => x.UploadDate).ToList(); //last uploaded video always comes first
        }
    }

    #endregion

    #region Localization

    [Serializable]
    public class LocalizationData
    {
        public string[] Languages = new string[] { };
        public Dictionary<string, string[]> LocalizationTextBindings = new Dictionary<string, string[]>();
        public Dictionary<string, string[]> LocalizationAudioBindings = new Dictionary<string, string[]>();

        public void AddLanguage(string lang)
        {
            var existingLanguages = Languages.ToList();
            Languages = new string[Languages.Length + 1];
            for (int i = 0; i < existingLanguages.Count; i++)
            {
                Languages[i] = existingLanguages[i];
            }
            Languages[Languages.Length - 1] = lang;
            Debug.Log(Languages.Length);

            var LocalizedTextTags = LocalizationTextBindings.Keys.ToList();
            foreach (var tag in LocalizedTextTags)
            {
                var existingBindings = LocalizationTextBindings[tag].ToList();
                LocalizationTextBindings[tag] = new string[Languages.Length];
                for (int i = 0; i < existingBindings.Count; i++)
                {
                    LocalizationTextBindings[tag][i] = existingBindings[i];
                }
            }
            var LocalizedAudioTags = LocalizationAudioBindings.Keys.ToList();
            foreach (var tag in LocalizedAudioTags)
            {
                var existingBindings = LocalizationAudioBindings[tag].ToList();
                LocalizationAudioBindings[tag] = new string[Languages.Length];
                for (int i = 0; i < existingBindings.Count; i++)
                {
                    LocalizationAudioBindings[tag][i] = existingBindings[i];
                }
            }
        }

        public void RemoveLanguage(string lang)
        {
            var list = Languages.ToList();
            var index = list.FindIndex(x => string.Equals(x, lang));
            list.RemoveAt(index);
            Languages = list.ToArray();
            var keys = LocalizationTextBindings.Keys.ToList();
            foreach (var key in keys)
            {
                var existingBindings = LocalizationTextBindings[key].ToList();
                existingBindings.RemoveAt(index);
                LocalizationTextBindings[key] = existingBindings.ToArray();
            }
            keys = LocalizationAudioBindings.Keys.ToList();
            foreach (var key in keys)
            {
                var existingBindings = LocalizationAudioBindings[key].ToList();
                existingBindings.RemoveAt(index);
                LocalizationAudioBindings[key] = existingBindings.ToArray();
            }
        }

        public void AddTextBinding(string language, string tag, string binding)
        {
            var index = Languages.ToList().FindIndex(x => string.Equals(x, language));

            if (!LocalizationTextBindings.ContainsKey(tag))
            {
                LocalizationTextBindings.Add(tag, new string[Languages.Length]);
            }

            LocalizationTextBindings[tag][index] = binding;
        }

        public void AddAudioBinding(string language, string tag, string binding)
        {
            var index = Languages.ToList().FindIndex(x => string.Equals(x, language));

            if (!LocalizationAudioBindings.ContainsKey(tag))
            {
                LocalizationAudioBindings.Add(tag, new string[Languages.Length]);
            }

            LocalizationAudioBindings[tag][index] = binding;
        }
    }

    #endregion

    #region Story Book

    [Serializable]
    public class StoryPageData
    {
        public string ImageUrl;
        public string PageStory;
        public List<float> TimeStamp;
    }

    [Serializable]
    public class StoryData
    {
        public string StoryName;
        public bool IsFree;
        public string UploadDate;
        public string ThumbnailUrl;
        public string VoiceOverUrl;
        public string PageBackgroundUrl;
        public string BookCoverUrl;
        /// <summary>
        /// Bundled with build so useless.
        /// </summary>
        public string BookCoverLeftPageUrl;
        public List<StoryPageData> PageList;
    }

    [Serializable]
    public class StoryCollection
    {
        public List<StoryData> Stories;
    }

    #endregion

    #region Coloring Activity

    [Serializable]
    public class ColoringBookData
    {
        public string ColoringPageUrl;
        public bool IsFree;

        public ColoringBookData()
        {
            ColoringPageUrl = String.Empty;
            IsFree = true;
        }
    }

    [Serializable]
    public class ColoringBookCollection
    {
        public List<ColoringBookData> ColoringBookPages;
    }

    #endregion

    #region Scratching Activity

    [Serializable]
    public class ScratchingBookPage
    {
        public string ScratchingBookUrl;
        public bool IsFree;

        public ScratchingBookPage()
        {
            ScratchingBookUrl = String.Empty;
            IsFree = true;
        }
    }

    [Serializable]
    public class ScratchingBookCollection
    {
        public List<ScratchingBookPage> ScratchingBookPages;
    }

    #endregion

    #region Join Element Puzzle

    [Serializable]
    public class JoinCharacterPuzzle
    {
        public string ImageUrl;
        public bool IsFree;
        [SerializeField]
        public List<Vector2> StarPositions;

        public JoinElementPuzzleTypes Type;
        public int StartIndex;
        public string AudioName;

        public JoinCharacterPuzzle()
        {
            this.ImageUrl = string.Empty;
            this.StarPositions = new List<Vector2>();
            this.StartIndex = 0;
            this.IsFree = true;
        }

        public JoinCharacterPuzzle(string imageUrl, List<Vector2> startPositions, JoinElementPuzzleTypes type, int startIndex, bool isFree)
        {
            this.ImageUrl = imageUrl;
            this.StarPositions = startPositions;
            this.Type = type;
            this.StartIndex = startIndex;
            this.IsFree = isFree;
        }
    }

    [Serializable]
    public class JoinPuzzleCollection
    {
        [SerializeField]
        public List<JoinCharacterPuzzle> Puzzles;
    }

    #endregion

    #region Piano Music

    public enum EMusicKey
    {
        None,

        [Description("-3")]
        A,

        [Description("-2")]
        ASharp,

        [Description("-1")]
        B,

        [Description("0")]
        C,

        [Description("1")]
        CSharp,

        [Description("2")]
        D,

        [Description("3")]
        DSharp,

        [Description("4")]
        E,

        [Description("5")]
        F,

        [Description("6")]
        FSharp,

        [Description("7")]
        G,

        [Description("8")]
        GSharp
    }

    public enum EMusicScale : int
    {
        None,

        [Description("0|2|4|5|7|9|11|12")]
        Major,

        [Description("0|2|3|5|7|8|10|12")]
        Minor,

        [Description("0|2|3|5|7|8|11|12")]
        HarmonicMinor,

        [Description("0|2|3|5|7|9|11|12")]
        MelodicMinor,

        [Description("0|2|4|7|9|12")]
        MajorPentatonic,

        [Description("0|3|5|8|10|12")]
        MinorPentatonic
    }

    [Serializable]
    public class PianoMusicData
    {
        public EMusicKey Key = EMusicKey.None;
        public EMusicScale Scale = EMusicScale.None;
        public Dictionary<float, int> Tab = new Dictionary<float, int>();
    }

    [Serializable]
    public class PianoMusicCollection
    {
        public List<PianoMusicData> Music = new List<PianoMusicData>();
    }

    #endregion

    #region Precut puzzle Activity

    [Serializable]
    public class PrecutPuzzleData
    {
        public string PuzzleImageUrl;
        public bool IsFree;

        public PrecutPuzzleData()
        {
            PuzzleImageUrl = String.Empty;
            IsFree = true;
        }
    }

    [Serializable]
    public class PrecutPuzzleCollection
    {
        /* public Dictionary<string, bool> PrecutPuzzleImages = new Dictionary<string, bool>();*/

        public List<PrecutPuzzleData> PrecutPuzzleImages;
    }

    #endregion

    #region OtherAppData

    [Serializable]
    public class OtherAppData
    {
        public string AppName;
        public string ThumbnailLink;
        public string AppStoreLink;
        public string GooglePlayLink;
        public string AmazonAppsLink;

        public OtherAppData()
        {
            AppName = "other App";
            ThumbnailLink = String.Empty;
            AppStoreLink = String.Empty;
            GooglePlayLink = String.Empty;
            AmazonAppsLink = String.Empty;
        }

        public OtherAppData(string name, string thumbnailURL, string gLink, string iLink, string amazonLink)
        {
            AppName = name;
            ThumbnailLink = thumbnailURL;
            AppStoreLink = iLink;
            GooglePlayLink = gLink;
            AmazonAppsLink = amazonLink;
        }
    }

    [Serializable]
    public class OtherAppCollection
    {
        public List<OtherAppData> OtherApps;
    }

    #endregion
}