﻿using System;
using System.ComponentModel;


public enum EService
{
    None = 0,
    Custom = 1,
}

public enum EEndPoints
{
#if AmazonStoreBuild
            [Description("https://1876484730.rsc.cdn77.org/vm/kidzooly/amazon/JSONs/Version.json")]
#elif AppleStoreBuild
    [Description("https://1876484730.rsc.cdn77.org/vm/kidzooly/ios/JSONs/Version.json")]
#else
    [Description("https://1876484730.rsc.cdn77.org/vm/kidzooly/android/JSONs/Version.json")]
#endif
    VersionEndPoint,

#if AmazonStoreBuild
            [Description("https://1876484730.rsc.cdn77.org/vm/kidzooly/amazon/JSONs/MainConfig.json")]
#elif AppleStoreBuild
    [Description("https://1876484730.rsc.cdn77.org/vm/kidzooly/ios/JSONs/MainConfig.json")]
#else
    [Description("https://1876484730.rsc.cdn77.org/vm/kidzooly/android/JSONs/MainConfig.json")]
    //[Description("https://vgdevs-4b69.kxcdn.com/Kidzooly_Main/Kidzooly_Main.json")]
#endif
    ConfigEndPoint,

}




public enum EScenes
{
    //ADDED ONE LINE OF CODE FOR PRESPLASH SCENE
    [Description("PreSplash")]
    PreSplash,

    [Description("Splash")]
    Splash,

    [Description("MainMenu")]
    MainMenu,

    [Description("Store")]
    Store,

    [Description("Story")]
    Story,

    [Description("Video")]
    Video,

    [Description("ColoringActivity")]
    ColoringActivity,

	[Description("ScratchingActivity")]
	ScratchingActivity,

    [Description("PuzzleActivity")]
    PuzzleActivity,

    [Description("JoinDotsActivity")]
    JoinDotsActivity,

    [Description("LearningWheelActivity")]
    LearningWheel,

    [Description("ShadowMatchActivity")]
    ShadowMatch,

    [Description("InteractiveRhymeGallery")]
    InteractiveRhyme,

	[Description("TracingMain")]
	TracingActivity,

	[Description("TracingGame")]
	TracingGame
}

public enum EDragDirections
{
    Right,
    Left,
    Up,
    Down
}