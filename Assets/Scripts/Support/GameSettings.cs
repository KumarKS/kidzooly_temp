﻿using UnityEngine;
using System;
using System.IO;
using System.ComponentModel;

#if UNITY_EDITOR
using UnityEditor;
#endif


[Serializable]
#if UNITY_EDITOR
[InitializeOnLoad]
#endif



public class GameSettings : ScriptableObject
{
    const string GameSettingsAssetName = "GameSettings";

    const string GameSettingsPath = "Resources";

    const string GameSettingsAssetExtension = ".asset";

    [Header("Debug")]
    public bool debugMode;
    [Header("Premium Debug")]
    public bool EditorPremium;

    [Header("Login Service")]
    public EService LoginService;

    [Header("Storage Service")]
    public EService StorageService;

    private static GameSettings instance;

    public static GameSettings GetInstance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load(GameSettingsAssetName) as GameSettings;

                if (instance == null)
                {

                    instance = CreateInstance<GameSettings>();
#if UNITY_EDITOR

                    FileStaticAPI.CreateFolder(GameSettingsPath);

                    string fullPath = Path.Combine(Path.Combine("Assets", GameSettingsPath),
                                          GameSettingsAssetName + GameSettingsAssetExtension
                                      );

                    AssetDatabase.CreateAsset(instance, fullPath);
#endif
                }
            }
            return instance;
        }
    }


}
