﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteToggles 
{

    static bool _initialized = false;
    public static bool SERVER_SIDE_VALIDATION = false;


    const string SERVER_SIDE_VALIDATION_KEY = "SERVER_SIDE_VALIDATION";


    public static void Initialize()
    {
        if (!_initialized)
        {
            SERVER_SIDE_VALIDATION = RemoteSettings.GetBool(SERVER_SIDE_VALIDATION_KEY, SERVER_SIDE_VALIDATION);
            _initialized = true;
        }
      
        
    }
}
