﻿using UnityEngine;


using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

public class EventBinding : MonoBehaviour
{


		/// <summary>
		/// Specifies which event on the source component to bind to
		/// </summary>
		public ComponentMemberInfo DataSource;
		
		/// <summary>
		/// Specifies which method on the target component to invoke when 
		/// the source event is triggered
		/// </summary>
		public ComponentMemberInfo DataTarget;
		
		/// <summary>
		/// If set to TRUE (default), this component will attempt to bind the 
		/// event handler when the component is Enabled.
		/// </summary>
		public bool AutoBind = true;
	
		/// <summary>
		/// If set to TRUE (default), this component will unbind the event
		/// handler (if bound) when the component is Disabled.
		/// </summary>
		public bool AutoUnbind = true;
		
	#region Private fields
	
		private bool isBound;
	
		private Component sourceComponent;
		private Component targetComponent;
	
		private EventInfo eventInfo;
		private FieldInfo eventField;
		private Delegate eventDelegate;
	
		//private MethodInfo handlerProxy;
		//private ParameterInfo[] handlerParameters;
	
	#endregion

	#region Unity events
	
		public void OnEnable ()
		{
				if (AutoBind && DataSource != null && !isBound && DataSource.IsValid && DataTarget.IsValid) {
						Bind ();
				}
		}
	
		public void Start ()
		{
				if (AutoBind && DataSource != null && !isBound && DataSource.IsValid && DataTarget.IsValid) {
						Bind ();
				}
		}
	
		public void OnDisable ()
		{
				if (AutoUnbind) {
						Unbind ();
				}
		}
	
		public void OnDestroy ()
		{
				Unbind ();
		}
	
	#endregion
	
	#region Public methods 
	
		/// <summary>
		/// Bind the source event to the target event handler
		/// </summary>
		public void Bind ()
		{
		
				if (isBound || DataSource == null)
						return;
		
				if (!DataSource.IsValid || !DataTarget.IsValid) {
						Debug.LogError (string.Format ("Invalid event binding configuration - Source:{0}, Target:{1}", DataSource, DataTarget));
						return;
				}
		
				sourceComponent = DataSource.Component;
				targetComponent = DataTarget.Component;
		
				var eventHandler = DataTarget.GetMethod ();
				if (eventHandler == null) {
						Debug.LogError ("Event handler not found: " + targetComponent.GetType ().Name + "." + DataTarget.MemberName);
						return;
				}
		
				if (bindToEventProperty (eventHandler)) {
						isBound = true;
						return;
				}
		
				if (bindToEventField (eventHandler)) {
						isBound = true;
						return;
				}
		
		}
	
		/// <summary>
		/// Unbind the source event and target event handler
		/// </summary>
		public void Unbind ()
		{
		
				if (!isBound)
						return;
		
				isBound = false;
		
				if (eventField != null) {
						var currentDelegate = (Delegate)eventField.GetValue (sourceComponent);
						var newDelegate = Delegate.Remove (currentDelegate, eventDelegate);
						eventField.SetValue (sourceComponent, newDelegate);
				} else if (eventInfo != null) {
						var removeMethod = eventInfo.GetRemoveMethod ();
						removeMethod.Invoke (sourceComponent, new object[] { eventDelegate });
				}
		
				eventInfo = null;
				eventField = null; 
				eventDelegate = null;
				//handlerProxy = null;
		
				sourceComponent = null;
				targetComponent = null;
		
		}
	
	#endregion
	
	#region Private utility methods
	
		private bool bindToEventField (MethodInfo eventHandler)
		{
		
				eventField = getField (sourceComponent, DataSource.MemberName);
				if (eventField == null) {
						return false;
				}
		
				try {
			
						//var eventMethod = eventField.FieldType.GetMethod ("Invoke");
						//var eventParams = eventMethod.GetParameters ();
						//var handlerParams = eventHandler.GetParameters ();
			
						
			
						
						#if !UNITY_EDITOR && UNITY_METRO
				eventDelegate = eventHandler.CreateDelegate( eventField.FieldType, targetComponent );
						#else
						eventDelegate = Delegate.CreateDelegate (eventField.FieldType, targetComponent, eventHandler, true);
						#endif
						
			
						var combinedDelegate = Delegate.Combine (eventDelegate, (Delegate)eventField.GetValue (sourceComponent));
						eventField.SetValue (sourceComponent, combinedDelegate);
			
				} catch (Exception err) {
						this.enabled = false;
						var errMessage = string.Format ("Event binding failed - Failed to create event handler for {0} ({1}) - {2}", DataSource, eventHandler, err.ToString ());
						Debug.LogError (errMessage, this);
						return false;
				}
		
				return true;
		
		}
	
	
		private bool bindToEventProperty (MethodInfo eventHandler)
		{
		
				eventInfo = sourceComponent.GetType ().GetEvent (DataSource.MemberName);
				if (eventInfo == null)
						return false;
		
				try {
			
						var eventDelegateType = eventInfo.EventHandlerType;
						var addMethod = eventInfo.GetAddMethod ();
			
						//var eventMethod = eventDelegateType.GetMethod ("Invoke");
						//var eventParams = eventMethod.GetParameters ();
						//var handlerParams = eventHandler.GetParameters ();
			
						
			
					
						#if !UNITY_EDITOR && UNITY_METRO
				eventDelegate = eventHandler.CreateDelegate( eventDelegateType, targetComponent );
						#else
						eventDelegate = Delegate.CreateDelegate (eventDelegateType, targetComponent, eventHandler, true);
						#endif
				
						
			
						addMethod.Invoke (DataSource.Component, new object[] { eventDelegate });
			
				} catch (Exception err) {
						this.enabled = false;
						var errMessage = string.Format ("Event binding failed - Failed to create event handler for {0} ({1}) - {2}", DataSource, eventHandler, err.ToString ());
						Debug.LogError (errMessage, this);
						return false;
				}
		
				return true;
		
		}
	
	#endregion
	
		private static FieldInfo getField (Component component, string fieldName)
		{
		
				if (component == null)
						throw new ArgumentNullException ("component");
		
				return
			component.GetType ()
				.GetFields ()
				.FirstOrDefault (f => f.Name == fieldName);
		
		}
}
