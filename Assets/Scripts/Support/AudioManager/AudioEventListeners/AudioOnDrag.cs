﻿using UnityEngine.EventSystems;


public class AudioOnDrag : AudioBase, IDragHandler
{
    public void OnDrag(PointerEventData eventData)
    {
        PlayAudio();
    }
}

