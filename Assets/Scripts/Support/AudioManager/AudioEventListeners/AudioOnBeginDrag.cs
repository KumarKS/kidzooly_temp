﻿using System;
using UnityEngine.EventSystems;

public class AudioOnBeginDrag : AudioBase, IBeginDragHandler
{
    public void OnBeginDrag(PointerEventData eventData)
    {
        PlayAudio();
    }
}
