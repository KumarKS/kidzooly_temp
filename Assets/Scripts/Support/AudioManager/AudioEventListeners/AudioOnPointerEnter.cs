﻿using UnityEngine.EventSystems;



public class AudioOnPointerEnter : AudioBase, IPointerEnterHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        PlayAudio();
    }
}
