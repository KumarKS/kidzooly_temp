﻿using UnityEngine.EventSystems;

public class AudioOnSelect : AudioBase, ISelectHandler
{

    public void OnSelect(BaseEventData eventData)
    {
        PlayAudio();
    }

}
