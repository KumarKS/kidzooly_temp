﻿using UnityEngine.EventSystems;


public class AudioOnPointerExit : AudioBase, IPointerExitHandler {

    public void OnPointerExit(PointerEventData eventData)
    {
        PlayAudio();
    }

}
