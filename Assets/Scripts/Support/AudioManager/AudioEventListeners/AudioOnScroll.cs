﻿using UnityEngine.EventSystems;

public class AudioOnScroll : AudioBase, IScrollHandler {

    public void OnScroll(PointerEventData eventData)
    {
        PlayAudio();
    }

    
}
