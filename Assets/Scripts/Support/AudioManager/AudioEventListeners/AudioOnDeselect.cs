﻿using UnityEngine.EventSystems;

public class AudioOnDeselect : AudioBase,IDeselectHandler {
   
    public void OnDeselect(BaseEventData eventData)
    {
        PlayAudio();
    }
}
