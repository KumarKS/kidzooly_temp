﻿using UnityEngine.EventSystems;


public class AudioOnUpdateSelected : AudioBase, IUpdateSelectedHandler
{
    public void OnUpdateSelected(BaseEventData eventData)
    {
       PlayAudio();
    }
}
