﻿using UnityEngine.EventSystems;

public class AudioOnDrop : AudioBase, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        PlayAudio();
    }
}
