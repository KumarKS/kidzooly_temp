﻿using UnityEngine.EventSystems;

public class AudioOnSubmit : AudioBase, ISubmitHandler {

    public void OnSubmit(BaseEventData eventData)
    {
        PlayAudio();
    }

    
}
