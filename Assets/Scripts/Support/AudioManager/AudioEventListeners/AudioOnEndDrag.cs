﻿using UnityEngine.EventSystems;

public class AudioOnEndDrag : AudioBase, IEndDragHandler
{
    public void OnEndDrag(PointerEventData eventData)
    {
        PlayAudio();
    }
}
