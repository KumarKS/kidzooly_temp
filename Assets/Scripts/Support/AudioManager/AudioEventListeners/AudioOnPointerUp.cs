﻿using UnityEngine.EventSystems;

public class AudioOnPointerUp : AudioBase, IPointerUpHandler {

    public void OnPointerUp(PointerEventData eventData)
    {
        PlayAudio();
    }

   
}
