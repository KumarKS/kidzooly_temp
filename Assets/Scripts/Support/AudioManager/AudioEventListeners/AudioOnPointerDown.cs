﻿using UnityEngine.EventSystems;

public class AudioOnPointerDown : AudioBase, IPointerDownHandler {

    public void OnPointerDown(PointerEventData eventData)
    {
        PlayAudio();
    }

    
}
