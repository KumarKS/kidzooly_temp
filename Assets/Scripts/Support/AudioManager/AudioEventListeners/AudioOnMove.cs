﻿using UnityEngine.EventSystems;

public class AudioOnMove : AudioBase, IMoveHandler {

    public void OnMove(AxisEventData eventData)
    {
       PlayAudio();
    }

}
