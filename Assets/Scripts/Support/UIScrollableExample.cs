﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIScrollableExample : MonoBehaviour
{
    public GameObject prefab;
    public UIScrollable scroll;
    public int ItemMax;
    public int ScrollTo;
    public bool doScroll = false;

    void Start()
    {
        List<string> alphabets = Enumerable.Range(1, ItemMax).Select(x => x.ToString()).ToList();
        scroll.SetPrefab(prefab)
            .SetData(alphabets).SetFunction(
                (data, index, obj) =>
                {
                    var d = ((List<string>)data);
                    obj.GetComponentInChildren<Text>().SetText(d[index]);
                }).Initialize();
        if (doScroll)
            DOTween.Sequence().AppendInterval(3f).AppendCallback(() => scroll.ScrollToIndex(ScrollTo)).Play();
    }
}
