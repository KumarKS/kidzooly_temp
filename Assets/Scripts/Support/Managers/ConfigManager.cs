﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;
using System.Collections;

public enum EConfigFileName
{
    [Description("LocalizationUrl")]
    LocalizationConfig,

    [Description("VideoDataUrl")]
    Video,

    [Description("StoryDataurl")]
    Stories,

    [Description("_BundledAssets/Colouring/ColoringBook")]
    ColoringBook,

    [Description("_BundledAssets/Scratching/ScratchingBook")]
    ScratchingBook,

    [Description("_BundledAssets/Joining_Dots/JoinDots")]
    JoinDots,

    [Description("_BundledAssets/Puzzle/PrecutPuzzle")]
    PrecutPuzzle,

    [Description("MainConfig")]
    MainConfig,

    [Description("PianoDataUrl")]
    PianoMusicData,

    [Description("OtherAppUrl")]
    OtherApps,

    [Description("IAPCatalogUrl")]
    IAPCatalog
}

[Serializable]
public class ConfigInfo
{
    public float ver = 0;
    public string md5 = "0";
}

public class ConfigManager : GenericManager<ConfigManager>
{
    public static Action<float> ProgressCounter;
    public static Action OnConfigLoaded;
    public static Action OnLocalizationTextLoaded;
    public static Action OnAudioCacheSuccess;
    public static Action OnAudioCacheFail;

    const int MAXTHUMBNAILDOWNLOADVIDEOS = 5;
    const int MAXTHUMBNAILDOWNLOADSTORIES = 6;

    void OnDestroy()
    {
        FileManager.RemoveReference(this);
    }



    public void FetchLocalizedAudio(Action OnAudioFetchSuccess, Action OnAudioFetchFail, Action<float> Progress = null)
    {
        var config = LocalDataManager.Instance.GetConfig<GameData.LocalizationData>(EConfigFileName.LocalizationConfig);
        var urls = new List<string>();
        var index =
            config.Languages.ToList()
                .FindIndex(x => string.Equals(x, LocalDataManager.Instance.SaveData.CurrentLanguage));
        foreach (var key in config.LocalizationAudioBindings.Keys)
        {
            urls.Add(config.LocalizationAudioBindings[key][index]);
        }

        if (!string.Equals(LocalDataManager.Instance.SaveData.CurrentLanguage, GameConstants.DEFAULT_LANGUAGE) && urls.Any())
        {
            FileManager.Instance.GetAudio(urls: urls.ToArray(), onComplete: (audioFiles) =>
              {
                  AudioController.AudioClips.Clear();

                  foreach (var key in config.LocalizationAudioBindings.Keys)
                  {
                      AudioController.AudioClips.SafeAdd(key, audioFiles.Find(x => x.name == config.LocalizationAudioBindings[key][index].GenerateUniqueId()));
                      urls.Add(config.LocalizationAudioBindings[key][index]);
                  }
                  AudioController.LocalizedAudioCached = true;
                  OnAudioCacheSuccess.SafeInvoke();
                  OnAudioFetchSuccess.SafeInvoke();
              }, owner: this, onFail: (error) =>
              {
                  Debug.Log("Couldnt Fetch Voice Overs");
                  OnAudioCacheFail.SafeInvoke();
                  OnAudioFetchFail.SafeInvoke();
              }, onProgress: Progress);
        }
        else
        {
            Progress.SafeInvoke(100);
            AudioController.LocalizedAudioCached = true;
            OnAudioCacheSuccess.SafeInvoke();
            OnAudioFetchSuccess.SafeInvoke();
        }
    }

    void FetchSubConfigs()
    {
        try
        {
            Debug.Log("Fetching Sub Configs");
            var config = LocalDataManager.Instance.GetConfig<GameData.GameConfigData>(EConfigFileName.MainConfig);


            var urls = config.GetType()
                .GetFields()
                .Select(field => (string)field.GetValue(config))
                .ToList();

            FileManager.Instance.GetData(urls: urls.ToArray(), onComplete: (data) =>
            {

                Debug.Log("Fetched Sub Configs");
                OnConfigReady();
            }, owner: this, onFail: (error) =>
            {
                Debug.Log("Couldn't Fetch Sub Configs");
                if (Application.internetReachability == NetworkReachability.NotReachable)
                {
                    UIToastNotification.Instance.TriggerToast("Check Your Connection!", 5f);
                    UIPopupBox.Instance.SetDataOk("You are offline.Check your internet connection to continue", FetchSubConfigs);
                }
                else
                    UIPopupBox.Instance.SetDataOk("Oops!, There seems to be an issue....", FetchSubConfigs);
            }, onProgress: ProgressCounter);
        }
        catch (Exception e)
        {
            Debug.Log("Caught Exception @FetchSubConfigs Exception Msg: " + e.Message);
            Debug.Log("Caught Exception @FetchSubConfigs Exception StackTrace: " + e.StackTrace);
            Debug.Log("Couldn't Fetch Sub Configs");
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                UIToastNotification.Instance.TriggerToast("Check Your Connection!", 5f);
                UIPopupBox.Instance.SetDataOk("You are offline.Check your internet connection to continue", FetchSubConfigs);
            }
            else
                UIPopupBox.Instance.SetDataOk("Oops!, There seems to be an issue....", FetchSubConfigs);
        }
    }

    void FetchMainConfig()
    {
        Debug.Log("Fetching Main Config");
        FileManager.RemoveFolder(EFileType.Data);
        try
        {
            FileManager.Instance.GetData(urls: EEndPoints.ConfigEndPoint.GetDescription(), onComplete: (obj) =>
              {
                  Debug.Log("Fetched Main Config");
                  LocalDataManager.Instance.SaveData.MainConfigData =
                      JsonConvert.DeserializeObject<GameData.GameConfigData>(obj[0]);
                  FetchSubConfigs();
              }, owner: this, onFail: (error) =>
              {
                  Debug.Log("Couldn't Fetch Main Config");
                  if (Application.internetReachability == NetworkReachability.NotReachable)
                  {
                      UIToastNotification.Instance.TriggerToast("Check Your Connection!", 5f);
                      UIPopupBox.Instance.SetDataOk("You are offline.Check your internet connection to continue", FetchMainConfig);
                  }
                  else
                      UIPopupBox.Instance.SetDataOk("Oops!, There seems to be an issue....", FetchMainConfig);
              }, onProgress: ProgressCounter);
        }
        catch (Exception e)
        {
            Debug.Log("Caught Exception @FetchMainConfig Exception Msg: " + e.Message);
            Debug.Log("Caught Exception @FetchMainConfig Exception StackTrace: " + e.StackTrace);
            Debug.Log("Couldn't Fetch Main Config");
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                UIToastNotification.Instance.TriggerToast("Check Your Connection!", 5f);
                UIPopupBox.Instance.SetDataOk("You are offline.Check your internet connection to continue", FetchMainConfig);
            }
            else
                UIPopupBox.Instance.SetDataOk("Oops!, There seems to be an issue....", FetchMainConfig);
        }
    }

    public void FetchVersion()
    {
        try
        {
            Debug.Log("Fetching Version...");
            FileManager.RemoveFile(EEndPoints.VersionEndPoint.GetDescription());
            FileManager.Instance.GetData(urls: EEndPoints.VersionEndPoint.GetDescription(), onComplete: (obj) =>
            {
                Debug.Log("Fetched Version...");
                var fetchedConfig = JsonConvert.DeserializeObject<ConfigInfo>(obj[0].ToString()).ver;
                Debug.Log("Current: " + fetchedConfig + ", Previous: " + LocalDataManager.Instance.SaveData.ConfigVersion);

                if (LocalDataManager.Instance.SaveData.ConfigVersion != fetchedConfig) //version check
                {
                    LocalDataManager.Instance.SaveData.ConfigVersion = fetchedConfig;
                    FetchMainConfig();
                }
                else
                {
                    OnConfigReady();
                }
            }, owner: this, onFail: (error) =>
            {
                Debug.Log("LocalDataManager.Instance.SaveData.ConfigVersion : " + LocalDataManager.Instance.SaveData.ConfigVersion);
                if (LocalDataManager.Instance.SaveData.ConfigVersion != 0)
                {
                    OnConfigReady();
                }
                else
                {
                    if (Application.internetReachability == NetworkReachability.NotReachable)
                    {
                        UIToastNotification.Instance.TriggerToast("Check Your Connection!", 5f);
                        UIPopupBox.Instance.SetDataOk("You are offline.Check your internet connection to continue", FetchVersion);
                    }
                    else
                        UIPopupBox.Instance.SetDataOk("Oops!, There seems to be an issue....", FetchVersion);
                }
            }, onProgress: ProgressCounter);
        }
        catch (Exception e)
        {
            Debug.Log("Caught Exception @FetchVersion Exception Msg: " + e.Message);
            Debug.Log("Caught Exception @FetchVersion Exception StackTrace: " + e.StackTrace);
            Debug.Log("Couldn't Fetch Version");
            if (LocalDataManager.Instance.SaveData.ConfigVersion != 0)
            {
                OnConfigReady();
            }
            else
            {
                if (Application.internetReachability == NetworkReachability.NotReachable)
                {
                    UIToastNotification.Instance.TriggerToast("Check Your Connection!", 5f);
                    UIPopupBox.Instance.SetDataOk("You are offline.Check your internet connection to continue", FetchVersion);
                }
                else
                    UIPopupBox.Instance.SetDataOk("Oops!, There seems to be an issue....", FetchVersion);
            }
        }
    }

    private List<string> GetAllSortedVideoThumbnails(List<GameData.VideoGroup> inVideoGroups)
    {
        List<GameData.VideoData> allVideoData = new List<GameData.VideoData>();
        if (inVideoGroups.Any())
        {
            List<GameData.VideoGroup> _currentVideoGroups = inVideoGroups.Clone().Take(MAXTHUMBNAILDOWNLOADVIDEOS).ToList();
            _currentVideoGroups.ForEach(x =>
           {
               List<GameData.VideoData> freeVideos = new List<GameData.VideoData>();
               List<GameData.VideoData> premiumVideos = new List<GameData.VideoData>();
               freeVideos.AddRange(from element in x.VideosList.Where(item => item.isFree.Equals(true))
                                   orderby element.UploadDate.FromUnixTime() descending
                                   select element);

               premiumVideos.AddRange(from element in x.VideosList.Where(item => item.isFree.Equals(false))
                                      orderby element.UploadDate.FromUnixTime() descending
                                      select element);

               allVideoData.AddRange(freeVideos.Concat(premiumVideos).ToList().Take(MAXTHUMBNAILDOWNLOADVIDEOS));
           });
        }
        return allVideoData.Select(x => x.ThumbnailUrl).ToList();
    }

    private List<string> GetAllSortedStoryThumbnails(List<GameData.StoryData> allStories)
    {
        var orderedList = allStories.OrderBy(o => o.UploadDate.FromUnixTime()).ToList().ToList();
        var orderedListByFree = orderedList.OrderBy(o => !o.IsFree).ToList().Take(MAXTHUMBNAILDOWNLOADSTORIES);
        return orderedListByFree.Select(x => x.ThumbnailUrl).ToList();
    }

    void DownloadAllImages()
    {
        var images = new List<string>() { };
        var rhymeGrpThumbs = LocalDataManager.Instance.GetConfig<GameData.VideoCollection>(EConfigFileName.Video).Rhymes.Select(x => x.GroupThumbnail).ToList().Take(MAXTHUMBNAILDOWNLOADVIDEOS);
        var showsGrpThumbs = LocalDataManager.Instance.GetConfig<GameData.VideoCollection>(EConfigFileName.Video).Shows.Select(x => x.GroupThumbnail).ToList().Take(MAXTHUMBNAILDOWNLOADVIDEOS);
        var storyThumbs = GetAllSortedStoryThumbnails(LocalDataManager.Instance.GetConfig<GameData.StoryCollection>(EConfigFileName.Stories).Stories);
        var rhymeThumbs = GetAllSortedVideoThumbnails(LocalDataManager.Instance.GetConfig<GameData.VideoCollection>(EConfigFileName.Video).Rhymes);
        var showsThumbs = GetAllSortedVideoThumbnails(LocalDataManager.Instance.GetConfig<GameData.VideoCollection>(EConfigFileName.Video).Shows);
        var otherAppThumbs = LocalDataManager.Instance.GetConfig<GameData.OtherAppCollection>(EConfigFileName.OtherApps).OtherApps.Select(x => x.ThumbnailLink).ToList();

        images.AddRange(otherAppThumbs);
        images.AddRange(storyThumbs);
        images.AddRange(rhymeGrpThumbs);
        images.AddRange(showsGrpThumbs);
        images.AddRange(rhymeThumbs);
        images.AddRange(showsThumbs);



        FileManager.Instance.GetImage(urls: images.ToArray(),
            onComplete: (x) =>
             {
                 Debug.Log("Batch Download Success");
                 // OnConfigLoaded.SafeInvoke();
             }, owner: this, onFail: (error) =>
              {
                  Debug.Log("Batch Download Failed");
              },/* onProgress: (x) => SplashLoader.Instance.SetText(x + "%"),*/ needDataOnComplete: false);
    }

    void OnConfigReady()
    {
        Debug.Log("OnConfigReady");

        OnLocalizationTextLoaded.SafeInvoke();
        LocalDataManager.Instance.Save();
        FirebaseRemoteConfig.Instance.Initiate(updatedValues =>
        {
            Debug.LogError("Inside OnConready store");
            try
            {
                PurchaseManager.Instance.InitializeStore(updatedValues);
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
            finally
            {

            }

        });

        StartCoroutine(DelayDownloadStart());
    }

    IEnumerator DelayDownloadStart()
    {
        yield return new WaitForSeconds(3f);
        DownloadAllImages();
        OnConfigLoaded.SafeInvoke();
    }
}