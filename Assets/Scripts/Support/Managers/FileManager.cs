﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public enum EFileType
{
	[Description (".avi|.mp4")]
	Video,

	[Description (".mp3")]
	Audio,

	[Description (".png|.jpg")]
	Image,

	[Description (".json")]
	Data,

	[Description (".")]
	Invalid
}

public enum EFailureReason
{
	INTERNETCONNECTION,

	STORAGEFULL,

	TECHNICAL,

	SELF
}

public class FMDownloadObject
{
	Action<float> ProgressCallback;
	object OnCompleteCallback;
	Action<EFailureReason> OnFailureCallback;
	readonly List<UnityWebRequest> Requests = new List<UnityWebRequest> ();
	readonly List<Coroutine> Coroutines = new List<Coroutine> ();
	public string Id = String.Empty;
	readonly bool NeedDataOnComplete = true;
	public Action<FMDownloadObject> OnDownloadComplete;
	readonly List<string> Urls = new List<string> ();
	public bool IsDownloadStarted = false;
	private int downloadCounter = 0;

	public UnityEngine.Object Owner;

	public static FMDownloadObject CreateRequest (List<string> urls, object onCompleteCallback, UnityEngine.Object owner, string id, Action<float> progressCallback = null,
	                                              Action<EFailureReason> onFailureCallback = null, bool needDataOnComplete = true)
	{
		return new FMDownloadObject (urls, onCompleteCallback, progressCallback, onFailureCallback, needDataOnComplete, owner, id);
	}

	public FMDownloadObject StartDownload ()
	{
		if (!IsDownloadStarted) {
			IsDownloadStarted = true;

			foreach (var req in Requests) {
				Coroutines.Add (FileManager.Instance.StartCoroutine (BatchDownload (req)));
			}
			Coroutines.Add (FileManager.Instance.StartCoroutine (UpdateBatchProgress ()));

		}
		return this;
	}

	FMDownloadObject (List<string> urls, object onCompleteCallback, Action<float> progressCallback, Action<EFailureReason> onFailureCallback, bool needDataOnComplete, UnityEngine.Object owner, string id)
	{
		this.Id = id;
		this.ProgressCallback = progressCallback;
		this.OnCompleteCallback = onCompleteCallback;
		this.OnFailureCallback = onFailureCallback;
		this.NeedDataOnComplete = needDataOnComplete;
		this.Owner = owner;
		Urls.AddRange (urls.Select (x => new Uri (x).AbsoluteUri));
		var randPool = Guid.NewGuid ().GetMD5CheckSum ();
		for (int i = 0, j = 0; i < Urls.Count; i++) {
			if (!FileManager.IsFileDownloaded (Urls [i])) {
				if (j > randPool.Length - 1)
					j = 0;

				Urls [i] = String.Concat (Urls [i], "?p=" + randPool [j++]);
				Requests.Add (UnityWebRequest.Get (new Uri (Urls [i]).AbsoluteUri));
			}
		}

	}

	IEnumerator BatchDownload (UnityWebRequest www)
	{
		yield return www.Send ();

		#if UNITY_5
		if (www.isError)

		#else
		if (www.isNetworkError || www.isHttpError)
		#endif
		{
			Debug.Log (string.Format ("Download Failed for Url ->  {0}  , Error  -> {1}", www.url,
				www.error));
			OnFailure (EFailureReason.INTERNETCONNECTION);
		} else
		{
			string message = string.Empty;
			try
			{
				FileManager.SaveToCache (www);
			}
			catch(IOException e)
			{
				message = e.Message;
			}
			if (string.IsNullOrEmpty (message))
			{
				OnBatchComplete (www);
			}
			else
			{
				if (message.ToLower ().Contains ("disk full"))
				{
					OnFailure (EFailureReason.STORAGEFULL);
				}
				else
				{
					OnFailure (EFailureReason.TECHNICAL);
				}
			}
		}
	}

	IEnumerator UpdateBatchProgress ()
	{
		while (true) {
			ProgressCallback.SafeInvoke ((int)((Requests.Select (dl =>
				dl.downloadProgress >= 0 ? dl.downloadProgress : 0
			).Sum () / Requests.Count) * 100));
			yield return null;
		}
	}

	void OnBatchComplete (UnityWebRequest www)
	{
		if (IsBatchDownloadOver ()) {

			Coroutines.ForEach (x => FileManager.Instance.StopCoroutine (x));
			Coroutines.Clear ();
			ProgressCallback.SafeInvoke (100f);
			var url = www.url.Split ('?') [0];
			for (int i = 0; i < Urls.Count; i++) {

				Urls [i] = Urls [i].Split ('?') [0];

			}
			switch (FileManager.GetFileType (url)) {
			case EFileType.Audio:
				{
					var audioClips = new List<AudioClip> ();
					if (NeedDataOnComplete) {
						Urls.ForEach ((x) => {
							FileManager.Instance.StartCoroutine (FileManager.LoadAudioFromCache (FileManager.GetFilePath (x), (y) => {
								audioClips.Add (y);
								if (audioClips.Count == Urls.Count) {
									if (OnCompleteCallback != null)
										((Action<List<AudioClip>>)OnCompleteCallback).SafeInvoke (audioClips);
									OnDownloadComplete.SafeInvoke (this);
								}
							}));
						});
					} else {
						if (OnCompleteCallback != null)
							((Action<List<AudioClip>>)OnCompleteCallback).SafeInvoke (audioClips);
						OnDownloadComplete.SafeInvoke (this);
					}


					break;
				}
			case EFileType.Video:
				{
					if (NeedDataOnComplete) {
						var videoUrls = Urls.Select (x => "file://" + FileManager.GetFilePath (x)).ToList ();
						if (OnCompleteCallback != null)
							((Action<List<string>>)OnCompleteCallback).SafeInvoke (videoUrls);
					} else {
						if (OnCompleteCallback != null)
							((Action<List<string>>)OnCompleteCallback).SafeInvoke (new List<string> ());
					}
					OnDownloadComplete.SafeInvoke (this);
					break;
				}
			case EFileType.Image:
				{
					if (NeedDataOnComplete) {
						var images = Urls.Select (FileManager.LoadImageFromCache).ToList ();
						if (OnCompleteCallback != null)
							((Action<List<Texture2D>>)OnCompleteCallback).SafeInvoke (images);
					} else {
						if (OnCompleteCallback != null)
							((Action<List<Texture2D>>)OnCompleteCallback).SafeInvoke (new List<Texture2D> ());
					}
					OnDownloadComplete.SafeInvoke (this);
					break;
				}
			case EFileType.Data:
				{
					if (NeedDataOnComplete) {
						var allData = Urls.Select (x => {
							var json = Utilities.LoadJsonData (x.GenerateUniqueId (), FileManager.DataSaveType,
								           Path.Combine (Utilities.SavePath, FileManager.GetFileType (x).ToString ()), "." + FileManager.GetFileFormat (x));

							return json;
						}).ToList ();
						if (OnCompleteCallback != null)
							((Action<List<string>>)OnCompleteCallback).SafeInvoke (allData);

					} else {
						if (OnCompleteCallback != null)
							((Action<List<string>>)OnCompleteCallback).SafeInvoke (new List<string> ());
					}
					OnDownloadComplete.SafeInvoke (this);
					break;
				}
			}
		}
	}

	public void OnFailure (EFailureReason error)
	{
		ProgressCallback.SafeInvoke (0f);
		Coroutines.ForEach (x => FileManager.Instance.StopCoroutine (x));
		Requests.ForEach (x => {
			x.Abort ();
			x.Dispose ();
		});
		Coroutines.Clear ();
		OnFailureCallback.SafeInvoke (error);
		NullCallBacks ();
		OnDownloadComplete.SafeInvoke (this);
	}

	public void NullCallBacks ()
	{
		OnCompleteCallback = OnFailureCallback = null;
	}

	public FMDownloadObject AppendDownload (string url, object onCompleteCallback, Action<float> progressCallback = null,
	                                        Action<EFailureReason> onFailureCallback = null)
	{
		switch (FileManager.GetFileType (url)) {
		case EFileType.Audio:
			{
				var callback = ((Action<List<AudioClip>>)this.OnCompleteCallback);
				callback += ((Action<List<AudioClip>>)onCompleteCallback);
				OnCompleteCallback = callback;
				ProgressCallback += progressCallback;
				OnFailureCallback = onFailureCallback;
				break;
			}
		case EFileType.Video:
			{
				var callback = ((Action<List<string>>)this.OnCompleteCallback);
				callback += ((Action<List<string>>)onCompleteCallback);
				OnCompleteCallback = callback;
				ProgressCallback += progressCallback;
				OnFailureCallback = onFailureCallback;
				break;
			}
		case EFileType.Image:
			{
				var callback = ((Action<List<Texture2D>>)this.OnCompleteCallback);
				callback += ((Action<List<Texture2D>>)onCompleteCallback);
				OnCompleteCallback = callback;
				ProgressCallback += progressCallback;
				OnFailureCallback = onFailureCallback;
				break;
			}
		case EFileType.Data:
			{
				var callback = ((Action<List<string>>)this.OnCompleteCallback);
				callback += ((Action<List<string>>)onCompleteCallback);
				OnCompleteCallback = callback;
				ProgressCallback += progressCallback;
				OnFailureCallback = onFailureCallback;
				break;
			}
		}
		return this;
	}

	public bool IsBatchDownloadOver ()
	{
		return (++downloadCounter == Requests.Count);
	}
}

public class FileManager : GenericSingleton<FileManager>
{
	protected override bool IsPersistant {
		get { return true; }
	}

	const long VideoCacheLimit = 1024 * 1024;
	//1GB
	const long ImageCacheLimit = 1024 * 1024;
	const long AudioCacheLimit = 1024 * 1024;
	const long DataCacheLimit = (512 * 512);
	//256mb
	static readonly Dictionary<string, FMDownloadObject> CurrentDownloads = new Dictionary<string, FMDownloadObject> ();
	static readonly Dictionary<string, List<string>> DownloadReferences = new Dictionary<string, List<string>> ();
	#if UNITY_EDITOR
	public static Utilities.ESaveTypes DataSaveType = Utilities.ESaveTypes.Readable;
	






#else
	public static Utilities.ESaveTypes DataSaveType = Utilities.ESaveTypes.Unreadable;
	#endif
	public static void RemoveFolder (EFileType folderType)
	{
		var dirInfo = (new DirectoryInfo (Utilities.SavePath));
		if (dirInfo.Exists) {
			var dirs = dirInfo.GetDirectories ();
			foreach (var dir in dirs) {
				if (string.Equals (dir.Name, folderType.ToString ())) {
					foreach (var file in dir.GetFiles()) {
						file.Delete ();
					}
					dir.Delete ();
					return;
				}
			}
		}
	}

	public static EFileType GetFileType (string url)
	{
		Uri uri = new Uri (url);
		var fileType = uri.Segments [uri.Segments.Length - 1].Split ('.');
		var values = Enum.GetValues (typeof(EFileType));
		foreach (EFileType val in values) {
			var formats = val.GetDescription ().Split ('|');
			foreach (var format in formats) {
				if (string.Equals (format.Replace (".", ""), fileType.Length > 0 ? Uri.UnescapeDataString (fileType [1]) : Uri.UnescapeDataString (fileType [0])))
					return val;
			}
		}

		return EFileType.Invalid;
	}

	public static string GetFileFormat (string url)
	{
		Uri uri = new Uri (url);
		var fileType = uri.Segments [uri.Segments.Length - 1].Split ('.');
		return fileType.Length > 0 ? Uri.UnescapeDataString (fileType [1]) : Uri.UnescapeDataString (fileType [0]);
	}

	public static string GetFilePath (string fileUrl)
	{
		fileUrl = new Uri (fileUrl).AbsoluteUri;
		return Uri.UnescapeDataString (Path.Combine (Path.Combine (Utilities.SavePath, GetFileType (fileUrl).ToString ()), fileUrl.GenerateUniqueId () + "." + GetFileFormat (fileUrl)));
	}

	public static Texture2D LoadImageFromCache (string fileUrl)
	{
		var bytes = File.ReadAllBytes (GetFilePath (fileUrl));
		var tex = new Texture2D (1, 1);
		tex.LoadImage (bytes);
		tex.name = GetFileName (fileUrl);
		return tex;
	}

	public static IEnumerator LoadAudioFromCache (string fileUrl, Action<AudioClip> onLoad, Action<EFailureReason> onFail = null)
	{
		if (Application.platform == RuntimePlatform.WindowsEditor ||
		    Application.platform == RuntimePlatform.WindowsPlayer)
			fileUrl = "file:///" + fileUrl;
		else
			fileUrl = "file://" + fileUrl;
		using (WWW www = new WWW (fileUrl)) {
			yield return www;

			if (string.IsNullOrEmpty (www.error)) {
				var aud = www.GetAudioClip ();
				aud.name = FileManager.GetFileName (fileUrl);
				onLoad.SafeInvoke (www.GetAudioClip ());
			} else {
				onFail.SafeInvoke (EFailureReason.TECHNICAL);
			}
		}
	}

	static double GetDirectorySizeInMb (string path)
	{
		if (!Directory.Exists (Utilities.SavePath))
			return 0;

		string[] files = Directory.GetFiles (path);
		string[] subdirectories = Directory.GetDirectories (path);

		double size = files.Sum (x => new FileInfo (x).Length);
		foreach (string s in subdirectories)
			size += GetDirectorySizeInMb (s);


		size = size / 1048576;

		return size;
	}

	static bool IsCacheFull (EFileType fileType, byte[] incomingFile)
	{
		double size = GetDirectorySizeInMb (Utilities.SavePath + fileType) + (incomingFile.Length / (double)(1024 * 1024));
		long cacheLimit = 0;
		switch (fileType) {
		case EFileType.Audio:
			{
				cacheLimit = AudioCacheLimit;
				break;
			}
		case EFileType.Video:
			{
				cacheLimit = VideoCacheLimit;
				break;
			}
		case EFileType.Image:
			{
				cacheLimit = ImageCacheLimit;
				break;
			}
		case EFileType.Data:
			{
				cacheLimit = DataCacheLimit;
				break;
			}
		}
		return size >= cacheLimit;
	}

	static void MakeSpaceForFile (EFileType fileType, byte[] fileBytes)
	{
		if (Directory.Exists (Utilities.SavePath + fileType) && IsCacheFull (fileType, fileBytes)) {
			Action<IEnumerable<FileInfo>> remover = (input) => {
				foreach (var file in input) {
					File.Delete (file.FullName);
				}
			};

			var dir = new DirectoryInfo (Utilities.SavePath + fileType.ToString ());

			List<FileInfo> files = dir.GetFiles ("*.*", SearchOption.TopDirectoryOnly).ToList ();
			files.Sort ((a, b) => a.LastAccessTimeUtc.CompareTo (b.LastAccessTimeUtc));
			int i = 0;
			while (IsCacheFull (fileType, fileBytes)) {
				if (i >= files.Count)
					return;
				remover.Invoke (files.Skip (i).Take (1));
				i++;
			}
		}
	}

	public static void SaveToCache (UnityWebRequest www)
	{
		var origUrl = www.url.Split ('?') [0];
		if (!Directory.Exists (Path.Combine (Utilities.SavePath, GetFileType (origUrl).ToString ()))) {
			Directory.CreateDirectory (Path.Combine (Utilities.SavePath, GetFileType (origUrl).ToString ()));
		}

		//MakeSpaceForFile(GetFileType(www.url), www.downloadHandler.data);
		if (GetFileType (origUrl) == EFileType.Data) {
			www.downloadHandler.text.SaveJson (origUrl.GenerateUniqueId (), DataSaveType, Path.Combine (Utilities.SavePath, GetFileType (origUrl).ToString ()), "." + GetFileFormat (origUrl));
		} else {
			using (var file = File.Open (GetFilePath (origUrl), FileMode.Create))
			using (var binary = new BinaryWriter (file)) {
				binary.Write (www.downloadHandler.data);
			}
		}
	}

	public static bool IsFileDownloaded (string url)
	{
		return File.Exists (GetFilePath (url));
	}

	public static void RemoveFile (string url)
	{
		if (IsFileDownloaded (url))
			File.Delete (GetFilePath (url));
	}

	public static bool IsFileDownloaded (IEnumerable<string> urls)
	{
		bool result = true;
		foreach (var url in urls) {
			result = IsFileDownloaded (url);
			if (!result)
				break;
		}

		return result;
	}

	public static bool IsDownloadInitiated (params string[] urls)
	{
		if (urls.Length > 0) {
			string id = String.Empty;
			if (urls.Length == 1) {
				id = new Uri (urls [0]).AbsoluteUri.GenerateUniqueId ();
			} else {
				id = urls.ToList ().GenerateUniqueId (); //urls.ToList().Aggregate((i, j) => new Uri(i).AbsoluteUri + new Uri(j).AbsoluteUri).GenerateUniqueId();
			}

			return CurrentDownloads.ContainsKey (id);

		}
		return false;
	}

	public static bool CancelDownload (params string[] urls)
	{
		if (urls.Length > 0) {
			string id = String.Empty;
			if (urls.Length == 1) {
				id = new Uri (urls [0]).AbsoluteUri.GenerateUniqueId ();
			} else {
				id = urls.ToList ().GenerateUniqueId ();// urls.ToList().Aggregate((i, j) => new Uri(i).AbsoluteUri + new Uri(j).AbsoluteUri).GenerateUniqueId();
			}
			if (CurrentDownloads.ContainsKey (id)) {
				CurrentDownloads [id].OnFailure (EFailureReason.SELF);
				return true;
			}
		}
		return false;
	}

	public static void SaveImageToGallery (string fileName, byte[] byteStream)
	{
		var location = "/mnt/sdcard/DCIM/KidZooly";


		Utilities.SetEnvironmentVariableForSerialization ();

		if (!Directory.Exists (location)) {
			Directory.CreateDirectory (location);
		}
		using (var file = File.Open (Path.Combine (location, fileName + ".jpg"), FileMode.Create))
		using (var binary = new BinaryWriter (file)) {
			binary.Write (byteStream);
		}
	}

	public static string GetFileName (string fileUrl)
	{
		Uri uri = new Uri (fileUrl);
		var fileName = uri.Segments [uri.Segments.Length - 1].Split ('.');
		return Uri.UnescapeDataString (fileName [0]);
	}

	public static string GetLocalFileName (string fileUrl)
	{
		int lastIndex = fileUrl.LastIndexOf ('/');
		return fileUrl.Substring (lastIndex + 1);
	}

	bool GetFile (UnityEngine.Object owner, object onComplete, Action<EFailureReason> onFail = null, Action<float> onProgress = null, bool needDataOnDownload = true, params string[] urls)
	{

		if (urls.Length <= 0) {
			onFail.SafeInvoke (EFailureReason.INTERNETCONNECTION);
			return false;
		}

		string id = string.Empty;
		var urlList = urls.ToList ().Where (x => x != string.Empty).ToList ();
		if (urlList.Count == 1) {
			id = new Uri (urlList [0]).AbsoluteUri.GenerateUniqueId ();
			if (GetFileType (urlList [0]) == EFileType.Invalid) {
				onFail.SafeInvoke (EFailureReason.INTERNETCONNECTION);
				return false;
			}
		} else {

			id = urlList.GenerateUniqueId ();//urlList.Aggregate((i, j) => new Uri(i).AbsoluteUri + new Uri(j).AbsoluteUri).GenerateUniqueId();

			foreach (var url in urlList) {
				if (GetFileType (url) == EFileType.Invalid) {
					onFail.SafeInvoke (EFailureReason.INTERNETCONNECTION);
					return false;
				}
			}
		}
		List<string> toDownload = new List<string> ();
		foreach (var url in urlList) {
			if (!IsFileDownloaded (url)) {
				toDownload.Add (url);
			}
		}
		if (toDownload.Any ()) {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				onFail.SafeInvoke (EFailureReason.INTERNETCONNECTION);
				return false;
			}

			var instanceId = owner.GetInstanceID ().ToString ();
			if (DownloadReferences.ContainsKey (instanceId))
				DownloadReferences [instanceId].Add (id);
			else
				DownloadReferences.Add (instanceId, new List<string> { id });

			if (!CurrentDownloads.ContainsKey (id)) {
				CurrentDownloads.Add (id, FMDownloadObject.CreateRequest (urlList.ToList (), onComplete, owner, id, onProgress, onFail, needDataOnDownload).StartDownload ());
				CurrentDownloads [id].OnDownloadComplete += OnComplete;
			} else {
				CurrentDownloads [id].AppendDownload (urlList [0], onComplete, onProgress, onFail);
			}
		}

		return !toDownload.Any ();
	}



	public void GetData (Action<List<string>> onComplete, UnityEngine.Object owner, Action<EFailureReason> onFail = null, Action<float> onProgress = null, bool needDataOnComplete = true, params string[] urls)
	{
		if (GetFile (owner, onComplete, onFail, onProgress, needDataOnComplete, urls)) {
			onProgress.SafeInvoke (100);
			onComplete.SafeInvoke (needDataOnComplete ? urls.Select (x => Utilities.LoadJsonData (x.GenerateUniqueId (), DataSaveType, Path.Combine (Utilities.SavePath, GetFileType (x).ToString ()), "." + GetFileFormat (x))).ToList () : null);
		}
	}



	public void GetImage (Action<List<Texture2D>> onComplete, UnityEngine.Object owner, Action<EFailureReason> onFail = null, Action<float> onProgress = null, bool needDataOnComplete = true, params string[] urls)
	{
        
		if (GetFile (owner, onComplete, onFail, onProgress, needDataOnComplete, urls)) {
			onProgress.SafeInvoke (100);
			onComplete.SafeInvoke (needDataOnComplete ? urls.Select (LoadImageFromCache).ToList() : null);
            
		}
	}

	public void GetLocalImage (Action<List<Texture2D>> onComplete, Action<EFailureReason> onFail = null, params string[] urls)
	{
        
		try
		{
			List<Texture2D> loadedImages = new List<Texture2D> ();
			foreach (var item in urls)
			{
               
				loadedImages.Add (Resources.Load<Texture2D> (item));
			}
			onComplete.SafeInvoke (loadedImages);
            
		}
		catch(Exception e)
		{
			Debug.Log (e.Message);
			onFail.Invoke (EFailureReason.TECHNICAL);
		}
	}

	public void GetAvailableImage (Action<List<Texture2D>> onComplete, UnityEngine.Object owner, Action<EFailureReason> onFail = null, Action<float> onProgress = null, bool needDataOnComplete = true, params string[] urls)
	{
		var availableImages = new List<string> ();
		foreach (var item in urls)
		{
			if (FileManager.IsFileDownloaded (item))
			{
				availableImages.Add (item);
			}
		}
		GetImage (onComplete, owner, onFail, onProgress, needDataOnComplete, availableImages.ToArray());
	}

	public void GetVideo (Action<List<string>> onComplete, UnityEngine.Object owner, Action<EFailureReason> onFail = null, Action<float> onProgress = null, bool needDataOnComplete = true, params string[] urls)
	{
		if (GetFile (owner, onComplete, onFail, onProgress, needDataOnComplete, urls)) {
			onProgress.SafeInvoke (100);
			onComplete.SafeInvoke (needDataOnComplete ? urls.Select (x => GetFilePath (x)).ToList () : null);
		}
	}



	public void GetAudio (Action<List<AudioClip>> onComplete, UnityEngine.Object owner, Action<EFailureReason> onFail = null, Action<float> onProgress = null, bool needDataOnComplete = true, params string[] urls)
	{
		if (GetFile (owner, onComplete, onFail, onProgress, needDataOnComplete, urls)) {
			onProgress.SafeInvoke (100);
			var audioClips = new List<AudioClip> ();
			var totalClips = urls.Length;
			if (needDataOnComplete) {
				urls.Each ((x, i) => {
					StartCoroutine (LoadAudioFromCache (GetFilePath (urls [i]), (y) => {
						audioClips.Add (y);
						if (audioClips.Count == totalClips) {
							onComplete.SafeInvoke (audioClips);
						}
					}));
				});
			} else
				onComplete.SafeInvoke (null);
		}
	}

	static void OnComplete (FMDownloadObject obj)
	{
		var id = obj.Id;
		RemoveReference (obj.Owner, obj);
		if (CurrentDownloads.ContainsKey (id)) {
			CurrentDownloads [id] = null;
			CurrentDownloads.Remove (id);
		}
	}

	public static void RemoveReference (UnityEngine.Object reference, FMDownloadObject obj = null)
	{
		var instanceId = reference.GetInstanceID ().ToString ();
		if (DownloadReferences.ContainsKey (instanceId)) {
			if (obj == null) {
				foreach (var id in DownloadReferences[instanceId]) {
					if (CurrentDownloads.ContainsKey (id)) {
						CurrentDownloads [id].NullCallBacks ();
					}
				}
			} else {
				if (CurrentDownloads.ContainsKey (obj.Id)) {
					CurrentDownloads [obj.Id].NullCallBacks ();
				}
			}

			DownloadReferences.Remove (instanceId);
		}
	}
}