﻿using UnityEngine;
using System.Collections;


public class GenericManager<X> : GenericSingleton<X> where X : GenericManager<X>
{
    protected override bool IsPersistant
    {
        get
        {
            return true;
        }
    }
}

