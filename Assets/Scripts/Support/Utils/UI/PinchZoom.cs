﻿using UnityEngine;

public class PinchZoom : MonoBehaviour
{
    Vector2 originalSize = Vector2.zero;
    Vector2 CheckBounds(Vector2 pos)
    {
        var rt = GetComponent<RectTransform>();
        var xMax = transform.position.x - (rt.offsetMin.x / 100);
        var xMin = transform.position.x - (rt.offsetMax.x / 100);
        var yMax = transform.position.y - (rt.offsetMin.y / 100);
        var yMin = transform.position.y - (rt.offsetMax.y / 100);

        pos = new Vector2(Mathf.Clamp(pos.x, xMin, xMax), Mathf.Clamp(pos.y, yMin, yMax));

        return pos;


    }

    public static bool isPinchZooming = false;
    float HandleScroll()
    {
        float scrollVal = 0;
        if (Camera.main.pixelRect.Contains(Input.mousePosition))
        {
            scrollVal = Input.GetAxis("Mouse ScrollWheel") * 100.0f;

        }
        return scrollVal;
    }
    void DoZoom(float factor)
    {
        if (factor != 0.0f)
        {
            var rt = (GetComponent<RectTransform>());
            var offsetMax = rt.offsetMax;
            var offSetMin = rt.offsetMin;
            var offsetMax_ = rt.offsetMax;
            var offSetMin_ = rt.offsetMin;
            offsetMax = new Vector2(offsetMax.x + factor, offsetMax.y + factor);
            offSetMin = new Vector2(offSetMin.x - factor, offSetMin.y - factor);
            rt.offsetMax = offsetMax;
            rt.offsetMin = offSetMin;
            if (rt.rect.size.x < originalSize.x && rt.rect.size.y < originalSize.y)
            {

                rt.offsetMax = offsetMax_;
                rt.offsetMin = offSetMin_;
                rt.rect.Set(0, 0, originalSize.x, originalSize.y);
            }
            else
            {
                transform.position = CheckBounds(transform.position);
            }

        }
    }
    float HandlePinch()
    {
        float deltaMagnitudeDiff = 0;
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            deltaMagnitudeDiff = (prevTouchDeltaMag - touchDeltaMag) * -1;
            isPinchZooming = true;
        }
        else
        {
            isPinchZooming = false;
        }
        return deltaMagnitudeDiff;
    }
    void Update()
    {
#if UNITY_EDITOR
        DoZoom(HandleScroll());

#else
          DoZoom(HandlePinch());
#endif


    }

    void Start()
    {
        originalSize = GetComponent<RectTransform>().rect.size;
    }
}