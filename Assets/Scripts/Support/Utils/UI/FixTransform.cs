﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixTransform : MonoBehaviour
{
    public bool LockRotation, LockLocation;

    private Quaternion _worldRotation;
    private Vector3 _worldLocation;

    // Use this for initialization
    void Awake()
    {
        _worldLocation = transform.position;
        _worldRotation = transform.rotation;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (LockLocation)
            transform.position = _worldLocation;
        if (LockRotation)
            transform.rotation = _worldRotation;
    }
}