﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLength : MonoBehaviour
{

	public Text TextComponent;
	public InputField InputComponent;

	public int LengthText
	{
		get
		{
			if (TextComponent)
			{
				return TextComponent.text.Length;
			}

			return 0;
		}
	}

	public int LengthInputField
	{
		get
		{
			if (InputComponent)
			{
				return InputComponent.text.Length;
			}

			return 0;
		}
	}
}
