﻿
public static class GameConstants
{
    public const string SAVE_DATA_KEY = "KidZooly";
    public const string DEFAULT_LANGUAGE = "English";
#if AppleStoreBuild
    public const string APP_VERSION = "Version_1.55(55)";
#elif AmazonStoreBuild
    public const string APP_VERSION = "Version_32(32)";
#elif GoogleStoreBuild
    public const string APP_VERSION = "Version_1.58(58)";
#endif
    public static string[] NOTIFICATIONTEXTS = { "Kids theme park is Open to Play. Click this to open the app.", "Animals are waiting to Roar. Let's Start learning", "Watch the latest Kids Learning Videos. Download Now", "Many stories are waiting to be told. Open the Storybook now", "Solving Kids Puzzles Improves Memory", "Interact with Kids Rhymes in Interactive Rhymes", "Exclusive Kids Content is available to watch now" };

    public const string JSON_ROOT_FOLDER = "GameData/";
    public const string GAME_JSON = "abc_performance";
}