﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public interface IStorageService
{

    void GetConfigVersion(Action<DataStorage> OnSuccess, Action<Exception> OnFailure);

    void GetConfigFile(Action<DataStorage> OnSuccess, Action<Exception> OnFailure, string url);


}
