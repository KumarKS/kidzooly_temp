﻿using System;
using UnityEngine;

public static class DateTimeExtensions
{
	public static DateTime FromUnixTime(this long unixTime)
	{
		var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		epoch = epoch.AddMilliseconds((double)unixTime);
		return epoch.ToLocalTime();
	}

    public static DateTime FromUnixTime(this string unixTime)
    {
        var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        epoch = epoch.AddSeconds(Convert.ToDouble(unixTime));
        return epoch.ToLocalTime();


    }

    public static string ToUnixTime(this DateTime unixTime)
    {
        string unixTimeStamp;
        DateTime zuluTime = unixTime.ToUniversalTime();
        DateTime unixEpoch = new DateTime(1970, 1, 1);
        unixTimeStamp = (zuluTime.Subtract(unixEpoch)).TotalSeconds.ToString();
        return unixTimeStamp;
    }

    public static double ToUnixTimeDouble(this DateTime unixTime)
    {
        DateTime zuluTime = unixTime.ToUniversalTime();
        DateTime unixEpoch = new DateTime(1970, 1, 1);
        return (zuluTime.Subtract(unixEpoch)).TotalSeconds;
    }

    public static string GetCurrentUnixTime()
    {
        string unixTimeStamp;
        DateTime currentTime = DateTime.Now;
        DateTime zuluTime = currentTime.ToUniversalTime();
        DateTime unixEpoch = new DateTime(1970, 1, 1);
        unixTimeStamp = ((int)(zuluTime.Subtract(unixEpoch)).TotalSeconds).ToString();
        return unixTimeStamp;
    }

    public static double DifferenceBetweenTwoUnixTime(string laterUnixTime, string formerUnixTime)
    {
        var later = Convert.ToDouble(laterUnixTime);
        var former = Convert.ToDouble(formerUnixTime);

        return (later - former);
    }


}
