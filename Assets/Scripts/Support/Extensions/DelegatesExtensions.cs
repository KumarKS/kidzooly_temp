﻿using UnityEngine;
using System.Collections;
using System;

public static class DelegatesExtensions
{

    public static void SafeInvoke(this Action callBack)
    {
        if (callBack != null)
        {
            callBack();
        }
    }

    public static void SafeInvoke<X>(this Action<X> callBack, X data)
    {
        if (callBack != null)
        {
            callBack(data);
        }
    }

    public static void SafeInvoke<X, Y>(this Action<X, Y> callBack, X data1, Y data2)
    {
        if (callBack != null)
        {
            callBack(data1, data2);
        }
    }

    public static void SafeInvoke<X, Y,Z>(this Action<X, Y,Z> callBack, X data1, Y data2, Z data3)
    {
        if (callBack != null)
        {
            callBack(data1, data2, data3);
        }
    }

    public static void SafeInvoke<X,Y,Z,W>(this Action<X, Y,Z,W> callBack, X data1, Y data2, Z data3, W data4)
    {
        if (callBack != null)
        {
            callBack(data1, data2, data3, data4);
        }
    }

}
