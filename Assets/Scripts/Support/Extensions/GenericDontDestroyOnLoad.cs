﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GenericDontDestroyOnLoad<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance = null;
    //private static bool applicationIsQuitting = false;
    private static bool isSpawnedAlready = false;
    // Use this for initialization

    protected virtual bool IsPersistant
    {
        get { return true; }
    }

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType(typeof(T)) as T;
                if (_instance == null)
                {
                    _instance = new GameObject().AddComponent<T>();
                    _instance.gameObject.name = _instance.GetType().Name;

                    isSpawnedAlready = true;
                }

                _instance.gameObject.AddComponent<DontDestroyOnLoad>();
            }


            return _instance;
        }
        set { _instance = value; }
    }

    public static bool HasInstance
    {
        get { return !IsDestroyed; }
    }

    public static bool IsDestroyed
    {
        get { return _instance == null; }
    }

    protected virtual void Awake()
    {
        if (IsPersistant)
        {
            if (isSpawnedAlready)
            {
                Debug.Log("Deleting duplicate - " + gameObject.GetInstanceID() + " " + gameObject.name);
                //Debug.Log("_instance - " + _instance.GetInstanceID());
                Destroy(gameObject);
            }

            isSpawnedAlready = true;

            //scene management
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        }
    }

    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        OnLevelLoaded(scene);
    }


    protected virtual void OnLevelLoaded(Scene scene)
    {
    }

    protected virtual void OnDestroy()
    {
        //scene management
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

    protected virtual void OnApplicationQuit()
    {
        _instance = null;
        //applicationIsQuitting = true;
    }
}