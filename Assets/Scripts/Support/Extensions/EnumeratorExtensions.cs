﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Text;
using UnityEngine.UI;
using System.Reflection;
using System.ComponentModel;

public class Iterator
{
    int currentValue = 0;

    public int Next()
    {
        return currentValue++;
    }

    public int GetIndex()
    {
        return currentValue;
    }

    public int Skip(int skipBy)
    {
        currentValue += skipBy;
        return currentValue++;
    }
}

public static class EnumeratorExtensions
{
    public static IEnumerable<T> GetValues<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }

    public static bool Has<T>(this System.Enum type, T value)
    {
        try
        {
            return (((int) (object) type & (int) (object) value) == (int) (object) value);
        }
        catch
        {
            return false;
        }
    }

    public static bool Is<T>(this System.Enum type, T value)
    {
        try
        {
            return (int) (object) type == (int) (object) value;
        }
        catch
        {
            return false;
        }
    }


    public static T Add<T>(this System.Enum type, T value)
    {
        try
        {
            return (T) (object) (((int) (object) type | (int) (object) value));
        }
        catch (Exception ex)
        {
            throw new ArgumentException(
                string.Format(
                    "Could not append value from enumerated type '{0}'.",
                    typeof(T).Name
                ), ex);
        }
    }


    public static T Remove<T>(this System.Enum type, T value)
    {
        try
        {
            return (T) (object) (((int) (object) type & ~(int) (object) value));
        }
        catch (Exception ex)
        {
            throw new ArgumentException(
                string.Format(
                    "Could not remove value from enumerated type '{0}'.",
                    typeof(T).Name
                ), ex);
        }
    }

    public static string GetDescription<T>(this T enumerationValue)
        where T : struct
    {
        Type type = enumerationValue.GetType();
        if (!type.IsEnum)
        {
            throw new ArgumentException("EnumerationValue must be of Enum type", "enumerationValue");
        }

        //Tries to find a DescriptionAttribute for a potential friendly name
        //for the enum
        MemberInfo[] memberInfo = type.GetMember(enumerationValue.ToString());
        if (memberInfo != null && memberInfo.Length > 0)
        {
            object[] attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attrs != null && attrs.Length > 0)
            {
                //Pull out the description value
                return ((DescriptionAttribute) attrs[0]).Description;
            }
        }
        //If we have no description attribute, just return the ToString of the enum
        return enumerationValue.ToString();
    }


    public static void MoveItem<T>(this List<T> list, int currentIndex, int destIndex)
    {
        T item = list[currentIndex];
        list.RemoveAt(currentIndex);
        list.Insert(destIndex, item);
    }

    /// <summary>
    /// Deep Copies The Class
    /// </summary>
    /// <returns>Class Copy</returns>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    /// <summary>
    /// Gets the random enum from the type input
    /// </summary>
    /// <returns>The random enum.</returns>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static T GetRandomEnum<T>()
    {
        System.Array A = System.Enum.GetValues(typeof(T));
        T v = (T) A.GetValue(UnityEngine.Random.Range(0, A.Length));

        return v;
    }

    /// <summary>
    /// Sorts the given list
    /// </summary>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static void SortByName<T>(this List<T> list)
    {
        list.Sort((x, y) => string.Compare(x.ToString(), y.ToString()));
    }

    /// <summary>
    /// Determines if given collectables is equal to the compared
    /// </summary>
    public static bool IsEqualTo<T>(this IList<T> list, IList<T> other)
    {
        if (list.Count != other.Count)
            return false;
        for (int i = 0, count = list.Count; i < count; i++)
        {
            if (!list[i].Equals(other[i]))
            {
                return false;
            }
        }
        return true;
    }


    public static void Each<T>(this IList<T> ie, Action<T, int> action)
    {
        for (int i = ie.Count() - 1; i >= 0; i--)
        {
            action(ie[i], i);
        }
    }


    public static T PickRandom<T>(this IEnumerable<T> source)
    {
        return source.PickRandom(1).Single();
    }

    public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
    {
        return source.Shuffle().Take(count);
    }

    public static T ToEnum<T>(this string str)
    {
        return (T) Enum.Parse(typeof(T), str);
    }

    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
    {
        return source.OrderBy(x => Guid.NewGuid());
    }

    public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source, Action<T> action)
    {
        foreach (T element in source)
        {
            action(element);
        }
        return source;
    }

    public static IEnumerable<T> TakeLast<T>(this IEnumerable<T> source, int N)
    {
        return source.Skip(Math.Max(0, source.Count() - N));
    }

    public static void AssignRandomElementsFromArray<T>(T[] arrayToAssign, T[] baseArray)
    {
        List<T> baseList = baseArray.ToList();

        for (int i = 0; i < arrayToAssign.Length; ++i)
        {
            int randomIndex = UnityEngine.Random.Range(0, baseList.Count() - 1); // (min, max) both inclusive

            arrayToAssign[i] = baseList[randomIndex];

            baseList.RemoveAt(randomIndex);

            Debug.Log("randomly picked element from index " + randomIndex + " from array of length " + baseArray.Length);
        }
    }


    public static bool FindAndRemove<T>(this List<T> list, T A)
    {
        var itemToRemove = list.Find(x => x.Equals(A));
        if (itemToRemove != null)
        {
            list.Remove(itemToRemove);
            return true;
        }
        return false;
    }

    public static List<T> RepeatedDefault<T>(int count)
    {
        return Repeated(default(T), count);
    }

    public static List<T> Repeated<T>(T value, int count)
    {
        List<T> ret = new List<T>(count);
        ret.AddRange(Enumerable.Repeat(value, count));
        return ret;
    }

    public static void NonDuplicatedAdd<T>(this List<T> list, T item)
    {
        if (!list.Contains(item))
        {
            list.Add(item);
        }
    }


    public static void SafeAdd<T, Y>(this IDictionary<T, Y> dic, T key, Y val)
    {
        if (dic.ContainsKey(key))
            dic[key] = val;
        else
        {
            dic.Add(key, val);
        }
    }

    public static Y SafeRetrive<T, Y>(this IDictionary<T, Y> dic, T key)
    {
        return dic.ContainsKey(key) ? dic[key] : default(Y);
    }

    public static void SetActiveInteractables<T>(this IEnumerable<T> mEnumerable, bool isActive = true) where T : Selectable
    {
        foreach (var item in mEnumerable)
        {
            item.interactable = isActive;
        }
    }

    public static void SetActiveToggles(this IEnumerable<Toggle> mEnumerable, bool isActive = true, bool invokeCallBack = false)
    {
        foreach (var item in mEnumerable)
        {
            item.isOn = isActive;

            if (invokeCallBack) //if true, invoke the callback
                item.onValueChanged.Invoke(isActive);
        }
    }

    public static WWWForm GetWWWForm(this IDictionary<string, string> queries)
    {
        WWWForm form = new WWWForm();
        foreach (KeyValuePair<string, string> arg in queries)
        {
            form.AddField(arg.Key, arg.Value);
        }
        return form;
    }

    /// <summary>
    /// checks if the object is among the list provided, list items as params
    /// </summary>
    public static bool IsEither<T>(this T obj, params T[] collection)
    {
        return collection.Contains(obj);
    }

    /// <summary>
    /// checks if the object is among the list provided, input the list
    /// </summary>
    public static bool IsEither<T>(this T obj, ICollection<T> collection)
    {
        return collection.Contains(obj);
    }

    /// <summary>
    /// give a random element among the given set
    /// </summary>
    public static T RandomAmong<T>(params T[] elements)
    {
        var v = (T) elements.GetValue(UnityEngine.Random.Range(0, elements.Length));

        return v;
    }

    /// <summary>
    /// retrun the next or previous item from a list with respect to steps
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection"></param>
    /// <param name="startIndex"></param>
    /// <param name="step"></param>
    /// <returns></returns>
    public static T JumpBy<T>(this List<T> collection, int startIndex, int step)
    {
        var calcStep = startIndex + step;
        if (calcStep < 0)
        {
            calcStep = collection.Count - Mathf.Abs(calcStep);
        }
        else if (calcStep >= collection.Count)
        {
            calcStep = calcStep - collection.Count;
        }
        
        return collection[calcStep];
    }
}