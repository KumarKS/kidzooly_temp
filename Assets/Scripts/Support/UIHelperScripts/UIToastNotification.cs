﻿
using UnityEngine;
using UnityEngine.UI;

public class UIToastNotification : UIPanel
{
	private static bool isSpawnedAlready = false;
	private static UIToastNotification _instance = null;
	public static UIToastNotification Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = FindObjectOfType(typeof(UIToastNotification)) as UIToastNotification;
				if (_instance == null)
				{
					var go = Instantiate(Resources.Load("Toast"), Container.transform) as GameObject;
					_instance = go.GetComponent<UIToastNotification>();
					//_instance.GetCanvas.worldCamera = Camera.main;
					_instance.gameObject.name = _instance.GetType().Name;
				}

				_instance.transform.SetParent(Container.transform);
			}

			isSpawnedAlready = true;
			return _instance;
		}
		set
		{
			_instance = value;
		}


	}
	static GameObject _container;
	static GameObject Container
	{
		get
		{
			if (_container == null)
			{
				var container = GameObject.Find("ToastContainer");
				if (container == null)
				{
					container = new GameObject("ToastContainer");
					container.AddComponent<DontDestroyOnLoad>();
				}
				_container = container;
			}
			return _container;
		}
		set
		{
			_container = value;
		}

	}

	[SerializeField]
	Text _ToastText;

	public Text GetToastText
	{
		get
		{
			return _ToastText;
		}
	}
	int CriticalLayerSortingOrder = 1005;

	protected override void Awake()
	{

		if (isSpawnedAlready)
		{
			Debug.Log("Deleting duplicate");
			Destroy(gameObject);
		}

		// DontDestroyOnLoad(this);
		isSpawnedAlready = true;
		GetCanvas.sortingOrder = CriticalLayerSortingOrder;
		base.Awake();
	}

	public void TriggerToast(string text = null, float time = -1.0f)
	{
		KillToast();
		GetToastText.SetText(text);
		ActivatePanel();
		CancelInvoke();
		if (time > 0)
			Invoke("KillToast", time);
	}

	public void KillToast()
	{
		DeactivatePanel();
		GetToastText.SetText();
	}

	protected virtual void OnDestroy()
	{
		if (!isSpawnedAlready)
		{
			_instance = null;
		}
	}
	protected virtual void OnApplicationQuit()
	{
		_instance = null;
	}
	//void OnLevelWasLoaded(int level)
	//{
	//    Instance.GetCanvas.worldCamera = Camera.main;
	//}

}
