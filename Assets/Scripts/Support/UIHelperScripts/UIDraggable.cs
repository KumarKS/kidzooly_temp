﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIDraggable : MonoBehaviour, IPointerDownHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public static bool IsDragging = false;
    private Vector2 pointerOffset;
    private RectTransform canvasRectTransform;
    private RectTransform panelRectTransform;
    private Canvas canvas;
    private Vector3 offSet;

    void Awake()
    {
        canvas = transform.root.GetComponentInChildren<Canvas>();
        if (canvas != null)
        {
            canvasRectTransform = canvas.transform as RectTransform;
        }
    }

    public void OnPointerDown(PointerEventData data)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(transform as RectTransform, data.position, data.pressEventCamera, out pointerOffset);
    }

    public void OnDrag(PointerEventData data)
    {
        if (PinchZoom.isPinchZooming)
            return;

        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, data.position, canvas.worldCamera, out pos);
        var newPos = CheckBounds(canvas.transform.TransformPoint(pos) + offSet);
#if UNITY_EDITOR
        transform.position = newPos;
#else
                 if (Input.touchCount == 1)
                        transform.position = newPos;     
#endif

    }
    Vector2 CheckBounds(Vector2 pos)
    {
        var rt = GetComponent<RectTransform>();
        var xMax = transform.position.x - (rt.offsetMin.x / 100);
        var xMin = transform.position.x - (rt.offsetMax.x / 100);
        var yMax = transform.position.y - (rt.offsetMin.y / 100);
        var yMin = transform.position.y - (rt.offsetMax.y / 100);

        var xPos = pos.x;
        var yPos = pos.y;
        if (xPos > xMax)
            xPos = xMax;
        if (xPos < xMin)
            xPos = xMin;
        if (yPos > yMax)
            yPos = yMax;
        if (yPos < yMin)
            yPos = yMin;
        pos = new Vector2(xPos, yPos);

        return pos;


    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        IsDragging = true;
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, eventData.position, canvas.worldCamera, out pos);
        offSet = transform.position - canvas.transform.TransformPoint(pos);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        IsDragging = false;
    }
}
