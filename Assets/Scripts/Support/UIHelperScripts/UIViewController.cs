﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;


public enum UIType
{
	StackedView,
	NormalView,
	BaseView
}

public class UIViewController : GenericSingleton<UIViewController>
{
	public class ViewInformation
	{
		public UIType mType;
		public UIPanel mPanel;

		public ViewInformation()
		{

		}

		public ViewInformation(UIPanel panel, UIType type)
		{
			this.mType = type;
			this.mPanel = panel;
		}

		public override bool Equals(object otherObj)
		{
			var other = (ViewInformation)otherObj;
			if (other.mPanel == this.mPanel)
				return true;
			else
			{
				return false;
			}
		}

		public override int GetHashCode()
		{
			return mPanel.GetHashCode();
		}
	}

	public LinkedList<ViewInformation> mPanelStack = new LinkedList<ViewInformation>();
	public Action<bool> OnLandedOnLastPanel;

	public void Flush()
	{
		while (mPanelStack.Count != 0)
		{
			mPanelStack.Last().mPanel.DeactivatePanel();
			mPanelStack.RemoveLast();
		}
	}

	public void Activate(UIPanel panel, UIType type = UIType.NormalView, Action callBack = null)
	{
		if (mPanelStack.Count > 0 && panel.GetInstanceID() == mPanelStack.Last().mPanel.GetInstanceID())
        {
            return;

        }
			 //early exit if trying to activate the same panel again

		switch (type)
		{
			case UIType.NormalView:
				{
					OnLandedOnLastPanel.SafeInvoke(false);
					if (mPanelStack.Count > 0)
						mPanelStack.Last().mPanel.DeactivatePanel();
					break;
				}
			case UIType.StackedView:
				{
					OnLandedOnLastPanel.SafeInvoke(false);
					break;
				}
			case UIType.BaseView:
				{
					OnLandedOnLastPanel.SafeInvoke(true);
					break;
				}
		}
		if (mPanelStack.Contains(new ViewInformation(panel, type)))
		{
			mPanelStack.Remove(new ViewInformation(panel, type));
		}
		mPanelStack.AddLast(new ViewInformation(panel, type));

		panel.ActivatePanel();
		callBack.SafeInvoke();
	}

	public void ShowBaseView()
	{

		while (mPanelStack.Count > 0 && mPanelStack.Last().mType != UIType.BaseView)
		{
			mPanelStack.Last().mPanel.DeactivatePanel();
			mPanelStack.RemoveLast();
		}

		//action to fire on landed on base view
		OnLandedOnLastPanel.SafeInvoke(true);
	}

	public void Back()
	{
		if (mPanelStack.Count <= 0)
			return;

		var panelInfo = mPanelStack.Last();
		switch (panelInfo.mType)
		{
			case UIType.NormalView:
				{
					Debug.Log("Deactivated A Normal View");
					mPanelStack.Last().mPanel.DeactivatePanel();
					mPanelStack.RemoveLast();
					if (mPanelStack.Count > 0)
					{
						var peekPanel = mPanelStack.Last();//.mPanel.ActivatePanel();
						peekPanel.mPanel.ActivatePanel();

						if (peekPanel.mType == UIType.BaseView && mPanelStack.Count == 1)
						{
							OnLandedOnLastPanel.SafeInvoke(true);
						}
						else
						{
							OnLandedOnLastPanel.SafeInvoke(false);
						}
					}
					break;
				}
			case UIType.StackedView:
				{
					Debug.Log("Deactivated Stacked Panel");
					mPanelStack.Last().mPanel.DeactivatePanel();
					mPanelStack.RemoveLast();
					if (mPanelStack.Count == 1)
					{
						OnLandedOnLastPanel.SafeInvoke(true);
					}
					else
					{
						OnLandedOnLastPanel.SafeInvoke(false);
					}
					break;
				}
			case UIType.BaseView:
				{
					Debug.Log("Flushing All Views");
					Flush();
					break;
				}
		}
	}

}
