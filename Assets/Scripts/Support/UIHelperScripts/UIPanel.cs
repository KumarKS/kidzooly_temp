﻿using UnityEngine;
using System;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(CanvasGroup))]
public class UIPanel : MonoBehaviour
{
    public bool IsPanelActive;
    public bool DisableGameObjectOnActivation = false;
    public bool isToggle;
    public bool isButton;
    public bool isDirty = true;
    public bool isActiveOnStart = true;
    public bool CheckForVisiblity = false;
    public bool IsVisible = false;

    public float FadeInTime = 0.2f;
    public float FadeOutTime = 0.2f;

    public List<Button> BackButtons = new List<Button>(); //if there is more than 2 back buttons viz. back and cancel button
    public bool DisableOnBackButton = false; public List<UIPanel> ChildUIPanelCache = new List<UIPanel>();
    public Action OnBecomeVisible;
    public Action OnBecomeInvisible;
    [SerializeField]
    CanvasGroup _CanvasGroup;


    public CanvasGroup GetCanvasGroup
    {
        get
        {
            if (_CanvasGroup == null)
                _CanvasGroup = GetComponent<CanvasGroup>();
            if (_CanvasGroup == null)
                _CanvasGroup = gameObject.AddComponent<CanvasGroup>();

            return _CanvasGroup;
        }
    }

    public CanvasGroup SetCanvasGroup
    {
        set { _CanvasGroup = value; }
    }

    [SerializeField]
    RectTransform _RectTransform;

    public RectTransform GetRectTransform
    {
        get
        {
            if (_RectTransform == null)
                _RectTransform = GetComponent<RectTransform>();

            return _RectTransform;
        }
    }
    [SerializeField]
    RectTransform _ParentRectTransform;

    public RectTransform GetParentRectTransform
    {
        get
        {
            if (_ParentRectTransform == null)
                _ParentRectTransform = GetCanvas.GetComponent<RectTransform>();

            return _ParentRectTransform;
        }

    }
    public RectTransform SetParentRectTransform
    {

        set { _ParentRectTransform = value; }



    }

    public RectTransform SetRectTransform
    {
        set { _RectTransform = value; }
    }

    [SerializeField]
    Toggle _Toggle;

    public Toggle GetToggle
    {
        get
        {
            if (isToggle && _Toggle == null)
            {
                _Toggle = GetComponent<Toggle>();
                if (_Toggle == null)
                    _Toggle = gameObject.AddComponent<Toggle>();
                isToggle = true;
            }
            return _Toggle;
        }
    }

    public Toggle SetToggle
    {
        set { _Toggle = value; }
    }

    [SerializeField]
    Button _Button;

    public Button GetButton
    {
        get
        {
            if (isButton && _Button == null)
            {
                _Button = GetComponent<Button>();
                if (_Button == null)
                    _Button = gameObject.AddComponent<Button>();
                isButton = true;
            }
            return _Button;
        }
        set { _Button = value; }
    }

    public Button SetButton
    {
        set { _Button = value; }
    }

    public Action CallBackOnPanelActivate = null;
    public Action CallBackOnPanelDeactivate = null;

    [SerializeField]
    Canvas _Canvas;
    private Vector3 curPos;
    private Vector3 lastPos;
    private float prevAlpha;

    public Canvas GetCanvas
    {
        get
        {
            if (_Canvas == null)
            {
                _Canvas = transform.root.GetComponent<Canvas>();
                if (_Canvas == null)
                {
                    _Canvas = transform.root.GetComponentInChildren<Canvas>();
                }
            }

            return _Canvas;
        }
        set { _Canvas = value; }
    }

    public Canvas SetCanvas
    {
        set { _Canvas = value; }
    }

    protected virtual void Awake()
    {
        if (isActiveOnStart)
            ActivatePanel();
        else
        {
            GetCanvasGroup.alpha = 0;
            DeactivatePanel();
        }

        if (BackButtons.Any()) //if there should be more than 1 back button
        {
            BackButtons.ForEach(x =>
            {

                Resources.UnloadUnusedAssets();
                x.onClick.RemoveAllListeners();
                x.onClick.AddListener(PressBack);
            });
        }
        lastPos = transform.position;
        prevAlpha = GetCanvasGroup.alpha;
    }

    public virtual Tweener ShowAnimation()
    {
        return GetCanvasGroup.DOFade(1, FadeInTime).SetUpdate(UpdateType.Fixed, true);
    }

    public virtual Tweener HideAnimation()
    {
        return GetCanvasGroup.DOFade(0, FadeInTime).SetUpdate(UpdateType.Fixed, true);
    }

    virtual public void DeactivateControl()
    {
        GetCanvasGroup.interactable = false;
        GetCanvasGroup.blocksRaycasts = false;
        IsPanelActive = false;

        if (DisableGameObjectOnActivation)
            gameObject.Deactivate();
    }

    virtual public void ActivateControl()
    {
        GetCanvasGroup.interactable = true;
        GetCanvasGroup.blocksRaycasts = true;
        IsPanelActive = true;
    }

    protected void CacheUIPanels()
    {
        if (!isDirty)
            return;
        GetChildUIPanels(gameObject);
        ChildUIPanelCache.Clear();

        isDirty = false;
    }

    void GetChildUIPanels(GameObject go)
    {
        Transform t = go.transform;
        foreach (Transform child in t)
        {
            var childPanel = child.GetComponent<UIPanel>();
            if (childPanel != null)
            {
                GetChildUIPanels(child.gameObject);
                ChildUIPanelCache.Add(childPanel);

            }
        }
    }

    public virtual void DeactivatePanel(bool quick = false)
    {
        CacheUIPanels();
        DeactivateControl();
        if (quick)
        {
            GetCanvasGroup.alpha = 0f;
            CallBackOnPanelDeactivate.SafeInvoke();
            IsVisible = false;
        }
        else
        {
            DOTween.Kill(this.GetInstanceID() + "Activate");
            HideAnimation().OnComplete(() => { CallBackOnPanelDeactivate.SafeInvoke(); IsVisible = false; }).SetId(this.GetInstanceID() + "Deactivate");
        }
    }

    virtual public void ActivatePanel(bool quick = false)
    {
        DOTween.Kill(GetCanvasGroup);
        if (gameObject)
            gameObject.Activate();

        CacheUIPanels();

        if (quick)
        {
            GetCanvasGroup.alpha = 1f;
            CallBackOnPanelActivate.SafeInvoke();
            IsVisible = true;
            ActivateControl();
        }
        else
        {
            DOTween.Kill(this.GetInstanceID() + "Deactivate");
            ShowAnimation()
                .OnStart(() => CallBackOnPanelActivate.SafeInvoke())
                .OnComplete(() =>
                {
                    IsVisible = true;
                    ActivateControl();
                }).SetId(this.GetInstanceID() + "Activate");
        }
    }

    public virtual void OnVisible()
    {
    }

    public virtual void OnInvisible()
    {
    }

    public bool IsAlphaOn()
    {
        return GetCanvasGroup.alpha > 0;
    }

    void CheckVisibility()
    {
        var screenBounds = new Bounds(GetParentRectTransform.rect.center, GetParentRectTransform.rect.size);

        Bounds myBounds =
               RectTransformUtility.CalculateRelativeRectTransformBounds(GetParentRectTransform.transform,
                 transform);
        bool isOutOfBounds = myBounds.max.x < screenBounds.min.x || myBounds.min.x > screenBounds.max.x ||
                                                                 myBounds.min.y > screenBounds.max.y || myBounds.max.y < screenBounds.min.y;
        if (!isOutOfBounds && IsAlphaOn())
        {
            if (!IsVisible)
            {
                IsVisible = true;
                CacheUIPanels();
                ChildUIPanelCache.ForEach(x =>
                {
                    x.IsVisible = true;
                    x.OnBecomeVisible.SafeInvoke();
                    x.OnVisible();
                });
                OnVisible();
                OnBecomeVisible.SafeInvoke();
            }
        }
        else if (isOutOfBounds || !IsAlphaOn())
        {
            if (IsVisible)
            {
                IsVisible = false;
                CacheUIPanels();
                ChildUIPanelCache.ForEach(x =>
                {
                    x.IsVisible = false;
                    x.OnBecomeInvisible.SafeInvoke();
                    x.OnInvisible();
                });
                OnInvisible();
                OnBecomeInvisible.SafeInvoke();
            }
        }
    }

    bool IsMoving()
    {
        if (transform.position == lastPos)
        {
            return false;
        }
        else
        {
            lastPos = transform.position;
            return true;
        }

    }
    bool HasAlphaChanged()
    {
        if (GetCanvasGroup.alpha == prevAlpha)
        {
            return false;
        }
        else
        {
            prevAlpha = GetCanvasGroup.alpha;
            return true;
        }

    }
    protected virtual void Update()
    {
        if (CheckForVisiblity)
        {
            //   if (IsMoving() || HasAlphaChanged())
            CheckVisibility();
        }
    }

    //to set basic datas
    public virtual void SetBasicData(Dictionary<string, object> data)
    {
    }

    public virtual void PressBack()
    {
        if (UIViewController.Instance)
        {
            if (!DisableOnBackButton)
            {
                UIViewController.Instance.Back();
            }
            else
            {
                DeactivatePanel();
            }
        }
    }
}