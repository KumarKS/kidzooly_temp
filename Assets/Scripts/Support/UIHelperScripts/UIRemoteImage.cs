﻿using UnityEngine;
using System;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class UIRemoteImage : UIPanel
{
    string _imageUrl = string.Empty;
    public bool isLoadOnRecieveUrl = false;
    public bool isLoadOnVisible = true;
    public bool isPictureLoaded = false;
    public Action OnPictureLoaded;
	private bool isLocal = false;
	private Texture2D downloadedTex = null;

    RawImage rawImage;

    public RawImage DynamicImage
    {
        get
        {
            if (rawImage == null)
                rawImage = GetComponent<RawImage>();
            
            return rawImage;
        }
        set
        {
            rawImage = value;
        }

    }

	public void SetUrl(string url, bool isLocal = false)
	{
		this._imageUrl = url;
		this.isLocal = isLocal;

		if (isLoadOnRecieveUrl)
			LoadImage();
	}

    public override void OnVisible()
    {
        if (isLoadOnVisible)
            Invoke("LoadImage", 1.5f);

    }

	void LoadImage()
    {
		if (!string.IsNullOrEmpty(_imageUrl) && downloadedTex == null)
        {
			if (!isLocal)
			{
				FileManager.Instance.GetImage (urls: _imageUrl, onComplete: (texture) =>
				{
                    downloadedTex = texture[0];
                    OnPictureLoaded.SafeInvoke();
                    //if data length is less than 1k, not a valid texture
                    if (texture[0].GetRawTextureData().LongLength < 1000)
                    {
                        Debug.Log("Texture Size too small");
                    }
                    if (DynamicImage)
                        DynamicImage.texture = texture[0];
                }, owner: this, onFail: (error) =>
				{
					Debug.Log ("Fail to download : " + _imageUrl);
				});
               
            } 
			else
			{
                FileManager.Instance.GetLocalImage(urls: _imageUrl, onComplete: (texture) =>
                {
                	downloadedTex = texture[0];
                	OnPictureLoaded.SafeInvoke();
                	//if data length is less than 1k, not a valid texture
                	if (texture[0].GetRawTextureData().LongLength < 1000)
                	{
                		Debug.Log("Texture Size too small");
                	}
                	if(DynamicImage)
                		DynamicImage.texture = texture[0];
                }, onFail: (error) =>
                {
                	Debug.Log("Fail to download : " + _imageUrl);
                });

                
            }
            
        }

    }

    public override void OnInvisible()
    {
        if (!string.IsNullOrEmpty(_imageUrl))
        {
            CancelInvoke("LoadImage");
            FileManager.CancelDownload(_imageUrl);

        }

    }


    void OnDisable()
    {
        FileManager.RemoveReference(this);

		VGInternet.Instance.statusChanged -= OnInternetStatusChange;
    }

	void OnEnable()
	{
		VGInternet.Instance.statusChanged += OnInternetStatusChange;
	}

	void OnInternetStatusChange (bool newStatus)
	{
		if (newStatus) 
		{
			LoadImage ();
		}
	}
}
