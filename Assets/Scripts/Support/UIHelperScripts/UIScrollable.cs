﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(ScrollRect))]
public class UIScrollable : MonoBehaviour
{
    protected RectTransform _prefab;
    protected object _data;
    protected Action<object, int, GameObject> _functionToApply;
    protected int _totalGameobjects;
    protected List<RectTransform> _objList = new List<RectTransform>();

    protected ScrollRect _scrollRect;

    protected float _elementWidth;
    protected float _elementHeight;

    protected ScrollRect GetScrollRect
    {
        get
        {
            if (_scrollRect == null)
            {
                _scrollRect = GetComponent<ScrollRect>();
                if (_scrollRect == null)
                    _scrollRect = gameObject.AddComponent<ScrollRect>();
            }
            return _scrollRect;
        }
    }

    public UIScrollable SetPrefab(GameObject prefab)
    {
        _prefab = prefab.GetComponent<RectTransform>();
        return this;
    }

    public UIScrollable SetFunction(Action<object, int, GameObject> function)
    {
        _functionToApply = function;
        return this;
    }

    public UIScrollable UpdateData(object data)
    {
        SetData(data);
        Initialize();
        return this;
    }

    public UIScrollable SetData(object data)
    {
        this._data = data;
        this._totalGameobjects = ((ICollection)data).Count;
        
        return this;
    }

    public void ClearContent()
    {
        GetScrollRect.content.gameObject.DestroyChildren();
        _objList.Clear();
        GetScrollRect.StopMovement();
    }

    public void Refresh()
    {
        GetScrollRect.content.gameObject.GetFirstLevelChildren().Each((child, index) =>
        {
            var go = child;
            _functionToApply.SafeInvoke(_data, index, go);
        });

    }

	public List<RectTransform> GetContents()
    {
		return _objList;
    }

    public void ScrollToIndex(int index, float maxDuration = 3)
    {
        if (index < 0) return; //if index is less than 0, early exit

        GetScrollRect.StopMovement(); //stoping movement while scrolling

        if (DOTween.IsTweening(GetScrollRect.content))
            DOTween.Kill(GetScrollRect.content); //kill tween if already tweening
        float scrollPos = 0;
		float distance = 0;
		if (GetScrollRect.vertical)
        {
			scrollPos = (index + .5f) * _elementWidth - GetScrollRect.viewport.rect.height/2f;
			scrollPos = -1 * Mathf.Clamp(scrollPos, 0, GetScrollRect.content.sizeDelta.y - GetScrollRect.viewport.rect.height/2f);
			distance = Mathf.Abs (GetScrollRect.content.anchoredPosition.y - scrollPos);
			GetScrollRect.content.DOAnchorPosY(scrollPos, maxDuration * distance/GetScrollRect.content.sizeDelta.y).SetEase(Ease.InOutSine)
                .SetAutoKill(true);
        }
        else
        {
			scrollPos = (index + .5f) * _elementWidth - GetScrollRect.viewport.rect.width/2f;
			scrollPos = -1 * Mathf.Clamp(scrollPos, 0, GetScrollRect.content.sizeDelta.x - GetScrollRect.viewport.rect.width/2f);
			distance = Mathf.Abs (GetScrollRect.content.anchoredPosition.x - scrollPos);
			GetScrollRect.content.DOAnchorPosX(scrollPos, maxDuration * distance/GetScrollRect.content.sizeDelta.x).SetEase(Ease.InOutSine)
				.SetAutoKill(true);
        }
    }

	protected virtual void SetViewPortSize()
    {
		HorizontalOrVerticalLayoutGroup layoutGroup = GetScrollRect.content.GetComponent<HorizontalOrVerticalLayoutGroup> ();
        if (GetScrollRect.vertical)
        {
            GetScrollRect.content.SetAnchor(AnchorPresets.HorStretchTop);
            GetScrollRect.content.sizeDelta = new Vector2(GetScrollRect.viewport.sizeDelta.x, _elementHeight * _totalGameobjects);
        }

        else
        {
            GetScrollRect.content.SetAnchor(AnchorPresets.VertStretchLeft);
            GetScrollRect.content.sizeDelta = new Vector2(_elementWidth * _totalGameobjects , GetScrollRect.viewport.sizeDelta.y);
        }
		layoutGroup.childAlignment = TextAnchor.MiddleCenter;
    }

    protected virtual void DoNullChecks()
    {
        if (_prefab == null)
        {
            Debug.Log("Prefab not set");
            return;
        }

        if (GetScrollRect.content == null)
        {
            Debug.Log("container not set");
            return;
        }
        var layoutGrp = GetScrollRect.content.GetComponent<HorizontalOrVerticalLayoutGroup>();
        if (layoutGrp == null)
        {
            Debug.Log("Layout Group Not Set");
            return;
        }
        Destroy(GetScrollRect.content.GetComponent<ContentSizeFitter>());
    }
    public bool Initialize()
    {
        DoNullChecks();
        ClearContent();
        CalculateContentHolder();
		SetViewPortSize ();
        PopulateContent();
        return true;
    }

    protected virtual void CalculateContentHolder()
    {
		HorizontalOrVerticalLayoutGroup layoutGroup = GetScrollRect.content.GetComponent<HorizontalOrVerticalLayoutGroup>();
		Vector2 _spacing = Vector2.zero;
        if (GetScrollRect.vertical)
        {
			_spacing = new Vector2(0, layoutGroup.spacing);
        }

        else
        {
			_spacing = new Vector2(layoutGroup.spacing, 0);
        }

        //width of the each item of the scrollrect 
		_elementWidth = _prefab.rect.width + (GetScrollRect.vertical ? _spacing.y : _spacing.x);

        //height of the each item of the scrollrect 
		_elementHeight = _prefab.rect.height + (GetScrollRect.vertical ? _spacing.y : _spacing.x);
    }

    void PopulateContent()
    {
		for (int i = 0; i < _totalGameobjects; i++)
        {
            var go = GetScrollRect.content.gameObject.AddChild(_prefab.gameObject);
            _functionToApply.SafeInvoke(_data, i, go);
            go.name = i.ToString();
            _objList.Add(go.GetComponent<RectTransform>());
        }
    }
}