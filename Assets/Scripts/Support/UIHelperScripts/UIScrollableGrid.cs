﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(ScrollRect))]
public class UIScrollableGrid : UIScrollable
{
	private int _numberOfRows;
	private int _numberOfColumns;
	private int _gameObjectBuffer;

    GridLayoutGroup gridLayoutgroup;
    GridLayoutGroup GetGridLayoutGroup
    {
        get
        {
            if (gridLayoutgroup == null)
            {
                gridLayoutgroup = GetScrollRect.content.GetComponent<GridLayoutGroup>();
                if (gridLayoutgroup == null)
                    gridLayoutgroup = GetScrollRect.content.gameObject.AddComponent<GridLayoutGroup>();
            }
            return gridLayoutgroup;
        }
    }

    protected override void CalculateContentHolder()
    {
		Vector2 _spacing = Vector2.zero;
        _spacing = GetScrollRect.content.GetComponent<GridLayoutGroup>().spacing;

        _elementWidth = GetGridLayoutGroup.cellSize.x + _spacing.x;

        _elementHeight = GetGridLayoutGroup.cellSize.y + _spacing.y;

        var fWidth = GetScrollRect.content.rect.width;
        var fheight = GetScrollRect.content.rect.height;

        if (GetScrollRect.vertical)
        {
            _numberOfRows = Mathf.FloorToInt(fheight / _elementHeight);
            if (GetGridLayoutGroup.constraint == GridLayoutGroup.Constraint.FixedColumnCount)
                _numberOfColumns = GetGridLayoutGroup.constraintCount;
            else
                _numberOfColumns = Mathf.FloorToInt(fWidth / _elementWidth);
        }

        else
        {
            _numberOfColumns = Mathf.FloorToInt(fWidth / _elementWidth);
            if (GetGridLayoutGroup.constraint == GridLayoutGroup.Constraint.FixedRowCount)
                _numberOfRows = GetGridLayoutGroup.constraintCount;
            else
                _numberOfRows = Mathf.FloorToInt(fheight / _elementHeight);

        }
    }

	protected override void SetViewPortSize()
    {
        
        if (GetScrollRect.vertical)
        {
            GetScrollRect.content.SetAnchor(AnchorPresets.HorStretchTop);
            GetScrollRect.content.sizeDelta = new Vector2(GetScrollRect.content.sizeDelta.x, _elementHeight * Mathf.CeilToInt(((float)_totalGameobjects / _numberOfColumns)));
            GetScrollRect.content.GetComponent<GridLayoutGroup>().startAxis = GridLayoutGroup.Axis.Horizontal;
        }

        else
        {
            GetScrollRect.content.SetAnchor(AnchorPresets.VertStretchLeft);
            GetScrollRect.content.sizeDelta = new Vector2(_elementWidth * Mathf.CeilToInt(((float)_totalGameobjects / _numberOfRows)), GetScrollRect.content.sizeDelta.y);
            GetScrollRect.content.GetComponent<GridLayoutGroup>().startAxis = GridLayoutGroup.Axis.Vertical;
        }
    }

    protected override void DoNullChecks()
    {
        if (_prefab == null)
        {
            Debug.Log("Prefab not set");
            return;
        }

        if (GetScrollRect.content == null)
        {
            Debug.Log("container not set");
            return;
        }
        if (GetScrollRect.content == null)
        {
            Debug.Log("container not set");
            return;
        }
        var layoutGrp = GetScrollRect.content.GetComponent<GridLayoutGroup>();
        if (layoutGrp == null)
        {
            Debug.Log("Grid Layout Group Not Set");
            return;
        }
        Destroy(GetScrollRect.content.GetComponent<ContentSizeFitter>());
    }
}