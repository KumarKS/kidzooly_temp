﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using System.ComponentModel;

public enum EDialogBoxSizes
{
	[Description("200|250")]
	Small_Portait,
	[Description("150|175")]
	Medium_Portrait,
	[Description("100|25")]
	Large_Portrait,
	[Description("1000|150")]
	Small_Landscape,
	[Description("650|100")]
	Medium_Landscape,
	[Description("350|50")]
	Large_Landscape,
}


[RequireComponent(typeof(CanvasGroup))]
public class UIPopupBox : UIPanel
{
	private static UIPopupBox _instance = null;
	static GameObject _container;
	private static bool isSpawnedAlready = false;
	static GameObject Container
	{
		get
		{
			if (_container == null)
			{
				var container = GameObject.Find("PopUpContainer");
				if (container == null)
				{
					container = new GameObject("PopUpContainer");
					container.AddComponent<DontDestroyOnLoad>();
				}
				_container = container;
			}
			return _container;
		}
		set
		{
			_container = value;
		}

	}
	public static UIPopupBox Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = FindObjectOfType(typeof(UIPopupBox)) as UIPopupBox;
				if (_instance == null)
				{
					var go = Instantiate(Resources.Load("PopUpBox"), Container.transform) as GameObject;
					_instance = go.GetComponent<UIPopupBox>();
					// _instance.GetCanvas.worldCamera = Camera.main;
					_instance.gameObject.name = _instance.GetType().Name;
				}

				_instance.transform.SetParent(Container.transform);

			}

			isSpawnedAlready = true;
			return _instance;
		}
		set
		{
			_instance = value;
		}


	}

	Action OnClickYes;
	Action OnClickNo;
	Action OnClickOk;
	Action OnClickClose;

	[Header("PopupBox UI")]
	public GameObject OKButton;
	public GameObject YesButton;
	public GameObject NoButton;
	public GameObject CloseButton;

	public Image ImageAnnouncement;
	public Text Description;

	public RectTransform ContainerPanel;
	int CriticalLayerSortingOrder = 1001;

	protected override void Awake()
	{

		if (isSpawnedAlready)
		{
			Debug.Log("Deleting duplicate");
			Destroy(gameObject);
		}

		//DontDestroyOnLoad(this);
		isSpawnedAlready = true;
		GetCanvas.sortingOrder = CriticalLayerSortingOrder;
		base.Awake();
	}
	public void SetDataYesNo(string displayText, Action yesImplementation, Action noImplementation, EDialogBoxSizes DSize = EDialogBoxSizes.Medium_Landscape)
	{
		ResetFormat();
		Description.SetText(displayText);
		OnClickYes = yesImplementation;
		OnClickNo = noImplementation;
		OnClickOk = null;
		OnClickClose = null;

		if (YesButton != null)
			YesButton.Activate();
		if (NoButton != null)
			NoButton.Activate();
		if (OKButton != null)
			OKButton.Deactivate();
		if (CloseButton != null)
			CloseButton.Deactivate();

		Description.Activate();
		ImageAnnouncement.Deactivate();

		AdjustDialogSize(DSize);

		ActivatePanel();
	}

	public void SetDataOk(string displayText, Action OkImplementation, EDialogBoxSizes DSize = EDialogBoxSizes.Medium_Landscape)
	{
		ResetFormat();
		Description.SetText(displayText);
		OnClickOk = OkImplementation;
		OnClickYes = null;
		OnClickNo = null;
		OnClickClose = null;

		if (YesButton != null)
			YesButton.Deactivate();
		if (NoButton != null)
			NoButton.Deactivate();
		if (OKButton != null)
			OKButton.Activate();
		if (CloseButton != null)
			CloseButton.Deactivate();

		Description.Activate();
		ImageAnnouncement.Deactivate();

		AdjustDialogSize(DSize);

		ActivatePanel();
	}

	public void SetDataImageAnnouncement(Sprite toDisplay, Action closeImplementation, EDialogBoxSizes DSize = EDialogBoxSizes.Medium_Landscape)
	{
		if (toDisplay == null) return;

		ImageAnnouncement.sprite = toDisplay;

		OnClickClose = closeImplementation;
		OnClickOk = null;
		OnClickYes = null;
		OnClickNo = null;

		if (YesButton != null)
			YesButton.Deactivate();
		if (NoButton != null)
			NoButton.Deactivate();
		if (OKButton != null)
			OKButton.Deactivate();
		if (CloseButton != null)
			CloseButton.Activate();

		Description.Deactivate();
		ImageAnnouncement.Activate();

		AdjustDialogSize(DSize);

		ActivatePanel();
	}

	public void SetImageProperties(Sprite imageSprite = null, Color? newColor = null)
	{
		ImageAnnouncement.Activate();
		ImageAnnouncement.sprite = imageSprite;
		ImageAnnouncement.DOColor(newColor ?? Color.white, 0.5f);
	}

	public void SetTextProperties(TextAnchor alignment = TextAnchor.MiddleCenter, FontStyle style = FontStyle.Normal, Color? newColor = null)
	{
		Description.alignment = alignment;
		Description.fontStyle = style;
		Description.color = newColor ?? Color.black;
		//Description.alignment;
	}

	void ResetFormat()
	{
		SetTextProperties();
	}



	public void OnClose()
	{
		CallBackOnPanelDeactivate += () =>
		{
			OnClickClose.SafeInvoke();
			CallBackOnPanelDeactivate = null;
		};
		DeactivatePanel();

	}

	public void OnYes()
	{
		CallBackOnPanelDeactivate += () =>
		{
			OnClickYes.SafeInvoke();
			CallBackOnPanelDeactivate = null;
		};
		DeactivatePanel();

	}

	public void OnNo()
	{
		CallBackOnPanelDeactivate += () =>
		{
			OnClickNo.SafeInvoke();
			CallBackOnPanelDeactivate = null;
		};
		DeactivatePanel();

	}

	public void OnOk()
	{
		CallBackOnPanelDeactivate += () =>
		{
			OnClickOk.SafeInvoke();
			CallBackOnPanelDeactivate = null;
		};
		DeactivatePanel();

	}

	void AdjustDialogSize(EDialogBoxSizes DSize)
	{
        //for now - >giving issues in 4:3 koz of hardcoded numbers
	    return;

		var desc = DSize.GetDescription().Split('|', (char)StringSplitOptions.RemoveEmptyEntries);
		var sizeVec = new Vector2(desc[0].ToInt(), desc[1].ToInt());

		//can change to percentage based, if problems occured 

		ContainerPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, GetCanvas.GetComponent<RectTransform>().sizeDelta.x - sizeVec.x);
		ContainerPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, GetCanvas.GetComponent<RectTransform>().sizeDelta.y - sizeVec.y);

	}

	//void OnLevelWasLoaded(int level)
	//{
	//    Instance.GetCanvas.worldCamera = Camera.main;
	//}
}
