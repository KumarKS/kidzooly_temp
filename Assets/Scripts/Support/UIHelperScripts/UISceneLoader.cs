﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class UISceneLoader : UIPanel
{
    #region Singleton

    protected static bool IsSpawnedAlready = false;
    static GameObject _container;

    protected static GameObject Container
    {
        get
        {
            if (_container == null)
            {
                var container = GameObject.Find("UISceneLoaderContainer");
                if (container == null)
                {
                    container = new GameObject("UISceneLoaderContainer");
                    container.AddComponent<DontDestroyOnLoad>();
                }
                _container = container;
            }
            return _container;
        }
        set { _container = value; }
    }

    protected static UISceneLoader _instance = null;

    public static UISceneLoader Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(UISceneLoader)) as UISceneLoader;
                if (_instance == null)
                {
                    var go = Instantiate(Resources.Load("UISceneLoader"), Container.transform) as GameObject;
                    _instance = go.GetComponent<UISceneLoader>();
                    //  _instance.GetCanvas.worldCamera = Camera.main;
                    _instance.gameObject.name = _instance.GetType().Name;
                }

                _instance.transform.SetParent(Container.transform);
            }

            IsSpawnedAlready = true;
            return _instance;
        }
        set { _instance = value; }
    }

    int CriticalLayerSortingOrder = 2000;

    #endregion


    bool isLoaderOn = false;
    int _LoaderTriggerCount;
    Sequence _rotationSeq;

    [SerializeField]
    protected float AngleIncreaseFactor = -45f;

    [SerializeField]
    protected float DelayFactor = 0.1f;

    [SerializeField]
    protected Image Loader;

    protected string PreviousSceneName;

    private bool isLoadingInProgress = false;

    protected override void Awake()
    {
        if (IsSpawnedAlready)
        {
            Debug.Log("Deleting duplicate");
            Destroy(gameObject);
        }

        if (transform.root == transform) //only if this is the root transform, else it gives a runtime warning
            DontDestroyOnLoad(this);

        IsSpawnedAlready = true;
        InitilizeLoader();
        GetCanvas.sortingOrder = CriticalLayerSortingOrder;

        SceneManager.sceneLoaded += SceneManager_sceneLoaded;

        base.Awake();
    }

    void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        isLoadingInProgress = false;
    }

    protected virtual void InitilizeLoader()
    {
        GetCanvas.sortingOrder = CriticalLayerSortingOrder;

        _rotationSeq = DOTween.Sequence();
        _rotationSeq.Append(Loader.transform.DOLocalRotate(new Vector3(0, 0, -AngleIncreaseFactor), 0f, RotateMode.FastBeyond360).SetEase(Ease.Flash).SetUpdate(UpdateType.Fixed))
            .SetDelay(DelayFactor)
            .SetLoops(-1, LoopType.Incremental);

        _rotationSeq.Pause(); //small hack to properly display the loading indicator
    }

    void StopLoader(bool forceStop = false)
    {
        _LoaderTriggerCount--;
        _LoaderTriggerCount = Mathf.Clamp(_LoaderTriggerCount, 0, _LoaderTriggerCount);

        //Debug.Log("Loader Count: " + _LoaderTriggerCount);

        if (_LoaderTriggerCount > 0 && forceStop == false)
        {
            return;
        }

        isLoaderOn = false;
        _rotationSeq.Pause();

        DeactivatePanel();
    }

    void StartLoader()
    {
        _LoaderTriggerCount++;

        if (isLoaderOn)
        {
            return;
        }

        if (GetCanvas)
        {
            GetCanvas.sortingOrder = CriticalLayerSortingOrder;
        }

        isLoaderOn = true;
        ActivatePanel();

        _rotationSeq.Restart(true);
    }


    public void LoadScene(string sceneName, bool showLoader = true)
    {
        if (isLoadingInProgress)
        {
            return;
        }
        isLoadingInProgress = true;
        if (sceneName == EScenes.Splash.GetDescription())
            Destroy(GameObject.Find("PersistantSingletons"));
        if (showLoader)
            StartLoader();
        if (ParentalLockSystem.isSpawnedAlready)
            ParentalLockSystem.Instance.DeactivatePanel(true);
        PreviousSceneName = SceneManager.GetActiveScene().name;
        StartCoroutine(LoadSceneInternal(sceneName));
    }

    public void LoadSceneIndex(int index)
    {
        StartLoader();
        StartCoroutine(LoadSceneInternal(SceneManager.GetSceneByBuildIndex(index).name));
    }

    public void LoadPreviousScene()
    {
        LoadScene(PreviousSceneName);
    }

    IEnumerator LoadSceneInternal(string sceneName)
    {
        yield return new WaitForSeconds(0.5f);
        var scene = SceneManager.LoadSceneAsync(sceneName);
        yield return scene;
        yield return new WaitForEndOfFrame();
        StopLoader();
    }

    protected virtual void OnDestroy()
    {
        if (!IsSpawnedAlready)
        {
            _instance = null;
        }
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

    protected virtual void OnApplicationQuit()
    {
        _instance = null;
    }
}