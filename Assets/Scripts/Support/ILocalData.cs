﻿using UnityEngine;
using System.Collections;

public interface ILocalData<T>
{
    void Save();

    T Load();

    T Create();

    bool IsSaveAvailable();

}
