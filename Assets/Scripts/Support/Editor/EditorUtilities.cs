﻿#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif
using UnityEngine;

public class EditorUtilities
{
#if UNITY_EDITOR

    [MenuItem("Tools/Clear Cache")]
    private static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        Empty(new DirectoryInfo(Utilities.SavePath));
    }

    public static void Empty(DirectoryInfo directory)
    {
        if (directory.Exists)
        {
            foreach (System.IO.FileInfo file in directory.GetFiles())
                file.Delete();

            foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories())
                subDirectory.Delete(true);
        }
    }

    [MenuItem("Tools/Game Settings")]
    private static void OpenGameSettings()
    {
        Selection.activeObject = GameSettings.GetInstance;
    }
    [MenuItem("Tools/Open Save Folder")]
    private static void ShowExplorer()
    {
        var itemPath = Utilities.SavePath.Replace(@"/", @"\");
        System.Diagnostics.Process.Start("explorer.exe", "/select," + itemPath);
    }
    [MenuItem("Tools/Load Keystore")]
    private static void LoadKeystore()
    {
        PlayerSettings.Android.keystorePass = "VGminds123";
        PlayerSettings.Android.keyaliasName = "kidzooly";
        PlayerSettings.Android.keyaliasPass = "VGminds123";
    }
#endif
}