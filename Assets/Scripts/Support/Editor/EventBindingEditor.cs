﻿using UnityEngine;

#if UNITY_EDITOR

using UnityAssets;
using UnityEditor;

using System;
//using System.Text;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

//custom editor for the Event binding
[CustomEditor(typeof(EventBinding))]
public class EventBindingEditor : _PropertyEditorSupport
{

    //reference to the original script
    EventBinding binder;

    //override calling input
   // bool overrideData = false;


    //initialize	
    protected override void Initialize()
    {
        binder = target as EventBinding;
    }


    //drawing the GUI
    public override void OnInspectorGUI()
    {


        BeginEdit();

        BeginSection("Data Source");
        if (binder.DataSource == null)
        {

            binder.DataSource = new ComponentMemberInfo()
            {
                Component = getDefaultComponent(binder.gameObject)
            };

        }

        var dataSource = binder.DataSource;

        //not taking any defaults
        /*if (dataSource.Component == null) {
                dataSource.Component = getDefaultComponent (binder.gameObject);
        }*/

        var sourceComponent = dfEditorUtil.ComponentField("Component", dataSource.Component);
        if (sourceComponent != dataSource.Component)
        {
            //dfEditorUtil.MarkUndo (binder, "Assign DataSource Component");
            dataSource.Component = sourceComponent;
        }

        if (sourceComponent == null)
        {
            EditorGUILayout.HelpBox("Missing component", MessageType.Error);
            return;
        }

        var sourceComponentMembers = getEventList(sourceComponent).Select(m => m.Name).ToArray();

        var memberIndex = findIndex(sourceComponentMembers, dataSource.MemberName);

        // If there is no event aready selected, attempt to select the Click event by default
        if (memberIndex == -1)
        {
            memberIndex = Mathf.Max(0, findIndex(sourceComponentMembers, "Click"));
        }

        var selectedIndex = EditorGUILayout.Popup("Event", memberIndex, sourceComponentMembers);
        if (selectedIndex >= 0 && selectedIndex < sourceComponentMembers.Length)
        {

            var memberName = sourceComponentMembers[selectedIndex];
            if (memberName != dataSource.MemberName)
            {
                //dfEditorUtil.MarkUndo (binder, "Assign DataSource Member");
                dataSource.MemberName = memberName;
            }

            //showSignatureButton (sourceComponent.GetType (), memberName);

        }

        if (!binder.DataSource.IsValid)
        {
            EditorGUILayout.HelpBox("Data source configuration is invalid", MessageType.Error);
            return;
        }

        // Do not proceed if the source binding is invalid
        var handlerType = binder.DataSource.GetMemberType();
        if (handlerType == null)
        {
            binder.DataTarget = new ComponentMemberInfo();
            return;
        }

        EndSection();

        BeginSection("Event Handler");

        var dataTarget = binder.DataTarget;
        if (dataTarget == null)
        {
            dataTarget = binder.DataTarget = new ComponentMemberInfo()
            {
                Component = getDefaultComponent(binder.gameObject)
            };
        }

        //not taking any defaults
        /*if (dataTarget.Component == null) {
                dataTarget.Component = getDefaultComponent (binder.gameObject);
        }*/

        var targetComponent = dfEditorUtil.ComponentField("Component", dataTarget.Component);
        if (targetComponent != dataTarget.Component)
        {
            dfEditorUtil.MarkUndo(binder, "Assign DataSource Component");
            dataTarget.Component = targetComponent;
        }

        if (targetComponent == null)
        {
            EditorGUILayout.HelpBox("Missing component", MessageType.Error);
            return;
        }

        var targetComponentMembers = getEventHandlers(targetComponent.GetType(), handlerType).Select(m => m.Name).ToArray();

        if (targetComponentMembers.Length == 0)
        {
            EditorGUILayout.HelpBox("Class " + targetComponent.GetType().Name + " does not define any compatible event handlers", MessageType.Error);
        }
        else
        {

            var memberIndex_event = Mathf.Max(0, findIndex(targetComponentMembers, dataTarget.MemberName));
            var selectedIndex_event = EditorGUILayout.Popup("Event Handler", memberIndex_event, targetComponentMembers);
            if (selectedIndex_event >= 0 && selectedIndex_event < targetComponentMembers.Length)
            {
                var memberName = targetComponentMembers[selectedIndex_event];
                if (memberName != dataTarget.MemberName)
                {
                    dfEditorUtil.MarkUndo(binder, "Assign DataSource Member");
                    dataTarget.MemberName = memberName;
                }
            }

        }

        EndSection();


        //auto binding section -- using alternative spacing style
        using (dfEditorUtil.BeginGroup("Automatic Binding"))
        {

            var autoBind = EditorGUILayout.Toggle("Auto Bind", binder.AutoBind);
            if (autoBind != binder.AutoBind)
            {
                dfEditorUtil.MarkUndo(binder, "Toggle AutoBind property");
                binder.AutoBind = autoBind;
            }

            var autoUnbind = EditorGUILayout.Toggle("Auto Unbind", binder.AutoUnbind);
            if (autoUnbind != binder.AutoUnbind)
            {
                dfEditorUtil.MarkUndo(binder, "Toggle AutoUnbind property");
                binder.AutoUnbind = autoUnbind;
            }

        }



        EndEdit();
    }

    private Component getDefaultComponent(GameObject gameObject)
    {

        /*var control = gameObject.GetComponent<MonoBehaviour> ();
        if (control != null)
                return control;*/

        var defaultComponent = gameObject.GetComponents<MonoBehaviour>().Where(c => c != target).FirstOrDefault();

        return defaultComponent;

    }

    private MethodInfo[] getEventHandlers(Type componentType, Type eventHandlerType)
    {

        var invoke = eventHandlerType.GetMethod("Invoke");
        if (invoke == null)
        {
            Debug.LogError("Could not retrieve event signature for " + eventHandlerType.Name);
            return new MethodInfo[0];
        }

        var delegateParams = invoke.GetParameters();

        var methods = componentType.GetMethods(BindingFlags.Public | BindingFlags.Instance)
         .Where(m =>
          !m.IsSpecialName &&
               !m.IsGenericMethod &&
               !m.IsAbstract &&
               !m.IsConstructor &&
               !m.IsDefined(typeof(HideInInspector), true) &&
               (m.ReturnType == typeof(void) || typeof(IEnumerator).IsAssignableFrom(m.ReturnType)) &&
               m.DeclaringType != typeof(MonoBehaviour) &&
               m.DeclaringType != typeof(Behaviour) &&
               m.DeclaringType != typeof(Component) &&
               m.DeclaringType != typeof(UnityEngine.Object) &&
               m.DeclaringType != typeof(System.Object) &&
               signatureIsCompatible(delegateParams, m.GetParameters())
        )
    .OrderBy(m => m.Name)
    .ToArray();

        return methods;

    }

    private EventInfo[] getEventList(Component component)
    {

        var list =
component.GetType()
    .GetEvents()
    .Where(p => typeof(Delegate).IsAssignableFrom(p.EventHandlerType))
    .OrderBy(p => p.Name)
    .ToArray();

        return list;

    }

    private int findIndex(string[] list, string value)
    {

        for (int i = 0; i < list.Length; i++)
        {
            if (list[i] == value)
                return i;
        }

        return -1;

    }

    private bool signatureIsCompatible(ParameterInfo[] lhs, ParameterInfo[] rhs)
    {
        return signatureIsCompatible(lhs, rhs, true);
    }

    /// <summary>
    /// Determines whether the dfEventBinding component class defines a 
    /// matching "proxy" method to forward event notifications
    /// </summary>
    /// <param name="lhs"></param>
    /// <returns></returns>
    private bool compatibleProxyMethodFound(ParameterInfo[] lhs)
    {

        var proxyMethod = typeof(EventBinding)
.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
    .Where(m =>
          m.IsDefined(typeof(EventProxyAttribute), true) &&
               signatureIsCompatible(lhs, m.GetParameters(), false)
        )
    .FirstOrDefault();

        return proxyMethod != null;

    }

    private bool signatureIsCompatible(ParameterInfo[] lhs, ParameterInfo[] rhs, bool allowProxy)
    {

        if (lhs == null || rhs == null)
            return false;

        // HACK: Allow for "notification handlers" - Event handlers that don't care
        // about event parameters, they just need to be invoked when the event fires
        if (lhs.Length > 0 && rhs.Length == 0)
        {
            if (allowProxy)
                return compatibleProxyMethodFound(lhs);
            else
                return false;
        }

        if (lhs.Length != rhs.Length)
            return false;

        for (int i = 0; i < lhs.Length; i++)
        {
            if (!areTypesCompatible(lhs[i], rhs[i]))
                return false;
        }

        return true;

    }

    private bool areTypesCompatible(ParameterInfo lhs, ParameterInfo rhs)
    {

        if (lhs.ParameterType.Equals(rhs.ParameterType))
            return true;

        if (lhs.ParameterType.IsAssignableFrom(rhs.ParameterType))
            return true;

        return false;

    }




}

#endif
