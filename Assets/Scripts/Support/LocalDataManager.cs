﻿#if !AMAZON_STORE
using Firebase.Analytics;
#endif
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[Serializable]
public class LocalSaveData
{
    public Dictionary<AudioController.EAudioLayer, bool> AudioLayerToggle = new Dictionary<AudioController.EAudioLayer, bool>() { { AudioController.EAudioLayer.Music, true }, { AudioController.EAudioLayer.Sound, true } };
    public bool VibrationSetting = true;

    
    private bool _IsPremiumUser = false;
    public bool AllowDownload = true;
    public bool Autoplay = true;
    public string CurrentLanguage = GameConstants.DEFAULT_LANGUAGE;
    public GameData.GameConfigData MainConfigData = new GameData.GameConfigData();

    public Dictionary<string, string> SubscriptionDetails = new Dictionary<string, string>();
   
    public float ConfigVersion = 0;
    public bool IsPremiumUser()
    {

       
#if UNITY_EDITOR
        return GameSettings.GetInstance.EditorPremium;
#else
     if (RemoteToggles.SERVER_SIDE_VALIDATION)
      {
        return SubscriptionDetails.ContainsKey("Title") && SubscriptionDetails.ContainsKey("Date") && LocalDataManager.Instance.stat;
      }
      else
      {
        return SubscriptionDetails.ContainsKey("Title") && SubscriptionDetails.ContainsKey("Date");
      }
#endif
    }

    
    public void LogEventWithParams(string eventCategory, string eventAction, string eventLabel)
    {
    #if !AMAZON_STORE
        var param = new Parameter(eventAction, eventLabel);
        Firebase.Analytics.FirebaseAnalytics.LogEvent(eventCategory, param);
    #endif
    }
    #region VideoPlayerSystem

    [SerializeField]
    public FavoriteVideoData FavoriteVideos = new FavoriteVideoData();

    #endregion

    #region Feedback

    [SerializeField]
    public FeedbackStatusData FeedbackData = new FeedbackStatusData();

    #endregion

	#region Session

	public int GetCurrentSession { get { return PlayerPrefs.GetInt ("SessionCount", 0); }}

	public void IncrementCurrentSession()
	{
		PlayerPrefs.SetInt("SessionCount", GetCurrentSession + 1);
		PlayerPrefs.Save ();
	}

    public int ShowInterestialAd { get { return PlayerPrefs.GetInt("AdCount", 0); } }

    public void IncrementCurrentActivitySession()
    {
        Debug.Log("IncrementCurrentActivitySession" + PlayerPrefs.GetInt("AdCount", ShowInterestialAd + 1));
        PlayerPrefs.SetInt("AdCount", ShowInterestialAd + 1);
        PlayerPrefs.Save();
    }

    #endregion
}

public class LocalDataManager : GenericSingleton<LocalDataManager>
{
    public LocalSaveData SaveData;
    public bool stat = false;
    enum SUBSCRIPTIONSTATUS : short
    {
        SUBSCRIPTION_RECOVERED = 1,
        SUBSCRIPTION_RENEWED = 2,
        SUBSCRIPTION_CANCELED = 3,
        SUBSCRIPTION_PURCHASED = 4,
        SUBSCRIPTION_ON_HOLD = 5,
        SUBSCRIPTION_IN_GRACE_PERIOD = 6,
        SUBSCRIPTION_RESTARTED = 7,
        SUBSCRIPTION_PRICE_CHANGE_CONFIRMED = 8,
        SUBSCRIPTION_DEFERRED = 9,
        SUBSCRIPTION_PAUSED = 10,
        SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED = 11,
        SUBSCRIPTION_REVOKED = 12,
        SUBSCRIPTION_EXPIRED = 13,
    }

    readonly List<SUBSCRIPTIONSTATUS> SubscriptionStartStatus = new List<SUBSCRIPTIONSTATUS>
    {
        SUBSCRIPTIONSTATUS.SUBSCRIPTION_PURCHASED,
        SUBSCRIPTIONSTATUS.SUBSCRIPTION_RENEWED,
        SUBSCRIPTIONSTATUS.SUBSCRIPTION_RESTARTED,
        SUBSCRIPTIONSTATUS.SUBSCRIPTION_RECOVERED,
    };
    Dictionary<EConfigFileName, object> Configs = new Dictionary<EConfigFileName, object>();

    void Start()
    {
        //incrementing the number of app sessions the moment local data manager is loaded 
        SaveData.FeedbackData.IncrementAppSession();


        //right now directly setting the screen to prevent diming,
        //later we can do it more eligently, only not to dim if a video is playing else we can dim.
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        RemoteToggles.Initialize();
        //string userID = PlayerPrefs.GetString("userID");
        //if (!String.IsNullOrEmpty(userID))
        //{
        //    CheckSubscriptionStatus();
        //    Debug.LogError("In Start Of LocalDataManager");
        //}


    }
    public void CheckClientAndServerStatus()
    {
        string transactionID = SaveData.SubscriptionDetails.SafeRetrive("TransactionID");
        if ((SaveData.SubscriptionDetails.ContainsKey("Title") && SaveData.SubscriptionDetails.ContainsKey("Date")) != LocalDataManager.Instance.stat)
        {
            SaveData.LogEventWithParams("Is_reciept_valid", "istrueorfalse", transactionID);
        }
    }
    public void CheckSubscriptionStatus()
    {
        stat = false;
#if !AMAZON_STORE
        FirebaseAPI.Instance.CheckSubscriptionStatus(
            (success) =>
            {
                if (int.TryParse(success, out int result))
                {
                    stat = SubscriptionStartStatus.Contains((SUBSCRIPTIONSTATUS)result);
                }
                else
                {
                    stat = false;
                    Debug.Log("No uid Key Found or Empty Dictionary");
                }
            }, 
            (error)=>
            {
                Debug.Log(error);
            });
#endif
    }
    #region ILocalData implementation

    void OnEnable()
    {
        SaveData = Load() ?? Create();
    }

    void OnDisable()
    {
        Save();
    }

    
    void OnApplicationPause(bool isPaused)
    {
		if (isPaused)
		{
			VGMinds.Analytics.AnalyticsManager.LogNumberOfVideosPlayedSession ();
			Save();
		}
          
    }

    public void Save()
    {
        
        JsonConvert.SerializeObject(SaveData).SaveJson(GameConstants.SAVE_DATA_KEY);
        Debug.Log("Save Successful");
    }

    public LocalSaveData Load()
    {
        var data = Utilities.LoadJsonData<LocalSaveData>(GameConstants.SAVE_DATA_KEY);
        
        
        if (data == default(LocalSaveData))
        {
            
            return null;
        }
        else
        {
            
            return data;
        }
            
    }

    public LocalSaveData Create()
    {
        SaveData = new LocalSaveData();
        Debug.Log("Created New Save Data");
        return SaveData;
    }

    #endregion

    public T GetConfig<T>(EConfigFileName configName)
    {
        try
        {
            return JsonConvert.DeserializeObject<T>(GetConfigJson(configName));
        }
        catch
        {
            return default(T);
        }
    }

	public T GetLocalConfig<T>(EConfigFileName configName)
	{
		var path = configName.GetDescription();
		TextAsset asset = Resources.Load<TextAsset>(path);
		return JsonConvert.DeserializeObject<T>(asset.text);
	}

    public string GetConfigJson(EConfigFileName ConfigName)
    {
        if (ConfigName == EConfigFileName.MainConfig)
        {
            var fieldValue = EEndPoints.ConfigEndPoint.GetDescription();
            if (FileManager.IsFileDownloaded(fieldValue))
                return Utilities.LoadJsonData(fieldValue.GenerateUniqueId(), FileManager.DataSaveType,
                   Path.Combine(Utilities.SavePath, FileManager.GetFileType(fieldValue).ToString()), "." + FileManager.GetFileFormat(fieldValue));
        }
        else
        {
            var field = typeof(GameData.GameConfigData)
                .GetFields().ToList().Find(x =>
                    x.Name == ConfigName.GetDescription());

            if (field != null)
            {
                var fieldValue = field.GetValue(SaveData.MainConfigData).ToString();
                if (FileManager.IsFileDownloaded(fieldValue))
                    return Utilities.LoadJsonData(fieldValue.GenerateUniqueId(), FileManager.DataSaveType,
                        Path.Combine(Utilities.SavePath, FileManager.GetFileType(fieldValue).ToString()), "." + FileManager.GetFileFormat(fieldValue));
            }
        }
        return string.Empty;
    }

    protected override bool IsPersistant
    {
        get { return true; }
    }
}