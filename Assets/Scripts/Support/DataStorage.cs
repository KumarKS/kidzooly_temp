﻿    using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Newtonsoft.Json.Linq;

[Serializable]
public class DataStorage
{

    public string DatabaseName;

   
    public string CollectionName;


    public List<JObject> JsonList = new List<JObject>();

    public DataStorage()
    {
        DatabaseName = "";
        CollectionName = "";
        JsonList = new List<JObject>();
    }
}
