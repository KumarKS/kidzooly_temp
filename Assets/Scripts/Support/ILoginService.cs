﻿using System;
using System.Collections.Generic;


public interface ILoginService
{

    void Login(Action<DataStorage> OnSuccess, Action<Exception> OnFailure);

}

