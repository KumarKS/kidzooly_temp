﻿
using UnityEngine;
using vgm.ads;
public class InteractiveRhymeController : MonoBehaviour
{

    public GameObject[] Buttons;
    public AudioController AudController;
    public InteractiveRhymesUIElement InteractiveRhyme;

    public int LockButtonsAfter;

    void Start()
    {


    }

    void OnEnable()
    {
        HardwareInputManager.OnBack += MainMenu;
        AdsCheckManager.Instance.ShowBannerIfAllowed();
    }

    void OnDisable()
    {
        HardwareInputManager.OnBack = null;
        AdsCheckManager.Instance.CloseBanner();

    }

    public void OnClick(InteractiveRhymesUIElement element)
    {
        LocalDataManager.Instance.SaveData.IncrementCurrentActivitySession();
        InteractiveRhyme = element;
        CheckUnlockedOrNot(InteractiveRhyme.sceneName);
    }



    void CheckUnlockedOrNot(string sceneName)
    {
        var premiumStatus = LocalDataManager.Instance.SaveData.IsPremiumUser();

        if (InteractiveRhyme != null && !InteractiveRhyme.IsFree && !premiumStatus)
        {
            UILoader.Instance.StartLoader();
            AudController.TriggerAudio("OnLockClick", () =>
            {
                UILoader.Instance.StopLoader();
                AdsCheckManager.Instance.CloseBanner();

                FreeTrialsPanel.Instance.Init(null, null, () => { AdsCheckManager.Instance.ShowBannerIfAllowed(); });
            });
        }

        else
        {
            PlayerPrefs.SetString("RhymeName", sceneName);
            if (AdsCheckManager.Instance.ShowInterestialIfAllowed(ActivityType.VIDEO))
            {
                Debug.Log("Not a premium user so show ad");
                InterestialService.ShowAdIfItIsReady(() => UISceneLoader.Instance.LoadScene("Rhyme"));
            }
            else
            {
                AdsCheckManager.Instance.CloseBanner();
                UISceneLoader.Instance.LoadScene("Rhyme");
            }

            AudController.TriggerAudio("OnClick");
        }
    }

    public void MainMenu()
    {
        if (UILoader.Instance.IsPanelActive)
        {
            return;
        }
        AdsCheckManager.Instance.CloseBanner();
        UISceneLoader.Instance.LoadScene(EScenes.MainMenu.GetDescription());

        double actiityEndTime = System.DateTime.Now.ToUnixTimeDouble();
        double totalTimeSpent = actiityEndTime - FerrisWheelController.ACTIVITY_START_TIME;
        FerrisWheelController.ACTIVITY_SPENT_TIME = totalTimeSpent;
        AnalyticsScript.instance.LogEvent("activity_session_time", "interactive_rhymes", totalTimeSpent);
    }
}
