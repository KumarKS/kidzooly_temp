﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class RhymeSpriteController : MonoBehaviour
{
    public SpriteAnimator idle;
    public SpriteAnimator onClick;
    public float Anim_delay = 0;

    public Vector3 scaleVal = new Vector3(1f, 1.01f, 1f);

    private SpriteRenderer renderer;

    private SpriteAnimator CurrentAnimator
    {
        get
        {
            if (currentAnimator == null)
            {
                currentAnimator = idle;
                currentAnimator.Reset();
            }
            return currentAnimator;
        }
        set
        {
            if (currentAnimator != value)
            {
                currentAnimator = value;
                if (currentAnimator == idle)
                {
                    seq.Play();
                }
                else
                {
                    seq.Pause();
                }
                currentAnimator.Reset();
            }
        }
    }

    SpriteAnimator currentAnimator;
    Sequence seq;

    Vector3 maxScale;
    Vector3 currentScale;



    void Awake()
    {
        currentScale = transform.localScale;
        maxScale = new Vector3(currentScale.x * scaleVal.x, currentScale.y * scaleVal.y, currentScale.z * scaleVal.z);
    }

    void OnEnable()
    {
        renderer = GetComponent<SpriteRenderer>();

        seq = DOTween.Sequence()
            .Append(transform.DOScale(maxScale, 0.75f))
            .Append(transform.DOScale(currentScale, 0.75f));

        seq.SetLoops(-1);
        seq.SetDelay(UnityEngine.Random.Range(0f, 0.5f));
        seq.Play();
        StartCoroutine(UpdateTiling());
    }

    void OnDisable()
    {
        seq.Kill();
        StopCoroutine(UpdateTiling());
    }

    private IEnumerator UpdateTiling()
    {
        yield return new WaitForSeconds(Anim_delay);
        while (true)
        {
            Sprite sprite = CurrentAnimator.GetSprite();
            if (sprite == null)
            {
                CurrentAnimator = idle;
            }
            else
            {
                renderer.sprite = sprite;
            }
            yield return new WaitForSeconds(1f / CurrentAnimator.fps);
        }
    }

    public void OnPointerClick()
    {
        if (onClick != null)
        {
            CurrentAnimator = onClick;
        }
    }
}

[Serializable]
public class SpriteAnimator
{
    public Sprite[] sprites;
    public AudioClip clip;
    public bool loop;
    public int RepeateAnimationCount = 0;
    public float fps = 10f;
    private int currentIndex = 0;

    public void Reset()
    {
        currentIndex = -1;
    }

    public Sprite GetSprite()
    {
        currentIndex++;

        if (currentIndex == 0 && clip != null)
        {
            AudioSource.PlayClipAtPoint(clip, Vector3.zero);
        }

        if (currentIndex == sprites.Length)
        {
            if (RepeateAnimationCount > 0)
            {
                currentIndex = 0;
                RepeateAnimationCount--;
            }
            else if (loop)
                currentIndex = 0;
            else
                return null;
            // if (loop)
            // 	currentIndex = 0;
            // else
            // 	return null;
        }
        return sprites[currentIndex];
    }
}