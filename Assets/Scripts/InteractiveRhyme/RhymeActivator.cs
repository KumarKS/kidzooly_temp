﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

//using UnityEditor.PackageManager;
using UnityEngine.Timeline;

public class RhymeActivator : MonoBehaviour
{

    Animator anim;
    AudioSource _audioSource;

    public GameObject[] InteractiveRhymesScenes;
    public AudioClip[] InteractiveRhymesAudios;

    void OnEnable()
    {
        HardwareInputManager.OnBack += OnBack;
    }

    void Start()
    {
        _audioSource = this.gameObject.GetComponent<AudioSource>();

        var nameStr = PlayerPrefs.GetString("RhymeName");
        _audioSource.clip = null;

        for (int i = 0; i < InteractiveRhymesScenes.Length; i++)
        {
            InteractiveRhymesScenes[i].SetActive(false);
            if (InteractiveRhymesScenes[i].name == nameStr)
            {
                InteractiveRhymesScenes[i].SetActive(true);
                _audioSource.clip = InteractiveRhymesAudios[i];
            }
        }

        _audioSource.Play();
        PerformanceManager.Instance.IncrementAttempts();
    }

    public void OnBack()
    {
        if (UILoader.Instance.IsPanelActive)
        {
            return;
        }
        PerformanceManager.Instance.IncrementSuccess();
        UISceneLoader.Instance.LoadScene(EScenes.InteractiveRhyme.GetDescription());
    }

    void OnDisable()
    {
        HardwareInputManager.OnBack -= OnBack;
    }
}
