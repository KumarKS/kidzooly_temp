﻿using UnityEngine;
using DG.Tweening;

public class AnimatorInputController : MonoBehaviour 
{
	Animator anim;

	void OnEnable ()
	{
		HardwareInputManager.OnClick += OnClick;
	}

	private void OnClick (Vector2 pos)
	{
		Vector2 v = Camera.main.ScreenToWorldPoint (pos);

		Collider2D[] col = Physics2D.OverlapPointAll (v, LayerMask.GetMask ("RhymeLayer"));

		if (col.Length > 0)
		{
			if (col [col.Length - 1].gameObject.GetComponent<Animator> () != null)
				anim = col [col.Length - 1].gameObject.GetComponent<Animator> ();
			else
				col [col.Length - 1].transform.parent.GetComponent<Animator> ();
			DOTween.Sequence ()
				.AppendInterval (0.1f)
				.AppendCallback (() => anim.SetTrigger ("Action"))
				.Play ();
		}
	}

	void OnDisable ()
	{
		HardwareInputManager.OnClick -= OnClick;
	}
}