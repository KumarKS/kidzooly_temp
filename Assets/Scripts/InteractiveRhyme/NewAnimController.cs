﻿using UnityEngine;
using System.Collections;
using System;


public class NewAnimController : MonoBehaviour
{
    Animator Girl_Animator;
    public String TriggerName;

    // Use this for initialization
    void Start()
    {
        Girl_Animator = GetComponent<Animator>();
    }

    // public void OnPointerClick()
    // {
    //     Girl_Animator.SetTrigger("OnClick");
    // }

    void OnMouseDown()
    {
        Girl_Animator.SetTrigger(TriggerName);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
