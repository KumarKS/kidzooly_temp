using UnityEngine;

public class SpriteInputController : MonoBehaviour 
{
	private void Update()
	{
		if (Input.GetKey (KeyCode.Mouse0))
		{
			OnClick (Input.mousePosition);
		}
	}

	private void OnClick (Vector2 pos)
	{
		Ray v = Camera.main.ScreenPointToRay (pos);
		var col = Physics2D.Raycast (v.origin, v.direction, Mathf.Infinity, LayerMask.GetMask ("RhymeLayer"));
		if (col != null && col.transform != null && col.transform.GetComponent<RhymeSpriteController> () != null)
		{
			col.transform.GetComponent<RhymeSpriteController> ().OnPointerClick();
		}
	}
}
