﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractiveRhymesUIElement : MonoBehaviour
{

    public Image Lock;
    public bool IsFree;
    public string sceneName;

    void Start()
    {
        var premiumStatus = LocalDataManager.Instance.SaveData.IsPremiumUser();
        if (!IsFree && !premiumStatus)
            Lock.enabled = true;
        else
            Lock.enabled = false;
    }


}
