﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FreeTrailsImageSwitcher : MonoBehaviour
{
    public Image kidsImage;
    public Image strip;

    public Sprite[] kidsImages;
    public Color[] stripColor;

    public UIPanel freeTrialsPanel;

    private int currentIndex;

    private void Awake()
    {
        freeTrialsPanel = GetComponent<FreeTrialsPanel>();
        freeTrialsPanel.CallBackOnPanelActivate += Enable;
    }

    private void OnDestroy()
    {
        freeTrialsPanel.CallBackOnPanelActivate -= Enable;
    }

    public void Enable()
    {
        currentIndex = Random.Range(0, kidsImages.Length - 1);
        kidsImage.sprite = kidsImages[currentIndex];
        currentIndex = Random.Range(0, stripColor.Length - 1);
        strip.color = stripColor[currentIndex];
    }

    public void Disable()
    {
        
        
    }

}
