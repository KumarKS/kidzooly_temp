﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class RateUsSession : MonoBehaviour
{
    private int _rateus_session;
    public UIPanel RateUsContainer;
    public Transform[] RateUsStars;

    void Start()
    {
#if TEST_MODE
        _rateus_session = 2;
#else
        _rateus_session = PlayerPrefs.GetInt("rateUsSessionCount",5);
#endif
        // RateUsContainer = GameObject.FindObjectOfType<UIPanel>();
        if (PlayerPrefs.GetInt("isRated", 0) != 1 && LocalDataManager.Instance.SaveData.FeedbackData.GetCurrentAppSession() % _rateus_session == 0)
        {
            PlayerPrefs.SetInt("isFirstEntry", (PlayerPrefs.GetInt("isFirstEntry", 0) + 1));
            if (PlayerPrefs.GetInt("isFirstEntry", 0) == 2)
            {
#if UNITY_ANDROID
                ShowRateUsBox();
#elif UNITY_IOS
                Device.RequestStoreReview();
#endif
            }
        }
    }

    public void OnClickRateUsDialog()
    {
        RateUsContainer.ActivatePanel();
        Sequence starSequence = DOTween.Sequence();
        for (int i = 0; i < RateUsStars.Length; i++)
        {
            starSequence.Append(
                RateUsStars[i].DOLocalRotate(new Vector3(0, 0, -15f), 0.25f)
                    .SetLoops(-1, LoopType.Yoyo)
            );
            starSequence.Append(
                RateUsStars[i].DOLocalRotate(new Vector3(0, 0, 15f), 0.25f)
                    .SetLoops(-1, LoopType.Yoyo)
            );
        }
        starSequence.SetLoops(-1, LoopType.Yoyo);
        starSequence.Play();
    }

    public void ShowRateUsBox()
    {
        UIPopupBox.Instance.SetDataYesNo("Enjoying Kidzooly app ?", OnClickRateUsDialog,
                () => { FeedbackSystem.Instance.Initialize(); }, EDialogBoxSizes.Small_Landscape);
    }

#if TEST_MODE
    void OnGUI()
    {
        // GUI.Button(new Rect(100, 100, 100, 100), PlayerPrefs.GetInt("rateUsSessionCount").ToString());
        // GUI.Button(new Rect(200, 100, 100, 100), PlayerPrefs.GetInt("isFirstEntry").ToString());
        // GUI.Button(new Rect(300, 100, 100, 100), (LocalDataManager.Instance.SaveData.FeedbackData.GetCurrentAppSession() % _rateus_session == 0).ToString());
        if (GUI.Button(new Rect(300, 100, 100, 100), "test"))
        {
            LocalDataManager.Instance.SaveData.FeedbackData.IncrementAppSession();
        }
    }
#endif
}
