﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class SubscribeUsButton : MonoBehaviour {

	public int hidePos, showPos;
	public Button button;

	private Sequence _seq;
	private RectTransform _rectPanel;

	IEnumerator Start () 
	{
		_rectPanel = button.GetComponent<RectTransform> ();
		_seq = DOTween.Sequence ();
		_seq.Append (_rectPanel.DOAnchorPosY(showPos, .5f));
		_seq.Append (_rectPanel.DOScale(Vector3.one * 1.1f, .5f).SetLoops(25, LoopType.Yoyo));
		_seq.AppendInterval (5f);
		_seq.Append (_rectPanel.DOAnchorPosY (hidePos, .5f));
		_seq.AppendInterval (15f);

		yield return new WaitForSeconds (5f);
		if (PurchaseManager.Instance.IsInitialized () && !LocalDataManager.Instance.SaveData.IsPremiumUser ())
		{
			button.onClick.AddListener (OnBtnClick);
			_seq.Play ().SetLoops (-1, LoopType.Restart);
		}
	}

	public void OnBtnClick()
	{
		FreeTrialsPanel.Instance.Init ();
	}

	void OnDestroy()
	{
		_seq.Kill ();
	}
}
