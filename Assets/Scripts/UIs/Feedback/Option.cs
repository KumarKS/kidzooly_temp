﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Option : MonoBehaviour
{
    public Toggle SelectToggle;
    public InputField OptionField;
    public int OptionIndex = 0;

    public void SetData(string inString, bool isCustom, int optionIndex, bool isSelected = false)
    {
        OptionIndex = optionIndex;
        OptionField.text = inString;
        SelectToggle.isOn = isSelected;
        if (isCustom)
        {
            OptionField.interactable = true;
            OptionField.text = string.IsNullOrEmpty(inString) ? string.Empty : inString;
            OptionField.placeholder.color = Color.gray;
            OptionField.textComponent.color = Color.black;
        }
        else
        {
            OptionField.interactable = false;
        }
    }

    public string GetCustomResponse()
    {
        return OptionField.text;
    }
}