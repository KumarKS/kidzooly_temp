﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackSystem : UIPanel
{
    #region Singleton

    static bool isSpawnedAlready = false;
    static GameObject _container;

    static GameObject Container
    {
        get
        {
            if (_container == null)
            {
                var container = GameObject.Find("FeedbackContainer");
                if (container == null)
                {
                    container = new GameObject("FeedbackContainer");
                    container.AddComponent<DontDestroyOnLoad>();
                }
                _container = container;
            }
            return _container;
        }
        set { _container = value; }
    }

    static FeedbackSystem _instance = null;

    public static FeedbackSystem Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(FeedbackSystem)) as FeedbackSystem;
                if (_instance == null)
                {
                    var go = Instantiate(Resources.Load("Feedback/Feedback"), Container.transform) as GameObject;
                    _instance = go.GetComponent<FeedbackSystem>();
                    //  _instance.GetCanvas.worldCamera = Camera.main;
                    _instance.gameObject.name = _instance.GetType().Name;
                    isSpawnedAlready = true;
                }
                _instance.transform.SetParent(Container.transform);
            }
            return _instance;
        }
        set { _instance = value; }
    }

    #endregion

    int CriticalLayerSortingOrder = 1000;

    [Header("Feedback System")]

    public InputField Message;

    [Header("ChildPanels")]
    public UIPanel RatingPanel;
    public UIPanel SuggestionPanel;

    private const int onRateUsDenied = 14;

    protected override void Awake()
    {
        if (isSpawnedAlready)
        {
            Debug.Log("Deleting duplicate");
            Destroy(gameObject);
        }

        if (transform.root == transform) //only if this is the root transform, else it gives a runtime warning
            DontDestroyOnLoad(this);

        isSpawnedAlready = true;
        GetCanvas.sortingOrder = CriticalLayerSortingOrder;
        base.Awake();
    }

    public void Initialize()
    {
		RatingPanel.ActivatePanel();
		SuggestionPanel.DeactivatePanel();

        ActivatePanel();
        PlayerPrefs.SetInt("rateUsSessionCount", onRateUsDenied);
    }

    public void OnFeedbackFinish()
    {
		StringBuilder feedback = new StringBuilder();

        feedback.AppendLine("Suggestion/Message : \n");
        feedback.AppendFormat("Message:\n{0}\n\n", Message.text);

		feedback.AppendLine("*********************************");
        StringBuilder deviceInfo = new StringBuilder();
        deviceInfo.AppendLine("Device Details-\n");
        deviceInfo.AppendLine("Device Model: " + SystemInfo.deviceModel);
        deviceInfo.AppendLine("Device Type: " + SystemInfo.deviceType);
        deviceInfo.AppendLine("Device UID: " + SystemInfo.deviceUniqueIdentifier);
        deviceInfo.AppendLine("Device OS: " + SystemInfo.operatingSystem);
        feedback.AppendLine(deviceInfo.ToString());
		feedback.AppendLine("*********************************");

		string data = feedback.ToString ();

		Debug.Log (data);
		VGMinds.Analytics.AnalyticsManager.LogFeedback (data);

		string mailSubject = Application.productName + " Ver: " + Application.version;
		string preUrl = Constant.MAILPREFIX + Constant.MAILADDRESS + "?subject=" + mailSubject + "&body=";
		string mailUrl = preUrl + data;
		Application.OpenURL (mailUrl);

		Message.text = string.Empty;
        LocalDataManager.Instance.SaveData.FeedbackData.SetRatingStatus(true);
    }

    #region ButtonActions

	public void OnPressLater()
	{
		DeactivatePanel ();
		PlayerPrefs.SetInt("rateUsSessionCount", onRateUsDenied);
	}

	public void OnPressAcceptHelp()
	{
		RatingPanel.DeactivatePanel();
		SuggestionPanel.ActivatePanel();
	}

	public void OnPressSend()
    {
		OnFeedbackFinish();
        DeactivatePanel();
        UIPopupBox.Instance.SetDataOk(LocalizedText.GetLocalizedText("OnFinishFeedback") ?? "Thanks for your feedback!", null, EDialogBoxSizes.Small_Landscape);
    }

    #endregion
}