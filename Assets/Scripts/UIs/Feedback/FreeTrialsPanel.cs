﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using System;

public class FreeTrialsPanel : UIPanel
{
    #region Singleton

    public static bool IsSpawnedAlready = false;
    static GameObject _container;

    protected static GameObject Container
    {
        get
        {
            if (_container == null)
            {
                var container = GameObject.Find("FreeTrialsPanelContainer");
                if (container == null)
                {
                    container = new GameObject("FreeTrialsPanelContainer");
                    container.AddComponent<DontDestroyOnLoad>();
                }
                _container = container;
            }
            return _container;
        }
        set { _container = value; }
    }

    protected static FreeTrialsPanel _instance = null;

    public static FreeTrialsPanel Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(FreeTrialsPanel)) as FreeTrialsPanel;
                if (_instance == null)
                {
                    var go = Instantiate(Resources.Load("FreeTrialsPanel"), Container.transform) as GameObject;
                    _instance = go.GetComponent<FreeTrialsPanel>();
                    //  _instance.GetCanvas.worldCamera = Camera.main;
                    _instance.gameObject.name = _instance.GetType().Name;
                }

                _instance.transform.SetParent(Container.transform);
            }

            IsSpawnedAlready = true;
            return _instance;
        }
        set { _instance = value; }
    }

    int CriticalLayerSortingOrder = 2000;

    #endregion
    public Button[] _acceptButtons;
    public Button _cancelButton, skipButton;

    public void Init(bool showParentalControls = true)
    {
        AnalyticsScript.instance.LogEvent("purchase", "clicked", "free_trial");
        if (LocalDataManager.Instance.SaveData.IsPremiumUser())
        {
            DeactivatePanel();
            if (showParentalControls)
            {
                ParentalLockSystem.Instance.InitializeLock(() =>
                {
                    UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription());
                });
            }
            else
            {
                UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription());
            }
        }
        else
        {
            ActivatePanel();
            _acceptButtons.ForEach((x) =>
           {
               x.onClick.RemoveAllListeners();
               x.onClick.AddListener(() =>
               {
                   if (showParentalControls)
                   {
                       DeactivatePanel();
                       ParentalLockSystem.Instance.InitializeLock(() =>
                       {
                           UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription());

                       });
                   }
                   else
                   {
                       UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription());
                       DeactivatePanel();
                   }
               });
           });

            _cancelButton.onClick.RemoveAllListeners();
            _cancelButton.onClick.AddListener(() => DeactivatePanel());
            skipButton.onClick.RemoveAllListeners();
            skipButton.onClick.AddListener(() => DeactivatePanel());
        }
    }

    public void RestorePurchase()
    {
        PurchaseManager.Instance.RestorePurchases(
            () =>
            {
                Debug.Log("Restoration Successful");
            },
            () =>
            {
                Debug.Log("Restoration Failed");
            }
            );
    }

    public void Init(Action OnSuccess, Action OnFail = null, Action OnComplete = null )
    {
        if (LocalDataManager.Instance.SaveData.IsPremiumUser())
        {
            DeactivatePanel();
            ParentalLockSystem.Instance.InitializeLock(
            () =>
                {
                    UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription());
                    OnSuccess.SafeInvoke();
                },
            () =>
                {
                    OnFail.SafeInvoke();
                });
        }
        else
        {
            ActivatePanel();
            _acceptButtons.ForEach((x) =>
           {
               x.onClick.RemoveAllListeners();
               x.onClick.AddListener(() =>
               {
                   DeactivatePanel();
                   ParentalLockSystem.Instance.InitializeLock(() =>
                   {
                       UISceneLoader.Instance.LoadScene(EScenes.Store.GetDescription());
                       OnSuccess.SafeInvoke();
                   });
               });
           });

            _cancelButton.onClick.RemoveAllListeners();
            _cancelButton.onClick.AddListener(() =>
           {
               vgm.ads.AdsCheckManager.Instance.CloseBanner();
               DeactivatePanel();
               OnFail.SafeInvoke();
               OnComplete?.Invoke();
           });
            skipButton.onClick.RemoveAllListeners();
            skipButton.onClick.AddListener(() =>
            {
                vgm.ads.AdsCheckManager.Instance.CloseBanner();
                DeactivatePanel();
                OnFail.SafeInvoke();
                OnComplete?.Invoke();

            });
        }
    }
}