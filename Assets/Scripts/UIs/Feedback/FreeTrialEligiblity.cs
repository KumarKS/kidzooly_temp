﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeTrialEligiblity : MonoBehaviour 
{
	void Start () 
	{
		if (LocalDataManager.Instance.SaveData.GetCurrentSession > 1 && PurchaseManager.Instance.IsInitialized () && !FreeTrialsPanel.IsSpawnedAlready && !LocalDataManager.Instance.SaveData.IsPremiumUser())
		{
			FreeTrialsPanel.Instance.Init ();
		}
	}
}
