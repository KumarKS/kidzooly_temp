﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackTest : MonoBehaviour
{
    public void PressFeedback()
    {
        UIPopupBox.Instance.SetDataYesNo(LocalizedText.GetLocalizedText("OnDoULike") ?? "Do you like Kidzooly?", Redirect, openFeedback, EDialogBoxSizes.Small_Landscape);
        UIPopupBox.Instance.SetImageProperties(newColor: new Color(0.8f, 0.8f, 0.8f));
    }

    void openFeedback()
    {
        FeedbackSystem.Instance.Initialize();
    }

    void Redirect()
    {
        UIPopupBox.Instance.SetDataOk("You will be redirect to the Play Store.", null);
    }
}