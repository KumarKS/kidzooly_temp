﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollPanel : MonoBehaviour
{
    [SerializeField]
    private RectTransform startPoint, endPoint;
    [SerializeField]
    int direction, speed = 1;
    private void Update()
    {
        if (EndPointReached())
        {
            ResetPosition();
        }
        else
        {
            Move();
        }
      
    }
    void Move()
    {
        this.transform.Translate(0, Time.deltaTime * direction * speed, 0);
    }

    void ResetPosition()
    {
        this.transform.position = startPoint.transform.position;

    }

    bool EndPointReached()
    {
        if(direction > 0)
        {
            if (transform.position.y >= (endPoint.transform.position.y * direction))
            {
                return true;
            }
            else
                return false;
        }
        else
        {
            if (transform.position.y <= (endPoint.transform.position.y))
            {
                return true;
            }
            else
                return false;
        }
        
    }
}
