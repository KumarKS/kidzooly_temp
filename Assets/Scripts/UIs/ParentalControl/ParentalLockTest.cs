﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentalLockTest : MonoBehaviour
{
    public void ParentalLock()
    {
        ParentalLockSystem.Instance.InitializeLock(() => { UIPopupBox.Instance.SetDataOk("Parental Lock is Open", null); });
    }
}