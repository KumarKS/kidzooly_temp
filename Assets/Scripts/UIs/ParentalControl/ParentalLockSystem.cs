﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class ParentalLockSystem : UIPanel
{
    #region Singleton

	public static bool isSpawnedAlready = false;
    static GameObject _container;

    static GameObject Container
    {
        get
        {
            if (_container == null)
            {
                var container = GameObject.Find("ParentalContainer");
                if (container == null)
                {
                    container = new GameObject("ParentalContainer");
                    container.AddComponent<DontDestroyOnLoad>();
                }
                _container = container;
            }
            return _container;
        }
        set { _container = value; }
    }

    static ParentalLockSystem _instance = null;

    public static ParentalLockSystem Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(ParentalLockSystem)) as ParentalLockSystem;
                if (_instance == null)
                {
                    var go = Instantiate(Resources.Load("ParentalLock/ParentalLock"), Container.transform) as GameObject;
                    _instance = go.GetComponent<ParentalLockSystem>();
                    //  _instance.GetCanvas.worldCamera = Camera.main;
                    _instance.gameObject.name = _instance.GetType().Name;
                    isSpawnedAlready = true;
                }
                _instance.transform.SetParent(Container.transform);
            }
            return _instance;
        }
        set { _instance = value; }
    }

    #endregion

    int CriticalLayerSortingOrder = 1000;

    public AudioController aud;

    private Action _onSucess;
    private Action _onCancel;

	public int CodeNumbers = 3;
	string FullPass_Int = "";
	string FullPass_String = "";
	public Animator anim;

	private int[] PassCodeArray;
    private int[] currentPassword;
    private int currentPasswordIndex;

	public GameObject Keypad;
	public Sprite NormalButton;
	public Sprite CorrectBtnSprite, WrongBtnSprite;

	public Color Wrong_Pass_Color;

	public Text InputField;
	public String InputPassword;
	public int InputKeyCounter = 0;

	bool PassCorrect = false;

	public Button[] KeyPadButtons;

    protected override void Awake()
    {
        if (isSpawnedAlready)
        {
            Debug.Log("Deleting duplicate");
            Destroy(gameObject);
        }

        if (transform.root == transform) //only if this is the root transform, else it gives a runtime warning
            DontDestroyOnLoad(this);

        isSpawnedAlready = true;
        GetCanvas.sortingOrder = CriticalLayerSortingOrder;

		CallBackOnPanelDeactivate += () =>
		{
			aud.StopAudio ("callthegrownupstohelp");
		};

        base.Awake();
    }

	void Start ()
	{
		ResetPassWord ();
	}

	public void ResetPassWord ()
	{
		FullPass_Int = "";
		FullPass_String = "";
		InputKeyCounter = 0;
		PassCorrect = false;
		ResetImages ();
		CreatePassWord ();
	}

	void ResetImages ()
	{
		for (int i = 0; i < KeyPadButtons.Length; i++) {
			KeyPadButtons [i].GetComponentInChildren <Image> ().sprite = NormalButton;
		}
	}

	void CreatePassWord ()
	{
        currentPassword = new int[3];
        PassCodeArray = new int[9];
		int GenratedPass_Int;
		string GenratedPass_Str;
		for (int i = 0; i < PassCodeArray.Length; i++) {
			PassCodeArray [i] = i + 1;
		}
		ReShuffle (PassCodeArray);
		for (int i = 1; i <= CodeNumbers; i++) {
			GenratedPass_Int = PassCodeArray [i];
			GenratedPass_Str = GenratedPass_Int.ToString ();
			FullPass_String += ConvertIntToWords (GenratedPass_Str);
			if(i != CodeNumbers)
				FullPass_String +=  "\r\n";
			FullPass_Int += GenratedPass_Str;
            currentPassword[i - 1] = GenratedPass_Int;
		}
        InputField.color = new Color(98f/255f, 123f/255f, 232f/255f);
		InputField.text = FullPass_String;
		EnableButtons ();
	}

	void ReShuffle <T> (T[] Code)
	{
		// Knuth shuffle algorithm :: courtesy of Wikipedia  
		for (int t = 0; t < Code.Length; t++) {
			T tmp = Code [t];
			int r = UnityEngine.Random.Range (t, Code.Length);
			Code [t] = Code [r];
			Code [r] = tmp;
		}
	}

	String ConvertIntToWords (String Number)
	{
		int _Number = Convert.ToInt32 (Number);
		String name = "";
		switch (_Number) {

			case 1:
				name = "One";
				break;
			case 2:
				name = "Two";
				break;
			case 3:
				name = "Three";
				break;
			case 4:
				name = "Four";
				break;
			case 5:
				name = "Five";
				break;
			case 6:
				name = "Six";
				break;
			case 7:
				name = "Seven";
				break;
			case 8:
				name = "Eight";
				break;
			case 9:
				name = "Nine";
				break;
		}
		return name;
	}

	public void OnKeyPadClick (int key)
	{
        if (key == currentPassword[currentPasswordIndex])
        {
            CorrectKey(key);
            currentPasswordIndex++;
        }
        else WrongKey(key);

        aud.TriggerAudio("OnKeyPress");
		if (InputKeyCounter >= CodeNumbers) {
			CheckIfCorrectPassword ();
			CheckIfWrongPassword ();
		}
	}

    

    void CheckIfCorrectPassword ()
	{
		if (InputPassword == FullPass_Int) {
            currentPasswordIndex = 0;
            DisableButtons ();
			aud.TriggerAudio("OnSuccess");
			PassCorrect = true;
			OnSuccess ();
		}
	}

	void CheckIfWrongPassword ()
	{
		if (InputKeyCounter == CodeNumbers && !PassCorrect) {
            currentPasswordIndex = 0;
			DisableButtons ();
			aud.TriggerAudio("OnFailure");
			Invoke ("ResetPassWord", 0.5f);

		}
	}
    private void WrongKey(int Key)
    {
        currentPasswordIndex = 0;
        KeyPadButtons[Key - 1].GetComponentInChildren<Image>().sprite = WrongBtnSprite;
        DisableButtons();
        aud.TriggerAudio("OnFailure");
        Invoke("ResetPassWord", 0.5f);
    }
    void CorrectKey (int Key)
	{	
		KeyPadButtons [Key - 1].GetComponentInChildren <Image> ().sprite = CorrectBtnSprite;
		if (InputKeyCounter >= 0 && InputKeyCounter < CodeNumbers) {
			if (InputKeyCounter == 0) {
				InputPassword = Key.ToString ();
				InputKeyCounter++;
			} else {
				InputPassword += Key;
				InputKeyCounter++;
			}
		}
	}

    public void InitializeLock(Action onSucces, Action onCancel = null)
    {
        _onSucess = onSucces;
        _onCancel = onCancel;

		ResetPassWord ();
        aud.TriggerAudio("callthegrownupstohelp");
		ActivatePanel(true);
    }


	void DisableButtons ()
	{
		for (int i = 0; i < KeyPadButtons.Length; i++) { 
			KeyPadButtons [i].enabled = false;
		}
	}

	void EnableButtons ()
	{
		for (int i = 0; i < KeyPadButtons.Length; i++) { 
			KeyPadButtons [i].enabled = true;
		}
	}

    void OnSuccess()
    {

        InputField.color = new Color(37f / 255f, 169f / 255f, 25f / 255f);
        var seq = DOTween.Sequence();
		seq.Append(InputField.DOText("Success", 0.5f))
            .AppendInterval(0.8f)
            .OnComplete(() =>
            {
                _onSucess.SafeInvoke();

				DeactivatePanel(true);
            });

        seq.Play();
    }

    public override void PressBack()
    {
        base.PressBack();

        if (_onCancel != null)
            _onCancel();
    }
}