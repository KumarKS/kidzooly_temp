﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.UI;
using DG.Tweening;


public class ShineObject : MonoBehaviour
{

    public int endPosX, endPosY, loopTime ;
    public float animationTime;
    public bool isYMoveAllowed, isXMoveAllowed;

    // Start is called before the first frame update
    // Update is called once per frame
    void Start()
    {
        
        Sequence mySequence = DOTween.Sequence();
        if (!transform.parent.gameObject.GetComponent<Mask>())
        {
            transform.parent.gameObject.AddComponent<Mask>();
        }

        if(isXMoveAllowed && !isYMoveAllowed) {
            
            mySequence.Append(this.transform.DOLocalMoveX(endPosX, animationTime)).AppendInterval(loopTime).SetLoops(-1, LoopType.Restart);
           
        }
        else if(!isXMoveAllowed && isYMoveAllowed)
        {
            
            mySequence.Append(this.transform.DOLocalMoveY(endPosY, animationTime)).AppendInterval(loopTime).SetLoops(-1, LoopType.Restart);
           
        }
        else if(isXMoveAllowed && isYMoveAllowed)
        {
           mySequence.Append(this.transform.DOLocalMove(new Vector3(endPosX * 2,endPosY * 2, 0), animationTime)).AppendInterval(loopTime).SetLoops(-1, LoopType.Restart);
            
        }
        mySequence.Play();

    }
}
