﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SplashLoader : UIPanel
{
	#region Singleton

	protected static bool IsSpawnedAlready = false;
	static GameObject _container;

	protected static GameObject Container {
		get {
			if (_container == null) {
				var container = GameObject.Find ("LoaderContainer");
				if (container == null) {
					container = new GameObject ("LoaderContainer");
					container.AddComponent<DontDestroyOnLoad> ();
				}
				_container = container;
			}
			return _container;
		}
		set { _container = value; }
	}

	protected static SplashLoader _instance = null;

	public static SplashLoader Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType (typeof(SplashLoader)) as SplashLoader;
				if (_instance == null) {
					var go = Instantiate (Resources.Load ("SplashLoader"), Container.transform) as GameObject;
					_instance = go.GetComponent<SplashLoader> ();
					//  _instance.GetCanvas.worldCamera = Camera.main;
					_instance.gameObject.name = _instance.GetType ().Name;
				}

				//_instance.transform.SetParent (Container.transform);
			}

			IsSpawnedAlready = true;
			return _instance;
		}
		set { _instance = value; }
	}

	int CriticalLayerSortingOrder = 1000;

	#endregion


	bool isLoaderOn = false;

	int _LoaderTriggerCount;
	Sequence _rotationSeq;

	[SerializeField]
	[Header ("Splash Loader")]
	protected float AngleIncreaseFactor = -45f;

	[SerializeField]
	protected float DelayFactor = 0.1f;

	[SerializeField]
	protected Image Loader;

	[SerializeField]
	protected Text LoaderText;

	[SerializeField]
	protected Text VersionText;


	protected override void Awake ()
	{
		if (LoaderText)
			LoaderText.Deactivate ();

		if (IsSpawnedAlready) {
			Debug.Log ("Deleting duplicate");
			Destroy (gameObject);
		}

		//if (transform.root == transform) //only if this is the root transform, else it gives a runtime warning
  //          DontDestroyOnLoad (this);

		IsSpawnedAlready = true;
		initilizeLoader ();
		GetCanvas.sortingOrder = CriticalLayerSortingOrder;
		base.Awake ();
	}

	void Start ()
	{
		OnAppFirstRun ();
	}

	protected virtual void initilizeLoader ()
	{
		GetCanvas.sortingOrder = CriticalLayerSortingOrder;

		_rotationSeq = DOTween.Sequence ();
		_rotationSeq.Append (Loader.transform.DOLocalRotate (new Vector3 (0, 0, -AngleIncreaseFactor), 0f, RotateMode.FastBeyond360).SetEase (Ease.Flash).SetUpdate (UpdateType.Fixed, true))
            .SetDelay (DelayFactor)
            .SetLoops (-1, LoopType.Incremental);

		_rotationSeq.Pause (); //small hack to properly display the loading indicator
	}

	public virtual void StopLoader (bool forceStop = false)
	{
		_LoaderTriggerCount--;
		_LoaderTriggerCount = Mathf.Clamp (_LoaderTriggerCount, 0, _LoaderTriggerCount);

		//Debug.Log("Loader Count: " + _LoaderTriggerCount);

		if (_LoaderTriggerCount > 0 && forceStop == false) {
			return;
		}

		isLoaderOn = false;
		_rotationSeq.Pause ();

		DeactivatePanel ();
		LoaderText.Deactivate ();
        //Destroy(gameObject);
	}

	public virtual void StartLoader ()
	{
		_LoaderTriggerCount++;
		LoaderText.Deactivate ();
		VersionText.Deactivate ();
		//  Debug.Log("Loader Count: " + _LoaderTriggerCount);

		if (isLoaderOn) {
			return;
		}

		if (GetCanvas) {
			GetCanvas.sortingOrder = CriticalLayerSortingOrder;
		}

		isLoaderOn = true;
		ActivatePanel ();

		_rotationSeq.Restart (true);
	}

	public virtual void SetText (string inText)
	{
		LoaderText.Activate ();
		LoaderText.SetText (inText);
	}

	public void SetVersion (string versionInfo)
	{
		if (!VersionText.IsActive ()) {
			VersionText.Activate ();
			VersionText.SetText ("Content Version: " + versionInfo);
		}
	}

	protected virtual void OnDestroy ()
	{
		if (!IsSpawnedAlready) {
			_instance = null;
		}
	}

	protected virtual void OnApplicationQuit ()
	{
		_instance = null;
	}

	void OnAppFirstRun ()
	{
		//Below entry helps us with resetting the rateus session
		PlayerPrefs.SetInt ("isFirstEntry", 0);
		Application.targetFrameRate = 60;
	}
}