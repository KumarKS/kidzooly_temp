﻿#if !AMAZON_STORE
using Firebase.Extensions;
#endif
using System;
using UnityEngine;

public static class FirebaseInit
{
#if !AMAZON_STORE
    public static Action OnFirebaseInitDone;
    static Firebase.DependencyStatus dependencyStatus = new Firebase.DependencyStatus();
    public static void InitializeFirebase(Action<string> callback)
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                Firebase.Installations.FirebaseInstallations.DefaultInstance.GetIdAsync().ContinueWith(idTask =>
                {
                    string device_id = idTask.Result;
                    PerformanceManager.Instance.SetDeviceId(device_id);
                    PerformanceManager.Instance.SetDeviceFID(device_id);
                    Debug.LogError("MY UNIQUE DEVICE ID : " + device_id);

                    callback(device_id);
                });

                OnFirebaseInitDone?.Invoke();

                if (GameData_Deserialize.instance)
                {
                    Debug.Log("GameData_Deserialize Json Parsing... ");
                    GameData_Deserialize.instance.StartParsing();
                }
            }
            else
            {
                Debug.LogError(
                "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });

    }
#endif
}
