﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.UI.Extensions;

public class FinishUI : UIPanel
{
    #region Singleton

    static bool isSpawnedAlready = false;
    static GameObject _container;

    static GameObject Container
    {
        get
        {
            if (_container == null)
            {
                var container = GameObject.Find("FinishUIContainer");
                if (container == null)
                {
                    container = new GameObject("FinishUIContainer");
                    container.AddComponent<DontDestroyOnLoad>();
                }
                _container = container;
            }
            return _container;
        }
        set { _container = value; }
    }

    static FinishUI _instance = null;

    public static FinishUI Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(FinishUI)) as FinishUI;
                if (_instance == null)
                {
                    var go = Instantiate(Resources.Load("FinishUI/FinishUI"), Container.transform) as GameObject;
                    _instance = go.GetComponent<FinishUI>();
                    //  _instance.GetCanvas.worldCamera = Camera.main;
                    _instance.gameObject.name = _instance.GetType().Name;
                    isSpawnedAlready = true;
                }
                _instance.transform.SetParent(Container.transform);
            }
            return _instance;
        }
        set { _instance = value; }
    }

    #endregion

    int CriticalLayerSortingOrder = 1000;


    [Header("Finish UI")]
    public Button OkButton;
    public AudioController aud;
    public CanvasGroup PanelCanvasGroup;
    public List<Image> StarImages;
    public ParticleSystem Effect;
    public CanvasGroup FinishBanner;
    public List<Image> DecorImages;

    private Action _onOK;

    private Sequence _showAnim;

    protected override void Awake()
    {
        if (isSpawnedAlready)
        {
            Debug.Log("Deleting duplicate");
            Destroy(gameObject);
        }

        if (transform.root == transform) //only if this is the root transform, else it gives a runtime warning
            DontDestroyOnLoad(this);

        isSpawnedAlready = true;
        GetCanvas.sortingOrder = CriticalLayerSortingOrder;
        base.Awake();

        Initialize();
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += (index, mode) => { RefreshFinishUI(); };
    }

    void Initialize()
    {
        OkButton.GetComponent<CanvasGroup>().DOFade(0, 0);
        OkButton.interactable = false;
        PanelCanvasGroup.interactable = false;

        FinishBanner.DOFade(0, 0);

        DecorImages.ForEach(x => { x.DOFade(0, 0); });

        StarImages.ForEach(x =>
        {
            x.rectTransform.DOScale(Vector3.zero, 0);
            x.DOFade(0, 0);
        });
    }

    public void RefreshFinishUI()
    {
        Initialize();
        DeactivatePanel();
        this.Deactivate();
    }

	public Tween ShowFinishStars(Action onOk, bool waitForInput = false, int numOfStars = 3) //, ScoreStars rating = ScoreStars.Three)
    {
        this.Activate();

        Initialize();

        ActivatePanel();

        _onOK = onOk;
        //  RatingScoreStars = rating;
        _showAnim = DOTween.Sequence();

        //banner animation
        _showAnim.AppendCallback(() =>
            {
                DecorImages.ForEach(x => { x.DOFade(1, 0.8f); });
                if (Effect)
                {
                    Effect.Simulate(0);
                }
            })
            .Join(FinishBanner.transform.DOMoveY(FinishBanner.transform.position.y + 150, 0.35f).From().SetEase(Ease.OutBounce))
            .Join(FinishBanner.DOFade(1, 0.2f));

        //star animation
        var numberOfStars = Mathf.Clamp(numOfStars, 0, 3);
        for (int i = 0; i < numberOfStars; i++)
        {
            _showAnim.Append(StarImages[i].rectTransform.DOScale(Vector3.one, 0.35f).SetEase(Ease.OutBounce))
                .Join(StarImages[i].DOFade(1, 0.3f));
        }

		if (waitForInput && OkButton)
		{
			_showAnim.Append(OkButton.GetComponent<CanvasGroup>().DOFade(1, 0.5f))
				.AppendCallback(() =>
				{
					OkButton.interactable = true;
					PanelCanvasGroup.interactable = true;
				});
			
			OkButton.onClick.RemoveAllListeners ();
			OkButton.onClick.AddListener (() =>
			{
				_onOK.SafeInvoke ();
				_onOK = null; //safe for not calling it twice

				//clearing the particle system on press ok
				if (Effect)
				{
					Effect.Stop (false, ParticleSystemStopBehavior.StopEmittingAndClear);
				}

				DeactivatePanel ();
			});
		} 
		else
		{
			_showAnim.AppendInterval (1f);
			_showAnim.AppendCallback (() =>
			{
				_onOK.SafeInvoke ();
				_onOK = null; //safe for not calling it twice

				//clearing the particle system on press ok
				if (Effect)
				{
					Effect.Stop (false, ParticleSystemStopBehavior.StopEmittingAndClear);
				}

				DeactivatePanel ();
			});
		}

		_showAnim.Play();

        if (PanelCanvasGroup)
        {
            EventTrigger trigger = PanelCanvasGroup.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener((data) =>
            {
                if (PanelCanvasGroup.interactable)
                {
					_onOK.SafeInvoke ();
					_onOK = null; //safe for not calling it twice

					//clearing the particle system on press ok
					if (Effect)
					{
						Effect.Stop (false, ParticleSystemStopBehavior.StopEmittingAndClear);
					}

					DeactivatePanel ();
                }
            });
            trigger.triggers.Add(entry);
        }
        aud.TriggerAudio("achievement");
        aud.TriggerAudio(EnumeratorExtensions.RandomAmong("awesome", "goodjob", "congratulations","fantastic","excellent","brilliant"));

        return _showAnim;
    }
}