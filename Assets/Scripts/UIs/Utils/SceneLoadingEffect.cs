﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoadingEffect : MonoBehaviour
{
    public static int MenuLaunch = 0;
    public bool DoAnimation = false;

    [Header("Zoom Effect")]
    public List<GameObject> Layers;

    public float AnimationTime = 2f;
    public float StartScale = 10f;
    public Ease EaseType;
    private float _animTime = 0;

    void Awake()
    {
        //clear any pending notifications or loader
        UIToastNotification.Instance.KillToast();
        UILoader.Instance.StopLoader(true);

        if (DoAnimation)
        {
            foreach (var layer in Layers)
            {
                layer.transform.localScale = Vector3.one * StartScale;
            }


            for (int j = 0; j < Layers.Count; j++)
            {
                var go = Layers[j];
                var canvasGroup = go.GetComponent<CanvasGroup>();
                var spriteRenderer = go.GetComponent<SpriteRenderer>();
                var image = go.GetComponent<Image>();
                if (canvasGroup != null)
                {
                    canvasGroup.DOFade(0, 0);
                }
                else if (spriteRenderer != null)
                {
                    spriteRenderer.DOFade(0, 0);
                }
                else if (image != null)
                {
                    image.DOFade(0, 0);
                }
            }
        }
    }

    void Start()
    {
        if (MenuLaunch > 0)
        {
            _animTime = 0;
        }
        else
        {
            _animTime = AnimationTime;
        }


        StartCoroutine(DoZoomAnimation());
        StartCoroutine(DoFadeAnimation());

        MenuLaunch++;
        Resources.UnloadUnusedAssets();
        System.GC.Collect();
        //StartCoroutine(OnlineCheck());
    }

    private IEnumerator DoZoomAnimation()
    {
        yield return new WaitForEndOfFrame();
        if (DoAnimation)
        {
            for (int j = 0; j < Layers.Count; j++)
            {
                Layers[j].transform.DOScale(Vector3.one, (_animTime / Layers.Count) * (j + 1)).SetEase(EaseType);
            }
        }
    }

    private IEnumerator DoFadeAnimation()
    {
        yield return new WaitForEndOfFrame();
        if (DoAnimation)
        {
            for (int j = 0; j < Layers.Count; j++)
            {
                var go = Layers[j];
                var canvasGroup = go.GetComponent<CanvasGroup>();
                var spriteRenderer = go.GetComponent<SpriteRenderer>();
                var image = go.GetComponent<Image>();
                if (canvasGroup != null)
                {
                    canvasGroup.DOFade(1, AnimationTime).SetEase(EaseType);
                }
                else if (image != null)
                {
                    image.DOFade(1, AnimationTime).SetEase(EaseType);
                }
                else if (spriteRenderer != null)
                {
                    spriteRenderer.DOFade(1, AnimationTime).SetEase(EaseType);
                }

                yield return true;
            }
        }
    }

    IEnumerator OnlineCheck()
    {
        for (;;)
        {
            Utilities.InternetStateChange(state =>
            {
                Debug.Log(state);
            });

            yield return new WaitForSeconds(1);
        }
    }
}

