﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;

namespace VGMEditorTools
{
	public class AdvancedEditorPlayer : EditorWindow 
	{
		private static string StartScenePath
		{
			get
			{
				return EditorPrefs.GetString ("StartScene");
			}
			set
			{
				EditorPrefs.SetString ("StartScene", value);
			}
		}
		private static string LastScenePath
		{
			get
			{
				return EditorPrefs.GetString ("EndScene");
			}
			set
			{
				EditorPrefs.SetString ("EndScene", value);
			}
		}

		private static bool IsPlaying
		{
			get
			{
				return EditorPrefs.GetInt ("IsPlaying", 0) == 1;
			}
			set
			{
				EditorPrefs.SetInt ("IsPlaying", value ? 1 : 0);
			}
		}

		private static bool ResetPrefs
		{
			get
			{
				return EditorPrefs.GetInt ("ResetPrefs", 0) == 1;
			}
			set
			{
				EditorPrefs.SetInt ("ResetPrefs", value ? 1 : 0);
			}
		}

		private static bool ResetLocalData
		{
			get
			{
				return EditorPrefs.GetInt ("ResetLocalData", 0) == 1;
			}
			set
			{
				EditorPrefs.SetInt ("ResetLocalData", value ? 1 : 0);
			}
		}


		[MenuItem("Window/Advanced Player")]
		public static void ShowWindow()
		{
			EditorWindow.GetWindow(typeof(AdvancedEditorPlayer));
		}

		private void OnGUI()
		{
			GUILayout.BeginVertical ();
			if (!IsPlaying)
			{
				if (GUILayout.Button ("Start Player"))
				{
					if (ResetPrefs)
					{
						PlayerPrefs.DeleteAll ();
					}
					if (ResetLocalData)
					{
						if (Directory.Exists(Application.persistentDataPath))
						{
							Directory.Delete (Application.persistentDataPath, true);
						}
					}
					if (string.IsNullOrEmpty (StartScenePath))
					{
						EditorUtility.DisplayDialog ("Info", "No starting scene saved", "Ok");
					} else
					{
						EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
						LastScenePath = EditorSceneManager.GetActiveScene ().path;
						EditorSceneManager.OpenScene (StartScenePath);
						EditorApplication.isPlaying = true;
					}
				}
			}
			else
			{
				if (GUILayout.Button ("Stop Player"))
				{
					EditorApplication.isPlaying = false;
				}
			}
			if (GUILayout.Button ("Start Scene : " + (string.IsNullOrEmpty(StartScenePath) ? "Not Set" : GetSimplifiedFileName(StartScenePath))))
			{
				StartScenePath = EditorSceneManager.GetActiveScene ().path;
			}
			if (GUILayout.Button ("End Scene : " + (string.IsNullOrEmpty(LastScenePath) ? "Not Set" : GetSimplifiedFileName(LastScenePath))))
			{
				LastScenePath = EditorSceneManager.GetActiveScene ().path;
			}

			ResetPrefs = GUILayout.Toggle (ResetPrefs, "Reset Prefs", GUILayout.MaxWidth (150));
			ResetLocalData = GUILayout.Toggle (ResetLocalData, "Reset LocalData", GUILayout.MaxWidth (150));
			GUILayout.EndVertical ();
		}

		private void OnEnable()
		{
			EditorApplication.playmodeStateChanged += PlayModeChanged;
		}

		private void OnDisable()
		{
			EditorApplication.playmodeStateChanged -= PlayModeChanged;
		}

		private static void PlayModeChanged()
		{
			if (EditorApplication.isPlaying || EditorApplication.isPlayingOrWillChangePlaymode || EditorApplication.isPaused)
			{
				IsPlaying = true;
			} 
			else
			{
				IsPlaying = false;
				EditorSceneManager.OpenScene (LastScenePath);
			}
		}

		public string GetSimplifiedFileName(string sceneName)
		{
			int lastDotIdx = sceneName.LastIndexOf('.');
			int lastSlashdx = sceneName.LastIndexOf ('/');
			return sceneName.Substring(lastSlashdx + 1, lastDotIdx - lastSlashdx - 1);
		}
	}
}