﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using System;
using UnityEditor.SceneManagement;
using System.ComponentModel;
using UnityEditor.Purchasing;
using UnityEngine.Purchasing;

namespace VGMEditorTools
{
	public static class KeyStoreInfo
	{
		public static string KeyStoreName 
		{
			get
			{
				return EditorPrefs.GetString ("KeyStoreName", string.Empty);
			}
			set
			{
				EditorPrefs.SetString ("KeyStoreName", value);
			}
		}

		public static string KeyStorePass 
		{
			get
			{
				return EditorPrefs.GetString ("KeyStorePass", string.Empty);
			}
			set
			{
				EditorPrefs.SetString ("KeyStorePass", value);
			}
		}

		public static string KeyAliasName 
		{
			get
			{
				return EditorPrefs.GetString ("KeyAliasName", string.Empty);
			}
			set
			{
				EditorPrefs.SetString ("KeyAliasName", value);
			}
		}

		public static string KeyAliasPass 
		{
			get
			{
				return EditorPrefs.GetString ("KeyAliasPass", string.Empty);
			}
			set
			{
				EditorPrefs.SetString ("KeyAliasPass", value);
			}
		}

		public static void Clear()
		{
			KeyStoreName = string.Empty;
			KeyStorePass = string.Empty;
			KeyAliasName = string.Empty;
			KeyAliasPass = string.Empty;
			Debug.Log ("Key Store successfully cleared");
		}

		public static void Load()
		{
			KeyStoreName = PlayerSettings.Android.keystoreName;
			KeyStorePass = PlayerSettings.Android.keystorePass;
			KeyAliasName = PlayerSettings.Android.keyaliasName;
			KeyAliasPass = PlayerSettings.Android.keyaliasPass;

			if (string.IsNullOrEmpty (KeyStoreName) || string.IsNullOrEmpty (KeyStorePass) || string.IsNullOrEmpty (KeyAliasName) || string.IsNullOrEmpty (KeyAliasPass))
			{
				Debug.LogError ("Key Store not properly loaded");
			}
			else
			{
				Debug.Log ("Key Store successfully loaded");
			}
		}
	}

	public class DevBuildOptions : EditorWindow
	{
		private bool toggleScenes 	= true;
		private static bool synced	= false;

		private float LastBuildSize 
		{
			get
			{
				return EditorPrefs.GetFloat ("LastBuildSize", 0);
			}
			set
			{
				EditorPrefs.SetFloat ("LastBuildSize", value);
			}
		}

		private float CurrentBuildSize 
		{
			get
			{
				return EditorPrefs.GetFloat ("CurrentBuildSize", 0);
			}
			set
			{
				EditorPrefs.SetFloat ("CurrentBuildSize", value);
			}
		}

		public string DifferenceInSize = "";

		private static bool HasStartedBuild 
		{
			get{ return EditorPrefs.GetBool ("HasStartedBuild", false);}
			set{EditorPrefs.SetBool ("HasStartedBuild", value);}
		}

		private static BuildType buildType
		{
			get{ return (BuildType) EditorPrefs.GetInt ("BuildType", 0);}
			set{EditorPrefs.SetInt ("BuildType", (int) value);}
		}

		private enum BuildType
		{
			None,
			AutoRun,
			ThenRun
		}
		private static BuildStore _buildStore
		{
			get
			{ 
				return (BuildStore) EditorPrefs.GetInt ("BuildStore", 0);
			}
			set
			{
				if (value != (BuildStore)EditorPrefs.GetInt ("BuildStore", 0))
				{
					EditorPrefs.SetInt ("BuildStore", (int) value); 
					UpdateScriptDefiningSymbols ();
					UpdateTargetStore ();
				}
			}
		}

		private enum BuildStore
		{
			[Description("GoogleStoreBuild")]
			GooglePlay,
			[Description("AmazonStoreBuild")]
			Amazon,
			[Description("AppleStoreBuild")]
			AppleAppStore,
			[Description("UnspecifiedStoreBuild")]
			Unspecified
		}

		private static string LastBuildFolder
		{
			get
			{
				return EditorPrefs.GetString ("LastSaveFolder");
			}
			set
			{
				EditorPrefs.SetString ("LastSaveFolder",value);
			}
		}

		public string saveFileName 
		{
			get 
			{ 
				return Application.identifier + "_" + EditorUserBuildSettings.activeBuildTarget + "_" + _buildStore.ToString() + ".apk";
			}
		}

		public string GetProjectBuildFolder
		{
			get
			{
				return Path.Combine (Application.dataPath.Replace ("Assets", ""), "Builds");
			}
		}

		public string GetTempBuildFolder
		{
			get
			{
				return Path.Combine (Environment.GetEnvironmentVariable("temp"), "UnityTempBuilds" + Path.AltDirectorySeparatorChar + Application.identifier + Path.AltDirectorySeparatorChar + "Builds");
			}
		}

		private SavedSceneCollection SceneCollection
		{
			get
			{
				if (_SceneCollection == null)
				{
					_SceneCollection = new SavedSceneCollection (new SavedScene[]{});
				}
				return _SceneCollection;
			}
			set
			{
				_SceneCollection = value;
			}
		}

		private SavedSceneCollection _SceneCollection;

		[MenuItem("Window/Dev Build Options")]
		public static void ShowWindow()
		{
			EditorWindow.GetWindow(typeof(DevBuildOptions));
			synced = false;
		}

		[PostProcessBuildAttribute(1)]
		public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) 
		{
			Debug.Log ("Refreshing");
			synced = false;
		}

		private void Resync()
		{
			try
			{
				LoadData ();
				if (HasStartedBuild)
				{
					string lastPath = EditorUserBuildSettings.GetBuildLocation (EditorUserBuildSettings.activeBuildTarget);
					if (!string.IsNullOrEmpty (lastPath))
					{
						FileInfo info = new FileInfo(lastPath);
						if (info.Exists)
						{
							LastBuildSize = CurrentBuildSize;

							CurrentBuildSize =  (float) info.Length / (float) (1024 * 1024);
							CurrentBuildSize =  (float) Math.Round ((double)CurrentBuildSize, 2);

							SetDiffInSize (CurrentBuildSize, LastBuildSize);
						}
						string path = Path.Combine (Environment.GetEnvironmentVariable ("localappdata"), "Unity" + Path.AltDirectorySeparatorChar + "Editor" + Path.AltDirectorySeparatorChar + "Editor.log");
						File.Copy (path, LastBuildFolder + "Editor.Log", true);
						if (buildType == BuildType.ThenRun)
						{
							Rebuild ();
						}
					}
				}
			}
			catch(Exception e)
			{
				Debug.Log ("Exception : " + e.Message);
				EditorUserBuildSettings.SetBuildLocation (EditorUserBuildSettings.activeBuildTarget, string.Empty);
			}
			finally
			{
				HasStartedBuild = false;
				synced = true;
			}
		}

		float scrollPosY = 0f;
		private void OnGUI()
		{
			if (!synced)
			{
				Resync ();
			}
			if (GUILayout.Button ("Sync From Player"))
			{
				SceneCollection = new SavedSceneCollection(EditorBuildSettings.scenes);
				toggleScenes = true;
			}
			if (SceneCollection != null && SceneCollection.Scenes != null && SceneCollection.Scenes.Length > 0)
			{
				GUILayout.Space (5);

				GUILayout.Box ("Last build size (MB) : " + CurrentBuildSize);

				if(!string.IsNullOrEmpty(DifferenceInSize))
				GUILayout.Box("( "+ DifferenceInSize +" )");

				if (GUILayout.Button ("Build"))
				{
					BuildPlayer ();
				}
				buildType = (BuildType) EditorGUILayout.EnumPopup (buildType);
				_buildStore = (BuildStore) EditorGUILayout.EnumPopup (_buildStore);

				GUILayout.BeginHorizontal ();
				if (GUILayout.Button ("Signed"))
				{
					PlayerSettings.Android.keystoreName = KeyStoreInfo.KeyStoreName;
					PlayerSettings.Android.keystorePass = KeyStoreInfo.KeyStorePass;
					PlayerSettings.Android.keyaliasName = KeyStoreInfo.KeyAliasName;
					PlayerSettings.Android.keyaliasPass = KeyStoreInfo.KeyAliasPass;
				}
				if (GUILayout.Button ("Unsigned"))
				{
					PlayerSettings.Android.keystoreName = string.Empty;
					PlayerSettings.Android.keystorePass = string.Empty;
					PlayerSettings.Android.keyaliasName = string.Empty;
					PlayerSettings.Android.keyaliasPass = string.Empty;
				}
				if (GUILayout.Button ("Load"))
				{
					KeyStoreInfo.Load ();
				}
				if (GUILayout.Button ("Clear"))
				{
					KeyStoreInfo.Clear ();
				}
				GUILayout.EndHorizontal ();

				if (GUILayout.Button ("Open Last Build Folder"))
				{
					OpenBuildFolder ();
				}
				if (GUILayout.Button (new GUIContent("Save Last Build", "Last Build will be saved from Temp folder")))
				{
					SaveLastBuild ();				
				}
				GUILayout.Space (10);
				if (GUILayout.Button ("Start Logcat"))
				{
					StartLogcat ();
				}
				if (GUILayout.Button ("Install last Build"))
				{
					Rebuild ();
				}
				if (GUILayout.Button ("Uninstall from Device"))
				{
					Uninstall ();
				}
				GUILayout.Space (10);

				string val = toggleScenes ? "Deactivate All Scenes" : "Activate All Scenes";

				EditorGUI.BeginChangeCheck ();
				if (GUILayout.Button (val))
				{
					toggleScenes = !toggleScenes;
					foreach(SavedScene scene in SceneCollection.Scenes) 
					{
						scene.enabled = toggleScenes;
					}
				}
				GUILayout.Space (5f);
				scrollPosY = GUILayout.BeginScrollView (new Vector2 (0, scrollPosY)).y;
				foreach(SavedScene scene in SceneCollection.Scenes) 
				{
					GUILayout.BeginHorizontal ();
					scene.enabled = GUILayout.Toggle (scene.enabled, GUIContent.none, GUILayout.MaxWidth(20f));
					if (GUILayout.Button (GetSimplifiedFileName (scene.path), GUILayout.MaxWidth(200f)))
					{
						EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ();
						EditorSceneManager.OpenScene (scene.path, OpenSceneMode.Single);
					}
					GUILayout.EndHorizontal ();
				}

				GUILayout.EndScrollView ();
				if (EditorGUI.EndChangeCheck ())
				{
					SaveData ();
				}
				if (GUILayout.Button ("Open All Scenes Additive"))
				{
					foreach (SavedScene scene in SceneCollection.Scenes)
					{
						EditorSceneManager.OpenScene (scene.path, OpenSceneMode.Additive);
					}
				}
				if (GUILayout.Button ("Mark All OpenScenes Dirty"))
				{
					EditorSceneManager.MarkAllScenesDirty ();
					EditorSceneManager.SaveOpenScenes ();
				}
			}
		}

		private void SetDiffInSize(float currentVal, float lastVal)
		{
			if (currentVal == 0 || lastVal == 0)
			{
				DifferenceInSize = "";
			}
			else
			{
				int val = (int) (((currentVal / lastVal) * 100f) - 100f);
				if (val < 0)
				{
					DifferenceInSize = "Reduced by " + (Mathf.Abs (val)) + "%";
				}
				else if (val > 0)
				{
					DifferenceInSize = "Increased by " + (Mathf.Abs (val)) + "%";
				}
				else
				{
					DifferenceInSize = "No Difference";
				}
			}
		}

		private void BuildPlayer()
		{
			if (SceneCollection != null && SceneCollection.IsValid())
			{
				SaveData ();
				DateTime dt = DateTime.Now;
				LastBuildFolder = GetTempBuildFolder + Path.AltDirectorySeparatorChar + dt.ToString ("yyyy_MM_dd_HH_mm") + Path.AltDirectorySeparatorChar;
				DirectoryInfo Directory = new DirectoryInfo (LastBuildFolder);
				if (!Directory.Exists)
				{
					Directory.Create ();
				}
				string extraDataFilename = "ExtraInfo.txt";
				StreamWriter writer = File.CreateText (LastBuildFolder + extraDataFilename);

				string val = "";
				foreach (SavedScene scene in SceneCollection.Scenes)
				{
					if (scene.enabled)
					{
						val += scene.path + Environment.NewLine;
					}
				}
				writer.WriteLine (val);
				writer.Close ();
				EditorUserBuildSettings.SetBuildLocation (EditorUserBuildSettings.activeBuildTarget, LastBuildFolder + saveFileName );
				BuildOptions opt = buildType == BuildType.AutoRun ? BuildOptions.AutoRunPlayer : BuildOptions.None;
				BuildPipeline.BuildPlayer (SceneCollection.GetEditorScenes(), LastBuildFolder + saveFileName, EditorUserBuildSettings.activeBuildTarget, opt);
				HasStartedBuild = true;
			}
			else
			{
				EditorUtility.DisplayDialog ("Error", "No scenes added", "Ok");
			}
		}

		private static void UpdateScriptDefiningSymbols()
		{
			string allOldSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup (EditorUserBuildSettings.selectedBuildTargetGroup);
			string[] symbols = allOldSymbols.Split (';');
			List<string> newSymbols = new List<string> ();
			foreach (var item in symbols)
			{
				if(!item.Contains ("StoreBuild"))
				{
					newSymbols.Add (item);
				}
			}
			newSymbols.Add (_buildStore.GetDescription ());
			string allNewSymbols = string.Empty;
			foreach (var item in newSymbols)
			{
				allNewSymbols += item + ';';
			}
			allNewSymbols = allNewSymbols.Remove (allNewSymbols.Length - 1);
			if (allOldSymbols == allNewSymbols)
			{
				Debug.Log ("No scripting symbols change required");
				return;
			}
			Debug.Log ("Change in Symbols detected : \n" + "Old : " + allOldSymbols + "\n" + "New : " + allNewSymbols);
			PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, allNewSymbols);
		}

		private static void UpdateTargetStore()
		{
			switch (_buildStore)
			{
				case BuildStore.Amazon:
					UnityPurchasingEditor.TargetAndroidStore (AppStore.AmazonAppStore);
					break;

				case BuildStore.GooglePlay:
					UnityPurchasingEditor.TargetAndroidStore (AppStore.GooglePlay);
					break;

				case BuildStore.AppleAppStore:
//					UnityPurchasingEditor.TargetAndroidStore (AppStore.AppleAppStore);
					break;

				default:
					UnityPurchasingEditor.TargetAndroidStore (AppStore.NotSpecified);
					break;
			}
			Debug.Log ("Changed buildstore : " + _buildStore.ToString());
		}

		private void Uninstall()
		{
			string path = Path.Combine(Path.Combine(EditorPrefs.GetString ("AndroidSdkRoot") , "platform-tools"),"adb ");
			string para = "uninstall " + Application.identifier;
			string cmd = path + para;
			System.Diagnostics.Process.Start ("Powershell.exe", cmd);
		}

		private void StartLogcat()
		{
			string path = Path.Combine(Path.Combine(EditorPrefs.GetString ("AndroidSdkRoot") , "platform-tools"),"adb ");
			string para = "logcat -s Unity";
			string cmd = path + para;
			System.Diagnostics.Process.Start ("Powershell.exe", cmd);
		}

		private static void Rebuild()
		{
			string path = Path.Combine(Path.Combine(EditorPrefs.GetString ("AndroidSdkRoot") , "platform-tools"),"adb ");
			string para = "install " + EditorUserBuildSettings.GetBuildLocation (EditorUserBuildSettings.activeBuildTarget);
			string cmd = path + para;
			System.Diagnostics.Process.Start ("Powershell.exe", cmd);
		}

		private static void OpenBuildFolder()
		{
			string location = EditorUserBuildSettings.GetBuildLocation (EditorUserBuildSettings.activeBuildTarget);
			location = location.Replace(@"/", @"\");
			System.Diagnostics.Process.Start("explorer.exe", "/select," + location);
		}

		private void SaveLastBuild()
		{
			DirectoryInfo tempBuildFolder = new DirectoryInfo (LastBuildFolder);
			if (!tempBuildFolder.Exists)
			{
				Debug.Log (tempBuildFolder.FullName);
				Debug.Log ("Not found.. Folder Deleted or Modified");
				return;
			}
			else
			{
				DirectoryInfo projectBuildFolder = new DirectoryInfo (GetProjectBuildFolder);
				if (!projectBuildFolder.Exists)
				{
					projectBuildFolder.Create ();
				}

				DirectoryInfo subFolder = projectBuildFolder.CreateSubdirectory (tempBuildFolder.Name);			
				foreach (FileInfo file in tempBuildFolder.GetFiles())
				{
					file.CopyTo (Path.Combine (subFolder.FullName, file.Name));
				}
			}
		} 

		public static string GetSimplifiedFileName(string sceneName)
		{
			int lastDotIdx = sceneName.LastIndexOf('.');
			int lastSlashdx = sceneName.LastIndexOf ('/');
			return sceneName.Substring(lastSlashdx + 1, lastDotIdx - lastSlashdx - 1);
		}

		private void SaveData()
		{
			EditorPrefs.SetString("SavedScenes", EditorJsonUtility.ToJson(SceneCollection, true));
		}

		private void LoadData()
		{
			EditorJsonUtility.FromJsonOverwrite(EditorPrefs.GetString ("SavedScenes"), SceneCollection);
		}
	}

	[System.Serializable]
	public class SavedSceneCollection
	{
		[SerializeField]
		public SavedScene[] Scenes;

		public SavedSceneCollection(SavedScene[] scenes)
		{
			Scenes = scenes;
		}

		public SavedSceneCollection(EditorBuildSettingsScene[] scenes)
		{
			Scenes = new SavedScene[scenes.Length];
			for (int i = 0; i < scenes.Length; i++)
			{
				Scenes [i] = new SavedScene (scenes[i]);
			}
		}

		public EditorBuildSettingsScene[] GetEditorScenes()
		{
			EditorBuildSettingsScene[] editorScenes = new EditorBuildSettingsScene[Scenes.Length];
			for (int i = 0; i < Scenes.Length; i++)
			{
				editorScenes [i] = new EditorBuildSettingsScene (Scenes [i].path, Scenes [i].enabled);
			}
			return editorScenes;
		}

		public string[] GetSceneNames()
		{
			string[] val = new string[Scenes.Length];
			for (int i = 0; i < Scenes.Length; i++)
			{
				val [i] = DevBuildOptions.GetSimplifiedFileName(Scenes[i].path);
			}
			return val;
		}

		public bool IsValid()
		{
			if (Scenes != null && Scenes.Length > 0)
			{
				foreach (SavedScene scene in Scenes)
				{
					if (scene.enabled)
					{
						return true;
					}
				}
			}
			return false;
		}
	}

	[System.Serializable]
	public class SavedScene
	{
		public string path;
		public bool enabled;

		public SavedScene(string path, bool enabled)
		{
			this.path = path;
			this.enabled = enabled;
		}

		public SavedScene(EditorBuildSettingsScene scene)
		{
			this.path = scene.path;
			this.enabled = scene.enabled;
		}
	}	
}