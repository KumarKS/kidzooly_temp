﻿using UnityEngine;
using System;
using UnityEngine.UI;
using GoogleMobileAds.Api;
using System.Collections.Generic;

public class AdManager : MonoBehaviour
{
    string App_ID = "ca-app-pub-1187210774328547~3315624366";
    string Banner_ID = "ca-app-pub-3940256099942544/6300978111";
    string Interstitial_ID = "ca-app-pub-3940256099942544/1033173712";
    public Text text;
    // Start is called before the first frame update

    private BannerView bannerView;
    private InterstitialAd interstitialAd;

    [Obsolete]
    void Start()
    {
        //AdRequest adRequest = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("1915F1CFC0D22C6DBB4C8ED97B0CCBA1").Build();
        Debug.Log("App_ID" + App_ID);
        MobileAds.Initialize(App_ID);
        Debug.Log("initialised");

        List<string> deviceIds = new List<string>();
        deviceIds.Add("689CD209338BE85DF673660317FAC28E");
        RequestConfiguration requestConfiguration = new RequestConfiguration
            .Builder()
            .SetTestDeviceIds(deviceIds)
            .build();
        Debug.Log("requestConfiguration device id" + requestConfiguration);
        MobileAds.SetRequestConfiguration(requestConfiguration);
    }

    public void RequestBannerAD()
    {
        Debug.Log("Request pressed");
        bannerView = new BannerView(Banner_ID, AdSize.Banner, AdPosition.Center);
        Debug.Log("this.bannerView" + this.bannerView);


        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += this.HandleOnAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += this.HandleOnAdFailedToLoad;
    }
    public void ReqBannerAd()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/6300978111";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/2934735716";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);

    }

    public void ShowBAnnerAD()
    {
        AdRequest request = new AdRequest.Builder().Build();
        Debug.Log("request before Build();" + request);
        if (request != null)
        {
            Debug.Log("inside if start ShowBAnnerAD");
            bannerView.LoadAd(request);
            Debug.Log("inside if end ShowBAnnerAD");
        }

        else
            Debug.Log("request is null ");
    }

    public void RequestInterstitialAD()
    {
        interstitialAd = new InterstitialAd(Interstitial_ID);
        Debug.Log("this.interstitialAd before Build();");
        AdRequest request = new AdRequest.Builder().Build();
        Debug.Log("this.interstitialAd request after Build();" + request);
        interstitialAd.LoadAd(request);
        Debug.Log("this.interstitialAd request after" + request);
        // Called when an ad request has successfully loaded.
        interstitialAd.OnAdLoaded += this.HandleOnAdLoaded;
        // Called when an ad request failed to load.
        interstitialAd.OnAdFailedToLoad += this.HandleOnAdFailedToLoad;
    }

    public void ShowInterstitialAD()
    {
        Debug.Log("request loaded");
        if (interstitialAd.IsLoaded())
        {
            Debug.Log("inside if start");
            interstitialAd.Show();
            Debug.Log("inside if end");
        }

        Debug.Log("addddd");
    }


    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        text.text = "Ad loaded";
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        text.text = "Ad loaded failed";
    }
}

